<?php

return [


    'facebook' => [
        'client_id' => '2513648528956787',
        'client_secret' => 'b3598cf9af7e2622a92d29cc7248fcb8',
        'redirect' => 'https://supervong.com/auth/facebook/callback',
    ],

    //Dev
    // 'facebook' => [
    //     'client_id' => '2553410358233461',
    //     'client_secret' => '20af797ec23c8080406e1c56bae83861',
    //     'redirect' => 'http://localhost:8000/auth/facebook/callback',
    // ],

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

];
