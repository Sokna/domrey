$(document).ready(function () {

    $('#btn-profile-address').click(function () {
        var btn = $(this);
        var address_id = $('#address_id');
        var first_name = $('#first_name');
        var last_name = $('#last_name');
        var gender = $('#gender');
        var phone = $('#phone');
        var email = $('#email');
        var address = $('#address');
        var about = $('#about');

        var phoneArr = {
            'phone_number1': $("#phone_number1").val(),
            'phone_number2': $("#phone_number2").val(),
        };

        //Payment Method
        var payment = { 
            cash: $('#cash').is(':checked'),
            aba: $('#aba').is(':checked'),
            wing: $('#wing').is(':checked'),
            true_money: $('#true_money').is(':checked')
        };

        //Bank account
        var bank_acc = {
            aba_acc: $('#aba_acc').val(),
            wing_acc: $('#wing_acc').val(),
            true_money_acc: $('#true_acc').val()
        };

        //Shipping
        var shipping = {
            shipping_day: $('#shipping_day').val(),
            shipping_fee: $('#shipping_fee').val()
        };

        if (validationEmpty([first_name, last_name, gender, phone, email, address, about])) {
            return false;
        }

        var formData = new FormData();
        formData.append('address_id', address_id.val());
        formData.append('first_name', first_name.val());
        formData.append('last_name', last_name.val());
        formData.append('gender', gender.val());
        formData.append('phone', phone.val());
        formData.append('phones', JSON.stringify(phoneArr));
        formData.append('email', email.val());
        formData.append('address', address.val());
        formData.append('about', about.val());

        formData.append('payment', JSON.stringify(payment));
        formData.append('bank_acc', JSON.stringify(bank_acc));
        formData.append('shipping', JSON.stringify(shipping));

        $.ajax({
            url: '/api/profile/address',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn-profile-address').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ' + btn.attr('data-lang') + '...');
            },
            complete: function () {
                $('#btn-profile-address').html(btn.attr('data-lang'));
                htmlAllert('Success!', 'Your profile has been saved successfully!');
            },
            success: function (json) {
                $('#address_id').val(json.id);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    });//End Post function

    //Class validation
    var validationEmpty = function(fields){

        for(var i = 0; i < fields.length; i++){
            if (fields[i].val().length == 0) {
                fields[i].addClass('is-invalid');
            }else{
                fields[i].removeClass('is-invalid');
            }
        }

        if($('#form-addrress').find('.is-invalid').length > 0){
            return true;
        }

        return false;

    };




});