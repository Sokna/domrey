$(document).ready(function () {
    var formData = new FormData();
    var files = [];

    $('#btn-submit').click(function () {
        var btn = $(this);
        var pro_id = $('#pro_id');
        var category_id = $('#category_id');
        var parent_category = $('#parent_category');
        var title = $('#title');
        var brand_id = $('#brand_id');
		var brand_name = $('#brand_name');
        var location_id = $('#location_id');
        var condition = $('#condition');
        var price = $('#price');
        var description = $('#description');
        var tags = $('#tags');

        
        
        if (validationEmpty([category_id, parent_category, location_id, title, price, description])){
            return false;
        }

        var images = $('#photo-matrix div img');

        if (images.length == 0 && pro_id.val() == 0) {
            alert('Please add photo.');
            return false;
        }

        
        for (var i = 0; i < images.length; i++) {

            var stringSrc = $(images[i]).attr('src');
            var name = $(images[i]).attr('data-name');
            var res = name.substring(name.length - 5, name.length - 4);

            if ($(images[i]).attr('data-edit') == 1) {
                formData.append("files[" + res + "]", stringSrc);
            } else if ($(images[i]).attr('data-edit') == -1) {
                formData.append("files[" + res + "]", 'delete');
            } else {
                formData.append("files[" + res + "]", '');
            }

        }

        //console.log($('#photo-matrix div img'));
        //return false;

        var deal_method = {
            meet_up: $('#meet_up').is(':checked'),
            mail_delivery: $('#mail_delivery').is(':checked')
        };


        formData.append('id', pro_id.val());
        formData.append('category_id', category_id.val());
        formData.append('parent_category', parent_category.val());
        formData.append('title', title.val());
        formData.append('brand_id', brand_id.val());
		formData.append('brand_name', brand_name.val());
        formData.append('location_id', location_id.val());
        formData.append('condition', condition.val());
        formData.append('price', price.val());
        formData.append('description', description.val());
        formData.append('deal_method', JSON.stringify(deal_method));
        formData.append('tags', tags.val());

        $.ajax({
            url: '/api/post/create',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                
                $('#btn-submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ' + btn.attr('data-lang') +'...');
            },
            complete: function () {
                $('#btn-submit').html(btn.attr('data-lang'));
                htmlAllert('Success!', 'Your data has been saved successfully!');
            },
            success: function (json) {
                window.location.href = "/post/detail/" + json.id;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    });//End Post function
    
    //Add photos martix
    var pro_id;
    var parent;
    $('#post-image').change(function () {
        if (this.files && this.files[0]) {
            if (pro_id > 0) {
                var dataKey = parent.attr('data-id');
                var dataValue = parent.attr('data-value');
                updateSingPhoto(pro_id, dataKey, dataValue, this.files[0], function (data) {
                    parent.attr('data-value', data.id);
                    parent.prepend('<i class="material-icons image_remove">delete_forever</i>');
                    parent.removeClass('no_image_add');

                });
            } else {
                files.push(this.files[0]);
                
            }
            var reader = new FileReader();
            reader.onload = function (e) {
                parent.html('<img src="' + e.target.result + '" style="height: 130px; width: 130px;" class="img-fluid"/>');
                //parent.prepend('<i class="material-icons image_remove">delete_forever</i>');
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
    
    $('#photo-matrix div i.image_remove').click(function (e) {
        // if ($(this).hasClass('add_new')){
        //     pro_id = $('#pro_id').val();
        //     parent = $(this);
        //     $('#post-image').click();
        //     $(this).addClass('removed');
        //     $(this).removeClass('add_new');
        // }else{
        //     if(confirm('Delete','Are you sure, you want to delete this photo?')){
        //         $(this).removeClass('removed');
        //         $(this).addClass('add_new');  
        //         $(this).addClass('no_image_add');        
        //         $(this).html($('<i class="material-icons">add_circle</i>'));
		// 		removeImage($(this).attr('data-value'));
		// 		$(this).attr('data-value',0);  
        //     }
        // }
    });

    var removeImage = function(id){
        var formData = new FormData();
        formData.append("id", id);
        $.ajax({
            url: '/api/post/removeImage',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                
            },
            complete: function () {
                htmlToast('Success!', 'Your photo has been removed successfully!');
            },
            success: function (json) {
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };

    //Update photo on edit page
    var updateSingPhoto = function (proId, dataKey, dataValue, Datafile, callBack){
        var formData = new FormData();
        formData.append("file", Datafile);
        formData.append("proId", proId);
        formData.append("dataKey", dataKey);
        formData.append("dataValue", dataValue);
        $.ajax({
            url: '/api/post/updatePhoto',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn-submit').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Saving...');
            },
            complete: function () {
                $('#btn-submit').html('Submit');
                //htmlAllert('Success!', 'Your photo has been uploaded successfully!');
                
            },
            success: function (json) {
                callBack(json);
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };

    //Class validation
    var validationEmpty = function (fields) {

        for (var i = 0; i < fields.length; i++) {
            if (fields[i].val().length == 0) {
                fields[i].addClass('is-invalid');
            } else {
                fields[i].removeClass('is-invalid');
            }
        }

        if ($('#form-post').find('.is-invalid').length > 0) {
            return true;
        }
        return false;
    };

    /**
     * Switch status
    */
    
    $('.switch-item').click(function(e){
        var id = $(this).attr('data-id');
        var status = 0;
        if ($(this).is(':checked')){
            status = 1;
        }
        var formData = new FormData();
        formData.append('id', id);
        formData.append('status', status);

        $.ajax({
            url: '/api/post/status',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                
            },
            complete: function () {
                
            },
            success: function (json) {
                //htmlAllert('Success!', 'Your status has been changed successfully!');
                if(json.status==1){
                    $('#row_' + id).removeClass('disabled_row');
                }else{
                    $('#row_' + id).addClass('disabled_row');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        //End 

    });


    // $('.panorama_wide_angle').click(function(ent){
    //     var id = $(this).attr('data-id');
    //     var seller = $(this).attr('data-seller');
    //     var status = 0;
    //     if ($(this).attr('data-status')==0) {
    //         status = 1;
    //     }

    //     updateStatus(id, status, seller);
    // });

  

    

    /****
     * image items
     */

    $('.product-items li img').click(function (e) {
        $('#image-display').attr('style', "background-image: url('" + $(this).attr('data-origin')+"')");
        $('#image-display').attr('data-index', $(this).attr('data-index'));
        $('.product-items li img').addClass('deactived');
        $(this).removeClass('deactived');
    });

    $('#image-display').click(function(){
        //$('#image-display').modal('toggle');
    });

    //Search Action
    var params = {
        'new': 'NEW',
        'used': 'USED'
    };

    $('#customCheck1').click(function () {
        if ($(this).is(':checked')) {
            params['new'] = 'NEW';
        }else{
            params['new'] = 'NA';
        }
        searchProducts();
    });

    $('#customCheck2').click(function(){
        if ($(this).is(':checked')) {
            params['used'] = 'USED';
        }else{
            params['used'] = 'NA';
        }
        searchProducts();
    });

    $('#btn-search-go').click(function(){
        searchProducts();
    });

    $('#location_id').change(function(){
        searchProducts();
    });

    $('#text-search').keyup(function (event){
        if (event.keyCode === 13) {
            event.preventDefault();
            $('#btn-search').click();
        }
    });

    $('#btn-search').click(function (event) {
        location.href = $('#btn-search').attr('data-url') + "/post/searchs/" + $('#text-search').val();
    });

    var searchProducts = function(){
        $.ajax({
            url: '/api/post/searchProducts',
            type: 'post',
            data: {
                'id': $('#category_id').val(),
                'text': $('#text-search').val(),
                'new': params['new'],
                'used': params['used'],
                'min': $('#minimum-price').val(),
                'max': $('#maximum-price').val(),
                'location_id': $('#location_id').val()
            },
            dataType: 'html',
            beforeSend: function () {
                $('#category-content').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            },
            complete: function () {

            },
            success: function (datahtml) {
                $('#category-content').html(datahtml);
                $('#category-content .grid_balckground_img').imageloader({
                    background: true,
                    callback: function (elm) {
                        $(elm).slideDown('slow');
                    }
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    };


    //Convert USD to Dollar

    $('#price').keyup(function () {
        var khr = ($('#price').val() * 4000).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
        $('.khr_riel').html(khr + ' Riel (KHR)');
    });


    //Category Parent select
    $('#parent_category').change(function(e){
        getChildCategory($(this).val());
    });

   

    var getChildCategory = function(id){

        if(!id){
            return false;
        }
        $.ajax({
            url: '/api/category/getCategory/' + id,
            type: 'post',
            data: {
                'parent_id': id,
            },
            dataType: 'json',
            beforeSend: function () {
                $('#category_id').html('');
            },
            complete: function () {

            },
            success: function (data) {
                $('#category_id').append('<option></option>');
                for (i in data) {
                    var name = data[i].name;
                    if ($("#parent_category").attr('data-lang')=='kh'){
                        name = data[i].name_kh;
                    }

                    if ($('#category_id').attr('data-id') == data[i].id){
                        $('#category_id').append('<option value="' + data[i].id + '" selected>' + name + '</option>');
                    }else{
                        $('#category_id').append('<option value="' + data[i].id + '">' + name + '</option>');
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    };

    getChildCategory($('#parent_category').val());

    
    var listing_croppie = $('#image-popup').croppie({
        enableExif: true,
        enableOrientation: true,
        enableResize: false,
        enableZoom: true,
        showZoomer: true,
        mouseWheelZoom: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' } 
            width: 600,
            height: 600,
            type: 'square' //circle
        },
        boundary: {
            width: 610,
            height: 610
        },
        update: function (cropper) { 
            
        }
    });
    
    $('#exampleModalForm').on('hidden.bs.modal', function (e) {
        // do something...
        $("#image-popup-thum").removeClass('d-none');
        $("#image-popup").addClass('d-none');
        $("#image-popup-thum").find('img').attr('src', '/images/placeholder.png');
        $('#btn-after-crop').addClass('invisible');
        $('#btn-ok-picture').addClass('invisible');
    })


    //When click images item to edit
    var image_selected;
    var is_new = false;
    $('#photo-matrix div').click(function (e) {
        e.preventDefault();
        is_new = true;
        image_selected = $(this);
        var imgSrc = image_selected.find('img').attr('src');
        listing_croppie.croppie('bind', {
            url: imgSrc,
            //points: [0, 469, 0, 469]
            // orientation: 1,
            // zoom: 1.2733
        }).then(function () {
            htmlForm();
           $("#image-popup-thum").find('img').attr('src', imgSrc);
           $("#image-popup-thum").removeClass('d-none');
           $("#btn-delete-listing").removeClass("d-none");
        });
        
    });


    //When click icon item to add
    
    $('#photo-matrix .no_image_add').click(function (e) {
        e.preventDefault();
        is_new = true;
        image_selected = $(this);
        listing_croppie.croppie('bind', {
            //url: '',
        }).then(function () {
            htmlForm();
            $("#image-popup").addClass('d-none');
            $("#image-popup-thum").removeClass('d-none');
            $("#btn-delete-listing").addClass("d-none");
        });
    });

    //Click upload file
    $("#btn-change-picture").click(function(){
        $('#change_image').click();
    });

    var ogininalImg = '';
    $('#change_image').on('change', function (e) {
        e.preventDefault();
        var reader = new FileReader();
        reader.onload = function (e) {
           
            $("#image-popup").removeClass('d-none');
            $("#image-popup-thum").addClass('d-none');
            $('#btn-after-crop').removeClass('invisible');
            $('#btn-ok-picture').removeClass('invisible');
            // console.log(e.target.result);
            ogininalImg = e.target.result;
            listing_croppie.croppie('bind', {
                url: e.target.result
            }).then(function () {

            });
           // $('#btn-after-crop').addClass('d-block');
        }
        reader.readAsDataURL(this.files[0]);
    });

    //Click ogininal button
    $("#btn-ok-picture").on('click', function(){
        //console.log(ogininalImg);
        if (is_new){
            image_selected.removeClass('no_image_add');
            image_selected.removeClass('add_new');
            image_selected.addClass('removed');
            image_selected.html(`<div class="item_container"><a href="#"><img src="${ogininalImg}" data-edit="1" data-name="100" width="125px;" hieght="125px;" class="resize_fit_center"/></a></div>`);
        }else{
            image_selected.find('img').attr('src', ogininalImg);
            image_selected.find('img').attr('data-edit', 1);
        }
    });

    //Click ok crop
    $('#btn-after-crop').on('click', function (e) {
        e.preventDefault();
        listing_croppie.croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (img) {
            if (image_selected.find('img').length>0){
                image_selected.find('img').attr('src', img);
                image_selected.find('img').attr('data-edit', 1);
                image_selected.find('img').removeClass('d-none');

                if (image_selected.find('i').length>0){
                    image_selected.find('i').addClass('d-none');
                }
                image_selected.removeClass('no_image_add');
                image_selected.removeClass('add_new');
            }else{
                image_selected.html(`<div class="item_container"><a href="#"><img src="${img}" data-edit="1" data-name="100" width="125px;" hieght="125px;" class="resize_fit_center"/></a></div>`);
            }
            
        });
    });

    $('#btn-delete-listing').click(function(ent){
        image_selected.find('img').attr('data-edit', -1);
        image_selected.find('img').attr('src', '/images/placeholder.png');
        image_selected.find('img').addClass('d-none');
        image_selected.find('.item_container').append(`<i class="material-icons img_add">add_circle</i>`);
        image_selected.addClass('no_image_add');
        image_selected.addClass('add_new');

        //remove modal image
        $(this).addClass("d-none");
        $("#image-popup-thum").find('img').attr('src', '/images/placeholder.png');
    });

    function getFileReader(url, callback){
        var request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.responseType = 'blob';
        request.onload = function () {
            var reader = new FileReader();
            reader.readAsDataURL(request.response);
            reader.onload = function (e) {
                //console.log('DataURL:', e.target.result);
                callback(e.target.result);
            };
        };
        request.send();
    }

    // $('#photo-matrix div img').each((index, value) => {

    //     console.log(value);
    //     getFileReader($(value).attr('src'), function(dataSrc){
    //         $(value).attr('src', dataSrc);
    //     });
    // });

});

var deleteListing = function (id, status, seller) {
    if (confirm('Are you sure, you want to delete this listing')) {
        updateStatus(id, status, seller);
    }
};


var updateStatus = function (id, status, seller) {
    var formData = new FormData();
    formData.append('id', id);
    formData.append('status', status);
    $.ajax({
        url: '/api/post/status',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {

        },
        complete: function () {

        },
        success: function (json) {
            //htmlAllert('Success!', 'Your status has been changed successfully!');
            window.location.href = "/sellers/" + seller;

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
    //End

};