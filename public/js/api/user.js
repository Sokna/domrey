$(document).ready(function () {
    var resize = $('#upload-demo').croppie({
        url: $('#upload-demo').attr('data-url'),
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' } 
            width: 150,
            height: 150,
            type: 'square' //circle square
        },
        boundary: {
            width: 160,
            height: 160
        }
    });

    $('#profile-image').click(function(){
        $('#change-profile').click();
    });

    $('#change-profile').change(function () {
        if (this.files && this.files[0]) {
            if (this.files[0].type == 'image/jpeg' || this.files[0].type == 'image/jpg' || this.files[0].type == 'image/png') {
                var reader = new FileReader();
                reader.onload = function (e) {
                    //$('#profile-image').attr('src', e.target.result);
                    resize.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        
                    });
                }
                reader.readAsDataURL(this.files[0]);

            }else{

                htmlAllert('Error', 'Invalid image type.');
            }

        } //End if

        else{
            htmlAllert('Error', 'Invalid image type.');
        }

    });

    $('#btn-profile-account').click(function () {
        var btn = $(this);
        var formData = new FormData();
        var name = $('#name');
        var username = $('#username');
        var phone = $('#phone');
        var user_id = $('#user_id');
        var email = $('#email');

        // if (validationEmpty([username, phone], '#form-account')) {
        //     return false;
        // }
        formData.append('user_id', user_id.val());
        formData.append('name', name.val());
        formData.append('username', username.val());
        formData.append('phone', phone.val());
        formData.append('email', email.val());

        resize.croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (img) {
            formData.append('file', img);
            $.ajax({
                url: '/api/profile/account',
                type: 'post',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#btn-profile-account').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ' + btn.attr('data-lang') + '...');
                   
                },
                complete: function () {
                    $('#btn-profile-account').html(btn.attr('data-lang'));

                
    
                },
                success: function (json) {
                    if (json.email) {
                        htmlAllert('Alert!', `Your enail ${json.email} already exist`);
                    } else {
                        htmlAllert('Success!', 'Your data has been saved successfully!');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });

        });
    });

    $('#btn-profile-password').click(function(){
        var current_password = $('#current_password');
        var new_password = $('#new_password');
        var confirm_password = $('#confirm_password');

        if (validationEmpty([current_password, new_password, confirm_password], '#form-account-password')) {
            return false;
        }

        if (newPasswordLen(new_password, '#form-account-password')){
            return false;
        }

        if (confrimPassword(new_password, confirm_password, '#form-account-password')){
            return false;
        }

        var formData = new FormData();
        formData.append('current_password', current_password.val());
        formData.append('new_password', new_password.val());
        formData.append('confirm_password', new_password.val());

        $.ajax({
            url: '/api/profile/changePassword',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn-profile-password').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Saving...');
            },
            complete: function () {
                $('#btn-profile-password').html('Save');
                htmlAllert('Success!', 'Your data has been saved successfully!');
            },
            success: function (json) {

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    });

    var newPasswordLen = function (new_pwd, validObject){
        if (new_pwd.val().length < 6) {
            new_pwd.addClass('is-invalid');
            $('.newpwd-error').html('New password must be at lest 6 digits');
        }else{
            new_pwd.removeClass('is-invalid');
            $('.newpwd-error').html('New password is required');
        }

        if ($(validObject).find('.is-invalid').length > 0) {
            return true;
        }
        return false;
    };

    //Password Not match
    var confrimPassword = function (new_pwd, confirm_pwd, validObject) {
        if (new_pwd.val() != confirm_pwd.val()){
            confirm_pwd.addClass('is-invalid');
            $('.confirm-error').html('Confirm password is not match');
        }else{
            confirm_pwd.removeClass('is-invalid');
            $('.confirm-error').html('Confirm password is required');
        }

        if ($(validObject).find('.is-invalid').length > 0) {
            return true;
        }
        return false;
    };

    //Class validation
    var validationEmpty = function (fields, validObject) {

        for (var i = 0; i < fields.length; i++) {
            if (fields[i].val().length == 0) {
                fields[i].addClass('is-invalid');
            } else {
                fields[i].removeClass('is-invalid');
            }
        }

        if ($(validObject).find('.is-invalid').length > 0) {
            return true;
        }
        return false;
    };



});