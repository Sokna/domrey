//Toast Message
var htmlToast = function (title, message, txttype ='text-success'){
    $('#toast').toast({delay: 3000});
    $('#toast').find('.mr-auto').html(title);
    $('#toast').find('.mr-auto').addClass(txttype);
    $('#toast').find('.toast-body').html(message);
    $('#toast').find('.toast-body').addClass(txttype);
    $('#toast').toast('show');
};

//Toast Message
var htmlAllert = function (title, message, txttype = 'text-success') {
    var alert = $('#exampleModalAlert');
    alert.find('.modal-title').text(title);
    alert.find('.modal-body').html(message);
    alert.modal({});
};

var htmlForm = function () {
    var alert = $('#exampleModalForm');
    //alert.find('.modal-title').text(title);
    //alert.find('.modal-body').html(htmlForm);
    alert.modal({});

    return alert;
};

$('.scrollable-dropdown li').click(function(){
    $('#search_concept').attr('data-title', $(this).attr('data-title'));
    $('#search_concept').text($(this).attr('data-title'));
});

var cusor = -1;
$("#text-search").keyup(function (ent) {
    ent = ent || window.event;
    $('#text-searchautocomplete-list').find('.item-result').css('background-color', 'white');
    var ll = $('#text-searchautocomplete-list').find('.item-result');
    if (ent.keyCode == 40) { //KeyDown
        cusor++;
        $(ll[cusor]).css('background-color', '#e9e9e9');
        $("#text-search").val($(ll[cusor]).attr('data-value'));
        return false;
    } else if (ent.keyCode == 38){ //KeyUp
        cusor--;
        $(ll[cusor]).css('background-color', '#e9e9e9');
        $("#text-search").val($(ll[cusor]).attr('data-value'));
        return false;
    }
    cusor = -1;
    goAutoCompleteSearch();
});


var goAutoCompleteSearch = function(){
    var text = $("#text-search").val();
    var formData = new FormData();
    formData.append('text', text);

    if (text == '' || text.length < 3) {
        return false;
    }

    $.ajax({
        url: '/api/post/autoComplete',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {

        },
        complete: function () {

        },
        success: function (data) {
            $('#text-searchautocomplete-list').html('');
            var duplicated = "";
            for (i in data) {
                var result = searchTags(data[i].tags, text);
                if (duplicated.search(result) < 0) {
                    $('#text-searchautocomplete-list').append("<div class='item-result' data-value='" + result + "'>" + result + "</div>");
                }
                duplicated = duplicated + result;
            }
            $('#text-searchautocomplete-list').css('display', 'block');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

var searchTags = function (strText, searchTxt) {
    var n = strText.indexOf(searchTxt);
    var newStr = '';
    if (n >= 0) {
        for (var i = n; i < strText.length; i++) {
            if (strText[i] == ',') {
                break;
            }
            newStr = newStr + strText[i];
        }
    }
    return newStr;
};


//Brand change event
var brandAutoComplete = function(brand_name){
    var formData = new FormData();
    formData.append('brand_name', brand_name);
    $.ajax({
        url: '/api/brand/getBrand',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (data) {
            $('#brand_list').html('');
            for (i in data) {
                $('#brand_list').append("<div class='item' onClick=\"getBrandOnlick(" + data[i].id + ",'" + data[i].name+"');\" data-id='" + data[i].id + "' data-value='" + data[i].name + "'>" + data[i].name + "</div>");
            }
            if (data.length > 0) {
                $('#brand_list').css('display', 'block');
            }
			$("#brand_id").val(0);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

};

var band_c =-1;
$("#brand_name").keyup(function (ent) {
    ent = ent || window.event;
    $('#brand_list').find('.item').css('background-color', 'white');
    var ll = $('#brand_list').find('.item');

    if ($("#brand_name").val() == '' || $("#brand_name").val().length < 2) {
        return false;
    }

    if (ent.keyCode == 13) { //Key Enter
        $('#brand_list').css('display', 'none');
        return false;
        band_c =-1;
    }
    if (ent.keyCode == 40) { //KeyDown
        band_c++;
        $(ll[band_c]).css('background-color', '#e9e9e9');
        $("#brand_name").val($(ll[band_c]).attr('data-value'));
        $("#brand_id").val($(ll[band_c]).attr('data-id'));
        return false;
    } else if (ent.keyCode == 38) { //KeyUp
        band_c--;
        $(ll[band_c]).css('background-color', '#e9e9e9');
        $("#brand_name").val($(ll[band_c]).attr('data-value'));
        $("#brand_id").val($(ll[band_c]).attr('data-id'));
        return false;
    }
    band_c = -1;
    brandAutoComplete($(this).val());
});
$('#cart_create_post').click(function(){
    $('#brand_list').css('display', 'none');
});

var getBrandOnlick = function(id, val){
    $("#brand_name").val(val);
    $("#brand_id").val(id);
    $('#brand_list').css('display', 'none');
};


//Reporting button
var html = '<input type="hidden" id="post_id"/><select class="form-control">' +
    '<option value="">Please select an option</option>' +
    '<option value="SP">Irrelevant keywords</option>' +
    '<option value="WC">Items wrongly categorized</option>' +
    '<option value="FK">Selling counterfeit items</option>' +
    '<option value="DP">Duplicate posts</option>' +
    '<option value="PI">Selling prohibited item</option>' +
    '<option value="HB">Offensive behaviour/content</option>' +
    '<option value="ML">Mispriced Listings</option>' +
    '<option value="OT">Other</option>' +
    '</select><br/>' +
    '<textarea placeholder="Report message" id="reporting-message" class="form-control d-none"></textarea>';
var htmlform = $("<div class='form-group'>" + html + "</div>");
var modal = $('#exampleModalCenter');

var btnReportListing = function(id){
    if (modal.attr('data-user')<=0){
        htmlAllert('Alert!', `<a href="/login" class="domrey-link">Log in</a> or <a href="/register" class="domrey-link">Create an account</a>`);
        return;
    }
    htmlform.find('input').val(id);
    modal.find('.modal-title').text(modal.attr('data-title'));
    modal.find('.modal-body').html(htmlform);
    $('#exampleModalCenter').modal({});
};

modal.find('button.btn-domrey').click(function (ent) {
    var formData = new FormData();
    formData.append('post_id', htmlform.find('input').val());
    formData.append('reson', htmlform.find('select').val());
    formData.append('reporting_smg', htmlform.find('textarea#reporting-message').val());
    $.ajax({
        url: '/api/post/reportListing',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (data) {
            htmlform.find('select').val('');
            htmlform.find('textarea#reporting-message').val('');
            $('#exampleModalCenter').modal('hide');

            htmlAllert('Success!', 'Your report has been sent successfully!');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

});

htmlform.find('select').change(function () {
    if ($(this).val() == 'OT') {
        htmlform.find('textarea').removeClass('d-none');
    } else {
        htmlform.find('textarea').addClass('d-none');
    }
    if ($(this).val() == '') {
        modal.find('.btn-domrey').attr('disabled', true);
    } else {
        modal.find('button.btn-domrey').removeAttr('disabled');
    }
});

//Like button
// $('.btn-like-listing').click(function(ent){
//     btnHeartLike(this);
// });

var btnHeartLike = function (thi, s=0){
    var thi = $(thi);
    var id = thi.attr('data-id');
    var formData = new FormData();
    formData.append('id', id);

    $.ajax({
        url: '/api/post/likeListing/' + id,
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (data) {
            if (data.user_id == 0) {

                htmlAllert('Alert!', `<a href="/login" class="domrey-link">Log in</a> or <a href="/register" class="domrey-link">Create an account</a>`);
                return false;
            }

            if (data.like == 1) {
                thi.find('img').attr('src', '/images/like-filled.svg');
                thi.find('span').text(data.rate);
            } else if(data.like == 0) {
                if (parseInt(thi.find('span').text()) ==1 && s==1){

                    thi.find('span').text(thi.attr('data-lang'));
                }else{
                    thi.find('span').text(data.rate);
                }
                thi.find('img').attr('src', '/images/like-outlined.svg');
            }else{
                thi.find('span').text(data.rate);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

};


$('.flage-lang').click(function (ent) {
    var that = $(this);
    var lang = that.attr("data-lang");
    var csrf_token = that.attr("data-csrf_token");

    
    var formData = new FormData();
    formData.append('lang', lang);
    formData.append('_token', csrf_token);

    $.ajax({
        url: '/home/language',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {

        },
        complete: function () {

        },
        success: function (data) {

            if(data.lang == 'kh'){
                // that.addClass("english-flage");
                // that.removeClass("khmer-flage");
                that.attr("data-lang", "en");
            }else{
                // that.addClass("khmer-flage");
                // that.removeClass("english-flage");
                that.attr("data-lang", "kh");
            }

            location.reload();
        }

    });


});


var itemW = $('.thumbnail-domrey').width();
var itemWd = $('.thumbnail-domrey-detail').width();
if(itemW > 0){
    $('.thumbnail-domrey').css('height', itemW);
}

if (itemWd > 0) {
    $('.thumbnail-domrey-detail').css('height', itemWd);
}

