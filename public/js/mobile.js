//Toast Message
var htmlAllert = function (title, message, txttype = 'text-success') {
    var alert = $('#exampleModalAlert');
    alert.find('.modal-title').text(title);
    alert.find('.modal-body').html(message);
    alert.modal({});
};


//Reporting button
var html = '<input type="hidden" id="post_id"/><select class="form-control">' +
    '<option value="">Please select an option</option>' +
    '<option value="SP">Irrelevant keywords</option>' +
    '<option value="WC">Items wrongly categorized</option>' +
    '<option value="FK">Selling counterfeit items</option>' +
    '<option value="DP">Duplicate posts</option>' +
    '<option value="PI">Selling prohibited item</option>' +
    '<option value="HB">Offensive behaviour/content</option>' +
    '<option value="ML">Mispriced Listings</option>' +
    '<option value="OT">Other</option>' +
    '</select><br/>' +
    '<textarea placeholder="Report message" id="reporting-message" class="form-control d-none"></textarea>';
var htmlform = $("<div class='form-group'>" + html + "</div>");
var modal = $('#exampleModalCenter');

var btnReportListing = function (id) {
    if (modal.attr('data-user') <= 0) {
        htmlAllert('Alert!', `<a href="/login" class="domrey-link">Log in</a> or <a href="/register" class="domrey-link">Create an account</a>`);
        return;
    }
    htmlform.find('input').val(id);
    modal.find('.modal-title').text(modal.attr('data-title'));
    modal.find('.modal-body').html(htmlform);
    $('#exampleModalCenter').modal({});
};

modal.find('button.btn-domrey').click(function (ent) {
    var formData = new FormData();
    formData.append('post_id', htmlform.find('input').val());
    formData.append('reson', htmlform.find('select').val());
    formData.append('reporting_smg', htmlform.find('textarea#reporting-message').val());
    $.ajax({
        url: '/api/post/reportListing',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (data) {
            htmlform.find('select').val('');
            htmlform.find('textarea#reporting-message').val('');
            $('#exampleModalCenter').modal('hide');

            htmlAllert('Success!', 'Your report has been sent successfully!');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

});

htmlform.find('select').change(function () {
    if ($(this).val() == 'OT') {
        htmlform.find('textarea').removeClass('d-none');
    } else {
        htmlform.find('textarea').addClass('d-none');
    }
    if ($(this).val() == '') {
        modal.find('.btn-domrey').attr('disabled', true);
    } else {
        modal.find('button.btn-domrey').removeAttr('disabled');
    }
});


//Like button
// var btnHeartLike = function (thi, s = 0) {
//     var thi = $(thi);
//     var id = thi.attr('data-id');
//     var formData = new FormData();
//     formData.append('id', id);

//     $.ajax({
//         url: '/api/post/likeListing/' + id,
//         type: 'post',
//         data: formData,
//         cache: false,
//         contentType: false,
//         processData: false,
//         beforeSend: function () {
//         },
//         complete: function () {

//         },
//         success: function (data) {
//             if (data.user_id == 0) {
//                 htmlAllert('Alert!', `<a href="/login" class="domrey-link">Log in</a> or <a href="/register" class="domrey-link">Create an account</a>`);
//                 return false;
//             }

//             if (data.like == 1) {
//                 thi.find('img').attr('src', '/images/like-filled.svg');
//                 thi.find('span').text(data.rate);
//             } else if (data.like == 0) {
//                 if (parseInt(thi.find('span').text()) == 1 && s == 1) {
//                     thi.find('span').text(thi.attr('data-lang'));
//                 } else {
//                     thi.find('span').text(data.rate);
//                 }
//                 thi.find('img').attr('src', '/images/like-outlined.svg');
//             } else {
//                 thi.find('span').text(data.rate);
//             }

//         },
//         error: function (xhr, ajaxOptions, thrownError) {
//             // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
//         }
//     });

// };


$("#footer-fix .item").on('click', function(ent){
    //Set container class 
    $("#container-fluid .item").addClass('d-none');
    $("#" + $(this).attr('data-id')).removeClass('d-none');
    
    //Active button
    $("#footer-fix .item").removeClass('active');
    $(this).addClass('active');
});





$('.owl-carousel').owlCarousel(
    {
        nav: false,
        navText: ["<i class='material-icons'>navigate_before</i>", "<i class='material-icons'>navigate_next</i>"],
        margin: 5,
        dots: false,
        responsiveClass: true,
        loop: false,
        autoWidth: true,
    }
);


function goBack() {
    window.history.back();
}

// $('#location_id').change(function () {
//     searchProducts();
// });

//Search Action
var params = {
    'new': 'NEW',
    'used': 'USED'
};
// $('#customCheck1').click(function () {
//     if ($(this).is(':checked')) {
//         params['new'] = 'NEW';
//     } else {
//         params['new'] = 'NA';
//     }
//     searchProducts();
// });

// $('#customCheck2').click(function () {
//     if ($(this).is(':checked')) {
//         params['used'] = 'USED';
//     } else {
//         params['used'] = 'NA';
//     }
//     searchProducts();
// });

$('#text-home-search').keyup(function (event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        var text = $(this).val();
        setTimeout(function () {
            $(this).val('');
        }, 3000);
        
        location.href = `/post/searchs/${text}`;
    }
});

$('#text-search').keyup(function (event) {
    if (event.keyCode === 13) {
        event.preventDefault();
        $('.collapse').collapse('hide');
        document.activeElement.blur();
        searchProducts();
    }
});


$('#btn-search-go').click(function () {
    $('.collapse').collapse('hide');
    searchProducts();
    
});

// $('#location_id').change(function () {
//     searchProducts();
// });

var searchProducts = function () {
    
    if ($('#customCheck1').is(':checked')) {
        params['new'] = 'NEW';
    } else {
        params['new'] = 'NA';
    }

    
    if ($('#customCheck2').is(':checked')) {
        params['used'] = 'USED';
    } else {
        params['used'] = 'NA';
    }

    $.ajax({
        url: '/api/post/searchProducts',
        type: 'post',
        data: {
            'id': $('#category_id').val(),
            'text': $('#text-search').val(),
            'new': params['new'],
            'used': params['used'],
            'min': $('#minimum-price').val(),
            'max': $('#maximum-price').val(),
            'location_id': $('#location_id').val()
        },
        dataType: 'html',
        beforeSend: function () {
            $('#spinner_text_search').removeClass('d-none');
        },
        complete: function () {
            $('#spinner_text_search').addClass('d-none');
        },
        success: function (datahtml) {
            $('#list_items').html(datahtml);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

};


function readMore() {
    $('#more_str').addClass('d-block');
    $('.read-more-link').addClass('d-none');
}


var btnHeartLike = function (thi, s = 0) {
    var thi = $(thi);
    var id = thi.attr('data-id');
    var formData = new FormData();
    formData.append('id', id);

    $.ajax({
        url: '/api/post/likeListing/' + id,
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (data) {
            if (data.user_id == 0) {

                htmlAllert('Alert!', `<a href="/login" class="domrey-link">Log in</a> or <a href="/register" class="domrey-link">Create an account</a>`);
                return false;
            }

            if (data.like == 1) {
                thi.find('img').attr('src', '/images/like-filled.svg');
                thi.find('span').text(data.rate);
            } else if (data.like == 0) {
                if (parseInt(thi.find('span').text()) == 1 && s == 1) {

                    thi.find('span').text(thi.attr('data-lang'));
                } else {
                    thi.find('span').text(data.rate);
                }
                thi.find('img').attr('src', '/images/like-outlined.svg');
            } else {
                thi.find('span').text(data.rate);
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            // alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });

};
