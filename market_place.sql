/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : market_place

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-01-20 08:32:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for addresses
-- ----------------------------
DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of addresses
-- ----------------------------
INSERT INTO `addresses` VALUES ('1', '1', 'SOKNA', 'PICH', '016 93 26 08', 'soknatest@gmail.com', null, 'Phnom Penh, Cambodia', '2020-01-17 15:55:49', '2020-01-17 08:55:49');
INSERT INTO `addresses` VALUES ('2', '5', 'Dara', 'Kok', '092 89 22 31 22', 'kokdara@gmail.com', null, '#23 EA, Street 271, Beong Tompon, Chamkarmon, Phnom Penh, Cambodia', '2020-01-18 11:04:19', '2020-01-18 04:04:19');

-- ----------------------------
-- Table structure for branches
-- ----------------------------
DROP TABLE IF EXISTS `branches`;
CREATE TABLE `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of branches
-- ----------------------------
INSERT INTO `branches` VALUES ('1', 'DELL', null);
INSERT INTO `branches` VALUES ('2', 'ASUS', null);
INSERT INTO `branches` VALUES ('3', 'TOYOTA', null);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `icon` varchar(20) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Following', 'Following', 'following.png', '1', '1', '1');
INSERT INTO `categories` VALUES ('2', 'Car', null, 'cars.png', '2', '1', '1');
INSERT INTO `categories` VALUES ('3', 'Home Services', null, 'home_services.png', '3', '1', '1');
INSERT INTO `categories` VALUES ('4', 'Property', null, 'property.png', '4', '1', '1');
INSERT INTO `categories` VALUES ('5', 'Electronics', null, 'electronics.png', '5', '1', '1');
INSERT INTO `categories` VALUES ('6', 'Phone & Tablate', null, 'phones_ablets.png', '6', '1', '1');
INSERT INTO `categories` VALUES ('7', 'Women Fashion', null, 'women_fashion.png', '7', '1', '1');
INSERT INTO `categories` VALUES ('8', 'Healty & Beauty', null, 'health_beauty.png', '9', '1', '1');
INSERT INTO `categories` VALUES ('9', 'Luxury', null, 'luxury.png', '10', '1', '1');
INSERT INTO `categories` VALUES ('10', 'Furniture', null, 'furniture.png', '11', '1', '1');
INSERT INTO `categories` VALUES ('11', 'Home Appliances', null, 'home_appliances.png', '12', '1', '1');
INSERT INTO `categories` VALUES ('12', 'Babies Kids', null, 'babies_Kids.png', '13', '1', '1');
INSERT INTO `categories` VALUES ('13', 'Men Fashion', null, 'men_fashion.png', '8', '1', '1');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2019_08_19_000000_create_failed_jobs_table', '1');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------
INSERT INTO `password_resets` VALUES ('soknatest@gmail.com', '$2y$10$hGx8QOOoo12l2aS5M9vicex/wbVHeEVj7WdicXsPLUrgp7ddw9Sgm', '2020-01-17 08:02:20');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `branch_id` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `price` decimal(15,2) DEFAULT 0.00,
  `condition` varchar(10) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('1', '5', 'IPhone X 256GB Space Gray #W0204', '1', 'Comes with Charging Head + Cable + Box\nNO Ear-piece due to hygiene reason\nCondition same as picture\nPhone is in FULL Working Condition\nBattery Capacity : 90%\nCall us to check on Stocks Availability before visiting our store \n\nSelf Collection and Cash Payment Only \n\nStore Location: 3 Temasek Boulevard @ Suntec City Tower 3, # 03-361. \n\nOpening Hours: \nMonday - Saturday : 10am to 8pm\nSunday / PH Day: Closed\n*Contact us for any other available timing', '600.00', 'NEW', null, '1', '1', '2020-01-15 08:01:24', '2020-01-17 09:39:41');
INSERT INTO `posts` VALUES ('2', '7', 'IPF APPROVED Sbd knee sleeve, inzer wrist wrap, lifting large belt', '2', 'Sbd knee sleeve (sizeS) : $55\nInzer wrist wrap : $25\nLifting large belt (sizeS) : $45\nBuy all at once:$115\n*Prices can nego*\n*logo on sbd knee sleeve faded off. The rest of the knee sleeve is still on good condition*\n\nTags: inzer titan sbd lifting large mark bell A7 slingshot powerlifting gear weightlifting gym knee sleeve wrist wrap belt ipf approved', '14.00', 'NEW', null, '1', '1', '2020-01-15 09:11:55', '2020-01-17 09:36:44');
INSERT INTO `posts` VALUES ('5', '10', 'Scotty Cameron Putter', '1', 'Newport 2 \n\nCan meet at different locations', '250.00', 'USED', null, '1', '1', '2020-01-15 09:57:42', '2020-01-17 09:34:21');
INSERT INTO `posts` VALUES ('6', '5', 'Fishing Reel', '2', 'Bought the wrong sized reel at decathlon for $20 letting it go at $10. Used it once or twice till I realised it was the wrong one', '10.00', 'USED', null, '1', '1', '2020-01-16 03:14:42', '2020-01-17 09:31:54');
INSERT INTO `posts` VALUES ('7', '13', 'Gangsta lifting wraps', '2', 'Used for a short while and got another pair of wraps as a gift to selling these!\nWorks amazing for bench pressing and squatting to protect your wrists and maximize force transfer.\n\nTags: powerlifting sbd Nike Romaleos adipower slippers deadlift squat bench press lifting weightlifting strongman wraps sleeves knee elbow wrist ankle calf gym Titan mark bell strong', '25.00', 'USED', null, '1', '1', '2020-01-16 03:36:53', '2020-01-17 09:29:44');
INSERT INTO `posts` VALUES ('8', '2', 'Mitsubishi Fuso Canter 3.0 Manual', null, 'Class 3 Licence Can Drive, No Need VPC, Just Passed Inspection, New 6 Months Road Tax, New Cushion, Beautiful And Clean Interior, Well Maintained, Serviced Regularly, Flexible Loan Available At Low Interest Rate, Trade In Welcome.\n\n37 minutes ago In Used Commercial Vehicles for Sale\n\nLoan Estimation\n\nEstimated based on MAS guidelines of min 30% downpayment.\nPlease check with the seller for actual terms!\n\nApply for the Carousell x UOB Car Loan', '26800.00', 'NEW', null, '1', '5', '2020-01-16 08:16:47', '2020-01-17 09:44:15');
INSERT INTO `posts` VALUES ('9', '7', 'Super Elegant Classic Chanel Leboy Boy Flap', '2', 'Another set of pics \n\nClassic design\n\nElegant color\n\nCalfskin\n\nMedium\n\n25 14 8 cm\n\nPerfect size \n\nRetail around 7k\n\nSuper value piece \n\nChic to carry\n\nWith card hologram and dustbag \n\nAll details as per listing description, please read \n\nEach piece coming with what indicated in each listing, and will always give two free dustbag, please read \n\nRefer to link below for more details and >350 positive feedbacks:\n\nhttps://sg.carousell.com/qvintagelux/\n\nFollow Instagram: QVintageLux or Whatsapp +65 9483 8802 for effective communications\n\nFollow & Say Hi in IG to enjoy S$20 off for $2000 & below, S$50 off for $2000-$6000, S$100 off for $6000 & above\n\nLow ballers, scammers and impolite people (those who asked tons of questions then MIA) will be ignored and blocked \n\nPlease don’t use poor condition pieces to compare with our good and very good condition items (ranked as condition A or even SA by professional preowned luxury shops and Japan auction house)\n\nWe have sorted hard to find the best value for money pieces - good or even very mint condition pieces with fair or low price\n\nOur margin is limited and we still need to bear with international courier charge\n\nThus lowballers don\'t disturb please\n\nItems are currently overseas, and we will pay the international courier charge for you\n\nIf you decide to buy, place 20% deposit with us (exact 20% no more no less easier to calculate the balance), and after the item has been shipped to SG, we do COD (cash on delivery)\n\nBasically, 20% deposit, arrange shipping, 3-10 business days, meet up in Sg for balance 80%\n\nAbout authenticity: Items are sorted from established preowned luxury shops globally with at least 5 years industry experience (one bag at least checked by two/ three professional authenticators) and Japan luxury auction house with license from police. We don\'t accept individuals selling to us in sg, to assure authenticity. All items listed have gone through strict authentication process by professional authenticators with many years’ experience in preowned luxury industry. Third party authentication is encouraged (eg bababebi for Hermes). \n\nAbout pricing: Our selling price is very fair, we also have compared the prices and conditions and ensured that the items we list is the best value for money, and we pay international courier charge for you, so please don’t offer unreasonably low price\n\nNOTE\n- After we have shipped the bag to Singapore, if you last minute change your mind, the 20% deposit is not refundable, please be consistent with your decision\n- If the value of the item is above $400, you will/ may have to borne the GST payable to Singapore customs', '3650.00', 'NEW', null, '1', '5', '2020-01-17 08:45:20', '2020-01-17 09:41:28');
INSERT INTO `posts` VALUES ('10', '2', 'Tern Eclipse P20 Folding Bike', null, 'Progress to  Tri-bike!  \ncondition 8/10', '1680.00', 'USED', null, '1', '5', '2020-01-17 09:50:05', '2020-01-17 09:50:16');
INSERT INTO `posts` VALUES ('11', '10', 'Kormes Heavy Duty Blender ( Commercial use, SG warranty', null, 'Selling Brand New Electric F&B Equipments( Singapore )\nWhatsapp / Text 9151 9054\nEmail: Fred@superiorkitchenequipment.com\n** Cheapest in Singapore guaranteed **\nVisit Website : www.superiorkitchenequipment.com\nAll items come with SG manufacturing warranty, local 3 pin plug with Safety Marks\n\nCooker\n----------	\n1)	3500W induction cooker ( Commercial use , SG warranty ) - $108 New \n2)	3500w Wok style Induction Cooker ( Commercial use , SG warranty )  - $218  New \n3)	3500w Heating conductor Cooker ( Commercialuse , SG warranty )  - $228 New\n4)	5000w Induction Cooker ( Commercial use , SG warranty ) - $268 New\n5)	Sous Vide Machine ( Commercial use , SG warranty )  - $128 New\n6)	18L Rice cooker Electric for 25paxs ( Commercial use , SG warranty ) - $148 New\n7)	28L Rice cooker Electric for 40paxs ( Commercial use , SG warranty ) - $178 New\n8)	36L Rice cooker Electric for 60paxs ( Commercial use , SG warranty ) - $218 New\n\nFryer / Boiler\n-------\n11)	Single Deep fryer 6L Electric ( Table top , Commercial use ) - $88 New\n12)	Single Deep fryer 10L Electric ( Table top , Commercial use ) - $198 New\n13)	Double Deep fryer 6L + 6L Electric ( Table top , Commercial use ) - $198 New\n14)	Double Deep fryer 10L + 10L Electric ( Table top , Commercial use ) - $398 New\n15)	Noodle Boiler Electric Cooker 2 Hole ( Commercial use , SG warranty ) - $198 New\n16)	Noodle Boiler Electric Cooker 4 Hole ( Commercial use , SG warranty ) - $388 New\n17)	Noode Boiler Electric Cooker 4 Hole Standing Set ( Refill Tap included ) - $588 New\n18)	Noodle Boiler LPG Gas Cooker 2 Hole ( Commercial use , SG warranty ) - $218 New\n       \n\nWarmer\n-----------\n21)	Bain Marie 3 hole Warmer ( Sauce / Soup , Commercial Use) - $180 New\n22)	Bain Marie 2 hole Warmer ( Sauce/ Soup , Commercial Use ) - $180 New\n23)	Bain Marie 4 hole Warmer ( Sauce / Soup , Commercial use ) - $228 New\n24)	19L Rice Warmer ( Sauce / Soup , Commercial Use ) - $218 New\n25)	10L Western Soup Warmer ( Sauce / Soup , Commercial Use ) - $98 New\n26)	Egg Tart / Fried Food 50cm warmer display Brand New - $258 New\n27)	Egg Tart / Fried Food 100cm warmer display Brand New - $398 New\n28)	Fries Warmer Kitchen Station Brand New - $398 New\n29)	Pau / Dim sum Display Warmer( Commercial use ) Brand New- $498 New\n\n\nOven & Bakeware\n-------\n30)	Double Deck Oven ( Pizza / Bakery / Grill , Commercial Use ) - $548 New\n31)	Single Deck Pizza Long Oven ( Pizza / Bakery / Grill , Commercial Use ) - $688 New\n32)	Bakery 5 Tier 90L Fan Commercial Oven ( Bakery/ Cakes , Commercial Use ) - $588 New\n33)	Single Waffle Machine ( Belgium Thick , Commercial use ) - $228 New\n34)	Single Waffle Bakery Thin Maker ( Thin , Commercial use ) - $198 New\n35)	Dough Mixer 5L ( Commercial use , SG warranty ) - $218 New\n       \n\n\nImpulse Sealer / Vacuum Sealer / Ice Machine / Chiller Freezer\n----------------------\n\n40)	Impulse Sealer Machine 20cm ( Commercial use ) Brand New - $24 New\n41)	Vacuum Sealer ( Commercial use ) Brand New - $168 New\n42)	Ice Machine 20kg bin ( 65kg production / 24hrs ) Commercial Brand New - $888 New\n43)	Ice Machine 120kg bin ( 200kg Production / 24 hrs ) Commercial BrandNew - $1988 New\n44)	4 Door Standing Freezer / Chiller( Commercial use , Café ) Brand New– $1688 New\n45)	1.2m Undercounter Chiller( Commercial use , Café ) Brand New - $988 New\n46)	1.5m Undercounter Chiller ( Commercial use , Café ) Brand New - $1388 New\n47)	108L Chest Freezer( Commercial use , Café ) Brand new - $228 New\n48)	528L Cheese Freezer ( Commercial use , Café ) Brand new - $888 New\n49)	Standing Glass Chiller( Commercial use , Café )Brand new - $888 New\n\n\nHot Water Dispenser\n-------------\n       60) Hot water dispenser 30L ( Commercial use , SG warranty ) - $198 New\n       62) Slim Hot water dispenser 10L ( Commercial Use , SG warranty ) - $188 New\n       63) Digital Hot Water dispenser 15L ( Commercial use, SG warranty ) - $298 New\n\nIce Blender / Ice Shaver / Hand Blender\n----------\n       71) Kormes Heavy Duty Blender ( Commercial use, SG warranty ) - $98 New\n       72) Ice Blender ( Micro Sun ) Commercial Heavy Duty- $148 New\n       73) Anti Noise Ice Blender Commercial use( Brand New )  - $228 New\n       74) Ice Shaver ( IceKachang , commercial use ) brand New - $298 New\n       75) Hand Blender Commercial( Sauce , soup , Commercial Use SG warranty ) - $158 New\n	\nHotplate & Griller / Microwave\n---------\n80.	Griddle Hotplate Flat( Commercial use ) - $218 New\n81.	Griddle Hotplate Small Flat 35cm ( Commercial Use ) - $198 New\n82.	Panini Griddle Salamander ( Sandwich, Steak , Burger , Commercial use ) - $188 New\n83.	Toaster BBQ Electric Griller ( Bread , Satay , BBQ , Commercial use )  - $198 New\n84.	Toaster Salamander Box ( Bread , Satay , BBQ , Commercial use ) - $318 New\n85.	Commercial Microwave ( SG warranty / Commercial ) Brand New - $699\n86.	4 slice Commercial bread Toaster ( SG warranty / Commercial use ) - $198\n87.	Kebab Griller Machine ( SG warranty / Commercial use ) - $698\n\nMisc\n------\n      \n90.	Café Outdoor Set ( 2 chairs / 1 table ) Brand new - $188 New\n91.	Meat Slicer 47cm ( Commercial use ) Brand new - $588 New\n92.	Meat Mincer ( Commercial use )Brand new - $268 New\n93.	Double Stainless Steel Sink Brand new - $288 New\n94.	Bubble Tea Sealer( Commercial use )Brand new - $588 New\n95.	Takoyaki Machine( Commercial use )Brand new - $288 New\n96.	Gyoza Dumpling Cooker ( Commercial use )Brand new - $288 New \n97.	Wireless Paging System Café ( 16pagers, commercial use )Brand new - $298 New\n98.	Dim Sum basket table top Steamer ( Commercial use )Brand new - $388 New\n       \nSelf Collection\n===========\nSuperior Kitchen Equipment\nS9 Industrial Building\n55 Serangoon North Ave 4  #04-08  S( 555859 )\n( Key postal in Google map please )\nPlease Whatsapp 9151 9054 before coming\n\nFor delivery Service \n$15 delivery fees by Lalamove\n( You need to Paynow full amount to 93390264 first, send in your name/ hp number / full address with postal / prefer date and time slot ) \nPlease check stocks availability before making payment', '98.00', 'NEW', null, '1', '5', '2020-01-17 09:52:19', '2020-01-17 09:52:22');
INSERT INTO `posts` VALUES ('12', '7', 'Burberry cotton jumpsuit gift set with hat and bib', null, 'Luxurious gift set for boys by British brand Burberry. The set includes a babygrow, a bandana bib and a hat with a cute top knot, all made from soft blue cotton jersey with washed Pale Stone check cotton trims.  It comes complete with a white, quilted cotton zip-up pouch.\nProduct number 261150\n100% soft & stretchy cotton jersey\n100% lightweight cotton trims\nPouch: 100% cotton, lining & padding: 100% polyester\nMachine wash (30*C)\nBabygrow: Buttons on one shoulder & popper fastenings between the legs\nBib: Popper fastenings\nStyle name: Colby\n4 piece set', '199.90', 'NEW', null, '1', '5', '2020-01-17 09:53:54', '2020-01-17 09:53:58');
INSERT INTO `posts` VALUES ('13', '4', 'The Cairnhill - 4 Bedroom for rent', null, 'The Cairnhill condominium is within walking distance to famed shopping and entertainment belt Orchard Road.\n\nThis freehold condominium is just minutes\' walk to Newton and Orchard MRT stations, putting you withing easy access to all parts of the city, including the Central Business District (CBD).\n\nSchools in the area include the esteemed Raffles Girls’ School, Anglo-Chinese School, ISS International School, and Chatsworth International School (Orchard Campus).\n\nThe Cairnhill comprises spacious apartments and penthouses with recent improvements done to the bathrooms, lightings and air-conditioning systems.\n\n••• 4 Bedrooms for Rent •••\n\n4 Bedroom, 4661 sqft\nLuxurious Living Hall \nGreat Size Bedrooms\nFully Equipped Kitchen\nSpacious and Cosy\nWell-Ventilated, Breezy and Bright\n\nBrad Chung at 9247 5707 for an Exclusive Viewing Appointment Now!\nView to Appreciate! Do Not Miss this Awesome Deal!\n\nLocation\n\nPostal Code\n\n229740\n\nDistrict\n\nD09 Orchard / River Valley\n\nStreet Name\n\n4 Cairnhill Rise\n\nMRT\n\nOrchard (NS22 TE14)\n\nUnit Details\n\nProperty Name\n\nThe Cairnhill\n\nType\n\nCondominium\n\nFacilities\n\nBarbeque Area, Gymnasium room, Squash court, Swimming pool\n\nBeds\n\n4 Bedrooms\n\nBaths\n\n4 Bathrooms\n\nFurnishing\n\nPartial\n\nFeatures\n\nAir-Conditioning, Cooker Hob/Hood, Renovated, Swimming Pool View, Water Heater', '6900.00', 'NEW', null, '1', '5', '2020-01-17 09:56:27', '2020-01-18 07:02:41');
INSERT INTO `posts` VALUES ('14', '7', 'Fashion Eyewear', null, 'Frame only = $19.90\nFrame with NO degree optical lens = $29.90\nFrame with degree and UV protect + multicoated lens = $39.90\nFrame with degree/no degree Bluelight Computer proctect lens = $69.90\nFrame with degree/no degree Color change Grey Photochromic lens = $69.90\n\nFor other lens type please PM\n\nTags: Eyewear , eyeglasses , glasses , prescription , spectacles , rayban , oakley , promo , promotion, 16210', '29.90', 'USED', null, '1', '5', '2020-01-17 09:58:20', '2020-01-18 07:03:20');

-- ----------------------------
-- Table structure for post_images
-- ----------------------------
DROP TABLE IF EXISTS `post_images`;
CREATE TABLE `post_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of post_images
-- ----------------------------
INSERT INTO `post_images` VALUES ('1', '1', '1_0.jpg', '2020-01-17 16:38:46', '2020-01-17 09:38:46');
INSERT INTO `post_images` VALUES ('2', '2', '2_0.jpg', '2020-01-17 16:36:17', '2020-01-17 09:36:17');
INSERT INTO `post_images` VALUES ('3', '2', '2_1.jpg', '2020-01-15 09:11:55', '2020-01-15 09:11:55');
INSERT INTO `post_images` VALUES ('4', '2', '2_2.jpg', '2020-01-15 09:11:55', '2020-01-15 09:11:55');
INSERT INTO `post_images` VALUES ('5', '5', '5_0.jpg', '2020-01-17 16:33:46', '2020-01-17 09:33:46');
INSERT INTO `post_images` VALUES ('6', '5', '5_1.jpg', '2020-01-15 09:57:42', '2020-01-15 09:57:42');
INSERT INTO `post_images` VALUES ('7', '5', '5_2.jpg', '2020-01-17 16:33:55', '2020-01-17 09:33:55');
INSERT INTO `post_images` VALUES ('8', '5', '5_3.jpg', '2020-01-17 16:33:59', '2020-01-17 09:33:59');
INSERT INTO `post_images` VALUES ('9', '5', '5_4.jpg', '2020-01-17 16:34:08', '2020-01-17 09:34:08');
INSERT INTO `post_images` VALUES ('10', '5', '5_5.jpg', '2020-01-17 16:34:11', '2020-01-17 09:34:11');
INSERT INTO `post_images` VALUES ('11', '5', '5_6.jpg', '2020-01-17 16:34:15', '2020-01-17 09:34:15');
INSERT INTO `post_images` VALUES ('12', '5', '5_7.jpg', '2020-01-17 16:34:18', '2020-01-17 09:34:18');
INSERT INTO `post_images` VALUES ('13', '2', '2_3.jpg', '2020-01-17 16:36:27', '2020-01-17 09:36:27');
INSERT INTO `post_images` VALUES ('14', '2', '2_4.jpg', '2020-01-17 16:36:42', '2020-01-17 09:36:42');
INSERT INTO `post_images` VALUES ('15', '2', '2_5.jpg', '2020-01-16 03:10:33', '2020-01-16 03:10:33');
INSERT INTO `post_images` VALUES ('16', '2', '2_6.jpg', '2020-01-17 16:36:35', '2020-01-17 09:36:35');
INSERT INTO `post_images` VALUES ('17', '2', '2_7.jpg', '2020-01-16 03:13:01', '2020-01-16 03:13:01');
INSERT INTO `post_images` VALUES ('18', '1', '1_1.jpg', '2020-01-17 16:38:48', '2020-01-17 09:38:48');
INSERT INTO `post_images` VALUES ('19', '1', '1_2.jpg', '2020-01-17 16:38:51', '2020-01-17 09:38:51');
INSERT INTO `post_images` VALUES ('20', '1', '1_3.jpg', '2020-01-17 16:38:54', '2020-01-17 09:38:54');
INSERT INTO `post_images` VALUES ('21', '1', '1_4.jpg', '2020-01-17 13:16:19', '2020-01-17 06:16:19');
INSERT INTO `post_images` VALUES ('22', '1', '1_5.jpg', '2020-01-17 16:38:57', '2020-01-17 09:38:57');
INSERT INTO `post_images` VALUES ('23', '1', '1_6.jpg', '2020-01-16 03:14:06', '2020-01-16 03:14:06');
INSERT INTO `post_images` VALUES ('24', '1', '1_7.jpg', '2020-01-16 03:14:09', '2020-01-16 03:14:09');
INSERT INTO `post_images` VALUES ('25', '6', '6_0.jpg', '2020-01-16 10:37:15', '2020-01-16 03:37:15');
INSERT INTO `post_images` VALUES ('26', '7', '7_0.jpg', '2020-01-17 10:10:21', '2020-01-17 03:10:21');
INSERT INTO `post_images` VALUES ('27', '8', '8_0.jpg', '2020-01-16 08:16:47', '2020-01-16 08:16:47');
INSERT INTO `post_images` VALUES ('28', '7', '7_1.jpg', '2020-01-17 16:29:37', '2020-01-17 09:29:37');
INSERT INTO `post_images` VALUES ('29', '7', '7_2.jpg', '2020-01-17 03:03:44', '2020-01-17 03:03:44');
INSERT INTO `post_images` VALUES ('30', '7', '7_3.jpg', '2020-01-17 03:03:52', '2020-01-17 03:03:52');
INSERT INTO `post_images` VALUES ('31', '7', '7_4.jpg', '2020-01-17 03:04:04', '2020-01-17 03:04:04');
INSERT INTO `post_images` VALUES ('32', '6', '6_1.jpg', '2020-01-17 16:31:27', '2020-01-17 09:31:27');
INSERT INTO `post_images` VALUES ('33', '9', '9_0.jpg', '2020-01-17 08:45:20', '2020-01-17 08:45:20');
INSERT INTO `post_images` VALUES ('34', '6', '6_2.jpg', '2020-01-17 09:31:29', '2020-01-17 09:31:29');
INSERT INTO `post_images` VALUES ('35', '6', '6_3.jpg', '2020-01-17 09:31:32', '2020-01-17 09:31:32');
INSERT INTO `post_images` VALUES ('36', '6', '6_4.jpg', '2020-01-17 09:31:35', '2020-01-17 09:31:35');
INSERT INTO `post_images` VALUES ('37', '9', '9_1.jpg', '2020-01-17 09:42:34', '2020-01-17 09:42:34');
INSERT INTO `post_images` VALUES ('38', '9', '9_2.jpg', '2020-01-17 09:42:41', '2020-01-17 09:42:41');
INSERT INTO `post_images` VALUES ('39', '9', '9_3.jpg', '2020-01-17 09:42:47', '2020-01-17 09:42:47');
INSERT INTO `post_images` VALUES ('40', '9', '9_4.jpg', '2020-01-17 09:42:56', '2020-01-17 09:42:56');
INSERT INTO `post_images` VALUES ('41', '9', '9_5.jpg', '2020-01-17 09:42:58', '2020-01-17 09:42:58');
INSERT INTO `post_images` VALUES ('42', '8', '8_1.jpg', '2020-01-17 09:45:20', '2020-01-17 09:45:20');
INSERT INTO `post_images` VALUES ('43', '8', '8_2.jpg', '2020-01-17 09:45:23', '2020-01-17 09:45:23');
INSERT INTO `post_images` VALUES ('44', '8', '8_3.jpg', '2020-01-17 09:45:29', '2020-01-17 09:45:29');
INSERT INTO `post_images` VALUES ('45', '8', '8_4.jpg', '2020-01-17 09:45:40', '2020-01-17 09:45:40');
INSERT INTO `post_images` VALUES ('46', '8', '8_5.jpg', '2020-01-17 09:45:43', '2020-01-17 09:45:43');
INSERT INTO `post_images` VALUES ('47', '10', '10_0.jpg', '2020-01-17 09:50:05', '2020-01-17 09:50:05');
INSERT INTO `post_images` VALUES ('48', '10', '10_1.jpg', '2020-01-17 09:50:05', '2020-01-17 09:50:05');
INSERT INTO `post_images` VALUES ('49', '10', '10_2.jpg', '2020-01-17 09:50:05', '2020-01-17 09:50:05');
INSERT INTO `post_images` VALUES ('50', '11', '11_0.jpg', '2020-01-17 09:52:19', '2020-01-17 09:52:19');
INSERT INTO `post_images` VALUES ('51', '12', '12_0.jpg', '2020-01-17 09:53:54', '2020-01-17 09:53:54');
INSERT INTO `post_images` VALUES ('52', '12', '12_1.jpg', '2020-01-17 09:53:54', '2020-01-17 09:53:54');
INSERT INTO `post_images` VALUES ('53', '13', '13_0.jpg', '2020-01-17 09:56:27', '2020-01-17 09:56:27');
INSERT INTO `post_images` VALUES ('54', '13', '13_1.jpg', '2020-01-17 09:56:27', '2020-01-17 09:56:27');
INSERT INTO `post_images` VALUES ('55', '13', '13_2.jpg', '2020-01-17 09:56:27', '2020-01-17 09:56:27');
INSERT INTO `post_images` VALUES ('56', '13', '13_3.jpg', '2020-01-17 09:56:27', '2020-01-17 09:56:27');
INSERT INTO `post_images` VALUES ('57', '13', '13_4.jpg', '2020-01-17 09:56:27', '2020-01-17 09:56:27');
INSERT INTO `post_images` VALUES ('58', '13', '13_5.jpg', '2020-01-17 09:56:28', '2020-01-17 09:56:28');
INSERT INTO `post_images` VALUES ('59', '13', '13_6.jpg', '2020-01-17 09:56:28', '2020-01-17 09:56:28');
INSERT INTO `post_images` VALUES ('60', '13', '13_7.jpg', '2020-01-17 09:56:28', '2020-01-17 09:56:28');
INSERT INTO `post_images` VALUES ('61', '13', '13_8.jpg', '2020-01-17 09:56:28', '2020-01-17 09:56:28');
INSERT INTO `post_images` VALUES ('62', '13', '13_9.jpg', '2020-01-17 09:56:28', '2020-01-17 09:56:28');
INSERT INTO `post_images` VALUES ('63', '13', '13_10.jpg', '2020-01-17 09:56:28', '2020-01-17 09:56:28');
INSERT INTO `post_images` VALUES ('64', '13', '13_11.jpg', '2020-01-17 09:56:28', '2020-01-17 09:56:28');
INSERT INTO `post_images` VALUES ('65', '14', '14_0.jpg', '2020-01-17 09:58:20', '2020-01-17 09:58:20');
INSERT INTO `post_images` VALUES ('66', '14', '14_1.jpg', '2020-01-17 09:58:20', '2020-01-17 09:58:20');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuserid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING HASH
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'soknapich', 'Avaro Sokna', '012 93 26 09', '1.jpg', 'soknatest@gmail.com', null, '$2y$10$iXe/1zsBHy9ow3smxtREa.u5xKGI2.BzwpkQ02iw6aIwIon7pto3.', null, 'admin', '2019-12-18 04:02:53', '2020-01-17 08:55:19');
INSERT INTO `users` VALUES ('5', 'darakok', 'Dara Kok', '016 93 2608', '5.png', 'darakok@gmail.com', null, '$2y$10$fpa/NRi83ZwkOHgkvnAm1.fqEV0oF3Rmk.tBeox.w.Q2qXGiBheUy', null, null, '2020-01-16 07:06:41', '2020-01-17 03:31:43');
