<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes(['verify' => true]);

//Admin Controller
Route::get('/admin',"AdminController@index");
Route::get('/dashboard',"AdminController@dashboard");
Route::get('/dashboard/category/{page?}/{limit?}',"AdminController@category");
Route::get('/dashboard/brand/{page?}/{limit?}',"AdminController@brand");
Route::get('/dashboard/listing/{page?}/{limit?}/{search?}',"AdminController@listing");
Route::get('/dashboard/user/{page?}/{limit?}',"AdminController@user");
Route::get('/dashboard/message',"AdminController@message");
Route::get('/dashboard/aeon/{page?}/{limit?}',"AdminController@aeon");
Route::get('/dashboard/apiAeon/{id?}',"AdminController@apiAeon");
Route::post('/dashboard/apiSaveAeon',"AdminController@apiSaveAeon");


//Home
Route::post('/home/language',  'HomeController@language');
Route::get('/',"HomeController@index");
Route::get('/post/category/{id?}',"HomeController@category");
Route::get('/post/detail/{id?}',"HomeController@detail");
Route::get('/post/images/{lid?}', "HomeController@images");
Route::get('/post/searchs/{text?}',"HomeController@searchs");
Route::get('/post/getLastId/{lid?}',"HomeController@getLastId");
Route::get('/post/viewMore/{lid?}',"HomeController@viewMore");
Route::get('/post/getLastIdByCategory/{cid?}/{lid?}',"HomeController@getLastIdByCategory");
Route::get('/post/viewMoreByCategory/{cid?}/{lid?}',"HomeController@viewMoreByCategory");

//Facebook
Route::get('/facebookLogin', function () {
    return view('facebookLogin');
});
Route::get('auth/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleFacebookCallback');  

//Logout
Route::get('/logout',"HomeController@logout");
Auth::routes(['register' => true]);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/carousel', 'HomeController@carousel');
//Post Routes

Route::get("/post/create/{id?}", "PostController@index");
Route::get("/post/createNew/{id?}", "PostController@createNew");
Route::get("/post/list/{page?}/{limit?}", "PostController@list");
Route::get("post/listingReport/{id?}", "PostController@listingReport");

//POST API Controllers
    Route::post('/api/post/create', 'API\PostController@store');
    Route::post('/api/post/listNowV2', 'API\PostController@listNowV2');
    Route::post('/api/post/updatePhoto', 'API\PostController@updatePhoto');
    Route::post('/api/post/status', 'API\PostController@statusChange');
    Route::post('/api/post/searchProducts', 'API\PostController@searchProducts');
    Route::post('/api/post/autoComplete', 'API\PostController@autoComplete');
    Route::post('/api/post/removeImage', 'API\PostController@removeImage');
    Route::post('/api/post/reportListing', 'API\PostController@reportListing');
    Route::post('/api/post/likeListing/{id?}', 'API\PostController@likeListing');

    //Profile API
    Route::post('/api/profile/address', 'API\ProfileController@store');
    Route::post('/api/profile/account', 'API\ProfileController@account');
    Route::post('/api/profile/changePassword', 'API\ProfileController@changePassword');
    Route::post('/api/profile/blockUser/{id?}', 'API\ProfileController@blockUser');
    Route::post('/api/account/findUser', 'API\LoginController@findUser');
    Route::post('/api/profile/changeCover', 'API\ProfileController@changeCover');

    //Category API
    Route::post('/api/category/update/{id?}', 'API\CategoryController@updateCategory');
    Route::post('/api/category/getCategory/{id?}', 'API\CategoryController@getCategory');

    //Brand API
    Route::post('/api/brand/getBrand', 'API\BrandController@getBrands');
    Route::post('/api/brand/update/{id?}', 'API\BrandController@update');

    //Chat
    Route::post('/api/chat/chat/{id?}', 'API\ChatController@chat');
    Route::post('/api/chat/readChatMessage', 'API\ChatController@readChatMessage');
    Route::post('/api/chat/updateChatMessage', 'API\ChatController@updateChatMessage');
	
	//Mobile
	Route::get('/api/mobile/getCategories/{id?}',"Mobile\PostController@getCategories");
	Route::get('/api/mobile/getListing',"Mobile\PostController@getListing");
	Route::get('/api/mobile/getOneListing/{id?}',"Mobile\PostController@getOneListing");
	Route::get('/api/mobile/getListingByCategory/{id?}',"Mobile\PostController@getListingByCategory");
	Route::get('/api/mobile/getListingByUser/{id?}',"Mobile\PostController@getListingByUser");
    Route::get('/api/mobile/getSearch/{text?}',"Mobile\PostController@getSearch");
	Route::post('/api/mobile/getUploadListing',"Mobile\PostController@getUploadListing");
	Route::get('/api/mobile/getZones',"Mobile\PostController@getZones");
	Route::get('/api/mobile/getBrands',"Mobile\PostController@getBrands");
	Route::get('/api/mobile/getTest',"Mobile\PostController@getTest");
    
    //LOGIN
    Route::post('/api/mobile/loginAPI',"API\LoginController@loginAPI");


//Profile Controllers
Route::get('/profile/profile',    'ProfileController@profile');
Route::get('/profile/account',    'ProfileController@account');
Route::get('/profile/chat/{id?}/{sender?}/{offer?}', 'ProfileController@chat');
Route::get('/profile/chatRoom',  'ProfileController@chatRoom');
Route::get('/profile/settings',  'ProfileController@settings');



//Sallers Controllers
Route::get('/sellers/{uuid?}', 'SellerController@seller');
Route::get('/me', 'SellerController@me');
Route::get('/myLike', 'SellerController@myLike');
Route::get('/seller/viewMore/{sid?}/{lid?}', 'SellerController@viewMore');
Route::get('/seller/viewMoreSeller/{sid?}/{lid?}', 'SellerController@viewMoreSeller');
Route::get('/seller/getLastId/{sid?}/{lid?}', 'SellerController@getLastId');
Route::get('/seller/getLastSellerId/{sid?}/{lid?}', 'SellerController@getLastSellerId');


// Mobile View
Route::get('/setMobileLayout/{view?}', 'HomeController@setMobileLayout');


Route::get('password/change',  'ChangePasswordController@index');
Route::post('password/change', 'ChangePasswordController@store');


//Static page
Route::get('pages/contact', 'PageController@contact');
Route::post('pages/contact', 'PageController@store');
Route::get('pages/advertise', 'PageController@advertise');

