<?php

namespace App\Http\Controllers\Mobile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Brand;
use App\PostImage;
use App\Report;
use Auth;
use App;

use App\Classes\Resize;

class PostController extends Controller
{
	
	public function getTest(){
		$postImg = PostImage::where('name','LIKE','%20_ss4%')->where('post_id',20)->first();
		if(!$postImg){
			echo 'false';
		}
		
		//return response()->json($postImg);
	}
	
	public function getBrands(){
		$brands = DB::select("SELECT DISTINCT (`name`) FROM brands WHERE status = 1");
		
		return response()->json($brands);
	}
	
	public function getZones(){
		$zones = DB::select("SELECT *FROM zones WHERE country_id = 36");
		
		return response()->json($zones);
	}
	
	public function getUploadListing(Request $request){
		//if No brand and Add new
		$brand_id = 0;
	
			
        $data = [];
		if ($request->isMethod('post')) {
			$post = new Post;
			if($request->id > 0){
				$post = Post::firstOrCreate(['id' => $request->id]);
			}
			$post->category_id = $request->category_id;
            $post->parent_category = $request->parent_category;
            $post->title = $request->title;
            $post->location_id = $request->location;
            $post->condition = $request->condition;
            $post->created_by = $request->user_id;
            $post->price = $request->price;
            $post->description = $request->description;
            $post->tags = $request->tags;
			
			
			
			$brand_id = $this->getBrandId($request->brand_name);
			if($brand_id >0){
				$post->brand_id = $brand_id;
			}
		
			
			
			if($post->save()){
				if ($request->has('fileToUpload')) {
                    foreach($request->file('fileToUpload')  as $k=>$image){
                        $name = ($post->id)."_$k.". $image->getClientOriginalExtension();
						$imgId = ($post->id)."_$k";
						$postImg = PostImage::where('name','LIKE','%'.$imgId.'%')->where('post_id', $post->id)->first();
						
						//Check no image save before and create new one
						if(!$postImg){
							$postImage = new PostImage;
							$postImage->name = $name;
							$postImage->post_id = $post->id;
							$postImage->save();
						}else{
							$postImg->name = $name;
							$postImg->save();
						}
		
						//Upload image file
                        $image->move(public_path().'/images/products', $name);

                        //150x150
                        $resizeObj = new Resize(public_path()."/images/products/".$name);
                        $resizeObj -> resizeImage(150, 150, 0);
                        $resizeObj -> saveImage(public_path()."/images/products/150x150/".$name, 100);
                        

                        //500x500
                        $resizeObj = new Resize(public_path()."/images/products/".$name);
                        $resizeObj -> resizeImage(500, 500, 0);
                        $resizeObj -> saveImage(public_path()."/images/products/500x500/".$name, 100);
                    }
                }
				
				$data['id']= $post->id;
			}
		}
			
		return response()->json($data);
	}
  
	public function getSearch($text){
		$data = DB::select("SELECT LOWER(posts.tags) AS tags  FROM posts WHERE LOWER(posts.tags) LIKE ?", ["%".strtolower($text)."%"]);
		
		$search_array = [];
		
		foreach($data as $d){
			$exstr = explode(" ", $d->tags);
			foreach($exstr as $str){
				if(preg_match("/{$text}/i", $str)) {
					if (!array_key_exists($str, $search_array)) {
						$search_array[$str] = $str;
					}
				}
			}
		}
		
		$data = [];
		foreach($search_array as $value){
			$data[] = [
				"tag" => $value
			];	
		}
		
        return response()->json($data);
	}
	
	public function getListingByUser($id){
		$listing = DB::select("
			SELECT
				post.id,
				post.title,
				post.description,
				post.price,
				post.`condition`,
				post.created_by,
				post.rate,
				post.location_id,
				post.category_id,
				post.parent_category,
				(SELECT `name` FROM brands WHERE brands.id = post.brand_id) AS brand_name,
				(SELECT `name` FROM categories WHERE categories.id = post.category_id) AS category,
			    (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
				(SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
				(SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.id  LIMIT 1) as image
			FROM
				posts post
			WHERE
				post.status = 1
				AND post.created_by = ?
			ORDER BY post.created_at DESC", [$id]);
		
		$data = [];
		
		foreach($listing as $list){
			$data[] = [
				"id" => $list->id,
				"uuser_id"=> $list->uuser_id,
				"created_by"=>$list->created_by,
				"name" => $list->title,
				"cover" => "{$list->image}",
				"images" =>$this->getImageItems($list->id),
				"category" => "{$list->category}",
				"category_id" => $list->category_id,
				"brand_name" => $list->brand_name,
				"parent_category" => $list->parent_category,
				"categoryTag" => "{$list->uuser_photo}",
				"price" =>$list->price,
				"condition" => ucfirst(strtolower($list->condition)),
				"likes" => $list->rate,
				"location_id" => $list->location_id,
				"isLike" => false,
				"isFavorite" => true,
				"comments" => 13,
				"rating" => "{$list->rate}",
				"row" => 1,
				"description" => $list->description
			];
			
		}
		
		return response()->json($data);
	}
	
	public function getListingByCategory($id){
		$listing = DB::select("
			SELECT
				post.id,
				post.title,
				post.description,
				post.price,
				post.`condition`,
				post.created_by,
				post.rate,
				post.location_id,
				post.category_id,
				post.parent_category,
				(SELECT `name` FROM brands WHERE brands.id = post.brand_id) AS brand_name,
				(SELECT `name` FROM categories WHERE categories.id = post.category_id) AS category,
			    (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
				(SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
				(SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.id  LIMIT 1) as image
			FROM
				posts post
			WHERE
				post.status = 1
				AND (post.category_id IN (SELECT id FROM categories WHERE parent_id = ?) OR post.category_id = ?)
			ORDER BY post.created_at DESC", [$id, $id]);
		
		$data = [];
		
		foreach($listing as $list){
			$data[] = [
				"id" => $list->id,
				"uuser_id"=> $list->uuser_id,
				"created_by"=>$list->created_by,
				"name" => $list->title,
				"cover" => "{$list->image}",
				"images" =>$this->getImageItems($list->id),
				"category" => "{$list->category}",
				"category_id" => $list->category_id,
				"brand_name" => $list->brand_name,
				"parent_category" => $list->parent_category,
				"categoryTag" => "{$list->uuser_photo}",
				"price" => $list->price,
				"condition" => ucfirst(strtolower($list->condition)),
				"likes" => $list->rate,
				"location_id" => $list->location_id,
				"isLike" => false,
				"isFavorite" => true,
				"comments" => 13,
				"rating" => "{$list->rate}",
				"row" => 1,
				"description" => $list->description
			];
			
		}
		
		return response()->json($data);
	}
	
	public function getOneListing($id){
		$listing = DB::select("
			SELECT
				post.id,
				post.title,
				post.description,
				post.price,
				post.`condition`,
				post.created_by,
				post.rate,
				post.location_id,
				post.category_id,
				post.parent_category,
				(SELECT `name` FROM brands WHERE brands.id = post.brand_id) AS brand_name,
				(SELECT `name` FROM categories WHERE categories.id = post.category_id) AS category,
			    (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
				(SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
				(SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.id  LIMIT 1) as image
			FROM
				posts post
			WHERE
				post.status = 1 AND post.id = ?
			ORDER BY created_at DESC", [$id]);
		
		$data = [];
		
		foreach($listing as $list){
			$data[] = [
				"id" => $list->id,
				"uuser_id"=> $list->uuser_id,
				"created_by"=>$list->created_by,
				"name" => $list->title,
				"cover" => "{$list->image}",
				"images" =>$this->getImageItems($list->id),
				"category" => "{$list->category}",
				"category_id" => $list->category_id,
				"brand_name" => $list->brand_name,
				"parent_category" => $list->parent_category,
				"categoryTag" => "{$list->uuser_photo}",
				"price" => $list->price,
				"condition" => ucfirst(strtolower($list->condition)),
				"likes" => $list->rate,
				"location_id" => $list->location_id,
				"isLike" => false,
				"isFavorite" => true,
				"comments" => 13,
				"rating" => "{$list->rate}",
				"row" => 1,
				"description" => $list->description
			];
			
		}
		
		return response()->json($data);
	}
	
	public function getListing(){
		$listing = DB::select("
			SELECT
				post.id,
				post.title,
				post.description,
				post.price,
				post.`condition`,
				post.created_by,
				post.rate,
				post.location_id,
				post.category_id,
				post.parent_category,
				(SELECT `name` FROM brands WHERE brands.id = post.brand_id) AS brand_name,
				(SELECT `name` FROM categories WHERE categories.id = post.category_id) AS category,
			    (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
				(SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
				(SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.id  LIMIT 1) as image
			FROM
				posts post
			WHERE
				post.status = 1 
			ORDER BY created_at DESC
			LIMIT 12 ");
		
		$data = [];
		
		foreach($listing as $list){
			$data[] = [
				"id" => $list->id,
				"uuser_id"=> $list->uuser_id,
				"created_by"=>$list->created_by,
				"name" => $list->title,
				"cover" => "{$list->image}",
				"images" => $this->getImageItems($list->id),
				"category" => "{$list->category}",
				"category_id" => $list->category_id,
				"brand_name" => $list->brand_name,
				"parent_category" => $list->parent_category,
				"categoryTag" => "{$list->uuser_photo}",
				"price" => $list->price,
				"condition" => ucfirst(strtolower($list->condition)),
				"likes" => $list->rate,
				"location_id"=>$list->location_id,
				"isLike" => false,
				"isFavorite" => true,
				"comments" => 13,
				"rating" => "{$list->rate}",
				"row" => 1,
				"description" => $list->description
			];
			
		}
		
		return response()->json($data);
		
	}
	
	public function getImageItems($id){
		
		$items = DB::select("SELECT *FROM post_images WHERE post_images.post_id =?", [$id]);
		$data = [];
		foreach($items as $item){
			$data[] = "{$item->name}?time=".time();
		}
		
		return $data;
	}
	
	public function getCategories($id=0){
		$qurey = "SELECT *FROM categories WHERE status=1 AND parent_id IS NULL ORDER BY sequence ASC";
		if($id>0){
			$qurey = "SELECT *FROM categories WHERE status=1 AND parent_id = $id ORDER BY sequence ASC";
		}
		$categories = DB::select($qurey);
		return response()->json($categories);
		
	}
	
	public function getBrandId($name){
		$brand = DB::select("SELECT id,name FROM brands WHERE name=? LIMIT 1",[$name]);
		$id = 0;
		if(count($brand)>0){
			$id = $brand[0]->id;
		}
		return $id;
	}



}
