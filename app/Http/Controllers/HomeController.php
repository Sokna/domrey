<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Classes\Resize;
use Illuminate\Support\Facades\Cookie;
use App;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware(['auth','verified']);
        App::setlocale(isset($_SESSION['lang'])? $_SESSION['lang']:'en');
    }
	
	public function getCategories(){

		$categories = DB::select("SELECT *FROM categories WHERE status=1 AND parent_id IS NULL ORDER BY sequence ASC");
		
		return response()->json($categories);
		
	}

    public function logout(){
        Auth::logout();

        return redirect()->action('HomeController@index');

    }


    public function setMobileLayout($view = 'home'){
        $_SESSION["mobile"] = $view;
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $limit = config('app.limit_home');
        $total = DB::table('posts')->where('status','=', 1)->count();

        $us_id = 0;
        if(Auth::user()){
            $us_id = Auth::user()->id;
        }
        

        $posts = DB::select("SELECT
                                post.id,
                                post.title,
                                post.description,
                                post.price,
                                post.`condition`,
                                post.created_by,
                                post.rate,
                                (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=? AND likes.`like`=1 LIMIT 1) AS like_user,
                                (SELECT TIMESTAMPDIFF(SECOND, post.created_at,  NOW())  % 60) as S,
                                (SELECT TIMESTAMPDIFF(MINUTE, post.created_at,  NOW())  % 60) as M,
                                (SELECT TIMESTAMPDIFF(HOUR, post.created_at,    NOW())  % 24) as H,
                                (SELECT TIMESTAMPDIFF(DAY,  post.created_at,    NOW())  % 30) as D,
                                (SELECT TIMESTAMPDIFF(MONTH, post.created_at,   NOW())  % 12) as MO,
                                (SELECT TIMESTAMPDIFF(YEAR, post.created_at,    NOW())) as Y,

                                (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                                (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                                (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC  LIMIT 1) as image
                        FROM
                                posts post
                        WHERE
                                post.status = 1 
                        ORDER BY created_at DESC
                        LIMIT ?", [$us_id, $limit]);

        $categories = DB::select("SELECT *FROM categories WHERE status=1 ORDER BY sequence ASC");

        $users = DB::select("
            SELECT 
                id, 
                uuserid,
                'name',
                photo,
                email,
                created_at,
                (SELECT TIMESTAMPDIFF(SECOND, users.lastlogin,  NOW())  % 60) AS S,
                (SELECT TIMESTAMPDIFF(MINUTE, users.lastlogin,  NOW())  % 60) AS M,
                (SELECT TIMESTAMPDIFF(HOUR, users.lastlogin,  NOW())  % 24) AS H,
                (SELECT TIMESTAMPDIFF(DAY, users.lastlogin,  NOW())  % 30) AS D,
                (SELECT TIMESTAMPDIFF(MONTH, users.lastlogin, NOW())  % 12)  AS MO,
                (SELECT TIMESTAMPDIFF(YEAR, users.lastlogin, NOW())) AS Y,
                (select COUNT(p.id) as products FROM posts p WHERE p.created_by=users.id AND p.status =1) as products
            FROM users wHERE active=1");


        if(Auth::user() && Auth::user()->id > 0){
            $log = User::firstOrCreate(['id' => Auth::user()->id]);
            $log->lastlogin = DB::raw('now()');
            $log->save();
        }

        $mobile = isset($_SESSION['mobile'])? $_SESSION['mobile']:'home';

        return view($mobile, 
            [
            'users'=>$users,
            'posts' =>$posts, 
            'categories' => $categories,
            'total_item'=>$total,
            'limit_item' => $limit
            ]
        );
    }


    public function viewMore($id){
        $limit = config('app.limit_home');
        $posts = DB::select("SELECT
                                post.id,
                                post.title,
                                post.description,
                                post.price,
                                post.`condition`,
                                post.created_by,
                                post.rate,
                                (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=".(Auth::user()?Auth::user()->id:0)." AND likes.`like`=1 LIMIT 1) AS like_user,
                                (SELECT TIMESTAMPDIFF(SECOND, post.created_at,  NOW())  % 60) as S,
                                (SELECT TIMESTAMPDIFF(MINUTE, post.created_at,  NOW())  % 60) as M,
                                (SELECT TIMESTAMPDIFF(HOUR, post.created_at,    NOW())  % 24) as H,
                                (SELECT TIMESTAMPDIFF(DAY,  post.created_at,    NOW())  % 30) as D,
                                (SELECT TIMESTAMPDIFF(MONTH, post.created_at,   NOW())  % 12) as MO,
                                (SELECT TIMESTAMPDIFF(YEAR, post.created_at,    NOW())) as Y,

                                (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                                (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                                (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                        FROM
                                posts post
                        WHERE
                                post.status = 1  AND post.id < ?
                        ORDER BY created_at DESC
                        LIMIT ?", [$id, $limit]);

        $categories = DB::select("SELECT *FROM categories WHERE status=1 ORDER BY sequence ASC");

        $users = DB::select("
            SELECT 
                id, 
                uuserid,
                'name',
                photo,
                email,
                created_at,
                (SELECT TIMESTAMPDIFF(SECOND, users.lastlogin,  NOW())  % 60) AS S,
                (SELECT TIMESTAMPDIFF(MINUTE, users.lastlogin,  NOW())  % 60) AS M,
                (SELECT TIMESTAMPDIFF(HOUR, users.lastlogin,  NOW())  % 24) AS H,
                (SELECT TIMESTAMPDIFF(DAY, users.lastlogin,  NOW())  % 30) AS D,
                (SELECT TIMESTAMPDIFF(MONTH, users.lastlogin, NOW())  % 12)  AS MO,
                (SELECT TIMESTAMPDIFF(YEAR, users.lastlogin, NOW())) AS Y,
                (select COUNT(p.id) as products FROM posts p WHERE p.created_by=users.id AND p.status =1) as products
            FROM users wHERE active=1");

        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile'] =='mobile.home')?'mobile.home_more':'posts.home_more';

        return view($mobile  , 
            [
            'users'=>$users,
            'posts' =>$posts, 
            'categories' => $categories,
            'lid'=> $lid
            ]
        );
        
    }

    public function getLastId($id){
        $limit = config('app.limit_home');
        $posts = DB::select("SELECT
                                post.id,
                                post.title,
                                post.description,
                                post.price,
                                post.`condition`,
                                post.created_by,
                                post.rate,
                                (SELECT (SELECT TIMESTAMPDIFF(SECOND, u.lastlogin,  NOW())  % 60)  from users u where u.id = post.created_by) as S,
                                (SELECT (SELECT TIMESTAMPDIFF(MINUTE, u.lastlogin,  NOW())  % 60)  from users u where u.id = post.created_by) as M,
                                (SELECT (SELECT TIMESTAMPDIFF(HOUR, u.lastlogin,    NOW())  % 24)  from users u where u.id = post.created_by) as H,
                                (SELECT (SELECT TIMESTAMPDIFF(DAY,  u.lastlogin,    NOW())  % 30)  from users u where u.id = post.created_by) as D,
                                (SELECT (SELECT TIMESTAMPDIFF(MONTH, u.lastlogin,   NOW())  % 12)  from users u where u.id = post.created_by) as MO,
                                (SELECT (SELECT TIMESTAMPDIFF(YEAR, u.lastlogin,    NOW()))  from users u where u.id = post.created_by) as Y,

                                (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                                (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                                (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC  LIMIT 1) as image
                        FROM
                                posts post
                        WHERE
                                post.status = 1 AND post.id < ?
                        ORDER BY created_at DESC
                        
                        LIMIT ?", [ $id, $limit]);
        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }
        return response()->json(['lid' => $lid,'count' => count($posts), 'limit' => $limit]);
    }

    public function category($id){

        $id_cates = DB::select("SELECT id FROM categories WHERE parent_id = ? OR id = ?", [$id,$id]);
        if(count($id_cates)<=0){
            abort(404);
        }
        $inids = [];
        foreach($id_cates as $cids){
            $inids[] = $cids->id;

        }

        $total = DB::table('posts')
            ->where('status','=', 1)
            ->whereIn('category_id', $inids)
            ->orWhere('category_id','=', $id)
            ->count();

        $limit = config('app.limit_category');

        $posts = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=".(Auth::user()?Auth::user()->id:0)." AND likes.`like`=1 LIMIT 1) AS like_user,
                            (SELECT TIMESTAMPDIFF(SECOND, post.created_at,  NOW())  % 60) as S,
                            (SELECT TIMESTAMPDIFF(MINUTE, post.created_at,  NOW())  % 60) as M,
                            (SELECT TIMESTAMPDIFF(HOUR, post.created_at,    NOW())  % 24) as H,
                            (SELECT TIMESTAMPDIFF(DAY,  post.created_at,    NOW())  % 30) as D,
                            (SELECT TIMESTAMPDIFF(MONTH, post.created_at,   NOW())  % 12) as MO,
                            (SELECT TIMESTAMPDIFF(YEAR, post.created_at,    NOW())) as Y,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status = 1
                                AND (post.category_id IN (SELECT id FROM categories WHERE parent_id = ?) OR post.category_id = ?)
                            ORDER BY created_at DESC LIMIT ?", [$id, $id, $limit]);

        $zones = DB::select("SELECT *FROM zones WHERE country_id = 36");

        $category = App\Category::find($id);
        $subCategories = DB::table('categories')->where('parent_id','=', $id)->get();

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile'] =='mobile.home')?'mobile.category':'category.index';

        return view($mobile,
            [
            'posts' => $posts, 
            'category' => $category,
            'subCategories'=>$subCategories,
            'zones'=> $zones,
            'total_item'=> $total,
            'limit_item' => $limit
        ]);
    }

    public function getLastIdByCategory($cid, $lid){
        $limit = config('app.limit_category');
        $posts = DB::select("SELECT
                            post.id
                            FROM
                                posts post
                            WHERE
                                post.status = 1
                                AND post.id < ?
                                AND (post.category_id IN (SELECT id FROM categories WHERE parent_id = ?) OR post.category_id = ?)
                            ORDER BY created_at DESC LIMIT ?", [$lid, $cid, $cid, $limit]);
        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }
        return response()->json(
            [
            'lid' => $lid,
            'cid' => $cid,
            'count' => count($posts), 
            'limit' => $limit
        ]);
    }

    public function viewMoreByCategory($cid, $id){
        $limit = config('app.limit_category');
        $posts = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=".(Auth::user()?Auth::user()->id:0)." AND likes.`like`=1 LIMIT 1) AS like_user,
                            (SELECT TIMESTAMPDIFF(SECOND, post.created_at,  NOW())  % 60) as S,
                            (SELECT TIMESTAMPDIFF(MINUTE, post.created_at,  NOW())  % 60) as M,
                            (SELECT TIMESTAMPDIFF(HOUR, post.created_at,    NOW())  % 24) as H,
                            (SELECT TIMESTAMPDIFF(DAY,  post.created_at,    NOW())  % 30) as D,
                            (SELECT TIMESTAMPDIFF(MONTH, post.created_at,   NOW())  % 12) as MO,
                            (SELECT TIMESTAMPDIFF(YEAR, post.created_at,    NOW())) as Y,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status = 1
                                AND  post.id < ?
                                AND (post.category_id IN (SELECT id FROM categories WHERE parent_id = ?) OR post.category_id = ?)
                            ORDER BY created_at DESC LIMIT ?", [$id, $cid, $cid, $limit]);

        $zones = DB::select("SELECT *FROM zones WHERE country_id = 36");

        $category = App\Category::find($cid);

        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }


        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile'] =='mobile.home')?'mobile.category_more':'category.search';

        return view($mobile,
            [
            'posts' => $posts, 
            'category' => $category,
            'zones'=> $zones,
            'lid' => $lid
        ]);
    }

    public function detail($id=0){
        $us_id = 0;
        if(Auth::user()){
            $us_id = Auth::user()->id;
        }
        
        $post = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.parent_category,
                            post.created_by,
                            post.rate,
                            post.deal_method,
                            post.status,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=? AND likes.`like`=1 LIMIT 1) AS like_user,
                            (SELECT `name` FROM zones WHERE zones.zone_id = post.location_id) as location_name,
                            (SELECT `name_kh` FROM zones WHERE zones.zone_id = post.location_id) as location_name_kh,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT `name_kh` FROM categories WHERE categories.id = post.category_id) as category_name_kh,
                            (SELECT `name` FROM categories WHERE categories.id = post.parent_category) as parent_category_name,
                            (SELECT `name_kh` FROM categories WHERE categories.id = post.parent_category) as parent_category_name_kh,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image,
                            (SELECT TIMESTAMPDIFF(SECOND, post.created_at,  NOW())  % 60) as S,
                            (SELECT TIMESTAMPDIFF(MINUTE, post.created_at,  NOW())  % 60) as M,
                            (SELECT TIMESTAMPDIFF(HOUR, post.created_at,    NOW())  % 24) as H,
                            (SELECT TIMESTAMPDIFF(DAY,  post.created_at,    NOW())  % 30) as D,
                            (SELECT TIMESTAMPDIFF(MONTH, post.created_at,   NOW())  % 12) as MO,
                            (SELECT TIMESTAMPDIFF(YEAR, post.created_at,    NOW())) as Y
                            FROM
                                posts post
                            WHERE
                                post.status = 1
                                AND post.id = ?
                            ORDER BY created_at DESC", [$us_id, $id]);

        
        if($id==0 || count($post)<=0){
            abort(404);
        }

        

        if(count($post)>0){
            $post = $post[0];
            $photos = DB::select("SELECT id,`name` FROM post_images WHERE post_id=? order by cover DESC, id ASC", [$id]);
            $address = DB::select("
                 SELECT
                    addresses.id,
                    addresses.first_name,
                    addresses.last_name,
                    addresses.email,
                    addresses.phone,
                    addresses.address,
                    users.uuserid,
                    users.created_at,
                    addresses.payment_method,
                    addresses.shipping,
                    users.email_verified_at,
                    users.phone as uphone,
                    users.facebook_id,
                    (SELECT TIMESTAMPDIFF(SECOND, users.lastlogin,  NOW())  % 60) AS S,
                    (SELECT TIMESTAMPDIFF(MINUTE, users.lastlogin,  NOW())  % 60) AS M,
                    (SELECT TIMESTAMPDIFF(HOUR,   users.lastlogin,  NOW())  % 24) AS H,
                    (SELECT TIMESTAMPDIFF(DAY,    users.lastlogin,  NOW())  % 30) AS D,
                    (SELECT TIMESTAMPDIFF(MONTH,  users.lastlogin,  NOW())  % 12)  AS MO,
                    (SELECT TIMESTAMPDIFF(YEAR,   users.lastlogin,  NOW())) AS Y,

                    (SELECT TIMESTAMPDIFF(HOUR,   users.created_at,  NOW())  % 24) AS hours,
                   	(SELECT TIMESTAMPDIFF(DAY,    users.created_at, NOW())  % 30) AS days,
	                (SELECT TIMESTAMPDIFF(MONTH,  users.created_at, NOW())  % 12)  AS months,
                    (SELECT TIMESTAMPDIFF(YEAR,   users.created_at, NOW())) AS years,
                    CONCAT(
                        DAY (users.created_at),' ',
                            MONTHNAME(users.created_at),', ',
                            YEAR (users.created_at)
                    ) AS dayname
                FROM
                    addresses,
                    users
                WHERE
                    users.id = addresses.user_id 

                    AND users.id=?", [$post->created_by]);

            $payment = json_decode($address[0]->payment_method);
            $shipping = json_decode($address[0]->shipping);
            $post->deal_method =  json_decode($post->deal_method);
            $post->address = $address[0];
            $post->photos = $photos;
        }

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.detail':'category.detail';

        return view($mobile, ['post'=> $post, 'payment'=> $payment, 'shipping'=> $shipping]);
    }

    public function searchs($text=''){
        
        $qurey = "SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=".(Auth::user()?Auth::user()->id:0)." AND likes.`like`=1 LIMIT 1) AS like_user,
                            (SELECT TIMESTAMPDIFF(SECOND, post.created_at,  NOW())  % 60) as S,
                            (SELECT TIMESTAMPDIFF(MINUTE, post.created_at,  NOW())  % 60) as M,
                            (SELECT TIMESTAMPDIFF(HOUR, post.created_at,    NOW())  % 24) as H,
                            (SELECT TIMESTAMPDIFF(DAY,  post.created_at,    NOW())  % 30) as D,
                            (SELECT TIMESTAMPDIFF(MONTH, post.created_at,   NOW())  % 12) as MO,
                            (SELECT TIMESTAMPDIFF(YEAR, post.created_at,    NOW())) as Y,

				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                (
                                (post.category_id IN (SELECT `id` FROM categories WHERE categories.`name` LIKE ?) AND post.status = 1) OR
                                (post.parent_category IN (SELECT `id` FROM categories WHERE categories.`name` LIKE ?) AND post.status = 1)
                                )
								OR ((LOWER(post.tags) LIKE ?) AND post.status = 1)
                            ORDER BY post.created_at DESC LIMIT 30";
        $text = strtolower($text);

        $posts = DB::select($qurey, ["%$text%","%$text%", "%$text%"]);

        $zones = DB::select("SELECT *FROM zones WHERE country_id = 36");

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.home_search':'category.searchs';

        return view($mobile, 
            ['text'=> $text,
            'zones' => $zones,
            'posts' => $posts
            ]
        );
    }

    public function images($lid=0){
        $images = DB::select("SELECT id,`name` FROM post_images WHERE post_id=? order by cover DESC, id ASC", [$lid]);
        return view('mobile.images', ['images' => $images]);
    }

    public function carousel(){
        $categories = DB::select("SELECT *FROM categories WHERE status=1 AND parent_id IS NULL ORDER BY sequence ASC");
        return view('layouts.carousel', ['categories' => $categories ]);
    }

    public function language(Request $request){
        //$request->session()->put('lang', $request->lang);
        //DB::table('languages')->where('id', 1)->update(['name' => $request->lang]);
        $_SESSION["lang"] = $request->lang;
        return response()->json(['lang' => $_SESSION["lang"]]);
    }

}
