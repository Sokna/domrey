<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware('auth');
    }

    public function getCategory(Request $request, $id){
        $child_category = DB::select("SELECT id, name,name_kh FROM categories WHERE status=1 AND parent_id=? ORDER BY name",[$id]);
        return response()->json($child_category);
    }

    public function updateCategory(Request $request, $id){

        $category = new Category;
        if($request->id >0){
            $category = Category::firstOrCreate(['id' => $request->id]);
        }
        
        $category->name = $request->category_name;
        $category->name_kh = $request->category_name_kh;
        $category->sequence = $request->sequence;
        $category->icon = $request->icon;
        $category->status = $request->status;
        
        if($request->parent_id > 0){
            $category->parent_id = $request->parent_id;
        }
        $category->save();
        
        return response()->json($request->all());
    }

}