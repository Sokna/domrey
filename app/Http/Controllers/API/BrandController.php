<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Brand;

class BrandController extends Controller
{

    public function getBrands(Request $request){
        $brand = DB::select("SELECT * FROM brands WHERE name like ? LIMIT 10", ["%".strtolower($request->brand_name)."%"]);
        return response()->json($brand);
    }


    public function update(Request $request, $id){

        $brand = new Brand;
        if($request->id >0){
            $brand = Brand::firstOrCreate(['id' => $request->id]);
        }
        
        $brand->name = $request->brand_name;
        $brand->status = $request->status;
        $brand->save();
        return $request->all();
    }


}