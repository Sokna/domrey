<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Classes\Resize;
use App\Post;
use App\Address;
use App\User;
use Auth;

class ProfileController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function account(Request $request)
    {
        $user= User::firstOrNew(['id'=>Auth::user()->id]);
        // if($request->has('file')){
        //     $image = $request->file('file');
        //     $name = $request->user_id.".".$image->getClientOriginalExtension();
        //     $image->move(public_path().'/images/users', $name); 
        //     $user->photo = $name;

        //     //Resize image
        //     $resizeObj = new Resize(public_path()."/images/users/".$name);
        //     $resizeObj -> resizeImage(150, 150, 0);
        //     $resizeObj -> saveImage(public_path()."/images/users/150x150/".$name, 100);

        // }

        if($request->file){
            $image_parts = explode(";base64,", $request->file);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);
            $name = "{$request->user_id}.png";
            $file = public_path()."/images/users/150x150/$name";
            file_put_contents($file, $image_base64);

              //Convert file
            $resizeObj = new resize($file);
            $resizeObj -> resize_custom_image($file, 200, 200);

            $user->photo = $name;
        }

       
        $haveUser = DB::table('users')->where('email','=', $request->email)->where('id','!=', Auth::user()->id)->first();
        
        if($haveUser){
            $data['email'] = $request->email;
            $data['id'] = Auth::user()->id;
        }else{
            $user->phone = $request->phone;
            $user->name = $request->name;
           // $user->name = $request->username;
            $user->email = $request->email;
            $data['id'] = $user->id;
        }
        $user->name = $request->name;
        $user->save();
        
        return response()->json($data);
    }


    public function changePassword(Request $request){
       // $users = DB::select('select * from users where id = ? and password=?', array(Auth::user()->id, Hash::make($request->current_password)));

        // $user= User::firstOrNew([
        //     'id'=>Auth::user()->id, 
        //     //'password'=> Hash::make($request->current_password)
        // ]);

        //$user->password = Hash::make($request->new_password);
        //$user->save();
        $users[] = Hash::make($request->current_password);
        return response()->json($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];
        if ($request->isMethod('post')) {
            $address = new Address;
            if($request->address_id > 0){
                $address = Address::firstOrNew(['id'=>$request->address_id]);
            }
            $address->user_id = Auth::user()->id;
            $address->first_name = $request->first_name;
            $address->last_name = $request->last_name;
            $address->gender = $request->gender;
            $address->phone = $request->phone;
            $address->phones = $request->phones;
            $address->email = $request->email;
            $address->address = $request->address;
            $address->about = $request->about;
            $address->payment_method = $request->payment;
            $address->payment_account = $request->bank_acc;
            $address->shipping = $request->shipping;
            $address->save();
            $data['id'] = $address->id;

        }
        return response()->json($data);
    }


    public function updatePhoto(Request $request){
           
    }

    public function blockUser(Request $request, $id){
        $user = User::find($id);
        $user->active = $request->active;
        $user->save();
        return response()->json($request->all());
    }

    
    public function changeCover(Request $request){
        //Save file
        $id = false;
        if(Auth::user()){
            $image_parts = explode(";base64,", $request->cover);
            $image_type_aux = explode("image/", $image_parts[0]);
            //$image_type = $image_type_aux[1];
            $image_base64 = base64_decode($image_parts[1]);

            $file_name ="cover_".Auth::user()->id.".png";
            
            //Save file to folder
            $file = public_path()."/images/users/$file_name";
            file_put_contents($file, $image_base64);

            $user = User::find((Auth::user()->id));
            $user->cover_photo = $file_name;

            //Convert file
            //$resizeObj = new resize($file);
            //$resizeObj -> resize_image($file, 1200, 1200);

            //Save file name to users
            $id =  $user->save();
           
        }

        return response()->json(['id' => $id ]);
    }

}
