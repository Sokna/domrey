<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App;
use App\Chat;
use App\ChatUser;
use App\Classes\Resize;

class ChatController extends Controller
{


    public function chat(Request $request, $id){

        $chat = new Chat;
        $chat_user_id = $request->chat_user_id;
        $post = App\Post::find($request->post_id);

        $chatUser = App\ChatUser::find($chat_user_id);

        if($request->chat_user_id == 0){
            $chatUser = new ChatUser;
            $chatUser->owner_id = $post->created_by;
            $chatUser->post_id = $request->post_id;
            $chatUser->chated_by = $request->sender;
            $chatUser->save();
            $chat_user_id = $chatUser->id;
        }

        //Change reviever message
        $receiver = $chatUser->owner_id;
        if($chatUser->owner_id == Auth::user()->id){
            $receiver = $chatUser->chated_by;
        }

        $chat->chat_user_id =   $chat_user_id;
        $chat->sender =    Auth::user()->id;
        $chat->receiver =  $receiver;
        $chat->message =   $request->message;
        
        if ($request->has('file')) {
            $f_name = time();
            $image = $request->file('file');
            $name  = $f_name.".". $image->getClientOriginalExtension();

            $part = public_path().'/images/messages';
            $image->move($part, $name);
            $chat->file = $name;
            $chat->is_file =  1;


            $resizeObj = new resize($part.'/'.$name);
            $resizeObj -> resize_image($part.'/'.$name, 600, 600);
        }

        $chat->save();
        $data = $this->readOneChatMessage($chat->id);

        return response()->json($data);

    }

    
    public function readChatMessage(Request $request){

        $chatUser = DB::select('select * FROM chat_users WHERE post_id=? AND chated_by=? LIMIT 1', [$request->post_id, $request->sender]);

        // if($request->sender == Auth::user()->id){
        //     $chatUser = DB::select('select * FROM chat_users WHERE post_id=? AND owner_id=?  chated_by=?  LIMIT 1', [$request->post_id, Auth::user()->id, $request->sender]);
        // }
        
        $chat_user_id = 0;

        if(count($chatUser)>0){
           $chat_user_id =  $chatUser[0]->id;
        }
        
        $messages = DB::select("
            SELECT
                chats.id,
                chats.chat_user_id,
                chats.message,
                chats.sender,
                chats.receiver,
                chats.created_at,
                chats.file,
                chats.is_file,
                chats.is_read,
                DATE_FORMAT(chats.created_at, '%d %b %H:%i %p') AS sending_time,
                (SELECT users.`name` FROM users WHERE users.id = chats.sender)  AS sender_name,
                (SELECT users.`name` FROM users WHERE users.id = chats.receiver) AS receiver_name,
                (SELECT users.photo FROM users WHERE users.id = chats.sender)    AS sender_photo,
                (SELECT users.photo FROM users WHERE users.id = chats.receiver)  AS receiver_photo
            FROM
                chats WHERE  chats.chat_user_id = ? AND chats.id > ?

            ORDER BY
                chats.id ASC", [$chat_user_id, $request->lastId]);

        return response()->json($messages);
    }


    public function readOneChatMessage($id){
        $message = DB::select("
            SELECT
                chats.id,
                chats.chat_user_id,
                chats.message,
                chats.sender,
                chats.receiver,
                chats.created_at,
                chats.file,
                chats.is_file,
                DATE_FORMAT(chats.created_at, '%d %b %H:%i %p') AS sending_time,
                (SELECT users.`name` FROM users WHERE users.id = chats.sender)  AS sender_name,
                (SELECT users.`name` FROM users WHERE users.id = chats.receiver) AS receiver_name,
                (SELECT users.photo FROM users WHERE users.id = chats.sender)    AS sender_photo,
                (SELECT users.photo FROM users WHERE users.id = chats.receiver)  AS receiver_photo
            FROM
                chats WHERE  chats.id = ? ", [$id]);

        return $message[0];
    }


    public function updateChatMessage(Request $request){

        $ids = explode(",", $request->ids);
        foreach($ids as $id){
            $chat = App\Chat::find($id);
            $chat->is_read = 1;
            $chat->save();
        }

        return response()->json($request->all());
    }

}