<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Brand;
use App\PostImage;
use App\Report;
use Auth;
use App;

use App\Classes\Resize;

class PostController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware('auth');
        App::setlocale(isset($_SESSION['lang'])? $_SESSION['lang']:'en');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function autoComplete(Request $request)
    {
        $data = DB::select("SELECT LOWER(posts.tags) AS tags  FROM posts WHERE LOWER(posts.tags) LIKE ?", ["%".strtolower($request->text)."%"]);
        return response()->json($data);
       
    }

    public function searchProducts(Request $request){

        $data = [
            $request->new, 
            $request->used
        ];

        $qurey = "SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=".(Auth::user()?Auth::user()->id:0)." AND likes.`like`=1 LIMIT 1) AS like_user,
                            (SELECT TIMESTAMPDIFF(SECOND, post.created_at,  NOW())  % 60) as S,
                            (SELECT TIMESTAMPDIFF(MINUTE, post.created_at,  NOW())  % 60) as M,
                            (SELECT TIMESTAMPDIFF(HOUR, post.created_at,    NOW())  % 24) as H,
                            (SELECT TIMESTAMPDIFF(DAY,  post.created_at,    NOW())  % 30) as D,
                            (SELECT TIMESTAMPDIFF(MONTH, post.created_at,   NOW())  % 12) as MO,
                            (SELECT TIMESTAMPDIFF(YEAR, post.created_at,    NOW())) as Y,

				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status = 1
                                AND (post.`condition` IN (?,?))
                            ";

        if(isset($request->id)){
            $data[] = $request->id;
            $data[] = $request->id; 
            $qurey.=" AND (post.category_id IN (SELECT id FROM categories WHERE parent_id = ?) OR post.category_id = ?) ";
        }

        if(isset($request->text)){
            $data[] ="%".strtolower($request->text)."%";
            $data[] ="%".strtolower($request->text)."%"; 
            $qurey.=" AND (LOWER(post.tags) LIKE ?  OR LOWER(post.title) LIKE ?) ";
        }

        if($request->max > 0){
            $qurey.=" AND (post.price BETWEEN ? AND ?) ";
            $data[] = $request->min; 
            $data[] = $request->max;
        }

        if($request->location_id > 0){
            $qurey.=" AND post.location_id=?";
            $data[] = $request->location_id;
        }

        $qurey.=" ORDER BY created_at DESC LIMIT 30";
        
 
        $posts = DB::select($qurey, $data);

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile'] =='mobile.home')?'mobile.category_search':'category.search';

        return view($mobile, ['posts'=> $posts]);
    }

    public function statusChange(Request $request){

        $post = Post::firstOrCreate(['id' => $request->id]);

        $status = $request->status;
        if($post->status == -2){
            $count = DB::table('reports')->where('post_id', '=',  $request->id)->count();
            $status = 1;
            
            if($count>=5){
                DB::table('reports')->where('post_id', '=',  $request->id)->delete();
            }
        }
        $post->status = $status;
        $post->save();
        
        return response()->json($request->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//if No brand and Add new
		$brand_id = $request->brand_id;
		if($request->brand_id == 0){
			$brand_id = $this->addNewBrand($request->brand_name);
		}
		
        $data = [];
        if ($request->isMethod('post')) {
            $post = new Post;
            if($request->id>0){
                $post = Post::firstOrCreate(['id' => $request->id]);
            }
            $post->category_id = $request->category_id;
            $post->parent_category = $request->parent_category;
            $post->title = $request->title;
            $post->brand_id = $brand_id;
            $post->location_id = $request->location_id;
            $post->condition = $request->condition;
            $post->created_by = Auth::user()->id;
            $post->price = $request->price;
            $post->description = $request->description;
            $post->deal_method = $request->deal_method;
            $post->tags = $request->tags;
            
           if($post->save()){
                
                foreach($request['files']  as $key =>  $image){

                    $data_name = $request['data_name'][$key];
                    $data_edit = $request['data_edit'][$key];
                    $data_cover = $request['data_cover'][$key];

                    if($data_edit == 'new'){
                        //Save file
                        $image_parts = explode(";base64,", $image);
                        $image_type_aux = explode("image/", $image_parts[0]);
                        $image_type = $image_type_aux[1];
                        $image_base64 = base64_decode($image_parts[1]);

                        $file = public_path()."/images/products/500x500/$data_name";
                        file_put_contents($file, $image_base64);
                        
                        $resizeObj = new resize($file);
                        $resizeObj -> resize_image($file, 1200, 1200);
                        
                        $file = public_path()."/images/products/150x150/$data_name";
                        file_put_contents($file, $image_base64);

                        $resizeObj = new resize($file);
                        $resizeObj -> resize_image($file, 200, 200);

                        //End save file

                        $postImage = new PostImage;
                        $postImage->name = $data_name;
                        $postImage->post_id = $post->id;
                        $postImage->cover = $data_cover;
                        $postImage->save();

                    }else if($data_edit == 'delete'){
                        $postImg = PostImage::where('name','=', $data_name)->where('post_id', $post->id)->first();
                        if($postImg){
                            $postImg->delete();
                        }

                    }else if($data_edit == 'update'){
                        //Update cover
                        $postImg = PostImage::where('name','=', $data_name)->where('post_id', $post->id)->first();
                        if($postImg){
                            $postImg->cover = $data_cover;
                            $postImg->save();
                        }

                    }
                
                }//End foreach

                $data['id']= $post->id;
            }
        }

        return response()->json($data);
    }


    public function updatePhoto(Request $request){
       
        $image = $request->file('file');
        $name = $request->proId."_".$request->dataKey.".".$image->getClientOriginalExtension();
        $postImage = new PostImage;
        if($request->dataValue>0){
               $postImage = PostImage::firstOrCreate(['id' => $request->dataValue]);
        }
        $postImage->name = $name;
        $postImage->post_id = $request->proId;
        $postImage->save();
        $image->move(public_path().'/images/products', $name);  
        
        //150x150
        $resizeObj = new Resize(public_path()."/images/products/".$name);
        $resizeObj -> resizeImage(150, 150, 0);
        $resizeObj -> saveImage(public_path()."/images/products/150x150/".$name, 100);


        //500x500
        $resizeObj = new Resize(public_path()."/images/products/".$name);
        $resizeObj -> resizeImage(500, 500, 0);
        $resizeObj -> saveImage(public_path()."/images/products/500x500/".$name, 100);

        return response()->json(['id'=>$postImage->id, 'name' => $name]);
    }


    public function removeImage(Request $request){
        $image = App\PostImage::find($request->id);
        $image->delete();
        
        return response()->json($request->all());
    }



    public function reportListing(Request $request){
        $message_code = [
            "SP" => 'Irrelevant keywords',
            "WC" => 'Items wrongly categorized',
            "FK" => 'Selling counterfeit items',
            "DP" => 'Duplicate posts',
            "PI" => 'Selling prohibited item',
            "HB" => 'Offensive behaviour/content',
            "ML" => 'Mispriced Listings',
            "OT" => 'Other'
        ];

        $message = $request->reporting_smg;
        if($request->reson != 'OT'){
            $message = $message_code[$request->reson];
        }

        $report = new Report;
        $report->post_id = $request->post_id;
        $report->code = $request->reson;
        $report->reported_by = (Auth::user())?Auth::user()->id:0;
        $report->message = $message;
        $report->save();

        $totalLike = DB::table('reports')->where('post_id','=', $request->post_id)->count();//->where('reported_by','>', 0)->count();

        $data['respone'] = $request->all();
        $data['reports_total'] = $totalLike;

        $likeCount = config('app.like_count');

        if($totalLike >= $likeCount){
            $post = Post::firstOrCreate(['id' => $request->post_id]);
            $post->status = -2;
            $post->save();
        }

        return response()->json($data);
    }


    public function likeListing(Request $request, $id){

        $user_id = 0;
        if(Auth::user()){
            $user_id = Auth::user()->id;
        }else{
            return response()->json([
                'id'=> $id,
                'user_id'=> $user_id,
                'like' => 0,
                'rate' => 0
            ]);
        }
        //End check user login

        $post = Post::firstOrCreate(['id' => $id]);
        $count = App\Like::where('post_id', $id)->where('user_id', $user_id)->count();

        if($count==0){

            $l = new App\Like();
            $l->post_id = $id;
            $l->user_id = $user_id;
            $l->like = 1;
            $l->save();

            $like = 1;
            $post->rate = $post->rate + 1;

        }else{

            $first = App\Like::where('post_id', $id)->where('user_id', $user_id)->first();

            if($first->like==1){
                $first->like = 0;
                $first->save();
                $post->rate = $post->rate -1;

            }else{
                
                $first->like = 1;
                $first->save();
                $post->rate = $post->rate + 1;
            }

            $like = $first->like;
        }

        $post->save();

        return response()->json(
        [
            'id'=> $post->id,
            'user_id'=> $user_id,
            'like' => $like,
            'rate' => $post->rate
        ]);
    }
	
	public function addNewBrand($name){
		$brand = new Brand;
		$brand->name = $name;
		$brand->save();
		return $brand->id;
    }
    


     public function listNowV2(Request $request){

        return response()->json($request->all());
     }
}
