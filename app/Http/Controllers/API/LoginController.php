<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth;
use App;

class LoginController extends Controller
{

    public function loginAPI(Request $request){
        $user = App\User::where('email', $request->email)->first();
        if($user){
            $validCredentials = Hash::check($request->password, $user->getAuthPassword());
            if($validCredentials){
                $data['user'] = $user;
                $data['address'] = App\Address::where("user_id", $user->id)->first();
                return response()->json($data);
            }else{
                return response()->json(['error'=>'Wrong password']);
            }
        }else{
            return response()->json(['error'=>'Email not found']);
        }
    }


    public function findUser(Request $request){
        $data = [];

        if($request->phone){
            $phone = $request->phone;
            $pre = $phone;
            if(substr($phone, 0 , 1)==0){
                $pre = substr($phone,1, strlen($phone));
            }
            $inderUser = DB::select("SELECT *FROM users WHERE LOWER(phone)=? OR LOWER(SUBSTRING(phone, 2, LENGTH(phone)))=?",[$phone,  $pre]);

            $data['phone'] = false;
            if($inderUser){
                $data['phone'] = true;
            }
        }elseif($request->name){
            $inderUser = DB::select("SELECT *FROM users WHERE LOWER(uuserid)=?",[strtolower($request->name)]);
            $data['name'] = false;
            if($inderUser){
                $data['name'] = true;
            } 
        }elseif($request->email){
            $inderUser = DB::select("SELECT *FROM users WHERE LOWER(email)=?",[strtolower($request->email)]);
            $data['email'] = false;
            if($inderUser){
                $data['email'] = true;
            } 
        }

        return response()->json( $data ); 
    }

}