<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;
use App\AeonItem;
use Auth;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function category($page = 1, $limit = 20){

        $total = DB::table('categories')->count();
        $pages = ($total / $limit);

        if((int)$pages == $pages){
            $pages = (int)$pages;
        }else{
            $pages = (int)$pages + 1;
        }

        //Offset 
        $offset = ($page - 1) * $limit;

        $categories = DB::select("
            SELECT
                child.id,
                (SELECT `name` FROM categories parent WHERE parent.id=child.parent_id) AS parent_name,
                (SELECT icon FROM categories parent WHERE parent.id=child.parent_id) AS parent_icon,
                child.`name`,
                 child.`name_kh`,
                child.parent_id,
                child.icon,
                child.sequence,
                child.`status`
            FROM
                categories child
            ORDER BY
                parent_name, child.`name`
            LIMIT ? OFFSET ?
        ", [$limit, $offset]);

        $parent_cate = DB::select("SELECT id,`name` FROM categories WHERE parent_id IS NULL ORDER BY `name` ASC");

        return view('admin.category', ['categories' => $categories, 'parent_cate' =>$parent_cate, 'pages'=> $pages, 'limit'=>$limit, 'page'=>$page ]);
    }

    
    public function brand($page = 1, $limit = 20){

        $total = DB::table('brands')->count();
        $pages = ($total / $limit);

        if((int)$pages == $pages){
            $pages = (int)$pages;
        }else{
            $pages = (int)$pages + 1;
        }
        //Offset 
        $offset = ($page - 1) * $limit;

        $brands = DB::select("SELECT *FROM brands ORDER BY name LIMIT ? OFFSET ?", [$limit, $offset]);
        return view('admin.brand', ['brands' => $brands, 'pages'=> $pages, 'limit'=>$limit, 'page'=>$page ]);
    }


    public function listing($page = 1, $limit = 20, $search = ''){

        $total = DB::table('posts')->count();
        
        $pages = ($total / $limit);

        if((int)$pages == $pages){
            $pages = (int)$pages;
        }else{
            $pages = (int)$pages + 1;
        }
        //Offset 
        $offset = ($page - 1) * $limit;

        $posts = DB::select("
            SELECT posts.*,
                   (SELECT post_images.name FROM post_images WHERE post_images.post_id=posts.id ORDER BY post_images.id ASC LIMIT 1) AS image,
                   (select users.name FROM users WHERE users.id=posts.created_by) AS user_name,
                   (select COUNT(*) FROM reports WHERE reports.post_id=posts.id) AS num_of_report
                   FROM posts ORDER BY created_at DESC LIMIT ? OFFSET ?
            ", [$limit, $offset]);

        return view('admin.listing', [
            'posts' => $posts,
            'pages'=> $pages,
            'limit'=> $limit,
            'page'=> $page,
            'search' => $search
        ]);
    }

    public function user($page = 1, $limit = 20){

        $total = DB::table('users')->count();
        $pages = ($total / $limit);

        if((int)$pages == $pages){
            $pages = (int)$pages;
        }else{
            $pages = (int)$pages + 1;
        }
        //Offset 
        $offset = ($page - 1) * $limit;

        $brands = DB::select("SELECT *FROM users ORDER BY name LIMIT ? OFFSET ?", [$limit, $offset]);
        return view('admin.user', ['brands' => $brands, 'pages'=> $pages, 'limit'=>$limit, 'page'=>$page ]);
    }

    public function dashboard(){

        if(Auth::user()->role == 'admin'){
            $total_listing = DB::table('posts')->count();
            $total_sellers = DB::table('users')->count();
            $total_likes = DB::table('likes')->where('like','=', 1)->count();
            $total_dislikes = DB::table('likes')->where('like','=', 0)->count();

            //
            $total_active_listing = DB::table('posts')->where('status','=', 1)->count();
            $total_inactive_listing = DB::table('posts')->where('status','=', 0)->count();
            $total_block_listing = DB::table('posts')->where('status','=', -2)->count();
            $total_delete_listing = DB::table('posts')->where('status','=', -1)->count();

            $recent_listing = DB::select("
                    SELECT
                        post.id,
                        post.title,
                        post.description,
                        post.price,
                        post.`condition`,
                        post.created_by,
                        post.rate,
                        (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC  LIMIT 1) as image
                    FROM
                                    posts post
                    WHERE
                                    post.status = 1 
                    ORDER BY created_at DESC
                    LIMIT 4");

            $top_ten_sellers = DB::select("SELECT
                users.id,
                (SELECT count(*) FROM posts WHERE posts.created_by = users.id) AS total
            FROM
                users
            ORDER BY total DESC
            LIMIT 10");

            $contacts = DB::table('contacts')->orderBy('created_at','DESC')->limit(4)->get();

            return view('admin.index', [
                'total_listing' => $total_listing, 
                'total_sellers' => $total_sellers,
                'total_likes' => $total_likes,
                'total_dislikes' => $total_dislikes,
                'total_active_listing' => $total_active_listing,
                'total_inactive_listing' => $total_inactive_listing,
                'total_block_listing' => $total_block_listing,
                'total_delete_listing' => $total_delete_listing,
                'top_ten_sellers' => count($top_ten_sellers),
                'recent_listing'=> $recent_listing,
                'contacts' => $contacts
            ]);

        }else{
            return redirect()->action('HomeController@index');
        }
    }

    public function index(Request $request){
        
        if($request->user_session && $request->user_session == 'Domrey@123' ){
            session(['log' => 'Domrey@123']);
            return redirect()->action('HomeController@index');
        }

        return view('layouts.admin');
    }


    public function message(){
        $contacts = DB::table('contacts')->orderBy('created_at','DESC')->limit(4)->get();
        return view('admin.message',['contacts'=>$contacts]);
    }


    public function aeon($page = 1, $limit = 300){
        $total = DB::table('aeon_items')->count();
        $pages = ($total / $limit);

        if((int)$pages == $pages){
            $pages = (int)$pages;
        }else{
            $pages = (int)$pages + 1;
        }
        //Offset 
        $offset = ($page - 1) * $limit;



        $items = DB::select("SELECT *FROM aeon_items ORDER BY ID DESC LIMIT ? OFFSET ?", [$limit, $offset]);

        $aeon_id = AeonItem::orderBy('id', 'desc')->first()->aeon_id;

        return view('admin.aeon', ['items'=> $items,  'pages'=> $pages, 'limit'=>$limit, 'page'=>$page , 'aeon_id'=> $aeon_id]);
    }

    public function apiAeon($id){
        $path = 'https://aeononlineshopping.com/index.php?route=product/product&product_id='.$id;
        $homepage = file_get_contents($path);
        return $homepage;
    }

    public function apiSaveAeon(Request $request){


        //$path = explode("/", $request->image);

        $aeon = new AeonItem;
        $aeon->aeon_id = $request->aeon_id;
        $aeon->code = $request->code;
        $aeon->brand = $request->brand;
        $aeon->image = $request->image;
        $aeon->price = $request->price;
        $aeon->title = $request->title;
        $aeon->desctiption = $request->desctiption;
        if($aeon->save()){
            $this->apiSaveImage($aeon->code, $aeon->image);
        }
        
        return response()->json($aeon);

    }


    public function apiSaveImage($name, $url){
        $img = public_path()."/images/aeon/catalog/$name.jpg";
        file_put_contents($img, file_get_contents($url));
    }


}