<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;
use Auth;

class SellerController extends Controller
{

    public function __construct(){
        App::setlocale(isset($_SESSION['lang'])? $_SESSION['lang']:'en');
    }

    public function account(){

       
    }

    public function seller($uuid=''){
        $user = DB::table('users')->where('uuserid','=',$uuid)->first();

        if(!$user){
            abort(404);
        }

        $total = DB::table('posts')->where('created_by','=', $user->id)->whereIn('status', [1])->count();
        $limit = config('app.limit');
        $posts = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
                            post.status,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=".(Auth::user()?Auth::user()->id:0)." AND likes.`like`=1 LIMIT 1) AS like_user,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status = 1
                                AND post.created_by = ?
                            ORDER BY created_at DESC LIMIT ?", [$user->id, $limit]);

        //$user = App\User::find($id);

        $user = DB::select("
                 SELECT
                    addresses.id,
                    addresses.first_name,
                    addresses.last_name,
                    addresses.email,
                    addresses.phone,
                    addresses.address,
                    addresses.about,
                    users.uuserid,
                    users.created_at,
                    users.photo,
                    users.cover_photo,
                    users.email_verified_at,
                    users.phone as uphone,
                    users.facebook_id,
                    addresses.phones,
                    addresses.payment_method,
                    addresses.payment_account,
                    CONCAT(
                        DAY (users.created_at),' ',
                            MONTHNAME(users.created_at),', ',
                            YEAR (users.created_at)
                    ) AS dayname,
                   	(SELECT TIMESTAMPDIFF(DAY, users.created_at,   NOW())  % 30) AS days,
	                (SELECT TIMESTAMPDIFF(MONTH, users.created_at, NOW())  % 12)  AS months,
                    (SELECT TIMESTAMPDIFF(YEAR, users.created_at,  NOW())) AS years,
                    (SELECT TIMESTAMPDIFF(SECOND, users.lastlogin, NOW())  % 60) as S,
                    (SELECT TIMESTAMPDIFF(MINUTE, users.lastlogin, NOW())  % 60) as M,
                    (SELECT TIMESTAMPDIFF(HOUR, users.lastlogin,   NOW())  % 24) as H,
                    (SELECT TIMESTAMPDIFF(DAY,  users.lastlogin,   NOW())  % 30) as D,
                    (SELECT TIMESTAMPDIFF(MONTH, users.lastlogin,  NOW())  % 12) as MO,
                    (SELECT TIMESTAMPDIFF(YEAR, users.lastlogin,   NOW())) as Y

                FROM
                    addresses,
                    users
                WHERE
                    users.id = addresses.user_id 

                    AND users.id=?", [$user->id]);

        if(count($user)<=0){
            return redirect()->action('HomeController@index');
        }

        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }
    
       $user[0]->phones =  json_decode($user[0]->phones);
       $user[0]->payment_method =  json_decode($user[0]->payment_method);
       $user[0]->payment_account =  json_decode($user[0]->payment_account);

       $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.seller':'seller.index';
       return view($mobile, 
        [
            'posts' => $posts, 
            'lid'=> $lid ,
            'limit_item' => $limit,
            'total_item'=> $total,
            'user' => $user[0]
        ]);
    }

    public function me(){
        // $user = DB::table('users')->where('uuserid','=',$uuid)->first();

        
        // if(!$user){
        //     abort(404);
        // }

        if(!Auth::user()){
            return redirect()->guest('/login');
        }

        $id = Auth::user()->id;

        $total = DB::table('posts')->where('created_by','=', $id)->whereIn('status',[1,0,-2])->count();
        $limit = config('app.limit');
        $posts = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
                            post.status,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=$id AND likes.`like`=1 LIMIT 1) AS like_user,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status in(0,1,-2)
                                AND post.created_by = ?
                            ORDER BY created_at DESC LIMIT ?", [$id, $limit]);

        //$user = App\User::find($id);

        $user = DB::select("
                 SELECT
                    addresses.id,
                    addresses.first_name,
                    addresses.last_name,
                    addresses.email,
                    addresses.phone,
                    addresses.address,
                    addresses.about,
                    users.uuserid,
                    users.created_at,
                    users.photo,
                    users.cover_photo,
                    users.email_verified_at,
                    users.phone as uphone,
                    users.facebook_id,
                    addresses.phones,
                    addresses.payment_method,
                    addresses.payment_account,
                    CONCAT(
                        DAY (users.created_at),' ',
                            MONTHNAME(users.created_at),', ',
                            YEAR (users.created_at)
                    ) AS dayname,
                    (SELECT TIMESTAMPDIFF(MINUTE, users.created_at,   NOW())  % 60) as minutes,
                    (SELECT TIMESTAMPDIFF(HOUR, users.created_at,   NOW())  % 24) as hours,
                   	(SELECT TIMESTAMPDIFF(DAY, users.created_at,   NOW())  % 30) AS days,
	                (SELECT TIMESTAMPDIFF(MONTH, users.created_at, NOW())  % 12)  AS months,
                    (SELECT TIMESTAMPDIFF(YEAR, users.created_at,  NOW())) AS years,

                    (SELECT TIMESTAMPDIFF(SECOND, users.lastlogin, NOW())  % 60) as S,
                    (SELECT TIMESTAMPDIFF(MINUTE, users.lastlogin, NOW())  % 60) as M,
                    (SELECT TIMESTAMPDIFF(HOUR, users.lastlogin,   NOW())  % 24) as H,
                    (SELECT TIMESTAMPDIFF(DAY,  users.lastlogin,   NOW())  % 30) as D,
                    (SELECT TIMESTAMPDIFF(MONTH, users.lastlogin,  NOW())  % 12) as MO,
                    (SELECT TIMESTAMPDIFF(YEAR, users.lastlogin,   NOW())) as Y

                FROM
                    addresses,
                    users
                WHERE
                    users.id = addresses.user_id 

                    AND users.id=?", [$id]);

        if(count($user)<=0){
            return redirect()->action('HomeController@index');
        }

        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }
    
       $user[0]->phones =  json_decode($user[0]->phones);
       $user[0]->payment_method =  json_decode($user[0]->payment_method);
       $user[0]->payment_account =  json_decode($user[0]->payment_account);

       //$mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.me':'seller.index';
       return view('mobile.me', 
        [
            'posts' => $posts, 
            'lid'=> $lid ,
            'limit_item' => $limit,
            'total_item'=> $total,
            'user' => $user[0]
        ]);
    }

    public function viewMoreSeller($sid, $id){
        $limit = config('app.limit');
        $posts = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
                            post.status,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=".(Auth::user()?Auth::user()->id:0)." AND likes.`like`=1 LIMIT 1) AS like_user,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status = 1
                                AND post.created_by = ?
                                AND post.id < ?
                            ORDER BY created_at DESC LIMIT ?", [$sid, $id,  $limit]);

        $user = App\User::find($sid);

        $user = DB::select("
                 SELECT
                    addresses.id,
                    addresses.first_name,
                    addresses.last_name,
                    addresses.email,
                    addresses.phone,
                    addresses.address,
                    addresses.about,
                    users.uuserid,
                    users.created_at,
                    users.photo,
                   	(SELECT TIMESTAMPDIFF(DAY, users.created_at,   NOW())  % 30) AS days,
	                (SELECT TIMESTAMPDIFF(MONTH, users.created_at, NOW())  % 12)  AS months,
                    (SELECT TIMESTAMPDIFF(YEAR, users.created_at,  NOW())) AS years,
                    (SELECT TIMESTAMPDIFF(SECOND, users.lastlogin, NOW())  % 60) as S,
                    (SELECT TIMESTAMPDIFF(MINUTE, users.lastlogin, NOW())  % 60) as M,
                    (SELECT TIMESTAMPDIFF(HOUR, users.lastlogin,   NOW())  % 24) as H,
                    (SELECT TIMESTAMPDIFF(DAY,  users.lastlogin,   NOW())  % 30) as D,
                    (SELECT TIMESTAMPDIFF(MONTH, users.lastlogin,  NOW())  % 12) as MO,
                    (SELECT TIMESTAMPDIFF(YEAR, users.lastlogin,   NOW())) as Y

                FROM
                    addresses,
                    users
                WHERE
                    users.id = addresses.user_id 

                    AND users.id=?", [$sid]);

        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }

        $data = ['posts' => $posts,'lid'=> $lid, 'user' => $user[0]];

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.seller_more':'seller.more';

        return view($mobile , $data);

    }


    public function viewMore($sid, $id){
        $limit = config('app.limit');
        $posts = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
                            post.status,
                            (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=".(Auth::user()?Auth::user()->id:0)." AND likes.`like`=1 LIMIT 1) AS like_user,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status in(0,1,-2)
                                AND post.created_by = ?
                                AND post.id < ?
                            ORDER BY created_at DESC LIMIT ?", [$sid, $id,  $limit]);

        $user = App\User::find($sid);

        $user = DB::select("
                 SELECT
                    addresses.id,
                    addresses.first_name,
                    addresses.last_name,
                    addresses.email,
                    addresses.phone,
                    addresses.address,
                    addresses.about,
                    users.uuserid,
                    users.created_at,
                    users.photo,
                   	(SELECT TIMESTAMPDIFF(DAY, users.created_at,   NOW())  % 30) AS days,
	                (SELECT TIMESTAMPDIFF(MONTH, users.created_at, NOW())  % 12)  AS months,
                    (SELECT TIMESTAMPDIFF(YEAR, users.created_at,  NOW())) AS years,
                    (SELECT TIMESTAMPDIFF(SECOND, users.lastlogin, NOW())  % 60) as S,
                    (SELECT TIMESTAMPDIFF(MINUTE, users.lastlogin, NOW())  % 60) as M,
                    (SELECT TIMESTAMPDIFF(HOUR, users.lastlogin,   NOW())  % 24) as H,
                    (SELECT TIMESTAMPDIFF(DAY,  users.lastlogin,   NOW())  % 30) as D,
                    (SELECT TIMESTAMPDIFF(MONTH, users.lastlogin,  NOW())  % 12) as MO,
                    (SELECT TIMESTAMPDIFF(YEAR, users.lastlogin,   NOW())) as Y

                FROM
                    addresses,
                    users
                WHERE
                    users.id = addresses.user_id 

                    AND users.id=?", [$sid]);

        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }

        $data = ['posts' => $posts,'lid'=> $lid, 'user' => $user[0]];

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.me_more':'seller.more';

        return view($mobile , $data);

    }

    public function getLastSellerId($sid, $id){
        $limit = config('app.limit');

        $posts = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status = 1
                                AND post.created_by = ?
                                AND post.id < ?
                            ORDER BY created_at DESC LIMIT ?", [$sid, $id,  $limit]);

        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }


        return response()->json(['lid' => $lid,'count' => count($posts), 'limit' => $limit]);
    
    }


    public function getLastId($sid, $id){
        $limit = config('app.limit');

        $posts = DB::select("SELECT
                            post.id,
                            post.title,
                            post.description,
                            post.price,
                            post.`condition`,
                            post.category_id,
                            post.created_by,
                            post.rate,
				            (SELECT `name` FROM categories WHERE categories.id = post.category_id) as category_name,
                            (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                            (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                            (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC   LIMIT 1) as image
                            FROM
                                posts post
                            WHERE
                                post.status in(0,1,-2)
                                AND post.created_by = ?
                                AND post.id < ?
                            ORDER BY created_at DESC LIMIT ?", [$sid, $id,  $limit]);

        $lid = 0;
        if(count($posts)>0){
            $lid = $posts[count($posts)-1]->id;
        }


        return response()->json(['lid' => $lid,'count' => count($posts), 'limit' => $limit]);
    
    }


    public function myLike(){
        if(!Auth::user()){
           return redirect()->guest('/login');
        }

        $us_id= Auth::user()->id;
        $likes = DB::table('likes')->select('post_id')
            ->where('user_id', '=' , $us_id)
            ->where('like', '=',1)
            ->get();

        $ids = '0';
        foreach($likes as $like){
            $ids.=",{$like->post_id}";
        }
        
        $limit = config('app.limit_home');

        $total = DB::table('posts')->where('status','=', 1)->count();

        $posts = DB::select("SELECT
                                post.id,
                                post.title,
                                post.description,
                                post.price,
                                post.`condition`,
                                post.created_by,
                                post.rate,
                                (SELECT user_id FROM likes WHERE likes.post_id = post.id AND likes.user_id=? AND likes.`like`=1 LIMIT 1) AS like_user,
                                (SELECT TIMESTAMPDIFF(SECOND, post.created_at,  NOW())  % 60) as S,
                                (SELECT TIMESTAMPDIFF(MINUTE, post.created_at,  NOW())  % 60) as M,
                                (SELECT TIMESTAMPDIFF(HOUR, post.created_at,    NOW())  % 24) as H,
                                (SELECT TIMESTAMPDIFF(DAY,  post.created_at,    NOW())  % 30) as D,
                                (SELECT TIMESTAMPDIFF(MONTH, post.created_at,   NOW())  % 12) as MO,
                                (SELECT TIMESTAMPDIFF(YEAR, post.created_at,    NOW())) as Y,

                                (SELECT uuserid  from users u where u.id = post.created_by) as uuser_id,
                                (SELECT photo  from users u where u.id = post.created_by) as uuser_photo,
                                (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC  LIMIT 1) as image
                        FROM
                                posts post
                        WHERE
                                post.id IN ({$ids}) AND post.status = 1
                        ORDER BY created_at DESC
                        LIMIT ?", [$us_id, $limit]);

        $categories = DB::select("SELECT *FROM categories WHERE status=1 ORDER BY sequence ASC");

        $users = DB::select("
            SELECT 
                id, 
                uuserid,
                'name',
                photo,
                email,
                created_at,
                (SELECT TIMESTAMPDIFF(SECOND, users.lastlogin,  NOW())  % 60) AS S,
                (SELECT TIMESTAMPDIFF(MINUTE, users.lastlogin,  NOW())  % 60) AS M,
                (SELECT TIMESTAMPDIFF(HOUR, users.lastlogin,  NOW())  % 24) AS H,
                (SELECT TIMESTAMPDIFF(DAY, users.lastlogin,  NOW())  % 30) AS D,
                (SELECT TIMESTAMPDIFF(MONTH, users.lastlogin, NOW())  % 12)  AS MO,
                (SELECT TIMESTAMPDIFF(YEAR, users.lastlogin, NOW())) AS Y,
                (select COUNT(p.id) as products FROM posts p WHERE p.created_by=users.id AND p.status =1) as products
            FROM users wHERE active=1");

        
        return view('mobile.like', 
            [
            'users'=>$users,
            'posts' =>$posts
            ]
        );
    }

}