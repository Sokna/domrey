<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;
use Auth;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware(['auth','verified']);
        $this->middleware('auth');
        App::setlocale(isset($_SESSION['lang'])? $_SESSION['lang']:'en');
    }

    public function index($id = 0){
        
        //$categories = DB::table('categories')->orderBy('sequence')->get();
        $categories = DB::select("SELECT id, name,name_kh FROM categories WHERE parent_id IS NULL AND status=1 ORDER BY name");
        //$brands = App\Brand::all();
        //if ($request->isMethod('post')) {
        //     var_dump($request->all());
        //}
        $data = [
            'categories' => $categories,
            //'brands' => $brands
        ];

        if( $id > 0){
            $post = App\Post::findOrFail($id);
            if($post->created_by == Auth::user()->id){
                $post->deal_method =  json_decode($post->deal_method);
                $data['post'] = $post;
                $data['brand'] = App\Brand::find($post->brand_id);
                $data['images'] = DB::select("SELECT post_images.id, post_images.name FROM post_images WHERE post_images.post_id = :post_id ORDER BY post_images.cover DESC , post_images.id  ASC", ['post_id'=>$id]);
                $data['img_count'] = count($data['images']);
            }
        }

        $data['zones'] = $zones = DB::select("SELECT *FROM zones WHERE country_id = 36");
        // var_dump($data['images']);
        return view('posts.create', $data);
    }

    public function list($page = 1, $limit = 20){
        
        $total = DB::table('posts')->where('created_by', '=', Auth::user()->id)->count();
        $pages = ($total / $limit);

        if((int)$pages == $pages){
            $pages = (int)$pages;
        }else{
            $pages = (int)$pages + 1;
        }
        //Offset 
        $offset = ($page - 1) * $limit;

        $posts = DB::select("SELECT
                        post.*,
                        (SELECT `name` from post_images pimg where pimg.post_id = post.id ORDER BY pimg.cover DESC  LIMIT 1) as image,
                        (SELECT count(*) as total FROM reports WHERE post_id = post.id) as report
                    FROM
                        posts post
                    WHERE
                        post.status in(0,1,-2)
                        AND post.created_by = ? 
                    ORDER BY created_at DESC LIMIT ? OFFSET ?", 
                    [Auth::user()->id, $limit, $offset]);

        return view('posts.list', ['posts' => $posts , 'pages'=> $pages, 'limits'=>$limit, 'page'=>$page]);
    }


    public function listingReport($id = 0){

        $post = App\Post::findOrFail($id);

        $reports = [];

        if($post){
            
            $post->image =  DB::table('post_images')->where('post_id','=', $post->id)->orderBy('cover', 'desc')->first();

            $reports = DB::select("SELECT
                reports.code,
                reports.message,
                CONCAT(
                    -- DAYNAME(reports.created_at),', ',
                    DAY (reports.created_at),' ',
                    MONTHNAME(reports.created_at),', ',
                    YEAR (reports.created_at), ' ', 
                    HOUR(reports.created_at),':',
                    MINUTE(reports.created_at)
                ) AS dayname,
                (
                    SELECT
                        NAME
                    FROM
                        users
                    WHERE
                        users.id = reports.reported_by
                ) AS username
            FROM
                reports
            WHERE reports.post_id=?
            ORDER BY reports.created_at DESC", [$id]);

        }
        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile'] =='mobile.home')?'mobile.report':'posts.listing_report';
        return view($mobile, ['reports'=>$reports, 'post'=>  $post ]);

    }


    public function createNew($id = 0){
        
        $categories = DB::select("SELECT id, name,name_kh FROM categories WHERE parent_id IS NULL AND status=1 ORDER BY name");

        for($i=0; $i < count($categories); $i++){
            $sub = DB::select("SELECT id, name,name_kh FROM categories WHERE parent_id=? AND status=1 ORDER BY name",[$categories[$i]->id]);
            $categories[$i]->children = $sub;
        }

        $data = [
            'categories' => $categories,
        ];

        if( $id > 0){
            $post = App\Post::findOrFail($id);
            $post->parent_cat_name = App\Category::find($post->parent_category);
            $post->cat_name = App\Category::find($post->category_id);
            
            if($post->created_by == Auth::user()->id){
                $post->deal_method =  json_decode($post->deal_method);
                $data['post'] = $post;
                $data['brand'] = App\Brand::find($post->brand_id);
                $data['images'] = DB::select("SELECT post_images.id, post_images.name,post_images.cover FROM post_images WHERE post_images.post_id = :post_id ORDER BY  post_images.id  ASC", ['post_id'=>$id]);
                $data['img_count'] = count($data['images']);
                $data['isNew'] = 0;
            }
        }else{
            $data['img_count'] = 0;
            $data['isNew'] = 1;
        }

        $data['zones'] = $zones = DB::select("SELECT *FROM zones WHERE country_id = 36");
        
        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile'] =='mobile.home')?'mobile.new_sell':'posts.create1';

        return view($mobile, $data);
    }
    
}
