<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ChatUser;
use App\Chat;
use App;
use Auth;

class ProfileController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware(['auth','verified']);
        $this->middleware('auth');
        App::setlocale(isset($_SESSION['lang'])? $_SESSION['lang']:'en');
    }


    public function account(){
        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.account':'profile.profile';
        return view($mobile, ['user'=> Auth::user()]);
    }

    public function profile(){
        $address = App\Address::where('user_id','=', Auth::user()->id)->first();
        if($address){
            $address->payment_method =  json_decode($address->payment_method);
            $address->payment_account =  json_decode($address->payment_account);
            $address->shipping =  json_decode($address->shipping);
            $address->phones =  json_decode($address->phones);
        }

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.address':'profile.address';
        return view($mobile, [
                'address' => $address
            ]
        );
    }

    public function chat($id, $sender = 0, $offer = 0){

        $post = App\Post::find($id);

        $chatUser = DB::select('select * FROM chat_users WHERE post_id=? AND chated_by=? LIMIT 1', [$id, $sender]);
        
        $messages = [];
        
        //Had chat before
        if(count($chatUser) > 0) {
            $chatUser = $chatUser[0];
            $messages = DB::select("
                SELECT
                    chats.id,
                    chats.chat_user_id,
                    chats.message,
                    chats.sender,
                    chats.receiver,
                    chats.created_at,
                    chats.file,
                    chats.is_file,
                    chats.is_read,
                    DATE_FORMAT(chats.created_at, '%d %b %H:%i %p') AS sending_time,
                    (SELECT users.`name` FROM users WHERE users.id = chats.sender)  AS sender_name,
                    (SELECT users.`name` FROM users WHERE users.id = chats.receiver) AS receiver_name,
                    (SELECT users.photo FROM users WHERE users.id = chats.sender)    AS sender_photo,
                    (SELECT users.photo FROM users WHERE users.id = chats.receiver)  AS receiver_photo
                FROM
                    chats
                WHERE chats.chat_user_id = ?

                ORDER BY
                    chats.id ASC", [$chatUser->id]);
        }

        $post_images = DB::select("SELECT
                                `name`
                            FROM
                                post_images
                            WHERE
                                post_id = ?
                            ORDER BY id ASC", [$post->id]);
        $post_image = '';
        if(count($post_images)>0){
            $post_image = $post_images[0]->name;
        }

        //Update Chat Message
        foreach($messages as $m){
            if($m->sender != Auth::user()->id && $m->is_read == 0 ){
                $chat = App\Chat::find($m->id);
                $chat->is_read = 1;
                $chat->save();
            }
        }


        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.chat':'profile.chat';
        return view($mobile, 
        [
            'messages'=>$messages, 
            'chatUser' => $chatUser, 
            'post'=> $post,
            'post_image'=> $post_image,
            'chated_id' => $sender,
            'offer' => $offer
        ]);
    }


    public function settings(){

        return view('mobile.settings');
    }


    public function chatRoom(){

        $chat_rooms = DB::select("SELECT
                            chat_users.post_id AS id,
                            (SELECT title  FROM posts  WHERE posts.id = chat_users.post_id)    AS title,
                            (SELECT price  FROM posts  WHERE posts.id = chat_users.post_id)    AS price,
                            (SELECT `name` FROM post_images WHERE post_images.post_id = chat_users.post_id ORDER BY post_images.post_id ASC LIMIT 1) AS image,
                            (SELECT `name` FROM users  WHERE users.id = chat_users.chated_by) AS name,
                            (SELECT photo  FROM users  WHERE users.id = chat_users.chated_by)  AS photo,
                            (SELECT `name` FROM users  WHERE users.id = chat_users.owner_id)   AS name_owner,
		                    (SELECT photo  FROM users  WHERE users.id = chat_users.owner_id)   AS photo_owner,
                            (SELECT message FROM chats WHERE chats.chat_user_id = chat_users.id ORDER BY chats.id DESC LIMIT 1) AS message,
                            (SELECT chats.created_at FROM chats WHERE chats.chat_user_id = chat_users.id ORDER BY chats.id DESC LIMIT 1) AS send_date,
                            DATE_FORMAT((SELECT chats.created_at FROM chats WHERE chats.chat_user_id = chat_users.id ORDER BY chats.id DESC LIMIT 1), '%d %b %H:%i %p') AS sending_time,
                            (SELECT count(*) FROM chats WHERE chats.chat_user_id = chat_users.id AND chats.is_read = 0 AND chats.sender != ?) AS unread,
                            chat_users.id AS room_id,
                            chat_users.chated_by,
                            chat_users.owner_id,
                            chat_users.created_at
                    FROM
                            chat_users
                    WHERE 
                            chat_users.owner_id = ? OR chat_users.chated_by = ?
                    ORDER BY send_date DESC  ", [Auth::user()->id, Auth::user()->id, Auth::user()->id]);

        $mobile = (isset($_SESSION['mobile']) && $_SESSION['mobile']=='mobile.home')? 'mobile.chat_room':'profile.chat_room';
        return view($mobile, ['chat_rooms' =>$chat_rooms]);
    }

}