<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\LastLogin;
use App\User;
use App\Address;
use Auth;
use App;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth','verified']);
        $this->middleware('guest')->except('logout');
         App::setlocale(isset($_SESSION['lang'])? $_SESSION['lang']:'en');
       
    }

    // check if authenticated, then redirect to dashboard
    protected function authenticated() {
        if (Auth::check()) {

            $last = new LastLogin();
            $last->to_do = 'Login';
            $last->user_id = Auth::user()->id;

            if($last->save()){
                $log = User::firstOrCreate(['id' => Auth::user()->id]);
                $log->lastlogin = $last->created_at;
                $log->save();
            }
        
            if(Auth::user()->active == 1){
                return redirect()->intended('/home');
            }else{
                Auth::logout();
                return redirect()->route('home');
            }
        }
    }

    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL) ? $this->username(): 'uuserid';
        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }



    public function redirectToFacebook() {
        return Socialite::driver('facebook')->redirect();
    }
    
    public function handleFacebookCallback() {
        try {
            $user = Socialite::driver('facebook')->user();
            $finduser = User::where('facebook_id', $user->id)->first();
            if ($finduser) {

                Auth::login($finduser);

                //Save log
                $last = new LastLogin();
                $last->to_do = 'Login';
                $last->user_id = Auth::user()->id;

                if($last->save()){
                    $log = User::firstOrCreate(['id' => Auth::user()->id]);
                    $log->lastlogin = $last->created_at;
                    $log->save();
                }


                return redirect('/home');
            } else {

                $userids = explode(" ", $user->name);
                $id = '';
                
                foreach($userids as $u){
                    $id.= preg_replace('/[^a-zA-Z0-9_ -]/s','', strtolower(trim($u))); 
                }

                if($id == ''){
                    $id = $user->id;
                }
                
                $data = [
                    'name' => $user->name,
                    'uuserid'=> $id,
                    'photo'=> 'avata.png',
                    'email' => $user->email,
                    'facebook_id' => $user->id
                ];

                if($user->email){
                    $data['email_verified_at'] = now();
                }

                $newUser = User::create($data);

                  //Add default address
                $first_name = '';
                $last_name = '';
                $address = new Address;
                $address->first_name = $first_name;
                $address->last_name = $last_name;
                $address->email = $user->email;
                $address->user_id = $newUser->id;
                $address->save();

                Auth::login($newUser);

                return redirect('/home');
            }
        }
        catch(Exception $e) {
            return redirect('auth/facebook');
        }
    }
}