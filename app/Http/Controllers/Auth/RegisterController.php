<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App;
use App\Address;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        App::setlocale(isset($_SESSION['lang'])? $_SESSION['lang']:'en');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string','min:4', 'max:255', 'unique:users'],
            //'email' => ['max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'string','max:15', 'unique:users'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // var_dump($data);
        // return;
        // $userids = explode(" ", $data['name']);
        // $id = '';
        // foreach($userids as $u){
        //    $id.= preg_replace('/[^a-zA-Z0-9_ -]/s','', strtolower(trim($u))); 
        // }

        // $findUser = DB::table('users')->where('uuserid','=', $id)->count();
        
        // if($findUser > 0){
        //     $id .= date("s");
        // }
        
        $phone = $data['phone'];

        if(substr($phone, 0 , 1) > 0){
            $phone = '0'.$phone;
        }

        $user = User::create([
            'name' => strtolower($data['name']),
            'photo' => 'avata.png',
            'uuserid' => strtolower($data['name']),
            'phone' => $phone,
            'email' => strtolower($data['email']),
            'password' => Hash::make($data['password']),
        ]);


        //Add default address
        $first_name = '';
        $last_name = '';
        // if(count($userids) > 1){
        //     $first_name = $userids[0];
        //     $last_name = $userids[1];
        // }
        $address = new Address;
        $address->first_name = $first_name;
        $address->last_name = $last_name;
        $address->email = $data['email'];
        $address->user_id = $user->id;
        $address->save();

        return $user;
    }
}
