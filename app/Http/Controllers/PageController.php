<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App;
use App\Contact;

class PageController extends Controller
{

    public function __construct(){
        App::setlocale(isset($_SESSION['lang'])? $_SESSION['lang']:'en');
    }


    public function contact(){

        return view('pages.contact', ['id'=> 0]);
    }


    public function store(Request $request){

        //var_dump($request->all());

        $request->validate([
            'name' => 'required|min:2',
            'email' => 'required|email:rfc',
            'description' => 'required|min:10',
        ]);

        $contact = new Contact;

        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->description = $request->description;

        $id = 0;

        if($contact->save()){
            $id = $contact->id;
        }
        return view('pages.contact', ['id'=>$id]);
    }


    public function advertise(){

        return view('pages.advertise');
    }


}
