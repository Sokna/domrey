<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // if($this->app->environment('production')){
        //     \URL::forceScheme('https');
        // }
        session_start();
        View::share('mega_menu', $this->mega_menu());
        View::share('lang',  isset($_SESSION['lang'])? $_SESSION['lang']:'en');
        
       
        $useragent =isset($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:'';
        $iPod = strpos($useragent, "iPod");
        $iPhone = strpos($useragent, "iPhone");
        $iPad = strpos($useragent, "iPad");
        $android = strpos($useragent, "Android");

       if($iPad || $iPhone || $iPod || $android){
            $_SESSION['mobile']= 'mobile.home';
        }else{
            $_SESSION['mobile']= 'home';
        }
        

        View::share('mobile',  isset($_SESSION['mobile'])? $_SESSION['mobile']:'home');

        // View::share('un_read', 4);
        view()->composer('*', function($view){
            if (Auth::check()) {
                $view->with('un_read', $this->getUnread(Auth::user()->id));
            }else {
                $view->with('un_read', 0);
            }
        });
    }

    public function getChildrenMenus($id){

        return DB::select("SELECT *FROM categories WHERE status=1 AND parent_id=?",[$id]);
    }

    public function mega_menu(){

        $mega_menu =[
            [
                'name'=>'Cars & Property',
                'name_kh'=>'រថយន្តនិងទ្រព្យសម្បត្តិ',
                'icon'=>'directions_car',
                'col' => 3,
                'children'=>[
                    [
                        'id'=> 2,
                        'name'=>'Cars',
                        'name_kh'=>'រថយន្ត',
                        'children' => $this->getChildrenMenus(2)
                    ],
                    [
                        'id'=> 14,
                        'name'=>'Cars Accessories',
                        'name_kh'=>'គ្រឿងបន្លាស់រថយន្ត',
                        'children' => $this->getChildrenMenus(14)
                    ],
                    [
                        'id'=> 4,
                        'name'=>'Properties',
                        'name_kh'=>'លក្ខណៈសម្បត្តិ',
                        'children' => $this->getChildrenMenus(4)
                    ],
                    [
                        'id'=> 16,
                        'name' => 'Motorbikes',
                        'name_kh'=>'ម៉ូតូ',
                        'children' => $this->getChildrenMenus(16)
                    ]
                ]
            ],
            [
                'name'=>'Fashion',
                'name_kh'=>'សម្រស់',
                'icon'=>'filter_vintage',
                'col' => 3,
                'children'=>[
                    [
                        'id'=> 13,
                        'name'=>"Men's Fashion",
                        'name_kh'=>'សម្រស់បុរស',
                        'children' => $this->getChildrenMenus(13)
                    ],
                    [
                        'id'=> 7,
                        'name'=>"Women's Fashion",
                        'name_kh'=>'សម្រស់នារី',
                        'children' => $this->getChildrenMenus(7)
                    ],
                    [
                        'id'=> 8,
                        'name'=>'Healty & Beauty',
                        'name_kh'=>'សុខភាព​និង​សម្រស់',
                        'children' => $this->getChildrenMenus(8)
                    ],
                    [
                        'id'=> 9,
                        'name' => 'Luxury',
                        'name_kh'=>'ប្រណីត',
                        'children' => $this->getChildrenMenus(9)
                    ]
                ]
            ],
            [
                'name'=>'Home & Living',
                'name_kh'=>'គេហដ្ឋាននិងការរស់នៅ',
                'icon'=>'home',
                'col' => 3,
                'children'=>[
                    [
                        'id'=> 10,
                        'name'=>"Furniture",
                        'name_kh'=>'គ្រឿងសង្ហារឹម',
                        'children' => $this->getChildrenMenus(10)
                    ],
                    [
                        'id'=> 29,
                        'name'=>"Gardening",
                        'name_kh'=>'ថែសួន',
                        'children' => $this->getChildrenMenus(29)
                    ],
                    [
                        'id'=> 11,
                        'name'=>'Home Appliances',
                        'name_kh'=>'គ្រឿងប្រើប្រាស់ក្នុងផ្ទះ',
                        'children' => $this->getChildrenMenus(11)
                    ],
                    [
                        'id'=> 27,
                        'name' => 'Assistive Devices',
                        'name_kh'=>'ឧបករណ៍ជំនួយ',
                        'children' => $this->getChildrenMenus(27)
                    ],
                    [
                        'id'=> 27,
                        'name' => 'Babies Kids',
                        'name_kh'=>'ទារកក្មេង',
                        'children' => $this->getChildrenMenus(12)
                    ],
                     [
                        'id'=> 22,
                        'name' => 'Books & Stationery',
                        'name_kh'=>'សៀវភៅនិងសម្ភារៈការិយាល័យ',
                        'children' => $this->getChildrenMenus(22)
                    ]
                ]
            ],
            [
                'name'=>'Mobiles & Electronics',
                'name_kh'=>'ទូរស័ព្ទនិងគ្រឿងអេឡិចត្រូនិច',
                'icon'=>'phone_android',
                'col' => 6,
                'children'=>[
                    [
                        'id'=> 5,
                        'name'=>"Electronics",
                        'name_kh'=>'អេឡិចត្រូនិច',
                        'children' => $this->getChildrenMenus(5)
                    ],
                    [
                        'id'=> 6,
                        'name'=>"Mobile Phone & Tablate",
                        'name_kh'=>'ទូរស័ព្ទនិងថេប្លេត',
                        'children' => $this->getChildrenMenus(6)
                    ]
                ]
            ],
            [
                'name'=>'Hobbies & Games',
                'name_kh'=>'ចំណូលចិត្តនិងហ្គេម',
                'icon'=>'games',
                'col' => 3,
                'children'=>[
                    [
                        'id'=> 21,
                        'name'=>"Learning & Enrichment",
                        'name_kh'=>'ការរៀនសូត្រនិងការលើកកម្ពស់',
                        'children' => $this->getChildrenMenus(21)
                    ],
                    [
                        'id'=> 30,
                        'name'=>"Food & Drinks",
                        'name_kh'=>'អាហារនិងភេសជ្ជៈ',
                        'children' => $this->getChildrenMenus(30)
                    ],
                    [
                        'id'=> 19,
                        'name'=>"Photography",
                        'name_kh'=>'ការថតរូប',
                        'children' => $this->getChildrenMenus(19)
                    ],
                     [
                        'id'=> 37,
                        'name'=>"Travel",
                        'name_kh'=>'ធ្វើដំណើរ',
                        'children' => $this->getChildrenMenus(37)
                     ],
                     [
                        'id'=> 25,
                        'name'=>"Music & Media",
                        'name_kh'=>'តន្ត្រីនិងប្រព័ន្ធផ្សព្វផ្សាយ',
                        'children' => $this->getChildrenMenus(25)
                     ],
                     [
                        'id'=> 20,
                        'name'=>"Sport",
                        'name_kh'=>'កីឡា',
                        'children' => $this->getChildrenMenus(20)
                     ],
                     [
                        'id'=> 26,
                        'name'=>"Pet Supplies",
                        'name_kh'=>'ការផ្គត់ផ្គង់សត្វចិញ្ចឹម',
                        'children' => $this->getChildrenMenus(26)
                     ],
                     [
                        'id'=> 17,
                        'name'=>"Toys & Games",
                        'name_kh'=>'ប្រដាប់ប្រដាក្មេងលេងនិងហ្គេម',
                        'children' => $this->getChildrenMenus(17)
                     ],
                     [
                        'id'=> 18,
                        'name'=>"Entertainment",
                        'name_kh'=>'ការកំសាន្ត',
                        'children' => $this->getChildrenMenus(18)
                     ],
                     [
                        'id'=> 23,
                        'name'=>"Design & Craft",
                        'name_kh'=>'រចនានិងសិប្បកម្ម',
                        'children' => $this->getChildrenMenus(23)
                     ],
                     [
                        'id'=> 28,
                        'name'=>"Vintage & Collectibles",
                        'name_kh'=>'រសជាតិនិងវត្ថុបុរាណ',
                        'children' => $this->getChildrenMenus(28)
                     ],
                     [
                        'id'=> 15,
                        'name'=>"Bicycles & PMDs",
                        'name_kh'=>'កង់ និង ចល័តផ្ទាល់ខ្លួន',
                        'children' => $this->getChildrenMenus(15)
                     ],
                ]   
            ],
            [
                'name'=>'Jobs',
                'name_kh'=>'ការងារ',
                'icon'=>'work',
                'col' => 3,
                'children'=>[
                    [
                        'id'=> 32,
                        'name'=>"Jobs",
                        'name_kh'=>'ការងារ',
                        'children' => $this->getChildrenMenus(32)
                    ]
                ]
            ],
            [
                'name'=>'Services',
                'name_kh'=>'សេវាកម្ម',
                'icon'=>'local_laundry_service',
                'col' => 3,
                'children'=>[
                    [
                        'id'=> 24,
                        'name'=>"Lifestyle Services",
                        'name_kh'=>'សេវាកម្មរបៀបរស់នៅ',
                        'children' => $this->getChildrenMenus(24)
                    ],
                    [
                        'id'=> 31,
                        'name'=>'Business Services',
                        'name_kh'=>'សេវាកម្មអាជីវកម្ម',
                        'children' => $this->getChildrenMenus(31)
                    ],
                    [
                        'id'=>3,
                        'name'=>'Home Services',
                        'name_kh'=>'សេវាកម្មគេហដ្ឋាន',
                        'children' => $this->getChildrenMenus(3)
                    ]
                ]
            ],
            [
                'name'=>'Other',
                'name_kh'=>'ផ្សេងៗ',
                'icon'=>'stars',
                'col' => 3,
                'children'=>[
                    [
                        'id'=> 33,
                        'name'=>"Finance",
                        'name_kh'=>'ហិរញ្ញវត្ថុ',
                        'children' => $this->getChildrenMenus(33)
                    ],
                    [
                        'id'=> 34,
                        'name'=>"Buletin Board",
                        'name_kh'=>'ព្រឹត្តិប័ត្រព័ត៌មាន',
                        'children' => $this->getChildrenMenus(34)
                    ],
                    [
                        'id'=> 36,
                        'name'=>'Everything Else',
                        'name_kh'=>'អ្វីៗផ្សេងទៀត',
                        'children' => $this->getChildrenMenus(36)
                    ]
                ]
            ],

        ];

        return $mega_menu;
    }


    public function getUnread($id){
        $db = DB::select("SELECT
			    SUM((SELECT count(*) FROM chats WHERE chats.chat_user_id = chat_users.id AND chats.is_read = 0 AND chats.sender != ?)) AS unread
            FROM
			    chat_users
            WHERE chat_users.owner_id = ? OR chat_users.chated_by = ? ",[$id, $id, $id]);
        $re = 0;
        if(count($db)>0){
            $re = $db[0]->unread;
        }
        return $re;
    }
}
