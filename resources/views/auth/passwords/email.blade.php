
    @extends(($mobile=='mobile.home')?'layouts.appMobile':'layouts.app')
    @section('title', 'Reset Password')
    @section('content')
        
        @if($mobile=='mobile.home')
            <div class="item">
                <!-- header -->
                <div class="row">
                    <div class="col-12 row-shadow" id="search-row">
                        <div class="back">
                            <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
                        </div>
                        <div  id="custom-search-input" class="search-row-setting">
                            <h5>@lang('home.reset_password')</h5>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email"
                                    class="col-12 col-form-label text-md-right">@lang('home.email')</label>

                                <div class="col-12">
                                    <input id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-domrey col-12">
                                        @lang('home.send_email')
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        @else
            <div class="container domrey-container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">

                                <div class="card">
                                    <div class="card-header">@lang('home.reset_password')</div>

                                    <div class="card-body">
                                        @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                        @endif

                                        <form method="POST" action="{{ route('password.email') }}">
                                            @csrf

                                            <div class="form-group row">
                                                <label for="email"
                                                    class="col-md-4 col-form-label text-md-right">@lang('home.email')</label>

                                                <div class="col-md-5">
                                                    <input id="email" type="email"
                                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                                        value="{{ old('email') }}" required autocomplete="email" autofocus>

                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-domrey">
                                                        @lang('home.send_email')
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

            
                        </div>
                    </div>

                </div>
            </div>
        @endif
    @endsection