@extends(($mobile=='mobile.home')?'layouts.appMobile':'layouts.app')
@section('title', 'Register')
@section('content')
@if($mobile=='mobile.home')

<div class="item">
    <!-- header -->
    <div class="row">
        <div class="col-12 row-shadow" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>
            <div id="custom-search-input" class="search-row-setting">
                <h5>@lang('home.sign_up')</h5>
            </div>
        </div>
    </div>
    <!-- body -->
    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-12">@lang('home.name') <span
                            class="text-danger">*</span></label>

                    <div class="col-12">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            name="name" value="{{ old('name') }}" required autocomplete="off" autofocus>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <span class="invalid-feedback" id="username_invalid" role="alert"></span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-12">@lang('home.email')</label>

                    <div class="col-12">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}"/>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <span class="invalid-feedback" id="email_invalid" role="alert"></span>
                    </div>
                </div>



                <div class="form-group row">
                    <label for="password" class="col-12">@lang('home.password') <span
                            class="text-danger">*</span></label>

                    <div class="col-12">
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <span class="invalid-feedback" role="alert">
                            <strong>Password at least 6 characters.</strong>
                        </span>

                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm"
                        class="col-12">@lang('home.confirm_password') <span
                            class="text-danger">*</span></label>

                    <div class="col-12">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            required autocomplete="new-password" />
                        <span class="invalid-feedback" role="alert" id="conform_password_invalid">
                            <strong>Password at least 6 characters.</strong>
                        </span>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="phone" class="col-12">@lang('home.mobile_phone') <span
                            class="text-danger">*</span></label>
                    <!-- <div class="col-md-4">
                                    <input type="text" id="phone_number" name="phone_number"  class="form-control" alue="{{ old('phone') }}" placeholder="+855 ********"/>
                                    <div id="recaptcha-container"></div>
                                    
                                    <input type="hidden" id="phone" name="phone" value="" class="form-control"/>

                                </div> -->
                    <div class="col-12">
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text"><img src="{{asset('images/cambodia-40px.png')}}"
                                        style="width: 24px;height: 24px;" />&nbsp;+855</div>
                            </div>
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone"
                                name="phone" value="{{ old('phone') }}" required maxlength="10" />
                            <div class="input-group-prepend"><button type="button" class="btn btn-domrey"
                                    id="btn-verifycode" onclick="phoneAuth();" disabled>@lang('home.verify')</button>
                            </div>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <span class="invalid-feedback" role="alert">
                                <strong>The phone has already been taken.</strong>
                            </span>
                            <div id="recaptcha-container"></div>
                        </div>

                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-12">
                        <button type="submit" class="btn btn-domrey col-12" id="btn-submit" disabled>
                            @lang('home.sign_up')
                        </button>
                    </div>
                    <div class="col-12 mt-3">
                        @lang('home.have_account') <a href="{{url('/login')}}"
                            class="domrey-link">@lang('home.login_here')</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>


@else

<div class="container domrey-container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header domrey-back" style="color:black;"><strong>@lang('home.sign_up')</strong></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">@lang('home.name') <span
                                    class="text-danger">*</span></label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="off" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="invalid-feedback" id="username_invalid" role="alert"></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">@lang('home.email')</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" />

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="invalid-feedback" id="email_invalid" role="alert"></span>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">@lang('home.password')
                                <span class="text-danger">*</span></label>

                            <div class="col-md-6">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <span class="invalid-feedback" role="alert">
                                    <strong>Password at least 6 characters.</strong>
                                </span>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm"
                                class="col-md-4 col-form-label text-md-right">@lang('home.confirm_password') <span
                                    class="text-danger">*</span></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password" />
                                <span class="invalid-feedback" role="alert" id="conform_password_invalid">
                                    <strong>Password at least 6 characters.</strong>
                                </span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">@lang('home.mobile_phone')
                                <span class="text-danger">*</span></label>
                            <!-- <div class="col-md-4">
                                    <input type="text" id="phone_number" name="phone_number"  class="form-control" alue="{{ old('phone') }}" placeholder="+855 ********"/>
                                    <div id="recaptcha-container"></div>
                                    
                                    <input type="hidden" id="phone" name="phone" value="" class="form-control"/>

                                </div> -->
                            <div class="col-md-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><img src="{{asset('images/cambodia-40px.png')}}"
                                                style="width: 24px;height: 24px;" />&nbsp;+855</div>
                                    </div>
                                    <input type="text" class="form-control @error('phone') is-invalid @enderror"
                                        id="phone" name="phone" value="{{ old('phone') }}" required maxlength="10" />
                                    <div class="input-group-prepend"><button type="button" class="btn btn-domrey"
                                            id="btn-verifycode" onclick="phoneAuth();"
                                            disabled>@lang('home.verify')</button></div>
                                    @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                    <span class="invalid-feedback" role="alert">
                                        <strong>The phone has already been taken.</strong>
                                    </span>
                                    <div id="recaptcha-container"></div>
                                </div>

                            </div>
                            <!-- <div class="col-md-2">
                                    <button type="button" class="btn btn-domrey" id="btn-verifycode" onclick="phoneAuth();" disabled>@lang('home.verify')</button>
                                </div> -->
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-domrey col-sm-12 col-md-3" id="btn-submit"
                                    disabled>
                                    @lang('home.sign_up')
                                </button>
                            </div>
                            <div class="col-md-12 mt-4 offset-md-8 text-ceter">
                                @lang('home.have_account') <a href="{{url('/login')}}"
                                    class="domrey-link">@lang('home.login_here')</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endif

<!-- Modal -->
<div class="modal fade" id="verifycodecenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">{{ __('Verify Code') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row" id="model_body_box">
                    <label for="verify_code"
                        class="col-md-4 col-form-label text-md-right">{{ __('Verify Code') }}</label>
                    <div class="col-md-6">
                        <input id="phone_code" placeholder="Enter your 6 digits" maxlength="6" type="text"
                            class="form-control" name="phone_code" />
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-6 pt-2">@lang('home.wait_code')</div>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-domrey" id="btn-sendcode" onclick="codeverify();">Verify</button>
            </div>
        </div>
    </div>
</div>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/6.0.2/firebase.js"></script>

<script>
var isVerified = false;
var isName = false;
var isEmail = true;
var isPassword = false;
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAyOXUiB0criUQUKC4-1UXCJDJ__MFvvJQ",
    authDomain: "domrey-e5bf7.firebaseapp.com",
    databaseURL: "https://domrey-e5bf7.firebaseio.com",
    projectId: "domrey-e5bf7",
    storageBucket: "domrey-e5bf7.appspot.com",
    messagingSenderId: "688878094980",
    appId: "1:688878094980:web:9f5f287b469375f17510d1",
    measurementId: "G-KF7K5XFRWQ"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


window.onload = function() {
    render();
};

function render() {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
        size: "invisible",
        callback: function(response) {}
    });
    recaptchaVerifier.render();
}

function phoneAuth() {
    //get the number
    var number = document.getElementById('phone').value;

    if (number == '') {
        alert("Please enter your phone number");
        return false;
    }
    $("#btn-verifycode").html("Sending...");

    //phone number authentication function of firebase
    //it takes two parameter first one is number,,,second one is recaptcha
    firebase.auth().signInWithPhoneNumber('+855' + number, window.recaptchaVerifier).then(function(confirmationResult) {
        //s is in lowercase
        window.confirmationResult = confirmationResult;

        coderesult = confirmationResult;

        //console.log(coderesult);
        setTimeout(function() {
            //canSubmit = true;
            $("#btn-verifycode").html("Send");
            $('#verifycodecenter').modal({});

        }, 2000);

        //$("#phone").val(number);
    }).catch(function(error) {
        $("#btn-verifycode").html("Send");
        alert(error.message);
    });
}

function codeverify() {
    var code = document.getElementById('phone_code').value;
    $("#btn-sendcode").html("Verifying");
    coderesult.confirm(code).then(function(result) {

        isVerified = true;
        $('#phone').addClass('is-valid');
        if (isEmail && isPassword && isName) {
            $("#btn-submit").removeAttr("disabled");
        }

        var user = result.user;
        //console.log(user);
        $("#btn-sendcode").html("Verify");
        $("#phone").attr('readonly', true);
        $("#btn-verifycode").attr('disabled', true);
        $("#btn-sendcode").addClass("d-none");

        $("#verifycodecenter").modal('hide');
        //$("#exampleModalLongTitle").html("Success");
        //$("#model_body_box").html(`<div class="col-12">Successfully verified</div>`);

    }).catch(function(error) {
        $("#btn-sendcode").html("Verify");
        isVerified = false;
        alert(error.message);
    });
}


function validateForm() {
    var nameRegex = /^[a-zA-Z0-9\-\_\.]+$/;
    var text = $('#name').val();
    var validUsername = text.match(nameRegex);
    var errorMsg =
        `<strong>Username may contain only letters (a-z), numbers (0-9), dashes (-), underscores (_), periods (.) and no more than one period (.) in a row</strong>`;
    if (validUsername == null) {
        $('#username_invalid').html(errorMsg);
        $('#name').addClass('is-invalid');
        isName = false;
    } else if (text.indexOf("..") > 0) {
        $('#username_invalid').html(errorMsg);
        $('#name').addClass('is-invalid');
        isName = false;
    } else {
        //$('#username_invalid').html('');
        //console.log(text);
        if (text.length <= 4){
            $('#username_invalid').html('<strong>The username at least 4 characters.</strong>');
            $('#name').addClass('is-invalid');
            isName = false;
            return false;
        }

        findUserByName();

    }

    // if(canSubmit && verified){
    //      $("#btn-submit").removeAttr("disabled");
    // }else{
    //      $("#btn-submit").attr("disabled",true);
    // }
}

$('#name').keyup(function(ent) {
    validateForm();
});
$('#name').change(function(ent) {
    validateForm();
});


$('#phone').keyup(function(ent) {
    if ($(this).val().length >= 8 && $(this).val().length <= 10) {
        findUserByPhone();
    }
});

var checkPasswordLen = function(that) {
    if ($(that).val().length >= 6) {
        $(that).addClass('is-valid');
        $(that).removeClass('is-invalid');
    } else {
        $(that).addClass('is-invalid');
        $(that).removeClass('is-valid');
    }

    return $(that).val();
}

$('#password').keyup(function(ent) {
    var isM = checkPasswordLen(this);

    if (isM != $('#password-confirm').val()) {
        $('#password-confirm').addClass('is-invalid');
        $('#password-confirm').removeClass('is-valid');
        $('#conform_password_invalid').html('<strong>The password confirmation does not match.</strong>');
        isPassword = false;
    } else {
        $('#password-confirm').addClass('is-valid');
        $('#password-confirm').removeClass('is-invalid');
        isPassword = true;
    }


    if (isEmail && isName && isPassword && isVerified) {
        $("#btn-submit").removeAttr("disabled");
    } else {
        $("#btn-submit").attr("disabled", true);

    }
});

$('#password-confirm').keyup(function(ent) {
    var isM = checkPasswordLen(this);

    if (isM != $('#password').val()) {
        $(this).addClass('is-invalid');
        $(this).removeClass('is-valid');
        $('#conform_password_invalid').html('<strong>The password confirmation does not match.</strong>');
        isPassword = false;
    } else {
        $(this).addClass('is-valid');
        $(this).removeClass('is-invalid');
        isPassword = true;
    }


    if (isEmail && isName && isPassword && isVerified) {
        $("#btn-submit").removeAttr("disabled");
    } else {
        $("#btn-submit").attr("disabled", true);

    }

});

$('#email').keyup(function(ent) {
    if(ValidateEmail($(this).val())){
        findUserByEmail();
    }
});


var ValidateEmail = function(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)){
        $('#email').addClass('is-valid');
        $('#email').removeClass('is-invalid');
        isEmail = true;
        return (true)
    }
    if(mail.length==0){
        isEmail = true;
        return (true);
    }

    $('#email_invalid').html('<strong>You have entered an invalid email address!.</strong>');
    $('#email').addClass('is-invalid');
    $('#email').removeClass('is-valid');
    isEmail = false;
    return (false)
}

var findUserByEmail = function() {
    var formData = new FormData();
    formData.append('email', $('#email').val());
    $.ajax({
        url: '/api/account/findUser',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {

        },
        complete: function() {

        },
        success: function(json) {
            if (json.email) {
                $('#email_invalid').html('<strong>The email has already been taken.</strong>');
                $('#email').addClass('is-invalid');
                $('#email').removeClass('is-valid');
                isEmail = false;
            } else {
                $('#email').addClass('is-valid');
                $('#email').removeClass('is-invalid');
                isEmail = true;
            }

            if (isEmail && isName && isPassword && isVerified) {
                $("#btn-submit").removeAttr("disabled");
            } else {
                $("#btn-submit").attr("disabled", true);

            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

var findUserByName = function() {
    var formData = new FormData();
    formData.append('name', $('#name').val());
    $.ajax({
        url: '/api/account/findUser',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {

        },
        complete: function() {

        },
        success: function(json) {
            if (json.name) {
                $('#username_invalid').html('<strong>The username has already been taken.</strong>');
                $('#name').addClass('is-invalid');
                $('#name').removeClass('is-valid');
                isName = false;
            } else {
                $('#name').addClass('is-valid');
                $('#name').removeClass('is-invalid');
                isName = true;
            }

            if (isEmail && isName && isPassword && isVerified) {
                $("#btn-submit").removeAttr("disabled");
            } else {
                $("#btn-submit").attr("disabled", true);

            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};

var findUserByPhone = function() {
    var formData = new FormData();
    formData.append('phone', $('#phone').val());
    $.ajax({
        url: '/api/account/findUser',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function() {

        },
        complete: function() {

        },
        success: function(json) {
            if (json.phone) {
                $('#phone').addClass('is-invalid');
                $("#btn-verifycode").attr("disabled", true);
            } else {
                $('#phone').removeClass('is-invalid');
                $("#btn-verifycode").removeAttr("disabled");
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
};
</script>
@endsection