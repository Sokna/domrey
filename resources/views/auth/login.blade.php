@extends(($mobile=='mobile.home')?'layouts.appMobile':'layouts.app')
@section('title', 'Login')
@section('content')
    
    <link href="{{asset('css/bootstrap-social/bootstrap-social.css')}}" rel="stylesheet" >
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    @if($mobile=='mobile.home')

         <div class="item">
                <!-- header -->
                <div class="row">
                    <div class="col-12 row-shadow" id="search-row">
                        <div class="back">
                            <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
                        </div>
                        <div  id="custom-search-input" class="search-row-setting">
                            <h5>@lang('home.login')</h5>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-12 col-form-label text-md-right">@lang('home.user_email') <span class="text-danger">*</span></label>

                                    <div class="col-12">
                                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-12 col-form-label text-md-right">@lang('home.password') <span class="text-danger">*</span></label>

                                    <div class="col-12">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-12">
                                        <!-- <div class="form-check"> -->
                                            <!-- <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="remember">
                                                @lang('home.remember')
                                            </label> -->
                                            @if (Route::has('password.request'))
                                                <a  href="{{ route('password.request') }}" class="domrey-link">
                                                @lang('home.forgot_password')
                                                </a>
                                            @endif

                                        <!-- </div> -->
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-domrey col-12">
                                        @lang('home.login')
                                        </button>
                                    </div>
                                    <div class="col-12 mt-4 text-left">
                                    @lang('home.no_account')  <a href="{{url('/register')}}" class="domrey-link"> @lang('home.register_here')</a>
                                    </div>
                                </div>
                                <div class="form-group row mb-0 mt-3">
                                    <div class="col-md-12 offset-md-4">
                                        <a href="https://supervong.com/auth/facebook" class="btn btn-block btn-social btn-facebook col-sm-12 col-md-3" style="color: #fff!important;">
                                            <span class="fa fa-facebook"></span> Login with facebook
                                        </a>
                                    </div>
                                </div>
                            </form>

                    </div>
                </div>
        </div>
    @else
        <div class="container domrey-container">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="card">
                        <div class="card-header domrey-back" style="color:black;"><strong>@lang('home.login')</strong></div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">@lang('home.user_email') <span class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="off" autofocus>

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">@lang('home.password') <span class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-8 offset-md-4">
                                        <!-- <div class="form-check"> -->
                                            <!-- <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                            <label class="form-check-label" for="remember">
                                                @lang('home.remember')
                                            </label> -->
                                            @if (Route::has('password.request'))
                                                <a  href="{{ route('password.request') }}" class="domrey-link">
                                                @lang('home.forgot_password')
                                                </a>
                                            @endif

                                        <!-- </div> -->
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-12 offset-md-4">
                                        <button type="submit" class="btn btn-domrey col-sm-12 col-md-3">
                                        @lang('home.login')
                                        </button>
                                    </div>
                                    <div class="col-md-12 offset-md-4 mt-4 text-left">
                                    @lang('home.no_account')  <a href="{{url('/register')}}" class="domrey-link"> @lang('home.register_here')</a>
                                    </div>
                                </div>
                                <div class="form-group row mb-0 mt-3">
                                    <div class="col-md-12 offset-md-4">
                                        <a href="https://supervong.com/auth/facebook" class="btn btn-block btn-social btn-facebook col-sm-12 col-md-3">
                                            <span class="fa fa-facebook"></span> Login with facebook
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endif
@endsection
