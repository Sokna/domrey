@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5> @lang('home.change_password') </h5></div>

                <div class="card-body">
                    <form method="POST" action="{{ url('password/change') }}">
                        @csrf

                        <!-- <div class="form-group row">
                            <div class="col-12">
                                @foreach ($errors->all() as $key =>$error)
                                <p class="text-danger">{{ $error }}</p>
                                @endforeach
                            </div>
                        </div> -->

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">@lang('home.current_password') <span
                                    class="text-danger">*</span></label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password"
                                    value="{{ old('current_password') }}"/>

                                @error('current_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">@lang('home.new_password')  <span
                                    class="text-danger">*</span></label>

                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password"
                                    value="{{ old('new_password') }}" required/>

                                @error('new_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">@lang('home.confirm_password')  <span
                                    class="text-danger">*</span></label>

                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control @error('new_confirm_password') is-invalid @enderror"
                                    name="new_confirm_password" value="{{ old('new_confirm_password') }}" required/>
                                
                                @error('new_confirm_password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror


                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-domrey">
                                    @lang('home.change_password')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection