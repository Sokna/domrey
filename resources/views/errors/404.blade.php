@extends(($mobile=='mobile.home')?'layouts.appMobile':'layouts.app')
@section('title', 'Error - 404 Domrey.com')
@section('content')
<link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

<style>
#notfound {
    position: relative;
    height: 65vh;
    top: 10px;
}

#notfound .notfound {
    position: absolute;
    left: 50%;
    top: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%)
}

.notfound {
    max-width: 710px;
    width: 100%;
    text-align: center;
    padding: 0 15px;
    line-height: 1.4
}

.notfound .notfound-404 {
    height: 200px;
    line-height: 200px
}

.notfound .notfound-404 h1 {
    font-size: 168px;
    margin: 0;
    color: #f3bd86;
    text-transform: uppercase
}

.notfound h2 {
    font-family: raleway, sans-serif;
    font-size: 22px;
    font-weight: 400;
    text-transform: uppercase;
    color: #222;
    margin: 0
}

.notfound-search {
    position: relative;
    padding-right: 123px;
    max-width: 420px;
    width: 100%;
    margin: 30px auto 22px
}

.notfound-search input {
    font-family: raleway, sans-serif;
    width: 100%;
    height: 40px;
    padding: 3px 15px;
    color: #222;
    font-size: 18px;
    background: #f8fafb;
    border: 1px solid rgba(34, 34, 34, .2);
    border-radius: 3px
}

.notfound-search button {
    font-family: raleway, sans-serif;
    position: absolute;
    right: 0;
    top: 0;
    width: 120px;
    height: 40px;
    text-align: center;
    border: none;
    cursor: pointer;
    padding: 0;
    color: #fff;
    font-weight: 700;
    font-size: 18px;
    border-radius: 3px
}

.notfound a {
    font-family: raleway, sans-serif;
    display: inline-block;
    font-weight: 700;
    border-radius: 15px;
    text-decoration: none;
    color: #39b1cb
}

.notfound a>.arrow {
    position: relative;
    top: -2px;
    border: solid #39b1cb;
    border-width: 0 3px 3px 0;
    display: inline-block;
    padding: 3px;
    -webkit-transform: rotate(135deg);
    -ms-transform: rotate(135deg);
    transform: rotate(135deg)
}

@media only screen and (max-width:767px) {
    .notfound .notfound-404 {
        height: 122px;
        line-height: 122px
    }

    .notfound .notfound-404 h1 {
        font-size: 122px
    }
}
</style>

<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <h1>404</h1>
        </div>
        <h2>Oops, The Page you are looking for can't be found!</h2>
        <form class="notfound-search">
            <input type="text" id="text_search" placeholder="Search..." />
            <button type="button" class="btn btn-domrey" id="btn-404">Search</button>
        </form>
        <a href="{{url('/')}}" class=""><span class="arrow"></span>Return To Homepage</a>
    </div>
</div>

<script>
    $('#btn-404').on('click',function(ent){
        var text = $('#text_search').val();
        window.location.href = "{{url('/post/searchs')}}/" + text;
    });

    $("#text_search").keyup(function (ent) {
        if (ent.keyCode == 13) { //Key Enter
            window.location.href = "{{url('/post/searchs')}}/" + $(this).val();
        }
    });
</script>

@endsection