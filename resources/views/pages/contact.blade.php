@extends('layouts.app')
@section('title', 'Contact Us')
@section('content')
    
 <div class="category-page domrey-container">
    @include('layouts.breadcrumb', ['dashboard' => __('home.contact_page'), 'page_title' => __('home.contact_contact_us')])

    <div class="row">
        <div class="col-md-3">
            <h5>@lang('home.contact_info')</h5>
            <div class="row mt-3">
                <div class="col-2">
                    <i class="material-icons">phone</i>
                </div>
                <div class="col-10">
                    +855 16 93 26 08
                </div>
            </div>

             <div class="row mt-2">
                <div class="col-2">
                    <i class="material-icons">email</i>
                </div>
                <div class="col-10">
                    supervong@info.com
                </div>
            </div>

        </div>
        <div class="col-md-9">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    @if($id>0)
                        
                        <div class="alert alert-success alert-dismissible fade show mb-3" role="alert">
                            @lang('home.contact_success')
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                    @endif
                    <h5 class="pb-3">@lang('home.contact_message')</h5>
                    <form action="{{url('pages/contact')}}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="title" class="font-weight-bold col-12">@lang('home.contact_your_name') <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}"/>
                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
          
                        </div>

                        <div class="form-group row">
                            <label for="title" class="font-weight-bold col-12">@lang('home.email') <span class="text-danger">*</span></label>
                            <div class="col-7">
                                <input type="text" id="email" name="email"  class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"/>
                            
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                           
                        </div>

                        <div class="form-group row">
                            <label for="title" class="font-weight-bold col-12">@lang('home.description') <span
                                    class="text-danger">*</span></label>
                            <div class="col-7">
                                <textarea  id="description" name="description"  rows="10" class="form-control @error('description') is-invalid @enderror">{{old('description')}}</textarea>
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                           
                        </div>

                        
                        <div class="form-group row">
                            <div class="col-7">
                                <button type="submit" id="btn-submit" class="btn btn-domrey" data-lang="@lang('home.submit')">
                                    @lang('home.submit') 
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>

    </div>

</div>

@endsection