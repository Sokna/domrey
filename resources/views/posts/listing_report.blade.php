@extends('layouts.app')
@section('title', __('home.my_item'))
@section('content')
<div class="category-page">
    @include('layouts.breadcrumb', ['dashboard' => __('home.dashboard'), 'page_title' => __('home.report_listing'), 'report' => 'Report Listing'])

    <div class="row">
        <div class="col-md-3">
            @include('layouts.nav')
        </div>
        <div class="col-md-9">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header"><h5>@lang('home.report_listing')</h5></div>
                        <div class="card-body">
                            <p class="p-2 text-danger" style="padding-bottom: 15px!important;">
                                @if($post->status==-2)
                                    @if(count($reports)>=5)
                                      <i class="material-icons" style="top: 5px;position: relative;">warning</i> @lang('home.system_block')
                                    @else
                                        <i class="material-icons" style="top: 5px;position: relative;">block</i> @lang('home.admin_block')
                                    @endif
                                @else
                                    <i class="material-icons" style="top: 5px;position: relative;">block</i> @lang('home.warning_block')
                                @endif
                            </p>
                            @if(@post)
                                <div class="p-2">
                                    <div class="thumbnail-domrey-70 float-left">
                                        <div class="grid_balckground_img_70" style="background-image: url('{{asset('images/products/150x150/'.$post->image->name)}}')"></div>
                                    </div>
                                    <div class="float-left" style="padding-left:10px;width: 90%;">
                                       <strong> {{$post->title}}</strong> <br/> $ {{$post->price}}
                                    </div>
                                </div>
                            @endif
                            <table class="table table-borderless">
                                <tbody>
                                    @foreach($reports as $report)
                                        <tr>
                                            <td class="text-left" style="width: 180px;">{{$report->dayname}}</td>
                                            <td class="text-left">{{$report->message}}</td>
                                            <td class="text-center">
                                                @if($report->username)
                                                    by: <a href="{{url('sellers/'.$report->username)}}" class="domrey-link"> &#64;{{$report->username}}</a>
                                                @else
                                                    by:   Anonymous
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <a class="btn btn-domrey" href="{{url('post/list')}}">@lang('home.back')</a>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection