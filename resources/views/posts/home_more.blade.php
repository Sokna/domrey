@foreach($posts as $p)
    <div class="col-6 col-md-4 col-lg-3 col-xl-2 text-center products">
        <div class="imag-product">
            <div class="avatar">
                <div class="row">
                    <div class="col-3 col-md-2 text-left">
                    <a href="{{url('/sellers/'.$p->uuser_id)}}">
                        <img src="{{asset('/images/users/150x150/'.$p->uuser_photo)}}?t={{time()}}" alt="" class="circle responsive-img"/>
                    </a>
                    
                    </div>
                    <div class="col-9 col-md-10 user-avatar text-left">
                        <a href="{{url('/sellers/'.$p->uuser_id)}}">
                            <span class="name">&#64;{{$p->uuser_id}}</span><br />
                            <span class="time">

                            @if($p->Y>0)
                                {{$p->Y}} @lang('home.years_ago')
                            @elseif($p->MO >0)
                                {{$p->MO}} @lang('home.months_ago')
                            @elseif($p->D >0)
                                @if($p->H>12)
                                    {{$p->D + 1}} @lang('home.days_ago')
                                @else
                                    {{$p->D}} @lang('home.days_ago')
                                @endif
                            @elseif($p->H >0)
                                {{$p->H}} @lang('home.months_ago')
                            @elseif($p->M >0 )
                                {{$p->M}} @lang('home.minutes_ago')
                            @elseif($p->S >0 )
                                {{$p->S}} @lang('home.seconds_ago')
                            @endif

                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class='thumbnail-domrey'>
                <a href="{{url('/post/detail/'.$p->id)}}">
                    <div class="grid_balckground_img"
                    style="background-image: url('{{asset('images/loading.svg')}}');" 
                    data-src="{{asset('/images/products/500x500/'.$p->image)}}">
                    </div>
                </a>
            </div>
            <div class="text-desc text-left">
                <div class="title">
                <a href="{{url('/post/detail/'.$p->id)}}">
                    {{Str::limit($p->title, $limit = 30, $end = '...')}}
                </a>
                </div>
                <div class="price-font">
                    ${{number_format($p->price)}}
                </div>
                <div class="description">
                <a href="{{url('/post/detail/'.$p->id)}}">
                    {{Str::limit($p->description, $limit = 30, $end = '...')}}
                </a>
                    
                </div>
                @if($p->condition=='USED')
                <div class="">
                    @lang('home.used')
                </div>
                @else
                <div class="">
                    @lang('home.new')
                </div>
                @endif
                <div class="like">
                <a class="btn-like-listing" data-id="{{$p->id}}" onclick="btnHeartLike(this)">
                    @if($p->like_user==(Auth::user()->id??-1))
                        <img src="{{asset('/images/like-filled.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$p->rate>0?$p->rate:''}}</span>
                    @else
                        <img src="{{asset('/images/like-outlined.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$p->rate>0?$p->rate:''}}</span>
                    @endif
                </a>
                <a id="dropdownMenuRe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg  height="16" width="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" class="_3EdKwhRm4f float-right">
                        <path fill-rule="nonzero" d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z" fill="#57585a"></path>
                    </svg>
                </a>
                <div class="dropdown-menu report-listing" aria-labelledby="dropdownMenuRe">
                    <button class="dropdown-item btn-report-listing" onclick="btnReportListing({{$p->id}})" type="button">@lang('home.report_listing')</button>
                </div>
                </div>
            </div>

        </div>
    </div>

@endforeach
<script>
    var itemW = $('.thumbnail-domrey').width();
    var itemWd = $('.thumbnail-domrey-detail').width();
    if(itemW > 0){
        $('.thumbnail-domrey').css('height', itemW);
    }

    if (itemWd > 0) {
        $('.thumbnail-domrey-detail').css('height', itemWd);
    }
</script>