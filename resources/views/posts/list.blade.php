@extends('layouts.app')
@section('title', __('home.my_item'))
@section('content')
<div class="category-page domrey-container">
    @include('layouts.breadcrumb', ['dashboard' => __('home.dashboard'), 'page_title' => __('home.my_item')])

    <div class="row">
        <div class="col-md-3">
            @include('layouts.nav')
        </div>
        <div class="col-md-9">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header"><h5>@lang('home.my_item')</h5></div>
                        <div class="card-body">
                            <a href="{{url('/post/createNew')}}" class="btn btn-domrey col-sm-12 col-md-1">@lang('home.sell')</a><br /><br />

                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col" colspan="2" style="width: 50%;">@lang('home.listing_title')</th>
                                        <th scope="col">@lang('home.condition')</th>
                                        <th scope="col">@lang('home.price')</th>
                                        <th scope="col">@lang('home.status')</th>
                                        <th scope="col">Reported</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($posts as $p)
                                    <tr class="{{($p->status==0)?'disabled_row':''}}" id="row_{{$p->id}}">
                                        <th scope="row" class="align-middle">{{$loop->index + 1}}</th>
                                        <td class="align-middle">
                                            <div class="thumbnail-domrey-70">
                                                <a href="{{ url('post/createNew') }}/{{$p->id}}">
                                                    <!--<img src="{{asset('images/products/150x150/'.$p->image)}}"
                                                        class="portrait"/>-->
                                                        <div class="grid_balckground_img_70" style="background-image: url('{{asset('images/products/150x150/'.$p->image)}}')">
                                                            @if($p->status==-2)
                                                                <i class="material-icons text-danger" style="font-size: 70px;">block</i>
                                                            @endif
                                                        </div>
                                                </a>
                                            </div>
                                        </td>
                                        <td class="align-middle">
                                            <a href="{{ url('post/createNew') }}/{{$p->id}}" class="d-none d-md-block">
                                                {{Str::limit($p->title, $limit = 40, $end = '...')}}
                                            </a>
                                        </td>
                                        <td class="align-middle text-center">
                                            @if($p->condition == 'NEW')
                                            <span class="badge badge-success">@lang('home.new')</span>
                                            @elseif($p->condition == 'USED')
                                            <span class="badge badge-warning">@lang('home.used')</span>
                                            @endif
                                        </td>
                                        <td class="align-middle text-center">$ {{$p->price}}</td>
                                        <th scope="col" class="align-middle text-center">
                                            @if($p->status >=0)
                                                <div class="custom-control custom-switch" title="Enabled / Disabled">
                                                    @if($p->status==1)
                                                    <input type="checkbox" data-id="{{$p->id}}" checked
                                                        class="custom-control-input switch-item "
                                                        id="customSwitch{{$loop->index + 1}}">
                                                    @else
                                                    <input type="checkbox" data-id="{{$p->id}}"
                                                        class="custom-control-input switch-item "
                                                        id="customSwitch{{$loop->index + 1}}">
                                                    @endif
                                                    <label class="custom-control-label image-cursor"
                                                        for="customSwitch{{$loop->index + 1}}"></label>
                                                </div>
                                            @else
                                                 <div class="custom-control custom-switch" title="Enabled / Disabled">
                                                    <input type="checkbox" class="custom-control-input">
                                                    <label class="custom-control-label image-cursor" for="customSwitch"></label>
                                                 </div>
                                            @endif
                                        </th>
                                        <td class="align-middle text-center">
                                            @if($p->status==-2)
                                                <a href="{{url('/post/listingReport/'. $p->id)}}" title="Blocked">
                                                    <i class="material-icons text-danger">block</i>
                                                </a>
                                            @else
                                                @if($p->report)
                                                    <a href="{{url('/post/listingReport/'. $p->id)}}" title="Reported {{$p->report}} times">
                                                        <span class="badge badge-danger">{{$p->report}}</span>
                                                    </a>
                                                @endif
                                            @endif

                                          
                                        </td>
                                        <td class="align-middle">
                                            <a href="{{ url('post/createNew') }}/{{$p->id}}" title="View Listing">
                                                <i class="material-icons">view_list</i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="row">
                                <div class="col-md-12 float-right">
                                        <ul class="pagination">
                                            <li class="page-item previous">
                                                <a href="{{url('/post/list/1/'.$limits)}}" class="page-link"><<</a>
                                            </li>
                                            @for ($i = 1; $i <= $pages; $i++)
                                                <li class="page-item {{$i==$page?'active':''}}">
                                                    <a href="{{url('/post/list/'.$i.'/'.$limits)}}" class="page-link">
                                                        {{$i}}
                                                    </a>
                                                </li>
                                            @endfor
                                            <li class="page-item next">
                                                <a href="{{url('/post/list/'.$pages.'/'.$limits)}}" class="page-link">>></a>
                                            </li>
                                        </ul>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



@endsection