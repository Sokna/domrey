@extends('layouts.app')
@section('title', __('home.new_sell'))
@section('content')
<div class="category-page">
    @include('layouts.breadcrumb', ['dashboard' => __('home.dashboard'), 'page_title' => __('home.new_sell')])
    <div class="row">
        <div class="col-md-3">
            @include('layouts.nav')
        </div>
        <div class="col-md-9">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card" id="cart_create_post">
                        <div class="card-header"><h5>@lang('home.new_sell')</h5></div>
                        <div class="card-body" id="form-post">
                            <input type="hidden" id="pro_id" value="{{ $post->id??0}}"/>
                            <div class="form-group">
                                <input type="file" id="post-image" class="d-none" accept="image/*"/>
                                <label for="title" class="font-weight-bold">@lang('home.add_photos') <span
                                        class="text-danger">*</span></label>
                                <div class="row" id="photo-matrix">
                                    @isset($images)
                                    @foreach($images as $key => $img)
                                        <div data-id="{{$key}}" data-value="{{$img->id}}" class="col-6 col-md-3 mb-2 text-center removed">
                                            <div class='item_container'>
                                                <a href="#">
                                                    <img src="{{asset('images/products/500x500')}}/{{$img->name}}" data-edit="0" data-name="{{$img->name}}" width="125px;" hieght="125px;" class="resize_fit_center" />
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                    @for($i = $img_count; $i < 8; $i++)
                                        <div data-id="{{$i}}" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add add_new">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                    @endfor
                                    @endisset
                                    @if(!isset($post))
                                        <div data-id="0" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                        <div data-id="1" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                        <div data-id="2" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                        <div data-id="3" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                        <div data-id="4" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                        <div data-id="5" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                        <div data-id="6" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                        <div data-id="7" data-value="0" class="col-6 col-md-3 mb-2 text-center no_image_add">
                                            <div class='item_container'>
                                                <i class="material-icons img_add">add_circle</i>
                                            </div>
                                        </div>
                                    @endif

                                        
                                </div>
                                <div class="modal fade" id="exampleModalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                    aria-hidden="true">
                                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">@lang('home.crop_image')</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-center">
                                                <div id="image-popup" class="d-none"></div>
                                                <div id="image-popup-thum" class="d-none">
                                                    <i class="material-icons image_remove d-none" id="btn-delete-listing">delete_forever</i>
                                                    <img src="{{asset('/images/placeholder.png')}}" style="width:600px;"/>
                                                </div>
                                                <button type="button" id="btn-change-picture" class="btn btn-domrey mt-2">@lang('home.brows_image')</button>
                                                <input type="file" id="change_image" class="d-none"/>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-domrey invisible"    data-dismiss="modal" id="btn-ok-picture">@lang('home.original')</button>
                                                <button type="button" class="btn btn-secondary invisible" data-dismiss="modal" id="btn-after-crop">@lang('home.crop_now')</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label for="category" class="font-weight-bold">@lang('home.category') <span
                                        class="text-danger">*</span> </label>
                                <select id="parent_category" class="form-control col-md-7" data-lang="{{$lang}}">
                                    <option></option>
                                    @foreach($categories as $cat)
                                        @isset($post->category_id)
                                            @if($post->parent_category == $cat->id)
                                                <option value="{{$cat->id}}" selected>{{($lang=='kh'?$cat->name_kh:$cat->name)}}</option>
                                            @else
                                                <option value="{{$cat->id}}">{{($lang=='kh'?$cat->name_kh:$cat->name)}}</option>
                                            @endif
                                        @endisset
                                        @if(!isset($post))
                                            <option value="{{$cat->id}}">{{($lang=='kh'?$cat->name_kh:$cat->name)}}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Category is required
                                </div>
                            </div>

                            
                            <div class="form-group">
                                <label for="category" class="font-weight-bold">@lang('home.sub_category') <span
                                        class="text-danger">*</span> </label>
                                <select id="category_id" class="form-control col-md-7" data-id="{{$post->category_id??''}}">
                                    <option></option>
                                </select>
                                <div class="invalid-feedback">
                                    Sub Category is required
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="title" class="font-weight-bold col-12">@lang('home.listing_title') <span
                                        class="text-danger">*</span></label>
                                <div class="emoji-span col-7">
                                    <input type="text" id="title" class="form-control ml-1" value="{{ $post->title?? ''}}" />
                                </div>
                                <div class="invalid-feedback">
                                    Title is required
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="font-weight-bold">@lang('home.location') <span
                                        class="text-danger">*</span></label>
                                
                                <select id="location_id" class="form-control col-md-7" />
                                    <!-- <option></option> -->
                                    @foreach($zones as $z)
                                       @isset($post->location_id)
                                            @if($post->location_id == $z->zone_id)
                                                <option value="{{$z->zone_id}}" selected>{{($lang=='kh'?$z->name_kh:$z->name)}}</option>
                                            @else
                                                <option value="{{$z->zone_id}}">{{($lang=='kh'?$z->name_kh:$z->name)}}</option>
                                            @endif
                                        @else
                                            <option value="{{$z->zone_id}}">{{($lang=='kh'?$z->name_kh:$z->name)}}</option>
                                        @endisset
                                    @endforeach
                                </select>
                                 <div class="invalid-feedback">
                                    Location is required
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="font-weight-bold">@lang('home.condition')</label>
                                <select id="condition" class="form-control col-md-2" />
                                    <option></option>
                                    @isset($post->condition)
                                         @if($post->condition=='NEW')
                                            <option value="NEW" selected>@lang('home.new')</option>
                                            <option value="USED">@lang('home.used')</option>
                                         @elseif($post->condition=='USED')
                                            <option value="NEW">@lang('home.new')</option>
                                            <option value="USED" selected>@lang('home.used')</option>
                                         @endif
                                    @else
                                        <option value="NEW">@lang('home.new')</option>
                                        <option value="USED">@lang('home.used')</option>

                                    @endisset
                                </select>
                                <div class="invalid-feedback">
                                    Condition is required
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="font-weight-bold">@lang('home.brand')</label>
                                <input type="hidden" id="brand_id" value="{{$post->brand_id??''}}"//>
                                <input type="text" id="brand_name" class="form-control col-md-4" value="{{$brand->name??''}}"/>
                                <div class="band_autocomplete" id="brand_list"></div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="font-weight-bold">@lang('home.price')  <span class="text-danger">*</span></label>
                                <input type="number" id="price" class="form-control col-md-3" value="{{ $post->price??''}}" placeholder="0.00"/>
                                <span class="khr_riel d-none d-md-block"> 
                                    @isset($post->price)
                                        {{ number_format($post->price * 4000, 0)}} @lang('home.riel')
                                    @endisset
                                </span>
                                <div class="invalid-feedback">
                                    Price is required
                                </div>
                                <div class="kh_riel-small d-block d-md-none">
                                    @isset($post->price)
                                        {{ number_format($post->price * 4000, 0)}} @lang('home.riel')
                                    @endisset
                                </div>
                            </div>
                            

                            <div class="form-group row">
                                <label for="title" class="font-weight-bold col-12">@lang('home.description') <span
                                        class="text-danger">*</span></label>
                                <div class="emoji-span col-12">
                                    <textarea class="form-control col-md-12 scroll ml-1" id="description" rows="10">{{ $post->description?? ''}}</textarea>
                                </div>
                                <div class="invalid-feedback">
                                    Description is required
                                </div>
                            </div>

                            <div class="form-group row">
                                 <label for="tags" class="font-weight-bold col-12">@lang('home.deal_method') </label>
                                <div class="col-6">
                                    <div class="custom-control custom-checkbox mb-3 mt-1">
                                        <input type="checkbox" class="custom-control-input" id="meet_up" {{(isset($post) && $post->deal_method && $post->deal_method->meet_up)?'checked':''}}/>
                                        <label class="custom-control-label" for="meet_up">@lang('home.meet_up')</label>
                                    </div>                  
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="mail_delivery" {{(isset($post) && $post->deal_method && $post->deal_method->mail_delivery)?'checked':''}}/>
                                        <label class="custom-control-label" for="mail_delivery">@lang('home.mail_delivery')</label>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="tags" class="font-weight-bold col-12">@lang('home.tags') </label>
                                <div class="col-6">
                                    <input type="text" id="tags" class="form-control" value="{{ $post->tags??''}}"/>
                                </div>
                            </div>

                            <div class="form-group" style="float: left">
                                <br /><br />
                                <button type="button" id="btn-submit" class="btn btn-domrey" data-lang="@lang('home.submit')">
                                    @lang('home.submit') 
                                </button>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
 <script type="text/javascript">//<![CDATA[

    window.onload=function(){
		$(document).ready(function() {
            $("#title").emojioneArea({
				pickerPosition: "right",
				tonesStyle: "bullet",
				events: {
					keyup: function (editor, event) {
						console.log(editor.html());
						console.log(this.getText());
					}
				}
			});
			$("#description").emojioneArea({
				pickerPosition: "right",
				tonesStyle: "bullet",
				events: {
					keyup: function (editor, event) {
						console.log(editor.html());
						console.log(this.getText());
					}
				}
			});
		});

    }
  //]]></script>
@endsection