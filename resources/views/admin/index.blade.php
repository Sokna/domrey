@extends('admin.dashboard')
@section('title', 'Post Advertising')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cog"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Total Listing</span>
                <span class="info-box-number">
                  {{$total_listing}}
                  <!-- <small>%</small> -->
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Sellers</span>
                <span class="info-box-number">{{$total_sellers}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-thumbs-up"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Like</span>
                <span class="info-box-number">{{$total_likes}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-thumbs-down"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Dislike</span>
                <span class="info-box-number">{{$total_dislikes}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">

          <div class="col-md-4">
            <!-- Info Boxes Style 2 -->
            <div class="info-box mb-3 bg-success">
              <span class="info-box-icon"><i class="fas fa-cubes"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Active Listings</span>
                <span class="info-box-number">{{$total_active_listing}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-warning">
              <span class="info-box-icon"><i class="fas fa-archive"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Inactive Listings</span>
                <span class="info-box-number">{{$total_inactive_listing}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-danger">
              <span class="info-box-icon"><i class="fas fa-ban"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Blocked Listings</span>
                <span class="info-box-number">{{$total_block_listing}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-dark">
              <span class="info-box-icon"><i class="fas fa-trash-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Deleted Listings</span>
                <span class="info-box-number">{{$total_delete_listing}}</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->

    
          </div>
          <!-- /.col -->
          <div class="col-md-4">
                    <!-- PRODUCT LIST -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Recently Posted Listing</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <ul class="products-list product-list-in-card pl-2 pr-2">
                  @foreach($recent_listing  as $listing)
                  <li class="item">
                    <div class="product-img">
                      <img src="{{asset('images/products/500x500/'.$listing->image)}}" alt="Product Image" class="img-size-50 rounded">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">{{$listing->title}}
                        <span class="badge badge-warning float-right">${{$listing->price}}</span></a>
                      <span class="product-description">
                        {{$listing->description}}
                      </span>
                    </div>
                  </li>
                  
                  @endforeach
                </ul>
              </div>
              <!-- /.card-body -->
              <div class="card-footer text-center">
                <a href="{{url('dashboard/listing')}}" class="uppercase">View All Listings</a>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>



           <div class="col-md-4">
                    <!-- PRODUCT LIST -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Messages</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <ul class="products-list product-list-in-card pl-2 pr-2">
                  @foreach($contacts  as $contact)
                  <li class="item">
                    <div class="product-img">
                      <img src="{{asset('images/users/avata.png')}}" alt="{{$contact->name}}" class="img-size-50 rounded">
                    </div>
                    <div class="product-info">
                      <a href="javascript:void(0)" class="product-title">{{$contact->email}}
                        
                        <span class="badge  float-right">{{$contact->name}}</span>
                        
                      </a>
                      <span class="product-description">
                        {{$contact->description}}
                      </span>
                    </div>
                  </li>
                  
                  @endforeach
                </ul>
              </div>
              <!-- /.card-body -->
              <div class="card-footer text-center">
                <a href="{{url('dashboard/message')}}" class="uppercase">View All Messages</a>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>

        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

  