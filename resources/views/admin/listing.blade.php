@extends('admin.dashboard')
@section('title', 'Listing')
@section('url', 'dashboard/listing')
@section('search', $search)
@section('content')
<style>
    .thumbnail-domrey-70 {
        position: relative;
        width: 70px;
        height: 70px;
        overflow: hidden;
        border-radius: 4px;
        margin: 0 auto;
        float: left;
    }
    .grid_balckground_img_70 {
        width: 70px;
        height: 70px;
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: cover;
    }

</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Listing</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Listing</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-left mb-md-3">
                   
                </div>
                <div class="col-md-6 text-right mb-md-3">
                    <button type="button" class="btn btn-warning" id="btn-create-post">
                        Add New
                    </button>
                </div>
            </div>

            <table class="table table-bordered" id="table-list">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th colSpan="2">Title</th>
                        <th class="align-middle text-center">Item Condition</th>
                        <th class="align-middle text-center">Price</th>
                        <th>Posted Date</th>
                        <th>Posted by</th>
                        <th class="align-middle text-center">Reported</th>
                        <th class="align-middle text-center">Block</th>
                        <th style="width: 50px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $b)
                    <tr>
                        <td class="align-middle text-center">{{$loop->index + 1}}</td>
                        <td class="align-middle">
                            <div class="thumbnail-domrey-70">
                                <div class="grid_balckground_img_70" style="background-image: url('{{asset('images/products/150x150/'.$b->image)}}'); width: 70px;">
                                    @if($b->status==-2)
                                        <i class="fas fa-ban text-danger" style="font-size: 70px;"></i>
                                    @endif
                                </div>
                                
                            </div>
                        </td>
                        <td class="align-middle">
                            <div class="float-left pl-2 ">{{$b->title}}</div>
                            
                        </td>
                        <td class="align-middle text-center">
                            @if($b->condition == 'NEW')
                            <span class="badge badge-success">{{$b->condition}}</span>
                            @elseif($b->condition == 'USED')
                            <span class="badge badge-warning">{{$b->condition}}</span>
                            @endif
                        </td>
                        <td class="align-middle text-center">$ {{$b->price}}</td>
                        <td class="align-middle text-center">{{date('j M, Y', strtotime($b->created_at))}}</td>
                        <td class="align-middle">{{$b->user_name}}</td>
                        <td class="align-middle text-center">{{$b->num_of_report}}</td>
                        <td class="align-middle text-center">
                            @if($b->status==1)
                            <div class="custom-control custom-switch">
                                <input type="checkbox" checked class="custom-control-input switch-item"
                                    id="customSwitch{{$loop->index + 1}}" data-id="{{$b->id}}">
                                <label class="custom-control-label" for="customSwitch{{$loop->index + 1}}"></label>
                            </div>
                            @else
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input switch-item"
                                    id="customSwitch{{$loop->index + 1}}" data-id="{{$b->id}}">
                                <label class="custom-control-label" for="customSwitch{{$loop->index + 1}}"></label>
                            </div>
                            @endif
                        </td>

                        <td class="text-center align-middle btn-edit-brand" data-id="{{$b->id}}"
                            data-title="{{$b->title}}" data-status="{{$b->status}}">
                            <a href="#">
                                <span class="badge bg-warning"><i class="fas fa-eye"></i></span>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="row">
                <div class="col-md-12 float-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <ul class="pagination">
                            <li class="paginate_button page-item previous" id="example2_previous">
                                <a href="{{url('/dashboard/listing/1/'.$limit)}}" aria-controls="example2"
                                    data-dt-idx="0" tabindex="0" class="page-link">
                                    << </a>
                            </li>
                            @for ($i = 1; $i <= $pages; $i++) <li
                                class="paginate_button page-item {{$i==$page?'active':''}}">
                                <a href="{{url('/dashboard/listing/'.$i.'/'.$limit)}}" aria-controls="example2"
                                    data-dt-idx="2" tabindex="0" class="page-link">{{$i}}</a>
                                </li>
                                @endfor
                                <li class="paginate_button page-item next" id="example2_next">
                                    <a href="{{url('/dashboard/listing/'.$pages.'/'.$limit)}}" aria-controls="example2"
                                        data-dt-idx="7" tabindex="0" class="page-link">>></a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>


        </div>
    </section>
</div>
<!-- /.content-wrapper -->

<!-- Model Alert -->
<div class="modal fade" id="modal-lg-alert">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script>
/**
 * Switch status
 */
var htmlAllert = function(title, body) {
    $('#modal-lg-alert').find('.modal-title').html(title);
    $('#modal-lg-alert').find('.modal-body').html(body);
    $('#modal-lg-alert').modal({});
};

$('.switch-item').click(function(e) {
    var id = $(this).attr('data-id');
    var status = -2;
    if ($(this).is(':checked')) {
        status = 1;
    }

    var formData = new FormData();
    formData.append('id', id);
    formData.append('status', status);

    $.ajax({
        url: '/api/post/status',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            
        },
        complete: function () {
            
        },
        success: function (json) {
            if(json.status==1){
                 htmlAllert('Success!', 'Listing have been unblocked successfully!');
            }else{
                 htmlAllert('Success!', 'Listing have been blocked successfully!');
            }
           
            
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
    //End

});
</script>

@endsection