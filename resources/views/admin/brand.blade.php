@extends('admin.dashboard')
@section('title', 'Brand')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Brand</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Brand</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-right mb-md-3">
                    <button type="button" class="btn btn-warning" id="btn-create-brand">
                        Add New
                    </button>
                </div>
            </div>

            <table class="table table-bordered" id="table-list">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Brand</th>
                        <th>Enabled</th>
                        <th style="width: 50px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($brands as $b)
                    <tr>
                        <td class="align-middle">{{$loop->index + 1}}</td>
                        <td>{{$b->name}}</td>
                        <td class="align-middle">
                            @if($b->status==1)
                            <div class="custom-control custom-switch">
                                <input type="checkbox" checked class="custom-control-input"
                                    id="customSwitch{{$loop->index + 1}}">
                                <label class="custom-control-label" for="customSwitch{{$loop->index + 1}}"></label>
                            </div>
                            @else
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input"
                                    id="customSwitch{{$loop->index + 1}}">
                                <label class="custom-control-label" for="customSwitch{{$loop->index + 1}}"></label>
                            </div>
                            @endif

                        </td>
                        <td class="text-center align-middle btn-edit-brand" data-id="{{$b->id}}"
                            data-title="{{$b->name}}" data-status="{{$b->status}}">
                            <a href="#">
                                <span class="badge bg-warning"><i class="fas fa-pen"></i></span>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="row">
                <div class="col-md-12 float-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <ul class="pagination">
                            <li class="paginate_button page-item previous" id="example2_previous"><a href="{{url('/dashboard/brand/1/'.$limit)}}"
                                    aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link"><<</a>
                            </li>
                            @for ($i = 1; $i <= $pages; $i++) <li
                                class="paginate_button page-item {{$i==$page?'active':''}}"><a
                                    href="{{url('/dashboard/brand/'.$i.'/'.$limit)}}" aria-controls="example2"
                                    data-dt-idx="2" tabindex="0" class="page-link">{{$i}}</a></li>
                                @endfor
                                <li class="paginate_button page-item next" id="example2_next"><a href="{{url('/dashboard/brand/'.$pages.'/'.$limit)}}"
                                        aria-controls="example2" data-dt-idx="7" tabindex="0" class="page-link">>></a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->

    <!-- Modal Edit -->
    <div class="modal fade" id="modal-lg-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="hidden" id="brand_id" class="form-control" />
                            <label for="brand_name">Brand</label>
                            <input type="text" id="brand_name" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="enabled">Enabled</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="customSwitchEdit">
                                <label class="custom-control-label" for="customSwitchEdit"></label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-warning" id="btn-save">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <script>
    $('.btn-edit-brand').click(function() {
        $('#modal-lg-edit').find('.modal-title').text('Edit - ' + $(this).attr('data-title'));
        $('#brand_name').val($(this).attr('data-title'));
        $('#brand_id').val($(this).attr('data-id'));

        if ($(this).attr('data-status') == 1) {
            $('#customSwitchEdit').attr('checked', true);
        } else {
            $('#customSwitchEdit').attr('checked', false);
        }
        $('#modal-lg-edit').modal({});
    });

    $('#btn-create-brand').click(function() {
        $('#modal-lg-edit').find('.modal-title').text('Create New');
        $('#brand_name').val('');
        $('#brand_id').val(0);
        $('#customSwitchEdit').attr('checked', true);
        $('#modal-lg-edit').modal({});
    });


    $('#btn-save').click(function(event) {
        var formData = new FormData();
        formData.append("brand_id", $('#brand_id').val());
        formData.append("brand_name", $('#brand_name').val());

        var check = 0;
        if ($('#customSwitchEdit').is(':checked')) {
            check = 1;
        }
        formData.append("status", check);

        $.ajax({
            url: '/api/brand/update/' + $('#brand_id').val(),
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#btn-save').html(
                    '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Saving...'
                );
            },
            complete: function() {
                $('#btn-save').html('Save');
                //htmlToast('Success!', 'Your photo has been uploaded successfully!');
            },
            success: function(json) {

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    </script>

</div>
<!-- /.content-wrapper -->
@endsection