@extends('admin.dashboard')
@section('title', 'Post Advertising')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Category</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Category</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-right mb-md-3">
                    <button type="button" class="btn btn-warning" id="btn-create-category">
                        Add New
                    </button>
                </div>
            </div>

            <table class="table table-bordered" id="table-list">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Category</th>
                        <th>Category Khmer</th>
                        <th>Sequence</th>
                        <th>Enabled</th>
                        <th style="width: 50px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $c)
                    <tr>
                        <td class="align-middle">{{$loop->index + 1}}</td>
                        <td>
                            @if($c->icon!='')
                                <img src="{{asset('images/explore/'.$c->icon)}}" class="img-circle"
                                    style="width: 40px; border: 1px solid #f3be89;" /> 
                            @endif
                            @if($c->parent_name)
                                @if($c->parent_icon!='')
                                <img src="{{asset('images/explore/'.$c->parent_icon)}}" class="img-circle"
                                    style="width: 40px; border: 1px solid #f3be89;" /> 
                                @endif

                                {{$c->parent_name}} -> {{$c->name}} 
                            @else
                                {{$c->name}}
                            @endif
                                
                        </td>
                        <td>{{$c->name_kh}}</td>
                        <td class="align-middle">{{$c->sequence}}</td>
                        <td class="align-middle">
                            @if($c->status==1)
                            <div class="custom-control custom-switch">
                                <input type="checkbox" checked class="custom-control-input"
                                    id="customSwitch{{$loop->index + 1}}">
                                <label class="custom-control-label" for="customSwitch{{$loop->index + 1}}"></label>
                            </div>
                            @else
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input"
                                    id="customSwitch{{$loop->index + 1}}">
                                <label class="custom-control-label" for="customSwitch{{$loop->index + 1}}"></label>
                            </div>
                            @endif
                        </td>
                        <td class="text-center align-middle btn-edit-category" data-id="{{$c->id}}"
                            data-title="{{$c->name}}" data-title-kh="{{$c->name_kh}}" data-sequence="{{$c->sequence}}" data-icon="{{$c->icon}}"
                            data-status="{{$c->status}}" data-parent="{{$c->parent_id}}">
                            <a href="#">
                                <span class="badge bg-warning"><i class="fas fa-pen"></i></span>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>


            <div class="row">
                <div class="col-md-12 float-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <ul class="pagination">
                            <li class="paginate_button page-item previous" id="example2_previous"><a href="{{url('/dashboard/category/1/'.$limit)}}"
                                    aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link"><<</a>
                            </li>
                            @for ($i = 1; $i <= $pages; $i++) <li
                                class="paginate_button page-item {{$i==$page?'active':''}}"><a
                                    href="{{url('/dashboard/category/'.$i.'/'.$limit)}}" aria-controls="example2"
                                    data-dt-idx="2" tabindex="0" class="page-link">{{$i}}</a></li>
                                @endfor
                                <li class="paginate_button page-item next" id="example2_next"><a href="{{url('/dashboard/category/'.$pages.'/'.$limit)}}"
                                        aria-controls="example2" data-dt-idx="7" tabindex="0" class="page-link">>></a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->

    <!-- Modal Edit -->
    <div class="modal fade" id="modal-lg-edit">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <input type="hidden" id="category_id" class="form-control" />
                            <label for="parent_name">Parent</label>
                            <select id="parent_id" class="form-control">
                                <option value="-1"></option>
                                @foreach($parent_cate as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="category_name">Name</label>
                            <input type="text" id="category_name" class="form-control" />
                        </div>
                         <div class="form-group">
                            <label for="category_name">Name Khmer</label>
                            <input type="text" id="category_name_kh" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="icon">Icon</label>
                            <input type="text" id="icon" class="form-control" placeholder="ex. jobs.png" />
                        </div>
                        <div class="form-group">
                            <label for="sequence">Sequence</label>
                            <input type="number" id="sequence" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="enabled">Enabled</label>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="customSwitchEdit">
                                <label class="custom-control-label" for="customSwitchEdit"></label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-warning" id="btn-save">Save</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal alert -->
    <div class="modal fade" id="modal-lg-alert">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">title</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <script>

    var htmlAllert = function(title, body){
        $('#modal-lg-alert').find('.modal-title').html(title);
        $('#modal-lg-alert').find('.modal-body').html(body);
        $('#modal-lg-alert').modal({});
    };

    $('.btn-edit-category').click(function() {
        $('#modal-lg-edit').find('.modal-title').text('Edit - ' + $(this).attr('data-title'));
        $('#category_name').val($(this).attr('data-title'));
        $('#category_name_kh').val($(this).attr('data-title-kh'));
        $('#category_id').val($(this).attr('data-id'));
        $('#sequence').val($(this).attr('data-sequence'));
        $('#parent_id').val($(this).attr('data-parent'));
        $('#icon').val($(this).attr('data-icon'));

        if ($(this).attr('data-status') == 1) {
            $('#customSwitchEdit').attr('checked', true);
        } else {
            $('#customSwitchEdit').attr('checked', false);
        }
        $('#modal-lg-edit').modal({});
    });

    $('#btn-create-category').click(function() {
        $('#modal-lg-edit').find('.modal-title').text('Create New');
        $('#category_name').val('');
        $('#category_name_kh').val('');
        $('#category_id').val(0);
        $('#sequence').val(99);
        $('#parent_id').val(0);
        $('#icon').val('');
        $('#customSwitchEdit').attr('checked', true);

        $('#modal-lg-edit').modal({});
    });


    $('#btn-save').click(function(event) {
        var formData = new FormData();
        formData.append("category_id", $('#category_id').val());
        formData.append("category_name", $('#category_name').val());
        formData.append("category_name_kh", $('#category_name_kh').val());
        formData.append("sequence", $('#sequence').val());
        formData.append("parent_id", $('#parent_id').val());
        formData.append("icon", $('#icon').val());

        var check = 0;
        if ($('#customSwitchEdit').is(':checked')) {
            check = 1;
        }
        formData.append("status", check);

        $.ajax({
            url: '/api/category/update/' + $('#category_id').val(),
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#btn-save').html(
                    '<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Saving...'
                    );
            },
            complete: function() {
                $('#btn-save').html('Save');
                htmlAllert('Success!', 'Your category has been saved successfully!');
                //htmlToast('Success!', 'Your photo has been uploaded successfully!');
            },
            success: function(json) {

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    </script>

</div>
<!-- /.content-wrapper -->
@endsection