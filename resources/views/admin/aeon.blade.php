@extends('admin.dashboard')
@section('title', 'AEON Items')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">AEON</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">AEON</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-right mb-md-3">
                    <button type="button" class="btn btn-warning" data-down="0" data-id="{{$aeon_id+1}}" id="btn-start">
                        Start download 
                    </button>
                </div>
            </div>
            <input type="hidden" id="_token" value="{{ csrf_token() }}"/>
            <table class="table table-bordered" id="table-list">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>AEON ID</th>
                        <th>Image</th>
                        <th>Product Code</th>
                        <th>Brand</th>
                        <th>Price</th>
                        <th>Title</th>
                    </tr>
                </thead>
                <tbody id="item_list">
                     @foreach($items as $item)
                        <tr>
                            <td class="align-middle">
                                {{$loop->index+1}}
                            </td>
                            <td class="align-middle">
                                {{$item->aeon_id}}
                            </td>
                            <td class="align-middle text-center">
                                <a href="{{$item->image}}" target="_blank">
                                    <img src="{{$item->image}}" width="70px"/> 
                                </a>
                            </td>
                            <td class="align-middle">
                                {{$item->code}}
                            </td>
                            <td class="align-middle">
                                {{$item->brand}}
                            </td>
                           
                            <td class="align-middle">
                                {{$item->price}}
                            </td>
                            <td class="align-middle">
                                {{$item->title}}
                            </td>
                        </tr>
                    @endforeach
                 
                </tbody>
            </table>

            <div class="row">
                <div class="col-md-12 float-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        <ul class="pagination">
                            <li class="paginate_button page-item previous" id="example2_previous"><a href="{{url('/dashboard/aeon/1/'.$limit)}}"
                                    aria-controls="example2" data-dt-idx="0" tabindex="0" class="page-link"><<</a>
                            </li>
                            @for ($i = 1; $i <= $pages; $i++) <li
                                class="paginate_button page-item {{$i==$page?'active':''}}"><a
                                    href="{{url('/dashboard/aeon/'.$i.'/'.$limit)}}" aria-controls="example2"
                                    data-dt-idx="2" tabindex="0" class="page-link">{{$i}}</a></li>
                                @endfor
                                <li class="paginate_button page-item next" id="example2_next"><a href="{{url('/dashboard/aeon/'.$pages.'/'.$limit)}}"
                                        aria-controls="example2" data-dt-idx="7" tabindex="0" class="page-link">>></a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->


    <script>
    
            var i = $('#btn-start').attr('data-id');

            var start = false;


            $('#btn-start').on('click', function(evt){
                if($(this).attr('data-down') == 0){
                    start = true;
                    $(this).attr('data-down', 1)
                    $(this).text('Stop download...')
                }else{
                    start = false;
                    $(this).attr('data-down', 0);
                    $(this).text('Start download')
                }
            });

            setInterval(function(){
                if(start){
                    $.ajax({
                            url: '/dashboard/apiAeon/' + i,
                            type: 'GET',
                            data: {},
                            dataType: 'html',
                            beforeSend: function () {
                            
                            },
                            complete: function () {
                                
                            },
                            success: function (datahtml) {
                                var image = $(datahtml).find('#content ul.thumbnails img').attr('data-zoom-image'); 
                                var title = $(datahtml).find('#content div.pro-content h1:first').text();
                                var price = $(datahtml).find('#content ul li.text-decor-bold h2').text();
                                var code = $(datahtml).find('#content div.pro-content ul.list-unstyled li:nth-child(2)').html();
                                var brand = $(datahtml).find('#content div.pro-content ul.list-unstyled li:first a').html();
                                var desctiption =  $(datahtml).find('#tab-description').text();
                                code = code.replace('<span class="text-decor">Product Code:</span>','');

                                var html = `<tr>
                                        <td></td>
                                        <td>${i}</td>
                                        <td><img src="${image}" width="50px"/></td>
                                        <td>${code}</td>
                                        <td>${brand}</td>
                                        <td>${price}</td>
                                        <td>${title}</td>
                                    </tr>`;
                                if(title){
                                    $('#item_list').prepend(html);
                                    $('#btn-start').attr('data-id', i);

                                    upload({
                                        'aeon_id': i,
                                        'code': code,
                                        'image': image,
                                        'title': title,
                                        'brand': brand,
                                        'price': price,
                                        'desctiption': desctiption,
                                        '_token': $('#_token').val()
                                    });
                                    
                                }
                                i++;
                                
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                i++;
                            }
                        });


                }//End if

                //console.log('Running..')
            }, 4000);


            var upload = function(data){


                $.ajax({
                    url: '/dashboard/apiSaveAeon',
                    type: 'post',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                        
                    },
                    complete: function() {
                    
                    },
                    success: function(json) {
                        if(json.id % 20 == 0){
                            $('#item_list').html('');
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                         console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            };

            

    </script>

</div>
<!-- /.content-wrapper -->
@endsection