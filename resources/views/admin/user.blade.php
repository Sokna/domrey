@extends('admin.dashboard')
@section('title', 'Brand')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">User</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">User</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-right mb-md-3">
                </div>
                <table class="table table-bordered" id="table-list">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>User Name</th>
                            <th>ID</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Joined Date</th>
                            <th>Last login</th>
                            <th>Block</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($brands as $b)
                        <tr>
                            <td class="align-middle">{{$loop->index + 1}}</td>
                            <td>
                                <img src="{{asset('images/users/150x150/'.($b->photo??'avata.png'))}}" class="border border-secondary" width="50px"/>
                                {{$b->name}}
                            </td>
                            <td class="align-middle">&#64;{{$b->uuserid}}</td>
                            <td class="align-middle">{{$b->phone}}</td>
                            <td class="align-middle">{{$b->email}}</td>
                            <td class="align-middle">{{date('j M, Y', strtotime($b->created_at))}}</td>
                            <td class="align-middle">{{date('j M, Y', strtotime($b->lastlogin))}}</td>
                            <td class="align-middle">
                                 @if($b->active==1)
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" checked class="custom-control-input switch-item"
                                            id="customSwitch{{$loop->index + 1}}" data-id="{{$b->id}}">
                                        <label class="custom-control-label" for="customSwitch{{$loop->index + 1}}"></label>
                                    </div>
                                @else
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input switch-item"
                                            id="customSwitch{{$loop->index + 1}}" data-id="{{$b->id}}">
                                        <label class="custom-control-label" for="customSwitch{{$loop->index + 1}}"></label>
                                    </div>
                                @endif
                            </td>
                            <td class="text-center align-middle btn-edit-brand" data-id="{{$b->id}}"
                            data-title="{{$b->name}}" data-status="{{$b->active}}">
                            <a href="#">
                                <span class="badge bg-warning"><i class="fas fa-eye"></i></span>
                            </a>
                        </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-12 float-right">
                        <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                            <ul class="pagination">
                                <li class="paginate_button page-item previous" id="example2_previous"><a
                                        href="{{url('/dashboard/user/1/'.$limit)}}" aria-controls="example2"
                                        data-dt-idx="0" tabindex="0" class="page-link">
                                        << </a>
                                </li>
                                @for ($i = 1; $i <= $pages; $i++) <li
                                    class="paginate_button page-item {{$i==$page?'active':''}}"><a
                                        href="{{url('/dashboard/user/'.$i.'/'.$limit)}}" aria-controls="example2"
                                        data-dt-idx="2" tabindex="0" class="page-link">{{$i}}</a></li>
                                    @endfor
                                    <li class="paginate_button page-item next" id="example2_next"><a
                                            href="{{url('/dashboard/user/'.$pages.'/'.$limit)}}"
                                            aria-controls="example2" data-dt-idx="7" tabindex="0"
                                            class="page-link">>></a>
                                    </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->

    
    <!-- Modal Edit -->
    <div class="modal fade" id="modal-lg-alert">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">title</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


</div>
<!-- /.content-wrapper -->

<script>
       /**
     * Switch status
    */
    var htmlAllert = function(title, body){
        $('#modal-lg-alert').find('.modal-title').html(title);
        $('#modal-lg-alert').find('.modal-body').html(body);
        $('#modal-lg-alert').modal({});
    };

    $('.switch-item').click(function(e){
        var id = $(this).attr('data-id');
        var active = 0;
        if ($(this).is(':checked')){
            active = 1;
        }

        var formData = new FormData();
        formData.append('active', active);

        $.ajax({
            url: '/api/profile/blockUser/' + id,
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                
            },
            complete: function () {
              
            },
            success: function (json) {
                htmlAllert('Success!', 'Your status has been changed successfully!');
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        //End 

    });

</script>
@endsection