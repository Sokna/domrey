@extends('admin.dashboard')
@section('title', 'Messages')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Message</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Message</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <table class="table table-bordered" id="table-list">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Message</th>
                        <th>Send date</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($contacts  as $contact)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$contact->name}}</td>
                            <td>{{$contact->email}}</td>
                            <td>{{$contact->description}}</td>
                            <td>{{ Carbon\Carbon::parse($contact->created_at)->format('d M-Y h:i:s a') }}</td>
                        </tr>
                    @endforeach    
                </tbody>
            </table>
        </div>
        <!--/. container-fluid -->
    </section>
    <!-- /.content -->


</div>
<!-- /.content-wrapper -->
@endsection