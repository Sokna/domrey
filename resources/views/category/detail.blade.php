@extends('layouts.app')
@section('title', ($post->title??'').' - $ '.($post->price??''))
@section('description', Str::limit(($post->description??''), $limit = 150, $end = '...'))
@section('url', url('/post/detail/'.$post->id??''))
@section('image', asset('/images/products/500x500/'.($post->image??'')))

@section('content')
<div class="category-page">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('home.home')</a></li>
            <li class="breadcrumb-item"><a href="{{url('/post/category/'.($post->parent_category??''))}}">{{($lang=='kh')?($post->parent_category_name_kh??''):($post->parent_category_name??'')}}</a></li>
            <li class="breadcrumb-item">
                <a
                    href="{{url('/post/category/'.($post->category_id??''))}}">{{($lang=='kh')?($post->category_name_kh??''):($post->category_name??'')}}</a></li>
            <!-- <li class="breadcrumb-item active" aria-current="page">{{$post->title??''}}</li> -->
        </ol>
    </nav>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="product-detail">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2 d-none d-md-block">
                            <ul class="product-items scroll image_items" id="image_items">
                                @isset($post->photos)
                                @foreach($post->photos as $photo)
                                @if($loop->index==0)
                                <li>
                                    <img src="{{asset('images/loading.svg')}}" data-src="{{asset('/images/products/150x150/'.$photo->name)}}" data-origin="{{asset('/images/products/500x500/'.$photo->name)}}" alt="{{$photo->name}}"
                                        class="rounded-0 actived image-items" style="border-radius: 4px!important;width: 65%;" data-index="{{$loop->index}}"/>
                                </li>
                                @else
                                <li>
                                    <img src="{{asset('images/loading.svg')}}" data-src="{{asset('/images/products/150x150/'.$photo->name)}}" data-origin="{{asset('/images/products/500x500/'.$photo->name)}}" alt="{{$photo->name}}"
                                        class="rounded-0 deactived image-items" style="border-radius: 4px!important;width: 65%;" data-index="{{$loop->index}}"/>
                                </li>
                                @endif
                                @endforeach
                                @endisset
                            </ul>
                        </div>
                        <div class="col-md-10 ">
                            <div class="thumbnail-domrey-detail">
                                <div class="grid_balckground_img_500"
                                style="background-image: url('{{asset('images/loading.svg')}}');"
                                data-src="{{asset('/images/products/500x500/'.$post->image)}}" id="image-display" data-index="0">
                                </div>
                            </div> 
                        </div>
                        <!-- <div class="col-md-2 d-block d-md-none small-screen-img-item">
                           
                        </div> -->
                    </div>
                    <div class="card d-none" style="margin-top: 20px;">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="card-text ">
                                    <table class="table table-sm table-borderless table-responsive">
                                        
                                        <tr>
                                            <td colspan="2" style="font-weight: 600;"> 
                                                Contact
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><i class="material-icons domrey-color">person</i></td>
                                            <td>{{$post->address->first_name??''}} {{$post->address->last_name??''}}</td>
                                        </tr>
                                        <tr>
                                            <td><i class="material-icons domrey-color">phone</i></td>
                                            <td><a href="tel:{{$post->user->phone??''}}">{{$post->address->phone??''}}</a></td>
                                        </tr>
                                        <tr>
                                            <td><i class="material-icons domrey-color">mail</i></td>
                                            <td>{{$post->address->email??''}}</td>
                                        </tr>
                                        <tr>
                                            <td><i class="material-icons domrey-color">map</i></td>
                                            <td>{{$post->address->address??''}}</td>
                                        </tr>
                                         <tr>
                                            <td><i class="material-icons domrey-color">link</i></td>
                                            <td><a href="{{url('/sellers/'.($post->created_by??'').'/'.($post->uuser_id??''))}}">{{url('/sellers/'.($post->created_by??'').'/'.($post->uuser_id??''))}}</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <br /><br />
                            <div class="col-md-6">
                                <a href="#" class="btn btn-lg btn-block btn-warning d-none">CHAT</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-desc text-left single-product">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-12 ml-1">
                                                <div class="row">
                                                    <div class="col-2 col-md-3">
                                                        <a href="{{url('/sellers/'.($post->uuser_id??''))}}">
                                                            <img src="{{asset('/images/users/150x150/'.($post->uuser_photo??''))}}?t={{time()}}" class="rounded-circle" width="40px"  style="margin-top: 15px;"/>
                                                        </a>
                                                    </div>
                                                    <div class="col-10 col-md-9 ml-10 ml-md-0" style="margin-top: 15px;font-size: 13px;">
                                                        <a href="{{url('/sellers/'.($post->uuser_id??''))}}" class="domrey-link-1">
                                                            {{$post->uuser_id??''}}
                                                        </a>
                                                        <p>
                                                        @if($post->Y>0)
                                                            {{$post->Y}} @lang('home.hours_ago')
                                                        @elseif($post->MO >0)
                                                            {{$post->MO}} @lang('home.months_ago')
                                                        @elseif($post->D >0)
                                                            @if($post->H>12)
                                                                {{$post->D + 1}} @lang('home.days_ago')
                                                            @else
                                                                {{$post->D}} @lang('home.days_ago')
                                                            @endif
                                                        @elseif($post->H >0)
                                                            {{$post->H}} @lang('home.hours_ago')
                                                        @elseif($post->M >0 )
                                                            {{$post->M}} @lang('home.minutes_ago')
                                                        @elseif($post->S >0 )
                                                            {{$post->S}} @lang('home.seconds_ago')
                                                        @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-6 col-md-6 mt-2">
                                                <a href="{{url('/profile/chat/'.($post->id??'').'/'.(Auth::user()->id??''))}}" class="btn btn-lg btn-block btn-domrey-chat  btn-chat-small" style="color: black!important;margin-left: 20px;">@lang('home.chat')</a>
                                            </div>
                                            <div class="col-6 col-md-6 mt-2 float-left">
                                                <a id="make-offer" data-id="{{$post->price??0}}" data-url="{{url('/profile/chat/'.($post->id??'').'/'.(Auth::user()->id??''))}}" class="btn btn-lg btn-block btn-danger btn-domrey-offer" style="color: white!important;">@lang('home.make_offer')</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 20px;">
                                    <div class="col-4 text-left price"><span class="ml-1">${{number_format($post->price??0)}}</span></div>
                                    <div class="col-8 text-right" style="padding-top: 8px;">
                                        <span id="link-shared" style="margin-right: 10px;">
                                            <img src="{{asset('/images/profile-share.svg')}}" style="width: 15px;position: relative;top: -2px;"/> @lang('home.share')
                                        </span>
                                        <span class="btn-like-listing" data-id="{{$post->id}}" data-lang="@lang('home.like')" onclick="btnHeartLike(this, 1)">
                                            @if($post->like_user==(Auth::user()->id??-1))
                                                <img src="{{asset('/images/like-filled.svg')}}" style="width: 15px;"/> 
                                                <span class="pr-3">
                                                    @if($post->rate<=0)
                                                        @lang('home.like')
                                                    @else 
                                                        {{$post->rate}}
                                                    @endif
                                                </span>
                                            @else
                                                <img src="{{asset('/images/like-outlined.svg')}}" style="width: 15px;"/>
                                                <span>
                                                    @if($post->rate<=0)
                                                        @lang('home.like')
                                                    @else 
                                                        {{$post->rate}}
                                                    @endif
                                                </span>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="title flex-wrap col-10 ml-1">
                                    <h1>{{Str::limit(($post->title??''))}}</h1>
                                </div>
                                <div class="col-1 like reporting_single">
                                    <a id="dropdownMenuRe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <svg  height="20" width="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="nonzero" d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z" fill="#57585a"></path>
                                        </svg>
                                    </a>
                                    <div class="dropdown-menu report-listing-single" aria-labelledby="dropdownMenuRe" style="left: 0px!important;">
                                        @if($post->created_by==(Auth::user()->id??0))
                                            <a href="{{url('/post/list')}}"><button class="dropdown-item"><i class="material-icons">view_list</i>@lang('home.list')</button></a>
                                            <a href="{{url('/post/createNew/'.$post->id??0)}}"><button class="dropdown-item"><i class="material-icons">edit</i>@lang('home.edit_listing')</button></a>
                                            <button class="dropdown-item panorama_wide_angle" onclick="updateStatus({{$post->id}},  0, '{{Auth::user()->name}}');" data-status="{{$post->status}}"><i class="material-icons">panorama_wide_angle</i>@lang('home.disabled')</button>
                                            <button class="dropdown-item delete_listing"      onclick="deleteListing({{$post->id}}, -1, '{{Auth::user()->name}}');"><i class="material-icons">delete</i>@lang('home.delete')</button>
                                        @else
                                             <button class="dropdown-item btn-report-listing" onclick="btnReportListing({{$post->id}})" type="button">@lang('home.report_listing')</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="text-left mt-4 mb-3 ml-1">
                                <a href="{{url('https://www.google.com/maps/place/'.($post->location_name??'').', Cambodia')}}"
                                    target="_blank">
                                    <img src="{{asset('/images/location_v3.svg')}}"
                                        style="width: 26px!important;" /><span class="icon-label">{{($lang=='kh')?($post->location_name_kh??''):($post->location_name??'')}}</span>
                                </a>
                            </div>
                           
                            <div class="text-left mt-4 mb-3 ml-1">
                                @if($post->deal_method && $post->deal_method->meet_up)
                                    <a href="#">
                                         <img src="{{asset('/images/listing_details_v7.svg')}}" style="width: 26px!important;" /><span class="icon-label">@lang('home.meet_up')</span>
                                    </a>
                                @endif
                            </div>
                            <div class="text-left mt-4 mb-3 ml-1">
                                @if($post->deal_method && $post->deal_method->mail_delivery)
                                    <a href="#">
                                         <img src="{{asset('/images/listing_details_v7.svg')}}" style="width: 26px!important;" /><span class="icon-label">@lang('home.mailing')</span>
                                    </a>
                                @endif
                            </div>

                            <div class="text-left mt-4 mb-3 ml-1">
                                @isset($post->condition)
                                    <a href="#">
                                        <img src="{{asset('/images/condition_v3.svg')}}" style="width: 26px!important;" /><span class="icon-label">@if($post->condition=='NEW')@lang('home.new')@else($post->condition=='USED')@lang('home.used')@endif</span>
                                    </a>
                                @endisset
                            </div>
                            
                            <div class="mt-4 mb-10 ml-1 description-single-page">{{Str::limit(($post->description??''), $limit = 285, $end = '')}}<span class="d-none" id="more_str">{{Str::substr(($post->description??''), 285)}}</span> @if(strlen(strip_tags(($post->description??''))) > 285)<br/><a href="javascript:readMore();" class="read-more-link">@lang('home.read_more')</a>@endif
                            </div>

                            <div class="mt-4 ml-1">
                                @if($shipping && $shipping->shipping_day)
                                    <h5 class="shipping">@lang('home.shipping_lbl')</h5>
                                    <div>
                                        @if($shipping->shipping_day)
                                            @lang('home.package_lbl') {{$shipping->shipping_day}} @lang('home.days') · @lang('home.fee'){{$shipping->shipping_fee}}
                                        @endif
                                    </div>
                                @endif
                                
                                @if($payment)
                                    <h5 class="shipping">@lang('home.payment_lbl')</h5>
                                    <div class="payment-icons">
                                        <div class="payment-icons-title">
                                            @lang('home.protection')
                                        </div>
                                        @if($payment->cash)
                                            <img src="{{asset('images/payments/cash.png')}}"  width="80px"/>
                                        @endif
                                        @if($payment->aba)
                                            <img src="{{asset('images/payments/aba.png')}}"  width="80px"/>
                                        @endif
                                        @if($payment->wing)
                                            <img src="{{asset('images/payments/wing.png')}}" width="70px"/>
                                        @endif
                                        @if($payment->true_money)
                                            <img src="{{asset('images/payments/true.png')}}" width="128px"/>
                                        @endif
                                    </div>
                                @endif
                                <p>
                                    @if( strlen($post->address->email_verified_at) > 0)
                                        <img  src="{{asset('images/verification-email.svg')}}" width="20px"/>
                                    @endif
                                    @if(strlen ($post->address->uphone) > 0)
                                        <img  src="{{asset('images/verification-mobile.svg')}}" width="20px"/>
                                    @endif

                                    @if($post->address->facebook_id)
                                        <img  src="{{asset('images/verification-facebook.svg')}}" width="20px"/>
                                    @endif

                                    @if(strlen($post->address->email_verified_at) > 0 || strlen($post->address->uphone)> 0)
                                        @lang('home.verified')
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="comments d-none">
                <div class="card" style="margin-top: 20px; border: none;">
                    <div class="card-header">
                        <h3>Comments</h3>
                    </div>
                    <div class="card-body">
                        <div class="media col-md-12">
                            <div class="fb-comments" data-href="{{url('/post/detail/'.($post->id??''))}}" data-mobile="true" data-numposts="5"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    // Define data for the popup
    var data = [];

    var items = $('#image_items li').find('img');

    for(var i =0; i < items.length; i++){
        data.push( {userAvatarUrl_img: $(items[i]).attr('data-origin'), title: ""});
    }


    //console.log(data);
    // initalize popup
    var magnificPopup = $.magnificPopup.instance; 
    $('#image-display').magnificPopup({
        key: 'my-popup',
        preloader: true,
        items: data,
        index: 0,
        type: 'inline',
        inline: {
            // Define markup. Class names should match key names.
            markup: '<div class="white-popup"><div class="mfp-close"></div>' +
            '<a class="mfp-userWebsite">' +
            '<div class="mfp-userAvatarUrl"></div>' +
             '<div class="mfp-title"></div>'+
            '</a>' +
            '</div>' },

        gallery: {
            enabled: true,
            tCounter: '%curr% of %total%'
        },

        callbacks: {
            markupParse: function (template, values, item) {},
            open: function() {
                var i = Number($('#image-display').attr('data-index'));
                if(i > 0){
                    magnificPopup.goTo(i);
                }
            }
        } 
    });

    

    function readMore(){
        $('#more_str').addClass('d-block');
        $('.read-more-link').addClass('d-none');
    }

    $('#make-offer').click(function(){
        var p = $(this);
        var btn = $('<button type="button" class="btn btn-domrey">@lang("home.send")</button>');
        var text = $('<input type="number" class="form-control"/>');
        modal.find('.modal-title').text("@lang('home.make_offer')");
        modal.find('.modal-body').html(text);
        modal.find('.modal-footer').html(btn);
        text.val(p.attr('data-id'));
        btn.click(function(){
            window.location.href = p.attr('data-url')+ '/' + text.val();
        });

        $('#exampleModalCenter').modal({});
    });


    $('#link-shared').click(function(ent){
        modal.find('.modal-title').text("@lang('home.share')");
        var html = `<div class="mb-2 shared-social"><a href="https://www.facebook.com/sharer/sharer.php?u={{url('/post/detail/'.($post->id??''))}}" target="_blank"><img src="{{asset('images/share-fb.svg')}}"/> <span class="ml-2">Facebook</span></a></div>
            <div class="mb-2 shared-social"><a href="https://twitter.com/intent/tweet?url={{url('/post/detail/'.($post->id??''))}}&text={{($post->title??'')}}" target="_blank"><img src="{{asset('images/share-twitter.svg')}}"/> <span class="ml-2">Twitter</span></a></div>
            <div class="mb-2 shared-social copy-link"><img src="{{asset('images/share-link.svg')}}"/> <span class="ml-2">Copy link</a></div>    
        `;

        modal.find('.modal-body').html(html);
        modal.find('.modal-footer').html('');
        modal.find('.modal-footer').removeClass('modal-footer');
        $('#exampleModalCenter').modal({});

        modal.find('.modal-body .copy-link').click(function(ent){
            var temp = $(`<input type="text" value="{{url('/post/detail/'.($post->id??''))}}"/>`);
            modal.find('.modal-body').append(temp);
            temp.select();
            document.execCommand("copy");
            temp.remove();
            $(this).find('span').text('Link Copied!');
        
    });

});

$(function() {
    $('.grid_balckground_img_500').imageloader({
        background: true,
        callback: function (elm) {
            $(elm).slideDown('slow');
        }
    });


    $('.image-items').imageloader(
      {
        //selector: '.defferred',
        callback: function (elm) {
          $(elm).slideDown();
        }
      }
    );

});

</script>
@endsection
