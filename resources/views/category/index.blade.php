@extends('layouts.app')
@section('title', ($lang=='kh'?($category->name_kh??''):($category->name??'')))
@section('content')

<div class="category-page">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('home.home')</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{($lang=='kh'?($category->name_kh??''):($category->name??''))}}</li>
        </ol>
    </nav>
    <input type="hidden"  id="category_id" value="{{$category->id??''}}"/>
    <div class="domrey-block">
        <h3>@lang('home.buy_des') {{($lang=='kh'?($category->name_kh??''):($category->name??''))}} @lang('home.domrey_des')</h3>
        {{$category->description??''}}
    </div>
    <div class="row">
        <div class="col-md-3 domrey-sm">
            <h2>@lang('home.filter')</h2>
            <form>
                <div class="form-group">
                  <select id="location_id" class="form-control">
                    <option value="0">@lang('home.location_select')</option>
                    @foreach($zones as $z)
                        <option value="{{$z->zone_id}}">{{($lang=='kh')?$z->name_kh:$z->name}}</otpion>
                    @endforeach
                  </select>
                </div>
                <div class="sidebar-title">
                   @lang('home.condition')
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" checked class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">@lang('home.new')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" checked class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">@lang('home.used')</label>
                </div>

                <div class="sidebar-title">
                    @lang('home.price')
                </div>
                <div class="form-group">
                    <input type="number" id="minimum-price" class="form-control" placeholder="@lang('home.from_min')">
                </div>
                <div class="form-group">
                    <input type="number" id="maximum-price" class="form-control" placeholder="@lang('home.to_max')">
                </div>
                <div class="form-group">
                    <button type="button" id="btn-search-go" class="btn btn-domrey col-sm-12 col-md-3">@lang('home.go')</button>
                </div>
            </form>

        </div>
        <div class="col-md-9">
            <div class="product-content">
                <div class="col-12 box-sadow">
                    <div class="row category-content" id="category-content">
                        @foreach($posts as $post)
                            <div class="col-6 col-md-4 col-lg-3 col-xl-3 text-center products">
                                <div class="imag-product">
                                    <div class="avatar">
                                        <div class="row">
                                            <div class="col-3 col-md-2 text-left">
                                            <a href="{{url('/sellers/'.$post->uuser_id)}}">
                                                <img src="{{asset('/images/users/150x150/'.$post->uuser_photo)}}?t={{time()}}" alt="" class="circle responsive-img"/>
                                            </a>
                                            </div>
                                            <div class="col-9 col-md-10 user-avatar text-left">
                                                <a href="{{url('/sellers/'.$post->uuser_id)}}">
                                                    <span class="name">{{$post->uuser_id}}</span><br />
                                                    <span class="time">
                                                        @if($post->Y>0)
                                                            {{$post->Y}} @lang('home.years_ago')
                                                        @elseif($post->MO >0)
                                                            {{$post->MO}} @lang('home.months_ago')
                                                        @elseif($post->D >0)
                                                            @if($post->H>12)
                                                                {{$post->D + 1}} @lang('home.days_ago')
                                                            @else
                                                                {{$post->D}} @lang('home.days_ago')
                                                            @endif
                                                        @elseif($post->H >0)
                                                            {{$post->H}} @lang('home.hours_ago')
                                                        @elseif($post->M >0 )
                                                            {{$post->M}} @lang('home.minutes_ago')
                                                        @elseif($post->S >0 )
                                                            {{$post->S}} @lang('home.seconds_ago')
                                                        @endif

                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="thumbnail-domrey">
                                        <a href="{{url('/post/detail/'.$post->id)}}">
                                            <div class="grid_balckground_img" 
                                            style="background-image: url('{{asset('images/loading.svg')}}');"
                                            data-src="{{asset('/images/products/500x500/'.$post->image)}}">
                                            
                                            </div>
                                        </a>
                                    </div>
                                    <div class="text-desc text-left">
                                        <div class="title">
                                            <a href="{{url('/post/detail/'.$post->id)}}">
                                                {{Str::limit($post->title, $limit = 30, $end = '...')}}
                                            </a>
                                        </div>
                                        <div class="price-font">
                                            ${{number_format($post->price)}}
                                        </div>
                                        <div class="description">
                                            <a href="{{url('/post/detail/'.$post->id)}}">
                                                {{Str::limit($post->description, $limit = 30, $end = '...')}}
                                            </a>
                                        </div>
                                        @if($post->condition=='USED')
                                            <div class="">
                                                @lang('home.used')
                                            </div>
                                        @else
                                            <div class="">
                                                @lang('home.new')
                                            </div>
                                        @endif
                                        @csrf
                                        <div class="like">
                                            <a class="btn-like-listing" data-id="{{$post->id}}" onclick="btnHeartLike(this)">
                                                @if($post->like_user==(Auth::user()->id??-1))
                                                    <img src="{{asset('/images/like-filled.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$post->rate>0?$post->rate:''}}</span>
                                                @else
                                                    <img src="{{asset('/images/like-outlined.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$post->rate>0?$post->rate:''}}</span>
                                                @endif
                                            </a>
                                            <a id="dropdownMenuRe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg  height="16" width="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" class="_3EdKwhRm4f float-right">
                                                    <path fill-rule="nonzero" d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z" fill="#57585a"></path>
                                                </svg>
                                            </a>
                                            <div class="dropdown-menu report-listing" aria-labelledby="dropdownMenuRe">
                                                <button class="dropdown-item btn-report-listing" onclick="btnReportListing({{$post->id}})" type="button">@lang('home.report_listing')</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                        @if(count($posts)<=0)
                            <div class="">@lang('home.no_listing')</div>
                        @endif
                    </div>
                    @if($total_item > $limit_item)
                        <div class="row">
                            <div class="col-12 text-center" style="margin-top:20px;">
                                <button type="button" id="view_more" class="btn btn-outline-secondary mb-2" data-cid="{{$category->id}}" data-lid="{{$post->id}}">@lang('home.view_more')</button>
                            </div>
                        </div>
                    @endif
                </div>
                <!-- <div class="text-center" style="margin-top: 40px;">
                    <button type="button" class="btn btn-outline-secondary">Load more...</button>
                </div> -->
            </div>

        </div>
    </div>
</div>
 <script>
    $('#view_more').click(function(){
        var formData = new FormData();
        var lid= $(this).attr('data-lid');
        var cid= $(this).attr('data-cid');
        $.ajax({
            url: '/post/getLastIdByCategory/' + cid +'/'+ lid,
            type: 'GET',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                 $('#view_more').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            },
            complete: function () {
                $('#view_more').html("@lang('home.view_more')");
            },
            success: function (json) {
                if(json.lid > 0){
                    $('#view_more').attr('data-lid', json.lid);
                    loadContent(cid, lid);
                }

                if(json.count < json.limit){
                    $('#view_more').addClass('d-none');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });


        var loadContent = function(cid, lid){
            $.ajax({
                url: '/post/viewMoreByCategory/' + cid +'/'+ lid,
                type: 'GET',
                data: {
                },
                dataType: 'html',
                beforeSend: function () {
                
                },
                complete: function () {

                },
                success: function (datahtml) {
                    $('#category-content').append(datahtml);
                    $('#category-content .grid_balckground_img').imageloader({
                        background: true,
                        callback: function (elm) {
                            $(elm).slideDown('slow');
                        }
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
    };

    
    });


     $(function() {
        $('.grid_balckground_img').imageloader({
            background: true,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });
    });
    
</script>

@endsection