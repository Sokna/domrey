@extends('layouts.app')
@section('title', __('home.search_for').' '.$text)
@section('content')

<div class="category-page">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">@lang('home.home')</li>
            <li class="breadcrumb-item"><a href="{{url('/')}}">@lang('home.search_for') "{{$text}}"</a></li>
        </ol>
    </nav>
    <p>
        <h3>@lang('home.search_for') "{{$text}}"</h3>
    </p>
    <div class="row">
        <div class="col-md-3">
            <form>
                <div class="form-group">
                    <select id="location_id" class="form-control">
                        <option value="0">@lang('home.location_select')</option>
                        @foreach($zones as $z)
                        <option value="{{$z->zone_id}}">{{$lang=='kh'?$z->name_kh:$z->name}}</otpion>
                        @endforeach
                    </select>
                </div>
                <div class="sidebar-title">
                    @lang('home.condition')
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" checked class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">@lang('home.new')</label>
                </div>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" checked class="custom-control-input" id="customCheck2">
                    <label class="custom-control-label" for="customCheck2">@lang('home.used')</label>
                </div>

                <div class="sidebar-title">
                    @lang('home.price')
                </div>
                <div class="form-group">
                    <input type="number" id="minimum-price" class="form-control" placeholder="@lang('home.from_min')">
                </div>
                <div class="form-group">
                    <input type="number" id="maximum-price" class="form-control" placeholder="@lang('home.to_max')">
                </div>
                <div class="form-group">
                    <button type="button" id="btn-search-go" class="btn btn-domrey">@lang('home.go')</button>
                </div>
            </form>


        </div>

        <div class="col-md-9">
            <div class="product-content">
                <div class="col-12 box-sadow">
                    <div class="row category-content" id="category-content">
                        @foreach($posts as $post)
                        <div class="col-6 col-md-4 col-lg-3 col-xl-3 text-center products">
                            <div class="imag-product">
                                <div class="avatar">
                                    <div class="row">
                                        <div class="col-3 col-md-2 text-left">
                                            <a href="{{url('/sellers/'.$post->uuser_id)}}">
                                                <img src="{{asset('/images/users/150x150/'.$post->uuser_photo)}}?t={{time()}}" alt=""
                                                    class="circle responsive-img" />
                                            </a>
                                        </div>
                                        <div class="col-9 col-md-10 user-avatar text-left">
                                            <a href="{{url('/sellers/'.$post->uuser_id)}}">
                                                <span class="name">{{$post->uuser_id}}</span><br />
                                                <span class="time">
                                                    @if($post->Y>0)
                                                        {{$post->Y}} @lang('home.years_ago')
                                                    @elseif($post->MO >0)
                                                        {{$post->MO}} @lang('home.months_ago')
                                                    @elseif($post->D >0)
                                                        @if($post->H>12)
                                                            {{$post->D + 1}} @lang('home.days_ago')
                                                        @else
                                                            {{$post->D}} @lang('home.days_ago')
                                                        @endif
                                                    @elseif($post->H >0)
                                                        {{$post->H}} @lang('home.hours_ago')
                                                    @elseif($post->M >0 )
                                                        {{$post->M}} @lang('home.minutes_ago')
                                                    @elseif($post->S >0 )
                                                        {{$post->S}} @lang('home.seconds_ago')
                                                    @endif
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="thumbnail-domrey">
                                    <a href="{{url('/post/detail/'.$post->id)}}">
                                        <!--<img src="{{asset('/images/products/150x150/'.$post->image)}}" class="portrait" />-->
                                        <div class="grid_balckground_img" style="background-image: url('{{asset('/images/products/500x500/'.$post->image)}}')"></div>
                                    </a>
                                </div>
                                <div class="text-desc text-left">
                                    <div class="title">
                                        <a href="{{url('/post/detail/'.$post->id)}}">
                                            {{Str::limit($post->title, $limit = 30, $end = '...')}}
                                        </a>
                                    </div>
                                    <div class="price-font">
                                        ${{number_format($post->price)}}
                                    </div>
                                    <div class="description">
                                        <a href="{{url('/post/detail/'.$post->id)}}">
                                            {{Str::limit($post->description, $limit = 30, $end = '...')}}
                                        </a>
                                    </div>
                                    @if($post->condition=='USED')
                                    <div class="">
                                         @lang('home.used')
                                    </div>
                                    @else
                                    <div class="">
                                        @lang('home.new')
                                    </div>
                                    @endif
                                    <div class="like">
                                        <a class="btn-like-listing" data-id="{{$post->id}}" onclick="btnHeartLike(this)">
                                            @if($post->like_user==(Auth::user()->id??-1))
                                                <img src="{{asset('/images/like-filled.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$post->rate>0?$post->rate:''}}</span>
                                            @else
                                                <img src="{{asset('/images/like-outlined.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$post->rate>0?$post->rate:''}}</span>
                                            @endif
                                        </a>
                                        <a id="dropdownMenuRe" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                            <svg height="16" width="16" viewBox="0 0 16 16"
                                                xmlns="http://www.w3.org/2000/svg" class="float-right">
                                                <path fill-rule="nonzero"
                                                    d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z"
                                                    fill="#57585a"></path>
                                            </svg>
                                        </a>
                                        <div class="dropdown-menu report-listing" aria-labelledby="dropdownMenuRe">
                                            <button class="dropdown-item btn-report-listing"
                                                type="button" onclick="btnReportListing({{$post->id}})">@lang('home.report_listing')</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach
                        @if(count($posts)==0)
                        <div class="text-center" style="margin: 0 auto">
                            <i class="material-icons" style="font-size: 100px;color: #f3bd86;">search</i><br />
                            <strong>@lang('home.search_not_found')</strong><br />
                            @lang('home.search_not_found_1')
                        </div>
                        @endif
                    </div>

                </div>
            </div>
        </div>

    </div>
    <script>
        var itemW = $('.thumbnail-domrey').width();
        var itemWd = $('.thumbnail-domrey-detail').width();
        if(itemW > 0){
            $('.thumbnail-domrey').css('height', itemW);
        }

        if (itemWd > 0) {
            $('.thumbnail-domrey-detail').css('height', itemWd);
        }


        $(function() {
            $('.grid_balckground_img').imageloader({
                background: true,
                callback: function (elm) {
                    $(elm).slideDown('slow');
                }
            });
        });

    </script>
    @endsection