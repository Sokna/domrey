@extends('layouts.app')
@section('title', __('home.account_setting'))
@section('content')
<div class="category-page domrey-container">
    @include('layouts.breadcrumb', ['dashboard' => __('home.profile'), 'page_title' => __('home.account_setting')])

    <div class="row">
        <div class="col-md-3">
           @include('layouts.nav')
        </div>
        <div class="col-md-9">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card" id="form-account">
                        <div class="card-header"><h5>@lang('home.account_setting')</h5></div>
                        <div class="card-body" id="form-account">
                            <input type="hidden" id="user_id" value="{{$user->id}}"/>
                            <div class="form-group text-center col-md-4">
                                <div id="upload-demo" data-url="{{asset('images/users/150x150') }}/{{$user->photo??'avata.png'}}?t={{time()}}"></div>
                                <button type="button" class="btn btn-domrey" id="profile-image">@lang('home.change')</button>
                                <!-- <a href="{{url('/sellers/'.$user->id.'/'.$user->uuserid)}}">{{url('/sellers/'.$user->id.'/'.$user->uuserid)}}</a> -->
                                <input type="file"  id="change-profile" accept="image/png, image/jpeg" class="d-none"/>
                                
                            </div>

                            <div class="form-group">
                                <label for="title" class="font-weight-bold">Name<span
                                        class="text-danger"> *</span>
                                </label>
                                <input type="text" id="name" class="form-control col-md-4" value="{{$user->name}}"/>
                                <div class="invalid-feedback">
                                    Name is required
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="title" class="font-weight-bold">@lang('home.name')<span
                                        class="text-danger"> *</span>
                                </label>
                                <input type="text" id="username" class="form-control col-md-4 is-valid" value="{{$user->uuserid}}" readonly/>
                                <div class="invalid-feedback">
                                    Username is required
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="font-weight-bold">@lang('home.phone_number')<span
                                        class="text-danger"> *</span>
                                </label>
                                <div class="input-group">
                                    <input type="text" id="phone" class="form-control col-md-4 {{$user->phone?'is-valid':'is-invalid'}}" value="{{$user->phone}}"/>
                                    <div class="input-group-prepend d-none">
                                        <button class="btn btn-domrey ml-1" id="inputGroupPhone">@lang('home.verify')</button>
                                    </div>
                                </div>

                                <div class="invalid-feedback">
                                    You don't verify your phone number yet
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="font-weight-bold">@lang('home.email1')<span
                                        class="text-danger"> *</span>
                                </label>
                                <div class="input-group">
                                    <input type="text" class="form-control col-md-4 {{$user->email_verified_at?'is-valid':'is-invalid'}}" id="email" value="{{$user->email}}"/> 
                                    <div class="input-group-prepend">
                                        <form class="d-inline" method="POST" action="{{url('email/resend')}}">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                            <button class="btn btn-domrey ml-1" id="inputGroupPrepend">@lang('home.verify')</button>
                                        </form>
                                    </div>
                                     <div class="invalid-feedback">
                                        @lang('home.email_verify')
                                    </div>
                                </div>
                               
                            </div>

                            <div class="form-group row">

                                <div class="col-sm-6 col-md-4">
                                    <button type="button" id="btn-profile-account" class="btn btn-domrey col-sm-12 col-md-4" data-lang="@lang('home.save')">
                                        @lang('home.save')
                                    </button>
                                </div>
                                <div class="col-sm-6 col-md-4 mt-3 text-left">
                                    <a href="{{url('password/change')}}" class="domrey-link">@lang('home.change_password')</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalChangePassowrd" tabindex="-1" role="dialog" aria-labelledby="modalChangePassowrd" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Change Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="form-account-password">
        <div class="form-group">
            <label for="title" class="font-weight-bold">Current Password<span class="text-danger">*</span></label>
            <input type="password" id="current_password" class="form-control" value=""/>
            <div class="invalid-feedback">
                Current password is required
            </div>
        </div>
         <div class="form-group">
            <label for="title" class="font-weight-bold">New Password<span class="text-danger">*</span></label>
            <input type="password" id="new_password" class="form-control" value=""/>
            <div class="invalid-feedback newpwd-error">
                New password is required
            </div>
        </div>

         <div class="form-group">
            <label for="title" class="font-weight-bold">Comfirm Password<span class="text-danger">*</span></label>
            <input type="password" id="confirm_password" class="form-control" value=""/>
            <div class="invalid-feedback confirm-error">
                Confirm password is required
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="btn-profile-password" class="btn btn-domrey">Save</button>
        
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
    // var resize = $('#upload-demo').croppie({
    //     url: "{{ asset('images/users/150x150') }}/{{$user->photo??'avata.png'}}",
    //     enableExif: true,
    //     enableOrientation: true,    
    //     viewport: { // Default { width: 100, height: 100, type: 'square' } 
    //         width: 150,
    //         height: 150,
    //         type: 'circle' //circle square
    //     },
    //     boundary: {
    //         width: 150,
    //         height: 150
    //     }
    // });


    // $('#change-profile').on('change', function () { 
    //     var reader = new FileReader();
    //         reader.onload = function (e) {
    //         resize.croppie('bind',{
    //             url: e.target.result
    //         }).then(function(){
    //             //console.log('jQuery bind complete');
    //         });
    //         }
    //         reader.readAsDataURL(this.files[0]);
    // });

</script>

@endsection
