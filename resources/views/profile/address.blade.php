@extends('layouts.app')
@section('title', __('home.my_address'))
@section('content')
<div class="category-page domrey-container">
    @include('layouts.breadcrumb', ['dashboard' => __('home.profile'), 'page_title' => __('home.my_address')])

    <div class="row">
        <div class="col-md-3">
           @include('layouts.nav')
        </div>
        <div class="col-md-9">
              <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header"><h5>@lang('home.my_address')</h5></div>
                        <div class="card-body" id="form-addrress">

                            <nav>
                                <div class="nav nav-tabs" id="nav-tab-address" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-address-tab" data-toggle="tab" href="#nav-address" role="tab"
                                        aria-controls="nav-address" aria-selected="true">@lang('home.general_info')</a>
                                    <a class="nav-item nav-link" id="nav-paymnent-tab" data-toggle="tab" href="#nav-paymnent" role="tab"
                                        aria-controls="nav-paymnent" aria-selected="false">@lang('home.payment_method')</a>

                                    <a class="nav-item nav-link" id="nav-shipping-tab" data-toggle="tab" href="#nav-shipping" role="tab"
                                        aria-controls="nav-shipping" aria-selected="false">@lang('home.shipping')</a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabAdress" style="margin-top:10px">
                                <div class="tab-pane fade show active" id="nav-address" role="tabpanel" aria-labelledby="nav-address-tab">
                                    <input type="hidden" id="address_id" value="{{$address->id??0}}"/>
                                    <div class="form-group">
                                        <label for="title" class="font-weight-bold">@lang('home.first_name')<span
                                                class="text-danger"> *</span>
                                        </label>
                                        <input type="text" id="first_name" class="form-control col-md-4" value="{{$address->first_name??''}}"/>
                                        <div class="invalid-feedback">
                                            First Name is required
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="title" class="font-weight-bold">@lang('home.last_name')<span
                                                class="text-danger"> *</span>
                                        </label>
                                        <input type="text" id="last_name" class="form-control col-md-4" value="{{$address->last_name??''}}"/>
                                        <div class="invalid-feedback">
                                            Last Name is required
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="title" class="font-weight-bold">@lang('home.gender')<span
                                                class="text-danger"> *</span>
                                        </label>
                                        <select id="gender" class="form-control col-md-2" />
                                            <option></option>
                                            @isset($address->gender)
                                                @if($address->gender=='M')
                                                    <option value="M" selected>Male</option>
                                                    <option value="F">Female</option>
                                                @else
                                                    <option value="M">Male</option>
                                                    <option value="F" selected>Female</option>
                                                @endif
                                            @else
                                                <option value="M">Male</option>
                                                <option value="F">Female</option>
                                            @endisset
                                        
                                        </select>
                                        <div class="invalid-feedback">
                                            Gender Number is required
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="title" class="font-weight-bold">@lang('home.phone_number')<span
                                                class="text-danger"> *</span>
                                        </label>
                                        <input type="text" id="phone" class="form-control col-md-3" value="{{$address->phone??''}}" placeholder="@lang('home.phone_number')"/>
                                        <div class="invalid-feedback">
                                            Phone Number is required
                                        </div>
                                        <input type="text" id="phone_number1" class="form-control col-md-3 mt-2" value="{{($address->phones && $address->phones->phone_number1 )? $address->phones->phone_number1:''}}" placeholder="@lang('home.phone_number1')"/>

                                        <input type="text" id="phone_number2" class="form-control col-md-3 mt-2" value="{{($address->phones && $address->phones->phone_number2 )? $address->phones->phone_number2:''}}"placeholder="@lang('home.phone_number2')"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="title" class="font-weight-bold">@lang('home.email1')<span
                                                class="text-danger"> *</span>
                                        </label>
                                        <input type="text" id="email" class="form-control col-md-3" value="{{$address->email??''}}"/>
                                        <div class="invalid-feedback">
                                            Email is required
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="title" class="font-weight-bold col-12">@lang('home.address')<span
                                                class="text-danger"> *</span>
                                        </label>
                                        <div class="emoji-span emoji-span-profile">
                                            <input type="text" id="address" class="form-control col-12 ml-1" value="{{$address->address??''}}"/>
                                        </div>
                                        <div class="invalid-feedback">
                                            Address is required
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="title" class="font-weight-bold col-12">@lang('home.about')<span
                                                class="text-danger"> *</span>
                                        </label>
                                        <div class="emoji-span emoji-span-profile">
                                            <textarea id="about" class="form-control col-12 ml-1" rows="10">{{$address->about??''}}</textarea>
                                        </div>
                                        <div class="invalid-feedback">
                                            About is required
                                        </div>
                                    </div>

                                    
                                 
                                    <!-- address -->
                                </div>
                                <div class="tab-pane fade" id="nav-paymnent" role="tabpanel" aria-labelledby="nav-paymnent-tab">
                                    <label for="title" class="font-weight-bold">@lang('home.select_payment')</label>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="cash" {{($address->payment_method && $address->payment_method->cash )?'checked':''}}/>
                                            <label class="custom-control-label" for="cash">Cash Delivery</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="aba" {{($address->payment_method && $address->payment_method->aba )?'checked':''}}/>
                                            <label class="custom-control-label" for="aba">ABA BANK</label>
                                            <input type="text" id="aba_acc" class="form-control col-sm-12 col-md-3" placeholder="@lang('home.bank_account')" value="{{($address->payment_account && $address->payment_account->aba_acc )?$address->payment_account->aba_acc:''}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="wing" {{($address->payment_method && $address->payment_method->wing )?'checked':''}}/>
                                            <label class="custom-control-label" for="wing">WING</label>
                                            <input type="text" id="wing_acc" class="form-control col-sm-12 col-md-3" placeholder="@lang('home.phone_number')" value="{{($address->payment_account && $address->payment_account->wing_acc )?$address->payment_account->wing_acc:''}}"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="true_money" {{($address->payment_method && $address->payment_method->true_money )?'checked':''}}/>
                                            <label class="custom-control-label" for="true_money">True Money</label>
                                            <input type="text" id="true_acc" class="form-control col-sm-12 col-md-3" placeholder="@lang('home.phone_number')" value="{{($address->payment_account && $address->payment_account->true_money_acc )?$address->payment_account->true_money_acc:''}}"/>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade" id="nav-shipping" role="tabpanel" aria-labelledby="nav-shipping-tab">
                                    <div class="form-group">
                                        <label for="title" class="font-weight-bold" style="text-transform: capitalize;">@lang('home.days')</label>
                                        <input type="number" id="shipping_day" placeholder="Days shipping" class="form-control col-md-4" value="{{($address->shipping && $address->shipping->shipping_day)?$address->shipping->shipping_day:''}}"/>
                                    </div>
                                     <div class="form-group">
                                        <label for="title" class="font-weight-bold">@lang('home.fee')</label>
                                        <input type="number" id="shipping_fee" placeholder="Fee of charge $" class="form-control col-md-4" value="{{($address->shipping && $address->shipping->shipping_fee )? $address->shipping->shipping_fee:''}}"/>
                                    </div>
                                </div>

                                <div class="form-group text-left">
                                    <button type="button" id="btn-profile-address" class="btn btn-domrey col-sm-2" data-lang="@lang('home.save')">
                                        @lang('home.save')
                                    </button>
                                </div>

                            </div>

                         

                         </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
 <script type="text/javascript">//<![CDATA[

    window.onload=function(){
		$(document).ready(function() {
            $("#address").emojioneArea({
				pickerPosition: "right",
				tonesStyle: "bullet",
				events: {
					keyup: function (editor, event) {
						console.log(editor.html());
						console.log(this.getText());
					}
				}
            });
            
			$("#about").emojioneArea({
				pickerPosition: "right",
				tonesStyle: "bullet",
				events: {
					keyup: function (editor, event) {
						console.log(editor.html());
						console.log(this.getText());
					}
				}
			});
		});

    }
  //]]></script>
@endsection