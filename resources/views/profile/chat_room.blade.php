@extends('layouts.app')
@section('title', __('home.message'))
@section('content')
<div class="category-page domrey-container">
    @include('layouts.breadcrumb', ['dashboard' => __('home.profile'), 'page_title' => __('home.message')])

    <div class="row">
        <div class="col-md-3">
            @include('layouts.nav')
        </div>
        <div class="col-md-9">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>@lang('home.message')</h5>
                        </div>
                        <div class="card-body" id="form-chat-room">
                            @if(count($chat_rooms)<=0) <div class="no-item">@lang('home.no_message')</div>
                        @endif
                        <table class="table table-sm">
                            <tbody>
                                @foreach($chat_rooms as $room)
                                <tr style="cursor: pointer;" onClick="gotToChat({{$room->id}},{{$room->chated_by}})">
                                    <td>
                                        @if($room->owner_id == Auth::user()->id)
                                        <div class="chat-message">
                                            <i class="material-icons" style="color: #f3be89;">inbox</i>
                                            <strong style="position: relative;top: -8px;">@lang('home.selling')</strong>
                                        </div>
                                        @else
                                        <div class="chat-message">
                                            <i class="material-icons" style="color: #f3be89;">send</i>
                                            <strong style="position: relative;top: -8px;">@lang('home.buying')</strong>
                                        </div>
                                        @endif

                                    </td>
                                    <td>

                                        <div class="chat-message">
                                            @if($room->owner_id == Auth::user()->id)
                                            <img src="{{asset('images/users/150x150/'.$room->photo)}}"
                                                class="rounded-circle border border-warning"
                                                style="width: 50px; height: 50px;" />
                                            <strong>{{$room->name}}</strong>
                                            @else
                                            <img src="{{asset('images/users/150x150/'.$room->photo_owner)}}"
                                                class="rounded-circle border border-warning"
                                                style="width: 50px; height: 50px;" />
                                            <strong>{{$room->name_owner}}</strong>
                                            @endif
                                        </div>

                                    </td>
                                    <td>

                                        @if($room->owner_id == Auth::user()->id)
                                        <div class="thumbnail-domrey-70 mr-2 float-left">
                                            <div class="grid_balckground_img_70"
                                                style="background-image: url('{{asset('images/loading.svg')}}');"
                                                data-src="{{asset('images/products/150x150/'.$room->image)}}"></div>
                                        </div>

                                        <div class="chat-message float-left">
                                            <strong>{{$room->title}}</strong> <br />
                                            <span>{{Str::limit($room->message, $limit = 100, $end = '...')}}</span>
                                            @if($room->unread>0)
                                            <span class="notification-1">{{$room->unread}}</span>
                                            @endif
                                        </div>
                                        @else
                                        <div class="thumbnail-domrey-70 mr-2 float-left">
                                            <div class="grid_balckground_img_70"
                                                style="background-image: url('{{asset('images/loading.svg')}}');"
                                                data-src="{{asset('images/products/150x150/'.$room->image)}}"></div>
                                        </div>

                                        <div class="chat-message float-left">
                                            <strong>{{$room->title}}</strong> <br />
                                            <span>{{Str::limit($room->message, $limit = 100, $end = '...')}}</span>
                                            @if($room->unread>0)
                                            <span class="notification-1">{{$room->unread}}</span>
                                            @endif
                                        </div>
                                        @endif
                                        </a>
                                    </td>
                                    <td>

                                        <div class="chat-message float-left">
                                            <span>{{$room->sending_time}}</span>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<script>
$(function() {
    $('.grid_balckground_img_70').imageloader({
        background: true,
        callback: function(elm) {
            $(elm).slideDown('slow');
        }
    });
});

function gotToChat(id, chated){

    location.href=`/profile/chat/${id}/${chated}`;
}
</script>
@endsection