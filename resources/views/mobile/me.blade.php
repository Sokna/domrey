@extends('layouts.appMobile')
@section('content')
<div class="item">
    <div class="row">
        <div class="col-12" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>

            <div id="custom-search-input">
                <div class="input-group search-control">
                    <span class="input-group-btn">
                        <i class="material-icons search-icon">search</i>
                    </span>
                    <input type="text" id="text-home-search" class="search-control-input" placeholder="@lang('home.search') Domrey..." />
                    <input type="hidden" id="category_id" />
                </div>
            </div>
            <div class="text-center float-right">
                <a href="{{url('myLike')}}"><img src="{{asset('/images/like-outlined.svg')}}" class="icon-bar-img mr-1" /></a>
                <a href="{{url('profile/chatRoom')}}"><img src="{{asset('/images/chat-outlined.svg')}}" class="icon-bar-img ml-1" />
                    @if($un_read>0)
                        <span class="notification">{{$un_read}}</span>
                    @endif
                </a>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12 pl-0 pr-0">
            <div class="seller-background" style="background-image: url({{ asset('images/'.($user->cover_photo?'users/'.$user->cover_photo:'web_cover.png')) }}?t={{time()}});">
            </div>
            <div class="profile-image">
                <img src="{{asset('images/loading.svg')}}" data-src="{{asset('/images/users/150x150/'.($user->photo??'avata.png'))}}?t={{time()}}"
                    class="rounded-circle image-profile" />

            </div>
        </div>
        <div class="col-12 pt-5">
            <div class="settings">
                <a href="{{ url('profile/settings') }} ">
                    <i class="material-icons">settings</i>
                </a>
            </div>
            <h5>{{$user->first_name}} {{$user->last_name}}</h5>
            <div class="pt-1">&#64;{{$user->uuserid}}</div>
            @isset($user)
            <div class="pt-2">@lang('home.joined'):
                {{$user->dayname}}
            </div>

            <div class="pt-2">
                @if(strlen($user->email_verified_at) <= 0 && strlen($user->uphone)<= 0 && !$user->facebook_id) @lang('home.not_verified') @else
                @lang('home.verified') @endif @if( strlen($user->email_verified_at) > 0)
                <img src="{{asset('images/verification-email.svg')}}" width="20px" />
                @endif

                @if(strlen ($user->uphone) > 0)
                <img src="{{asset('images/verification-mobile.svg')}}" width="20px" />
                @endif
                
                @if($user->facebook_id)
                    <img src="{{asset('images/verification-facebook.svg')}}" width="20px" />
                @endif

            </div>
            @endisset
        </div>
    </div>

    <div class="row">
        <div class="col-12 pt-2">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-listing-tab" data-toggle="tab" href="#nav-listing"
                        role="tab" aria-controls="nav-listing" aria-selected="true">
                        @lang('home.listing') ({{$total_item}})
                    </a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab"
                        aria-controls="nav-contact" aria-selected="false">
                        @lang('home.contact')
                    </a>
                    <a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab"
                        aria-controls="nav-about" aria-selected="false">
                        @lang('home.about')
                    </a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-listing" role="tabpanel"
                    aria-labelledby="nav-listing-tab">
                    <!-- Tabe listing     -->
                    <div class="row" id="list_items">
                    
                        @foreach($posts as $post)
                            <div class="col-6 col-sm-4 text-center product-item">
                                <div class='thumbnail-domrey' style="height: 150px;">
                                    <a href="{{url('/post/detail/'.$post->id)}}">
                                        <div class="grid_balckground_img"
                                            style="background-image: url('{{asset('images/loading.svg')}}');" 
                                            data-src="{{asset('/images/products/500x500/'.$post->image)}}">
                                        </div>
                                    </a>
                                </div>
                                <div class="text-desc text-left">
                                    <div class="title">
                                        <a href="{{url('/post/detail/'.$post->id)}}">
                                            {{Str::limit(Str::title($post->title), $limit = 16, $end = '...')}}
                                        </a>
                                    </div>
                                    <div class="price-font">
                                        ${{number_format($post->price)}}
                                    </div>
                                    <div class="description">
                                        <a href="{{url('/post/detail/'.$post->id)}}">
                                            {{Str::limit(Str::title($post->description), $limit = 18, $end = '...')}}
                                        </a>
                                    </div>
                                    <div class="contition pt-2 pb-4">
                                        @if($post->condition=='USED')
                                        @lang('home.used')
                                        @else
                                        @lang('home.new')
                                        @endif

                                        <div class="btn-group float-right">
                                            <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                @if($post->status==0)
                                                    <span class="disabled"><i class="material-icons">lock</i></span>
                                                @elseif($post->status==-2)
                                                    <span class="disabled"><i class="material-icons">block</i></span>
                                                @else
                                                    <span class="active"><i class="material-icons">public</i></span>
                                                @endif
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                @if($post->status==0)
                                                    <button type="button" class="dropdown-item btn-publish" onclick="updateStatus({{$post->id}},  1, '{{Auth::user()->name}}');" data-status="{{$post->status}}"><i class="material-icons">public</i> Publish</button>
                                                @elseif($post->status==-2)
                                                    <a class="dropdown-item btn-publish" href="{{url('post/listingReport/'.$post->id)}}"><i class="material-icons">info</i> Info</a>
                                                @else
                                                    <button type="button" class="dropdown-item btn-publish" onclick="updateStatus({{$post->id}},  0, '{{Auth::user()->name}}');" data-status="{{$post->status}}"><i class="material-icons">lock</i> Private</button>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <div class="edit-listing text-center">
                                        <a href="{{url('/post/createNew/'.$post->id??0)}}" class="float-left">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <span class="text-center">
                                            <img src="{{asset('/images/like-outlined.svg')}}" class="text-center" width="20px"/> 
                                            <span class="pl-0">{{$post->rate>0?$post->rate:''}}</span>
                                        </span>
                                        <a onclick="deleteListing({{$post->id}}, -1, '{{Auth::user()->name}}');" class="float-right">
                                            <i class="material-icons">delete</i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        @if(count($posts)<=0)
                            <div class="col-12 mt-3">
                                No listing yet
                            </div>
                        @endif

                    </div>

                    @isset($post)
                        @if($total_item > count($posts))
                            <div class="row">
                                <div class="col-12 mt-3 text-center">
                                    <button type="button" id="view_more" data-uid="{{$post->created_by}}" data-lid="{{$post->id}}" class="btn btn-outline-secondary">
                                        @lang('home.view_more')
                                    </button>
                                </div>
                            </div>
                        @endif
                    @endisset
                </div>
                 <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    @isset($user)
                        
                            <div class="row">
                                <div class="col-12 mt-3">
                                    <table>
                                        @if($user->phone)
                                        <tr>
                                            <td class="phones_prefix align-middle" data-val="{{$user->phone}}">
                                                <i class="material-icons domrey-color">phone</i>@if($user->phones->phone_number1){{__('1')}}@endif
                                            </td>
                                            <td class="align-middle">
                                                <a href="tel:{{$user->phone}}" class="domrey-link">{{$user->phone}}</a>
                                               
                                            </td>
                                        </tr>
                                        @endif
                                        @if($user->phones)                       
                                        @if($user->phones->phone_number1)
                                            <tr>
                                                <td class="phones_prefix align-middle" data-val="{{$user->phones->phone_number1}}">
                                                    <i class="material-icons domrey-color">phone</i>2</td>
                                                <td class="align-middle">
                                                    <a href="tel:{{$user->phones->phone_number1}}" class="domrey-link">{{$user->phones->phone_number1}}</a>
                                                </td>
                                            </tr>
                                        @endif
                                        @if($user->phones->phone_number2)
                                            <tr>
                                                <td class="phones_prefix align-middle" data-val="{{$user->phones->phone_number2}}"><i class="material-icons domrey-color">phone</i>3</td>
                                                <td class="align-middle">
                                                    <a href="tel:{{$user->phones->phone_number2}}" class="domrey-link">{{$user->phones->phone_number2}}</a>
                                                </td>
                                            </tr>
                                        @endif

                                        @endif
                                        <tr>
                                            <td class="align-middle pt-2"><i class="material-icons domrey-color">mail</i></td>
                                            <td class="align-middle">
                                                <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                            </td>
                                        </tr>
                                        @if($user->address)
                                            <tr>
                                                <td class="align-middle"><i class="material-icons domrey-color">home</i></td>
                                                <td class="align-middle">{{$user->address}}</td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                                <div class="col-12">
                                    @if($user->payment_method && ($user->payment_method->cash || $user->payment_method->aba || $user->payment_method->wing || $user->payment_method->true_money))
                                        <div class="domrey-seller-page mt-2 mb-2">
                                            <h6>@lang('home.payment_method')</h6>
                                            @if($user->payment_method->cash)
                                                <img src="{{asset('images/payments/cash.png')}}"  width="50px"/>
                                            @endif
                                            @if($user->payment_method->aba)
                                                <img src="{{asset('images/payments/aba.png')}}"  width="50px"/>
                                            @endif 
                                            @if($user->payment_method->wing)
                                                <img src="{{asset('images/payments/wing.png')}}" width="50px"/>
                                            @endif
                                            @if($user->payment_method->true_money)
                                                <img src="{{asset('images/payments/true.png')}}" width="90px"/>
                                            @endif
                                        </div>
                                    @endif
                                </div>

                                <div class="col-12">
                                    <table>
                                        @if($user->payment_account && $user->payment_account->aba_acc)
                                            <tr>
                                                <td style="width: 120px;">ABA Account</td>
                                                <td>
                                                    {{$user->payment_account->aba_acc}}
                                                </td>
                                            </tr>
                                        @endif
                                        @if($user->payment_account &&  $user->payment_account->wing_acc)
                                            <tr>
                                                <td style="width: 120px;">WING</td>
                                                <td>
                                                    {{$user->payment_account->wing_acc}}
                                                </td>
                                            </tr>
                                        @endif
                                        @if($user->payment_account && $user->payment_account->true_money_acc)
                                            <tr>
                                                <td style="width: 120px;">True Money</td>
                                                <td>
                                                    {{$user->payment_account->true_money_acc}}        
                                                </td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                       
                    @endisset
                </div>

                <div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
                    <div class="row">
                        <div class="col-12 mt-3">
                            {{$user->about??''}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
<script>
    $(function() {

        $('.image-profile').imageloader({
            background: false,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });


        $('.grid_balckground_img').imageloader({
            background: true,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });
    });


    window.onscroll = function() {
        var header = document.getElementById("search-row");
        var d = document.documentElement;
        var offset = d.scrollTop + window.innerHeight;
        var height = d.offsetHeight;
        if (offset >= height) {
            var lid = $('#view_more').attr('data-lid');
            var uid = $('#view_more').attr('data-uid');
            if (lid > 0) {
                //loadingMore(uid, lid);
                console.log('OK');
            }
        }

        //Fix search box

        var sticky = header.offsetTop;
        if (window.pageYOffset > sticky) {
            header.classList.add("search-row-fix");
        } else {
            header.classList.remove("search-row-fix");
        }
    };

    $('#view_more').on('click', function(e){
        var lid = $(this).attr('data-lid');
        var uid = $(this).attr('data-uid');
        if (lid > 0) {
            loadingMore(uid, lid);
            //console.log('OK');
        }
    });

    var loadingMore = function(uid, lid){
        var formData = new FormData();
        $.ajax({
            url: '/seller/getLastId/'+ uid +'/' + lid,
            type: 'GET',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                //$('#view_more').removeClass('d-none');
            },
            complete: function () {
                //$('#view_more').addClass('d-none');
            },
            success: function (json) {
                if(json.lid > 0){
                    $('#view_more').attr('data-lid', json.lid);
                    loadContent(uid, lid);
                }

                if(json.count < json.limit){
                    $('#view_more').addClass('d-none');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

     var loadContent = function(uid, lid){
            $.ajax({
                url: '/seller/viewMore/'+ uid +'/' + lid,
                type: 'GET',
                data: {
                },
                dataType: 'html',
                beforeSend: function () {
                     $('#view_more').html(`<span class="spinner-border spinner-border-xl" role="status" aria-hidden="true"></span>`);
                },
                complete: function () {
                    $('#view_more').html(`@lang('home.view_more')`);
                },
                success: function (datahtml) {
                    $('#list_items').append(datahtml);
                    $('#list_items .thumbnail-domrey').css('height', itemWidth);
                    $('#list_items .grid_balckground_img').imageloader({
                        background: true,
                        callback: function (elm) {
                            $(elm).slideDown('slow');
                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
    };


    var deleteListing = function (id, status, seller) {
        if (confirm('Are you sure, you want to delete this listing')) {
            updateStatus(id, status, seller);
        }
    };


    var updateStatus = function (id, status, seller) {
        var formData = new FormData();
        formData.append('id', id);
        formData.append('status', status);
        $.ajax({
            url: '/api/post/status',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {

            },
            complete: function () {

            },
            success: function (json) {
                //htmlAllert('Success!', 'Your status has been changed successfully!');
                window.location.href = "/me/" + seller;

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        //End

    };

    var info = function(){
        htmlAllert("Info", 'Your listing have been blocked, please contact administrator!');
    };

</script>
@endsection