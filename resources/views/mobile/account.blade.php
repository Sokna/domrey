@extends('layouts.appMobile')
@section('content')
<div class="item">
    <!-- header -->
    <div class="row">
        <div class="col-12 row-shadow" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>
            <div  id="custom-search-input" class="search-row-setting">
                <h5>@lang('home.edit_profile')</h5>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <input type="hidden" id="user_id" value="{{$user->id}}"/>
            
            <div class="form-group text-center col-12">
                <div id="upload-demo" data-url="{{asset('images/users/150x150') }}/{{$user->photo??'avata.png'}}?t={{time()}}"></div>
                <button type="button" class="btn btn-domrey" id="profile-image">@lang('home.change')</button>
                <input type="file"  id="change-profile" accept="image/png, image/jpeg"  class="d-none"/>
                
            </div>

            <div class="form-group">
                <label for="title" class="font-weight-bold">Name<span
                        class="text-danger"> *</span>
                </label>
                <input type="text" id="name" class="form-control col-12" value="{{$user->name}}"/>
                <div class="invalid-feedback">
                    Name is required
                </div>
            </div>


            <div class="form-group">
                <label for="title" class="font-weight-bold">@lang('home.name')<span
                        class="text-danger"> *</span>
                </label>
                <input type="text" id="username" class="form-control col-12 is-valid" value="{{$user->uuserid}}" readonly/>
                <div class="invalid-feedback">
                    Name is required
                </div>
            </div>

            <div class="form-group">
                <label for="title" class="font-weight-bold">@lang('home.phone_number')<span
                            class="text-danger"> *</span>
                    </label>
                    <input type="text" id="phone" class="form-control col-12 {{$user->phone?'is-valid':'is-invalid'}}" value="{{$user->phone}}" readonly/>
                    <div class="invalid-feedback">
                        You don't verify your phone number yet
                    </div>
            </div>

            <div class="form-group">
                    <label for="title" class="font-weight-bold">
                        @lang('home.email1')
                        <span class="text-danger"> *</span>
                    </label>
                    <div class="input-group">
                        <input type="text" class="form-control col-12 {{$user->email_verified_at?'is-valid':'is-invalid'}}" id="email" value="{{$user->email}}"/> 
                        <div class="input-group-prepend">
                            <form class="d-inline" method="POST" action="{{url('email/resend')}}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <button class="btn btn-domrey ml-1" id="inputGroupPrepend">@lang('home.verify')</button>
                            </form>
                        </div>
                            <div class="invalid-feedback">
                            @lang('home.email_verify')
                        </div>
                    </div>  
            </div>

            <div class="form-group">
                <button type="button" id="btn-profile-account" class="btn btn-domrey col-sm-3" data-lang="@lang('home.save')">
                    @lang('home.save')
                </button>
                
                <div class="col-12 pl-0 pr-0 mt-3 text-left">
                    <a href="{{url('/password/change')}}" class="domrey-link">@lang('home.change_password')</a>
                </div>
            </div>


        </div>
    </di>
</div>

<script>

$(document).ready(function () {
    var resize = $('#upload-demo').croppie({
        url: $('#upload-demo').attr('data-url'),
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' } 
            width: 150,
            height: 150,
            type: 'square' //circle square
        },
        boundary: {
            width: 160,
            height: 160
        }
    });

    // Button  change photo
    $('#profile-image').click(function(){
        $('#change-profile').click();
    });

    $('#change-profile').change(function () {
        if (this.files && this.files[0]) {
            if (this.files[0].type == 'image/jpeg' || this.files[0].type == 'image/jpg' || this.files[0].type == 'image/png') {
                var reader = new FileReader();
                reader.onload = function (e) {
                    resize.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        
                    });
                }
                reader.readAsDataURL(this.files[0]);

            }else{
                htmlAllert('Error', 'Invalid image type.');
            }
        } //End if
        else{

             htmlAllert('Error', 'Invalid image type.');
        }

    });


    $('#btn-profile-account').click(function () {
        var btn = $(this);
        var formData = new FormData();
        var name = $('#name');
        var username = $('#username');
        var phone = $('#phone');
        var user_id = $('#user_id');
        var email = $('#email');

        // if (validationEmpty([username, phone], '#form-account')) {
        //     return false;
        // }
        formData.append('user_id', user_id.val());
        formData.append('name', name.val());
        formData.append('username', username.val());
        formData.append('phone', phone.val());
        formData.append('email', email.val());

        resize.croppie('result', {
            type: 'canvas',
            size: 'original'
        }).then(function (img) {
            formData.append('file', img);
            $.ajax({
                url: '/api/profile/account',
                type: 'post',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    $('#btn-profile-account').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ' + btn.attr('data-lang') + '...');
                },
                complete: function () {
                    $('#btn-profile-account').html(btn.attr('data-lang'));
                },
                success: function (json) {
                    if (json.email) {
                        htmlAllert('Alert!', `Your enail ${json.email} already exist`);
                    } else {
                        //htmlAllert('Success!', 'Your data has been saved successfully!');
                        goBack();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });

        });
    });

});


</script>

@endsection