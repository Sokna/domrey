@extends('layouts.appMobile')
@section('content')
<div class="item">
    <div class="row">
        <div class="col-12 row-shadow" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>
            <div  id="custom-search-input" class="search-row-setting">
                <h5>@lang('home.report_listing')</h5>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <p class="p-2 text-danger" style="padding-bottom: 15px!important;">
                @if($post->status==-2)
                    @if(count($reports)>=5)
                        <i class="material-icons" style="top: 5px;position: relative;">warning</i> @lang('home.system_block')
                    @else
                        <i class="material-icons" style="top: 5px;position: relative;">block</i> @lang('home.admin_block')
                    @endif
                @else
                    <i class="material-icons" style="top: 5px;position: relative;">block</i> @lang('home.warning_block')
                @endif
            </p>
            @if(@post)
            <div class="col-3 pb-3 float-left">
                <div class="thumbnail-domrey-70">
                    <div class="grid_balckground_img_70" style="background-image: url('{{asset('images/products/150x150/'.$post->image->name)}}')"></div>
                </div>
            </div>
            <div class="col-9 pt-2 float-left">
                <strong> {{$post->title}}</strong> <br/> $ {{$post->price}}
            </div>
                
            @endif
            <table class="table table-responsive">
                <tbody>
                    @foreach($reports as $report)
                        <tr>
                            <td class="text-left" style="width: 180px;">{{$report->dayname}}</td>
                            <td class="text-left">{{$report->message}}</td>
                            <td class="text-center">
                                @if($report->username)
                                    by: <a href="{{url('sellers/'.$report->username)}}" class="domrey-link"> &#64;{{$report->username}}</a>
                                @else
                                    by:   Anonymous
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <a class="btn btn-domrey" href="javascript:goBack()">@lang('home.back')</a>
        </div>
    </div>


</div>
@endsection