@extends('layouts.appMobile')
@section('content')
<div class="item">
    <div class="row">
        <div class="col-12 row-shadow" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>
            <div  id="custom-search-input" class="search-row-setting">
                <h5>@lang('home.setting')</h5>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12">
            <ul class="list-group list-group-flush setting-items">
                <li class="list-group-item d-flex justify-content-between" onclick="location.href='{{url("profile/account")}}'">@lang('home.edit_profile')<i class="material-icons">keyboard_arrow_right</i></li>
                <li class="list-group-item d-flex justify-content-between" onclick="location.href='{{url("profile/profile")}}'">@lang('home.my_address')<i class="material-icons">keyboard_arrow_right</i></li>
                <li class="list-group-item d-flex justify-content-between" onclick="location.href='{{url("password/change")}}'">@lang('home.change_password')<i class="material-icons">keyboard_arrow_right</i></li>
                <li class="list-group-item d-flex justify-content-between" data-lang="{{($lang=='kh')?'en':'kh'}} " data-csrf_token="{{ csrf_token() }}" onclick="onChangeLanguage(this)">@lang('home.language') ({{($lang=='kh')?'English':'ខ្មែរ'}})</li>
                <li class="list-group-item d-flex justify-content-between text-danger" onclick="location.href='{{url("logout")}}'">@lang('home.logout')</li>
            </ul>
        </div>
    </div>

</div>
<script>
var onChangeLanguage = function(that){
    var that = $(that);
    var lang = that.attr("data-lang");
    var csrf_token = that.attr("data-csrf_token");

    var formData = new FormData();
    formData.append('lang', lang);
    formData.append('_token', csrf_token);

    $.ajax({
        url: '/home/language',
        type: 'post',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {

        },
        complete: function () {

        },
        success: function (data) {
            location.reload();
        }

    });

}
</script>
@endsection