@extends('layouts.appMobile')
@section('title', __('home.search_for').' '.$text)
@section('content')
    <div class="item">
        <div class="row">
            <div class="col-12" id="search-row">
                <div class="back">
                    <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
                </div>

                <div id="custom-search-input">
                    <div class="input-group search-control">
                        <span class="input-group-btn">
                            <i class="material-icons search-icon">search</i>
                        </span>
                        <input type="text" id="text-home-search" class="search-control-input" value="{{$text}}" placeholder="@lang('home.search') Domrey..." />
                        <input type="hidden"  id="category_id" value="{{$category->id??''}}"/>
                    </div>
                </div>
                <div class="text-center float-right">
                    <a href="{{url('myLike')}}"><img src="{{asset('/images/like-outlined.svg')}}" class="icon-bar-img mr-1" /></a>
                    <a href="{{url('profile/chatRoom')}}"><img src="{{asset('/images/chat-outlined.svg')}}" class="icon-bar-img ml-1" />
                        @if($un_read>0)
                            <span class="notification">{{$un_read}}</span>
                        @endif
                    </a>
                </div>
            </div>
        </div>

        <!-- Filter control -->
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-4">
                        <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
                            aria-controls="collapseExample">
                            <img src="{{asset('images/filter.svg')}}" width="18px" /> @lang('home.filter')
                        </a>
                    </div>
                    <div class="col-8">
                        In: All
                        <input type="hidden" id="category_id" value="{{$category->id??0}}"/>
                    </div>
                </div>
            </div>
            <div class="col-12 pt-2 pb-3">
                <div class="collapse" id="collapseExample">
                    <form class="mt-3">
                        <div class="form-group">
                            <select id="location_id" class="form-control">
                                <option value="0">@lang('home.location_select')</option>
                                @foreach($zones as $z)
                                    <option value="{{$z->zone_id}}">{{($lang=='kh')?$z->name_kh:$z->name}}</otpion>
                                @endforeach
                              
                            </select>
                        </div>
                        <div class="pb-3">
                            @lang('home.condition')
                        </div>
                        <div class="col-6 pl-1 pb-3 float-left">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" checked class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">@lang('home.new')</label>
                            </div>
                        </div>
                        <div class="col-6 pl-1 pb-3 float-left">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" checked class="custom-control-input" id="customCheck2">
                                <label class="custom-control-label" for="customCheck2">@lang('home.used')</label>
                            </div>
                        </div>
                        <div class="pb-2 pt-3">
                            @lang('home.price')
                        </div>
                        <div class="form-group">
                            <input type="number" id="minimum-price" class="form-control"
                                placeholder="@lang('home.from_min')">
                        </div>
                        <div class="form-group">
                            <input type="number" id="maximum-price" class="form-control" placeholder="@lang('home.to_max')">
                        </div>
                        <div class="form-group">
                            <button type="button" id="btn-search-go"
                                class="btn btn-domrey col-sm-12 col-md-2">@lang('home.go')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="row" id="list_items">
            @foreach($posts as $post)
            <div class="col-6 col-sm-4 mt-0 text-center product-item">
                <div class="row">
                    <div class="col-3 col-sm-2 text-left avatar">
                        <a href="{{url('/sellers/'.$post->uuser_id)}}">
                            <img src="{{asset('/images/users/150x150/'.$post->uuser_photo)}}?t={{time()}}" class="domrey-circle"/>
                        </a>
                    </div>
                    <div class="col-9 col-sm-10 user-avatar text-left pl-2 pl-sm-3">
                        <a href="{{url('/sellers/'.$post->uuser_id)}}">
                            <span class="name">{{$post->uuser_id}}</span><br />
                            <span class="time">
                                @if($post->Y>0)
                                    {{$post->Y}} @lang('home.years_ago')
                                @elseif($post->MO >0)
                                    {{$post->MO}} @lang('home.months_ago')
                                @elseif($post->D >0)
                                    @if($post->H>12)
                                        {{$post->D + 1}} @lang('home.days_ago')
                                    @else
                                        {{$post->D}} @lang('home.days_ago')
                                    @endif
                                @elseif($post->H >0)
                                    {{$post->H}} @lang('home.hours_ago')
                                @elseif($post->M >0 )
                                    {{$post->M}} @lang('home.minutes_ago')
                                @elseif($post->S >0 )
                                    {{$post->S}} @lang('home.seconds_ago')
                                @endif

                            </span>
                        </a>
                    </div>
                </div>

                <div class='thumbnail-domrey' style="height: 150px;">
                    <a href="{{url('/post/detail/'.$post->id)}}">
                        <div class="grid_balckground_img" 
                            style="background-image: url('{{asset('images/loading.svg')}}');" 
                            data-src="{{asset('/images/products/500x500/'.$post->image)}}">
                        </div>
                    </a>
                </div>
                <div class="text-desc text-left">
                    <div class="title">
                        <a href="{{url('/post/detail/'.$post->id)}}">
                            {{Str::limit(Str::title($post->title), $limit = 16, $end = '...')}}
                        </a>
                    </div>
                    <div class="price-font">
                        ${{number_format($post->price)}}
                    </div>
                    <div class="description">
                        <a href="{{url('/post/detail/'.$post->id)}}">
                            {{Str::limit(Str::title($post->description), $limit = 18, $end = '...')}}
                        </a>
                    </div>
                    <div class="contition pt-2 pb-4">
                        @if($post->condition=='USED')
                            @lang('home.used')
                        @else
                            @lang('home.new')
                        @endif
                    </div>
                    <div class="edit-listing">
                        <a class="float-left" data-id="{{$post->id}}" onclick="btnHeartLike(this)">
                            @if($post->like_user==(Auth::user()->id??-1))
                            <img src="{{asset('/images/like-filled.svg')}}" class="float-left" width="20px" /> 
                                <span class="pl-1">{{$post->rate>0?$post->rate:''}}</span>
                            @else
                            <img src="{{asset('/images/like-outlined.svg')}}" class="float-left" width="20px"/> 
                                <span class="pl-1">{{$post->rate>0?$post->rate:''}}</span>
                            @endif
                        </a>
                        <div class="btn-group float-right">
                            <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg height="16" width="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="nonzero"
                                        d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z"
                                        fill="#57585a"></path>
                                </svg>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item btn-report-listing"  data-id="{{$post->id}}"
                                    onclick="btnReportListing({{$post->id}})">@lang('home.report_listing')</button>
                            </div>
                        </div>
                    </div>
                    <!-- end like button -->

                </div>
            </div>
            @endforeach
        </div>
    </div>

    <script>
         $(function() {
            $('.grid_balckground_img').imageloader({
                background: true,
                callback: function (elm) {
                    $(elm).slideDown('slow');
                }
            });
        });

        window.onscroll = function() {
            var header = document.getElementById("search-row");
            var d = document.documentElement;
            var offset = d.scrollTop + window.innerHeight;
            var height = d.offsetHeight;
            if (offset >= height) {
                var lid= $('#view_more').attr('data-lid');
                if(lid>0){
                    //loadingMore(lid);
                }
            }

            //Fix search box        
            var sticky = header.offsetTop;
            if (window.pageYOffset > sticky) {
                header.classList.add("search-row-fix");
            } else {
                header.classList.remove("search-row-fix");
            }
        };

    </script>
@endsection