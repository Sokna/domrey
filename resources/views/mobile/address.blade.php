@extends('layouts.appMobile')
@section('content')
<div class="item">
    <!-- header -->
    <div class="row">
        <div class="col-12 row-shadow" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>
            <div  id="custom-search-input" class="search-row-setting">
                <h5>@lang('home.my_address')</h5>
            </div>
        </div>
    </div>

    <!-- body -->
    <div class="row">
        <div class="col-12">
            <nav>
                <div class="nav nav-tabs" id="nav-tab-address" role="tablist">
                    <a class="nav-item nav-link active" id="nav-address-tab" data-toggle="tab" href="#nav-address" role="tab"
                        aria-controls="nav-address" aria-selected="true">@lang('home.general_info')</a>
                    <a class="nav-item nav-link" id="nav-paymnent-tab" data-toggle="tab" href="#nav-paymnent" role="tab"
                        aria-controls="nav-paymnent" aria-selected="false">@lang('home.payment_method')</a>

                    <a class="nav-item nav-link" id="nav-shipping-tab" data-toggle="tab" href="#nav-shipping" role="tab"
                        aria-controls="nav-shipping" aria-selected="false">@lang('home.shipping')</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabAdress" style="margin-top:10px">
                <div class="tab-pane fade show active" id="nav-address" role="tabpanel" aria-labelledby="nav-address-tab">
                    <input type="hidden" id="address_id" value="{{$address->id??0}}"/>
                    <div class="form-group">
                        <label for="title" class="font-weight-bold">@lang('home.first_name')<span
                                class="text-danger"> *</span>
                        </label>
                        <input type="text" id="first_name" class="form-control col-12" value="{{$address->first_name??''}}"/>
                        <div class="invalid-feedback">
                            First Name is required
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title" class="font-weight-bold">@lang('home.last_name')<span
                                class="text-danger"> *</span>
                        </label>
                        <input type="text" id="last_name" class="form-control col-12" value="{{$address->last_name??''}}"/>
                        <div class="invalid-feedback">
                            Last Name is required
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title" class="font-weight-bold">@lang('home.gender')<span
                                class="text-danger"> *</span>
                        </label>

                        <select id="gender" class="form-control col-12"/>
                            <option></option>
                            @isset($address->gender)
                                @if($address->gender=='M')
                                    <option value="M" selected>Male</option>
                                    <option value="F">Female</option>
                                @else
                                    <option value="M">Male</option>
                                    <option value="F" selected>Female</option>
                                @endif
                            @else
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            @endisset
                        
                        </select>
                        
                        <div class="invalid-feedback">
                            Gender Number is required
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="title" class="font-weight-bold">@lang('home.phone_number')<span
                                class="text-danger"> *</span>
                        </label>
                        <input type="text" id="phone" class="form-control col-12" value="{{$address->phone??''}}" placeholder="@lang('home.phone_number')"/>
                        <div class="invalid-feedback">
                            Phone Number is required
                        </div>
                        <input type="text" id="phone_number1" class="form-control col-12 mt-2" value="{{($address->phones && $address->phones->phone_number1 )? $address->phones->phone_number1:''}}" placeholder="@lang('home.phone_number1')"/>

                        <input type="text" id="phone_number2" class="form-control col-12 mt-2" value="{{($address->phones && $address->phones->phone_number2 )? $address->phones->phone_number2:''}}"placeholder="@lang('home.phone_number2')"/>
                    </div>

                    <div class="form-group">
                        <label for="title" class="font-weight-bold">@lang('home.email1')<span
                                class="text-danger"> *</span>
                        </label>
                        <input type="text" id="email" class="form-control col-12" value="{{$address->email??''}}"/>
                        <div class="invalid-feedback">
                            Email is required
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="font-weight-bold col-12">@lang('home.address')<span
                                class="text-danger"> *</span>
                        </label>
                        <div class="col-12">
                            <input type="text" id="address" class="form-control" value="{{$address->address??''}}"/>
                        </div>
                        <div class="invalid-feedback">
                            Address is required
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="font-weight-bold col-12">@lang('home.about')<span
                                class="text-danger"> *</span>
                        </label>
                        <div class="col-12">
                            <textarea id="about" class="form-control" rows="8">{{$address->about??''}}</textarea>
                        </div>
                        <div class="invalid-feedback">
                            About is required
                        </div>
                    </div>

                    <!-- address -->
                </div>

                <div class="tab-pane fade" id="nav-paymnent" role="tabpanel" aria-labelledby="nav-paymnent-tab">
                    <label for="title" class="font-weight-bold">@lang('home.select_payment')</label>

                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="cash" {{($address->payment_method && $address->payment_method->cash )?'checked':''}}/>
                            <label class="custom-control-label" for="cash">Cash Delivery</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="aba" {{($address->payment_method && $address->payment_method->aba )?'checked':''}}/>
                            <label class="custom-control-label" for="aba">ABA BANK</label>
                            <input type="text" id="aba_acc" class="form-control col-12" placeholder="@lang('home.bank_account')" value="{{($address->payment_account && $address->payment_account->aba_acc )?$address->payment_account->aba_acc:''}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="wing" {{($address->payment_method && $address->payment_method->wing )?'checked':''}}/>
                            <label class="custom-control-label" for="wing">WING</label>
                            <input type="text" id="wing_acc" class="form-control col-12" placeholder="@lang('home.phone_number')" value="{{($address->payment_account && $address->payment_account->wing_acc )?$address->payment_account->wing_acc:''}}"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="true_money" {{($address->payment_method && $address->payment_method->true_money )?'checked':''}}/>
                            <label class="custom-control-label" for="true_money">True Money</label>
                            <input type="text" id="true_acc" class="form-control col-12" placeholder="@lang('home.phone_number')" value="{{($address->payment_account && $address->payment_account->true_money_acc )?$address->payment_account->true_money_acc:''}}"/>
                        </div>
                    </div>

                </div>

                <div class="tab-pane fade" id="nav-shipping" role="tabpanel" aria-labelledby="nav-shipping-tab">

                    <div class="form-group">
                        <label for="title" class="font-weight-bold" style="text-transform: capitalize;">@lang('home.days')</label>
                        <input type="number" id="shipping_day" placeholder="Days shipping" class="form-control col-12" value="{{($address->shipping && $address->shipping->shipping_day)?$address->shipping->shipping_day:''}}"/>
                    </div>

                    <div class="form-group">
                        <label for="title" class="font-weight-bold">@lang('home.fee')</label>
                        <input type="number" id="shipping_fee" placeholder="Fee of charge $" class="form-control col-12" value="{{($address->shipping && $address->shipping->shipping_fee )? $address->shipping->shipping_fee:''}}"/>
                    </div>

                </div>

                <div class="form-group text-left">
                    <button type="button" id="btn-profile-address" class="btn btn-domrey col-sm-3" data-lang="@lang('home.save')">
                        @lang('home.save')
                    </button>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
    $('#btn-profile-address').click(function () {
        var btn = $(this);
        var address_id = $('#address_id');
        var first_name = $('#first_name');
        var last_name = $('#last_name');
        var gender = $('#gender');
        var phone = $('#phone');
        var email = $('#email');
        var address = $('#address');
        var about = $('#about');

        var phoneArr = {
            'phone_number1': $("#phone_number1").val(),
            'phone_number2': $("#phone_number2").val(),
        };

        //Payment Method
        var payment = { 
            cash: $('#cash').is(':checked'),
            aba: $('#aba').is(':checked'),
            wing: $('#wing').is(':checked'),
            true_money: $('#true_money').is(':checked')
        };

        //Bank account
        var bank_acc = {
            aba_acc: $('#aba_acc').val(),
            wing_acc: $('#wing_acc').val(),
            true_money_acc: $('#true_acc').val()
        };

        //Shipping
        var shipping = {
            shipping_day: $('#shipping_day').val(),
            shipping_fee: $('#shipping_fee').val()
        };

        if (validationEmpty([first_name, last_name, gender, phone, email, address, about])) {
            return false;
        }

        var formData = new FormData();
        formData.append('address_id', address_id.val());
        formData.append('first_name', first_name.val());
        formData.append('last_name', last_name.val());
        formData.append('gender', gender.val());
        formData.append('phone', phone.val());
        formData.append('phones', JSON.stringify(phoneArr));
        formData.append('email', email.val());
        formData.append('address', address.val());
        formData.append('about', about.val());

        formData.append('payment', JSON.stringify(payment));
        formData.append('bank_acc', JSON.stringify(bank_acc));
        formData.append('shipping', JSON.stringify(shipping));

        $.ajax({
            url: '/api/profile/address',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#btn-profile-address').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ' + btn.attr('data-lang') + '...');
            },
            complete: function () {
                $('#btn-profile-address').html(btn.attr('data-lang'));
                //htmlAllert('Success!', 'Your profile has been saved successfully!');
                goBack();
            },
            success: function (json) {
                $('#address_id').val(json.id);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    });//End Post function

    //Class validation
    var validationEmpty = function(fields){

        for(var i = 0; i < fields.length; i++){
            if (fields[i].val().length == 0) {
                fields[i].addClass('is-invalid');
            }else{
                fields[i].removeClass('is-invalid');
            }
        }

        if($('#nav-address').find('.is-invalid').length > 0){
            return true;
        }
        return false;
    };
});
</script>
@endsection