@extends('layouts.appMobile')
@section('title', ($post->title??'').' - $ '.($post->price??''))
@section('description', Str::limit(($post->description??''), $limit = 150, $end = '...'))
@section('url', url('/post/detail/'.$post->id??''))
@section('image', asset('/images/products/500x500/'.($post->image??'')))

@section('content')

    <!-- Photo cover -->
    <div class="row">
        <div class="thumbnail-domrey-detail" data-id="{{$post->id}}">
            <div class="grid_balckground_img_500"
                style="background-image: url('{{asset('images/loading.svg')}}');"
                data-src="{{asset('/images/products/500x500/'.$post->image)}}" id="image-display" data-index="0">
                <div class="back">
                    <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
                </div>
                @isset($post->photos)
                    <div class="photos">
                        <i class="material-icons">photo</i> <span> {{count($post->photos)}} Photos</span>
                    </div>
                @endisset
            </div>  
        </div>
    </div>

    <!-- Listing title -->
    <div class="row">
        <div class="col-12 pt-3">
            <div class="title flex-wrap">
                <h6>{{Str::limit(($post->title??''))}}</h6>
            </div>
        </div>
    </div>

    <!-- by user -->
    <div class="row">
        <div class="col-12 pt-3">
            <div class="title">
                <i class="material-icons">access_time</i>
                @if($post->Y>0)
                    {{$post->Y}} @lang('home.hours_ago')
                @elseif($post->MO >0)
                    {{$post->MO}} @lang('home.months_ago')
                @elseif($post->D >0)
                    @if($post->H>12)
                        {{$post->D + 1}} @lang('home.days_ago')
                    @else
                        {{$post->D}} @lang('home.days_ago')
                    @endif
                @elseif($post->H >0)
                    {{$post->H}} @lang('home.hours_ago')
                @elseif($post->M >0 )
                    {{$post->M}} @lang('home.minutes_ago')
                @elseif($post->S >0 )
                    {{$post->S}} @lang('home.seconds_ago')
                @endif
                by <a href="{{url('/sellers/'.($post->uuser_id??''))}}" class="domrey-link"> {{$post->uuser_id??''}}</a>
            </div>
        </div>
    </div>

    <!-- Price -->
    <div class="row">
        <div class="col-12 pt-3">
            <div class="title">
              <i class="material-icons">local_offer</i> ${{number_format($post->price??0)}}
            </div>
        </div>
    </div>

     <!-- Condition -->
    @isset($post->condition)
    <div class="row">
        <div class="col-12 pt-3">
            <div class="title">
              <i class="material-icons">local_play</i>
              @if($post->condition=='NEW')@lang('home.new')@else($post->condition=='USED')@lang('home.used')@endif
            </div>
        </div>
    </div>
    @endisset

    <!-- Like -->
    <div class="row">
        <div class="col-12 pt-3">
            <div class="title">
              <i class="material-icons">favorite_border</i>  {{$post->rate}} @lang('home.like')
            </div>
        </div>
    </div>

    <!-- Category -->
    <div class="row">
        <div class="col-12 pt-3">
            <div class="title">
              <i class="material-icons">toc</i> In 
              <a href="{{url('/post/category/'.($post->parent_category??''))}}" class="domrey-link">{{($lang=='kh')?($post->parent_category_name_kh??''):($post->parent_category_name??'')}}</a> /
              <a href="{{url('post/category/'.$post->category_id)}}" class="domrey-link">{{($lang=='kh')?($post->category_name_kh??''):($post->category_name??'')}}</a>
            </div>
        </div>
    </div>

    <!-- Description -->
    <div class="row">
        <div class="col-12 pt-3">
            <div class="title description">
              <i class="material-icons">error_outline</i>  
                <p class="flex-wrap">{{Str::limit(($post->description??''), $limit = 285, $end = '')}}<span class="d-none" id="more_str">{{Str::substr(($post->description??''), 285)}}</span> @if(strlen(strip_tags(($post->description??''))) > 285)<br/><a href="javascript:readMore();" class="read-more-link domrey-link">@lang('home.read_more')</a>@endif</p>
            </div>
        </div>
    </div>
    

    <!-- Getting this -->
    <div class="row">
        <div class="col-12 pt-3 pb-2">
            <h6>@lang('home.getting_this')</h6>
            <div class="title">
              <i class="material-icons">location_on</i> 
                <a href="{{url('https://www.google.com/maps/place/'.($post->location_name??'').', Cambodia')}}" class="domrey-link" target="_blank">
                    {{($lang=='kh')?($post->location_name_kh??''):($post->location_name??'')}}
                </a>
            </div>
        </div>

        @if($post->deal_method && $post->deal_method->meet_up)
            <div class="col-12 pb-2">
                <div class="title">
                <i class="material-icons">local_play</i> 
                @lang('home.meet_up')
                </div>
            </div>
        @endif

        @if($post->deal_method && $post->deal_method->mail_delivery)
            <div class="col-12 pb-3">
                <div class="title">
                <i class="material-icons">mail</i> 
                @lang('home.mailing')
                </div>
            </div>
        @endif
        
        
    </div>

    <!-- Payment -->
    <div class="row">
        <div class="col-12 pt-3 pb-3">
            <div class="title">
                @if($payment)
                    <h6 class="shipping">@lang('home.payment_lbl')</h6>
                    <div class="payment-icons">
                        <div class="payment-icons-title pb-2">
                            @lang('home.protection')
                        </div>
                        @if($payment->cash)
                            <img src="{{asset('images/payments/cash.png')}}"  width="80px"/>
                        @endif
                        @if($payment->aba)
                            <img src="{{asset('images/payments/aba.png')}}"  width="80px"/>
                        @endif
                        @if($payment->wing)
                            <img src="{{asset('images/payments/wing.png')}}" width="70px"/>
                        @endif
                        @if($payment->true_money)
                            <img src="{{asset('images/payments/true.png')}}" width="128px"/>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Sharing this -->
    <div class="row">
        <div class="col-12 pt-3 pb-3">
            <h6>@lang('home.share_this_listing')</h6>
            <div class="title share-social">
                <a href="https://www.facebook.com/sharer/sharer.php?u={{url('/post/detail/'.($post->id??''))}}" target="_blank">
                    <img src="{{asset('images/share-fb.svg')}}" class="mr-2"  style="width: 30px;"/>
                </a>
                <a href="https://twitter.com/intent/tweet?url={{url('/post/detail/'.($post->id??''))}}&text={{($post->title??'')}}" target="_blank">
                    <img src="{{asset('images/share-twitter.svg')}}" class="mr-2" style="width: 30px;"/>
                </a>
                <a href="javascript:copyLink()">
                    <img src="{{asset('images/share-link.svg')}}" class="mr-2" style="width: 30px;"/> <span class="share-copied d-none" style="font-size: 11px;">Link Copied!</span>
                </a>
            </div>
        </div>
    </div>


    <!-- Meet the  seller -->
    <div class="row">
        <div class="col-12 pt-3 pb-3">
            <h6>@lang('home.meet_seller')</h6>
            <div class="title">

                <table class="table table-borderless">
                    <tr>
                        <td class="text-left pl-0" width="50px">
                            <a href="{{url('/sellers/'.($post->uuser_id??''))}}" class="domrey-link">
                                <img src="{{asset('/images/users/150x150/'.($post->uuser_photo??''))}}?t={{time()}}" class="rounded-circle" width="50px"/>
                            </a>
                        </td>
                        <td class="text-left">
                            <p class="user-joined">
                                <a href="{{url('/sellers/'.($post->uuser_id??''))}}" class="domrey-link">{{$post->uuser_id??''}}</a><br/>
                                    @lang('home.joined')
                                    @if($post->address->years>0)
                                        {{$post->address->years}} @lang('home.years_ago')
                                    @elseif($post->address->months >0)
                                        {{$post->address->months}} @lang('home.months_ago')
                                    @elseif($post->address->days >0)
                                        @if($post->address->hours > 12)
                                            {{$post->address->days + 1}} @lang('home.days_ago')
                                        @else
                                            {{$post->address->days}} @lang('home.days_ago')
                                        @endif

                                    @endif
                                    <br/>
                                    @if(strlen($post->address->email_verified_at) <= 0 && strlen($post->address->uphone)<= 0)
                                        @lang('home.not_verified')
                                    @else
                                        @lang('home.verified')
                                    @endif

                                    @if( strlen($post->address->email_verified_at) > 0)
                                        <img  src="{{asset('images/verification-email.svg')}}" width="13px"/>
                                    @endif

                                    @if(strlen ($post->address->uphone) > 0)
                                        <img  src="{{asset('images/verification-mobile.svg')}}" width="13px"/>
                                    @endif

                                    @if($post->address->facebook_id)
                                        <img  src="{{asset('images/verification-facebook.svg')}}" width="13px"/>
                                    @endif

                            </p>
                        </td>
                    </tr>
                </table>
           
                
                
            </div>
        </div>
    </div>

    <div class="footer-fix row" id="footer-fix">
            <div class="col-4 text-left">
                <div class="btn-like-listing btn-listing-single" data-id="{{$post->id}}" data-lang="@lang('home.like')" onclick="btnHeartLike(this, 1)">
                    @if($post->like_user==(Auth::user()->id??-1))
                        <img src="{{asset('/images/like-filled.svg')}}" style="width: 20px;"/> 
                        <span>
                            @if($post->rate<=0)
                                @lang('home.like')
                            @else 
                                {{$post->rate}}
                            @endif
                        </span>
                    @else
                        <img src="{{asset('/images/like-outlined.svg')}}" style="width: 20px;"/>
                        <span>
                            @if($post->rate<=0)
                                @lang('home.like')
                            @else 
                                {{$post->rate}}
                            @endif
                        </span>
                    @endif
                </div>
            </div>
            <div class="col-4 pt-3 pl-0">
                 <a href="{{url('/profile/chat/'.($post->id??'').'/'.(Auth::user()->id??''))}}" class="btn btn-domrey-chat">@lang('home.chat')</a>
            </div>
            <div class="col-4 pt-3 pl-0">
                <button id="make-offer" data-id="{{$post->price??0}}" data-url="{{url('/profile/chat/'.($post->id??'').'/'.(Auth::user()->id??''))}}" class="btn btn-domrey-danger">@lang('home.offer')</button>
            </div>
    </div>

    <script>

        $(function() {
            $('.grid_balckground_img_500').imageloader({
                background: true,
                callback: function (elm) {
                    $(elm).slideDown('slow');
                }
            });
        });


        $('#make-offer').click(function () {
            var p = $(this);
            var btn = $('<button type="button" class="btn btn-domrey">@lang("home.send")</button>');
            var text = $('<input type="number" class="form-control"/>');
            modal.find('.modal-title').text("@lang('home.make_offer')");
            modal.find('.modal-body').html(text);
            modal.find('.modal-footer').html(btn);
            text.val(p.attr('data-id'));
            btn.click(function () {
                window.location.href = p.attr('data-url') + '/' + text.val();
            });

            $('#exampleModalCenter').modal({});
        });

        var copyLink = function(){
            var temp = $(`<input type="text" value="{{url('/post/detail/'.($post->id??''))}}"/>`);
            $('.share-social').append(temp);
            temp.select();
            document.execCommand("copy");
            temp.remove();
            $('.share-copied').removeClass('d-none');
        }

        $('.thumbnail-domrey-detail').click(function(){
            location.href = `/post/images/` + $(this).attr('data-id');
        });

    </script>
@endsection