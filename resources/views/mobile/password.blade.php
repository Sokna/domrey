@extends('layouts.appMobile')
@section('content')
<div class="item">
    <div class="row">
        <div class="col-12 row-shadow" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>
            <div id="custom-search-input" class="search-row-setting">
                <h5>@lang('home.change_password')</h5>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form method="POST" action="{{ url('password/change') }}">
                @csrf
                <!-- <div class="form-group row">
                    <div class="col-12">
                        @foreach ($errors->all() as $error)
                        <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    </div>
                </div> -->

                <div class="form-group row">
                    <label for="password" class="col-12 col-form-label text-md-right">@lang('home.current_password') <span
                            class="text-danger">*</span></label>

                    <div class="col-12">
                        <input id="password" type="password" class="form-control @error('current_password') is-invalid @enderror" name="current_password"
                            value="{{ old('current_password') }}" required />

                        @error('current_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-12 col-form-label text-md-right">@lang('home.new_password') <span
                            class="text-danger">*</span></label>

                    <div class="col-12">
                        <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password"
                            value="{{ old('new_password') }}" required />

                        @error('new_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-12 col-form-label text-md-right">@lang('home.confirm_password') <span
                            class="text-danger">*</span></label>

                    <div class="col-12">
                        <input id="new_confirm_password" type="password" class="form-control @error('new_confirm_password') is-invalid @enderror"
                            name="new_confirm_password" value="{{ old('new_confirm_password') }}" required />

                        @error('new_confirm_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-domrey col-12">
                            @lang('home.change_password')
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection