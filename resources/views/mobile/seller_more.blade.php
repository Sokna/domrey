@foreach($posts as $post)
    <div class="col-6 col-sm-4 text-center product-item">
        <div class='thumbnail-domrey' style="height: 150px;">
            <a href="{{url('/post/detail/'.$post->id)}}">
                <div class="grid_balckground_img"
                    style="background-image: url('{{asset('images/loading.svg')}}');"
                    data-src="{{asset('/images/products/500x500/'.$post->image)}}">
                    
                </div>
            </a>
        </div>
        <div class="text-desc text-left">
            <div class="title">
                <a href="{{url('/post/detail/'.$post->id)}}">
                    {{Str::limit(Str::title($post->title), $limit = 16, $end = '...')}}
                </a>
            </div>
            <div class="price-font">
                ${{number_format($post->price)}}
            </div>
            <div class="description">
                <a href="{{url('/post/detail/'.$post->id)}}">
                    {{Str::limit(Str::title($post->description), $limit = 18, $end = '...')}}
                </a>
            </div>
            <div class="contition pt-2 pb-4">
                @if($post->condition=='USED')
                @lang('home.used')
                @else
                @lang('home.new')
                @endif
            </div>
            <div class="edit-listing">
                <a class="float-left" data-id="{{$post->id}}" onclick="btnHeartLike(this)">
                    @if($post->like_user==(Auth::user()->id??-1))
                    <img src="{{asset('/images/like-filled.svg')}}" class="float-left" width="20px" /> 
                        <span class="pl-1">{{$post->rate>0?$post->rate:''}}</span>
                    @else
                    <img src="{{asset('/images/like-outlined.svg')}}" class="float-left" width="20px"/> 
                        <span class="pl-1">{{$post->rate>0?$post->rate:''}}</span>
                    @endif
                </a>
                <div class="btn-group float-right">
                    <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg height="16" width="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="nonzero"
                                d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z"
                                fill="#57585a"></path>
                        </svg>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <button type="button" class="dropdown-item btn-report-listing"  data-id="{{$post->id}}"
                            onclick="btnReportListing({{$post->id}})">@lang('home.report_listing')</button>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endforeach