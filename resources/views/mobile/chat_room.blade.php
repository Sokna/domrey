@extends('layouts.appMobile')
@section('title', __('home.message'))
@section('content')
<div class="item">

    <div class="row">
        <div class="col-12 row-shadow" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>
            <div id="custom-search-input" class="search-row-setting">
                <h5>@lang('home.message')</h5>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div id="form-chat-room">
                @if(count($chat_rooms)<=0) <div class="no-item">@lang('home.no_message')</div>
            @endif
            <table class="table table-responsive">
                <tbody>
                    @foreach($chat_rooms as $room)
                    <tr  onClick="gotToChat({{$room->id}},{{$room->chated_by}})">
                        <!-- <a href="{{ url('/profile/chat/'.$room->id.'/'.$room->chated_by)}}"> -->
                        @if($room->owner_id == Auth::user()->id)
                        <td class="align-middle">
                            <div class="thumbnail-domrey-50 float-left">
                                <div class="grid_balckground_img_50"
                                    style="background-image: url('{{asset('images/loading.svg')}}');"
                                    data-src="{{asset('images/products/150x150/'.$room->image)}}">
                                </div>
                            </div>

                        </td>
                        <td class="align-middle">
                            <div class="chat-message float-left">
                                <strong>{{Str::limit($room->title, $limit = 16, $end = '...')}}</strong>
                                <span>{{Str::limit($room->message, $limit = 16, $end = '...')}}</span>
                                @if($room->unread>0)
                                <span class="notification">{{$room->unread}}</span>
                                @endif
                            </div>
                        </td>
                        @else
                        <td class="align-middle">
                            <div class="thumbnail-domrey-50 float-left">
                                <div class="grid_balckground_img_50"
                                    style="background-image: url('{{asset('images/loading.svg')}}');"
                                    data-src="{{asset('images/products/150x150/'.$room->image)}}">
                                </div>
                            </div>

                        </td>
                        <td class="align-middle">
                            <div class="chat-message float-left">
                                <strong>{{Str::limit($room->title, $limit = 16, $end = '...')}}</strong>
                                <span>{{Str::limit($room->message, $limit = 16, $end = '...')}}</span>
                                @if($room->unread>0)
                                <span class="notification">{{$room->unread}}</span>
                                @endif
                            </div>

                        </td>
                        @endif

                        <td class="align-middle text-center">
                                <div class="chat-message">
                                    @if($room->owner_id == Auth::user()->id)
                                    <!-- <img src="{{asset('images/users/150x150/'.$room->photo)}}" class="rounded-circle border border-warning" style="width: 50px; height: 50px;"/> -->
                                    <strong>{{$room->name}}</strong>
                                    @else
                                    <!-- <img src="{{asset('images/users/150x150/'.$room->photo_owner)}}"  class="rounded-circle border border-warning"  style="width: 50px; height: 50px;"/> -->
                                    <strong>{{$room->name_owner}}</strong>
                                    @endif
                                </div>
                            
                        </td>

                        <td class="align-middle text-center">
                            <div class="chat-message">
                                <span>{{$room->sending_time}}</span>
                            </div>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>


    </div>
</div>
</div>
<script>
$(function() {
    $('.grid_balckground_img_50').imageloader({
        background: true,
        callback: function(elm) {
            $(elm).slideDown('slow');
        }
    });
});

function gotToChat(id, chated){

    location.href=`/profile/chat/${id}/${chated}`;
}
</script>

@endsection