@extends('layouts.appMobile')
@section('content')
    <div class="item">
        <div class="row">
            <div class="col-12" id="search-row">
                <div id="custom-search-input" class="searcg_home">
                    <div class="input-group search-control">
                        <span class="input-group-btn">
                            <i class="material-icons search-icon">search</i>
                        </span>
                        <input type="text" id="text-home-search" class="search-control-input" placeholder="@lang('home.search') Domrey..." />
                    </div>
                </div>
                <div class="text-center float-right">
                    <a href="{{url('myLike')}}"><img src="{{asset('/images/like-outlined.svg')}}" class="icon-bar-img mr-1" /></a>
                    <a href="{{url('profile/chatRoom')}}"><img src="{{asset('/images/chat-outlined.svg')}}" class="icon-bar-img ml-1" />
                        @if($un_read>0)
                            <span class="notification">{{$un_read}}</span>
                        @endif
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 pl-1 pr-1">
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item text-center active">
                            <img src="{{asset('images/loading.svg')}}" data-src="{{asset('images/slide/01.jpg')}}" class="img-fluid rounded item-slide"/>
                        </div>
                        <div class="carousel-item text-center">
                            <img src="{{asset('images/loading.svg')}}" data-src="{{asset('images/slide/01.jpg')}}" class="img-fluid rounded item-slide"/>
                        </div>
                        <div class="carousel-item text-center">
                            <img src="{{asset('images/loading.svg')}}" data-src="{{asset('images/slide/01.jpg')}}" class="img-fluid rounded item-slide"/>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <i class="material-icons">navigate_before</i>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <i class="material-icons">navigate_next</i>
                    </a>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-12 pl-2 pr-2 mt-3 mb-2">
                <h5>@lang('home.explore')</h5>
                <div class="owl-carousel owl-theme">
                    @for($i = 1; $i < 36; $i=$i+2 )
                        @if(!$categories[$i]->parent_id)
                            <div class="item-owl-domrey-category">
                                <div class="item-row">
                                    <a href="{{url('/post/category/'.$categories[$i-1]->id)}}">
                                        <img src="{{ asset('images/explore/'.($categories[$i-1]->icon??'following.png'))}}" class="img-fluid"/>
                                        <div class="text-center text-category">
                                            {{Str::limit(($lang=='kh'?$categories[$i-1]->name_kh:$categories[$i-1]->name), $limit = 14, $end = '')}}
                                        </div>
                                    </a>
                                </div>
                                <div class="item-row">
                                    <a href="{{url('/post/category/'.$categories[$i]->id)}}">
                                        <img src="{{ asset('images/explore/'.($categories[$i]->icon??'following.png'))}}" class="img-fluid"/>
                                        <div class="text-center text-category">
                                            {{Str::limit(($lang=='kh'?$categories[$i]->name_kh:$categories[$i]->name), $limit = 14, $end = '')}}
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endfor
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 pl-2 pr-2">
                <h5>@lang('home.top_product')</h5>
            </div>
        </div>

        <div class="row" id="list_items">
            @foreach($posts as $post)
            <div class="col-6 col-sm-4 mt-0 text-center product-item">
                <div class="row">
                    <div class="col-3 col-sm-2 text-left avatar">
                        <a href="{{url('/sellers/'.$post->uuser_id)}}">
                            <img src="{{asset('/images/users/150x150/'.$post->uuser_photo)}}?t={{time()}}" class="domrey-circle"/>
                        </a>
                    </div>
                    <div class="col-9 col-sm-10 user-avatar text-left pl-2 pl-sm-3">
                        <a href="{{url('/sellers/'.$post->uuser_id)}}">
                            <span class="name">{{$post->uuser_id}}</span><br />
                            <span class="time">
                                @if($post->Y>0)
                                    {{$post->Y}} @lang('home.years_ago')
                                @elseif($post->MO >0)
                                    {{$post->MO}} @lang('home.months_ago')
                                @elseif($post->D >0)
                                    @if($post->H>12)
                                        {{$post->D + 1}} @lang('home.days_ago')
                                    @else
                                        {{$post->D}} @lang('home.days_ago')
                                    @endif
                                @elseif($post->H >0)
                                    {{$post->H}} @lang('home.hours_ago')
                                @elseif($post->M >0 )
                                    {{$post->M}} @lang('home.minutes_ago')
                                @elseif($post->S >0 )
                                    {{$post->S}} @lang('home.seconds_ago')
                                @endif

                            </span>
                        </a>
                    </div>
                </div>

                <div class='thumbnail-domrey' style="height: 150px;">
                    <a href="{{url('/post/detail/'.$post->id)}}">
                        <div class="grid_balckground_img" 
                            style="background-image: url('{{asset('images/loading.svg')}}');" 
                            data-src="{{asset('/images/products/500x500/'.$post->image)}}">
                        </div>
                    </a>
                </div>
                <div class="text-desc text-left">
                    <div class="title">
                        <a href="{{url('/post/detail/'.$post->id)}}">
                            {{Str::limit(Str::title($post->title), $limit = 16, $end = '...')}}
                        </a>
                    </div>
                    <div class="price-font">
                        ${{number_format($post->price)}}
                    </div>
                    <div class="description">
                        <a href="{{url('/post/detail/'.$post->id)}}">
                            {{Str::limit(Str::title($post->description), $limit = 18, $end = '...')}}
                        </a>
                    </div>
                    <div class="contition pt-2 pb-4">
                        @if($post->condition=='USED')
                            @lang('home.used')
                        @else
                            @lang('home.new')
                        @endif
                    </div>
                    
                    <div class="edit-listing">
                        <a class="float-left" data-id="{{$post->id}}" onclick="btnHeartLike(this)">
                            @if($post->like_user==(Auth::user()->id??-1))
                            <img src="{{asset('/images/like-filled.svg')}}" class="float-left" width="20px" /> 
                                <span class="pl-1">{{$post->rate>0?$post->rate:''}}</span>
                            @else
                            <img src="{{asset('/images/like-outlined.svg')}}" class="float-left" width="20px"/> 
                                <span class="pl-1">{{$post->rate>0?$post->rate:''}}</span>
                            @endif
                        </a>
                        <div class="btn-group float-right">
                            <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg height="16" width="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="nonzero"
                                        d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z"
                                        fill="#57585a"></path>
                                </svg>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <button type="button" class="dropdown-item btn-report-listing"  data-id="{{$post->id}}"
                                    onclick="btnReportListing({{$post->id}})">@lang('home.report_listing')</button>
                            </div>
                        </div>
                    </div>
                    <!-- end like button -->

                </div>
            </div>
            @endforeach
        </div>

        <div class="row">
            <div class="col-12 mt-3 text-center">
                <button type="button" id="view_more" data-lid="{{$post->id??0}}" class="btn btn-outline-secondary">
                    @lang('home.view_more')
                </button>
            </div>
        </div>


    </div>

    <script>

        $(document.body).on('touchmove', headerFixed); // for mobile

        $(window).on('scroll', headerFixed); 

        function headerFixed(){
            var header = document.getElementById("search-row");
            var d = document.documentElement;
            //Fix search box
            var sticky = header.offsetTop;
            if (window.pageYOffset > sticky) {
                header.classList.add("search-row-fix");
            } else {
                header.classList.remove("search-row-fix");
            }

            //more();
        }


        function more(){
            if( $(window).scrollTop() + window.innerHeight >= document.body.scrollHeight ) { 
                var lid= $('#view_more').attr('data-lid');
                if(lid > 0){
                    loadingMore(lid);
                }
            }

        };


        $(function() {
            $('.grid_balckground_img').imageloader({
                background: true,
                callback: function (elm) {
                    $(elm).slideDown('slow');
                }
            });

            
             $('.item-slide').imageloader({
                background: false,
                callback: function (elm) {
                    $(elm).slideDown('slow');
                }
            });
            
        });

        // window.onscroll = function() {
        //     var header = document.getElementById("search-row");
        //     var d = document.documentElement;
        //     var offset = d.scrollTop + window.innerHeight;
        //     var height = d.offsetHeight;
        //     if (offset >= height) {
        //         var lid= $('#view_more').attr('data-lid');
        //         if(lid>0){
        //             //loadingMore(lid);
        //         }
        //     }

        //     //Fix search box
            
        //     var sticky = header.offsetTop;
        //     if (window.pageYOffset > sticky) {
        //         header.classList.add("search-row-fix");
        //     } else {
        //         header.classList.remove("search-row-fix");
        //     }
        // };

        $('#view_more').on('click', function(e){
            var lid = $(this).attr('data-lid');
            if (lid > 0) {
                loadingMore(lid);
                //console.log('OK');
            }
        });

        
        var loadingMore = function(lid){
            var formData = new FormData();
            $.ajax({
                url: '/post/getLastId/' + lid,
                type: 'GET',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function () {
                    
                },
                complete: function () {
                    
                },
                success: function (json) {
                    if(json.lid > 0){
                        $('#view_more').attr('data-lid', json.lid);
                        loadContent(lid);
                    }

                    if(json.count < json.limit){
                        $('#view_more').addClass('d-none');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };

        var loadContent = function(lid){
            $.ajax({
                url: '/post/viewMore/' + lid,
                type: 'GET',
                data: {
                },
                dataType: 'html',
                beforeSend: function () {
                    $('#view_more').html(`<span class="spinner-border spinner-border-xl" role="status" aria-hidden="true"></span>`);
                },
                complete: function () {
                    $('#view_more').html(`@lang('home.view_more')`);
                },
                success: function (datahtml) {
                    $('#list_items').append(datahtml);
                    $('#list_items .thumbnail-domrey').css('height', itemWidth);

                    $('#list_items .grid_balckground_img').imageloader({
                        background: true,
                        callback: function (elm) {
                            $(elm).slideDown('slow');
                            
                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        };

    </script>
@endsection