@extends('layouts.appMobile')
@section('title', __('home.new_sell'))
@section('content')
<!-- Custom styles for this template -->
<link href="{{ asset('css/sell.css') }}?time={{csrf_token()}}" rel="stylesheet" />

<div class="item">
    <!-- header -->
    <div class="row">
        <div class="col-12 row-shadow" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">close</i></a>
            </div>
            <div id="custom-search-input" class="search-row-setting">
                <h5>@lang('home.sell')</h5>
            </div>
        </div>
    </div>

    <!-- body -->
    <div class="row">
        <div class="col-12">
            <h5>@lang('home.what_to_list')</h5>
            <p>@lang('home.what_to_descrip')</p>
        </div>
    </div>

    <!-- body -->
    <div class="row">
        <div class="col-12">
            <div class="select-photos text-center">
                <div class="_3uIwePhaxz">
                    <div class="_3nH6adLACP ztxVml-Wqh _14ECgRVNZW">
                        <img alt="Select photo icon" src="{{asset('images/select-photo.svg')}}">
                    </div>
                    <label class="_28Szm2xRQd">
                        <input accept="image/png, image/jpeg" class="P2llUzsDMi _14ECgRVNZW" id="form-photos"
                            multiple type="file" style="display: none;"/>
                        <div class="_3te3WYAwbY">
                        </div>
                        <button class="btn btn-domrey" type="button"
                            id="button-photos">@lang('home.select_photos')</button>
                    </label>
                    <p class="_1gJzwc_bJS _2NNa9Zomqk mT74Grr7MA _2m1WFlGyTw lqg5eVwdBz _19l6iUes6V _3k5LISAlf6">
                        @lang('home.up_to_photos')
                    </p>
                </div>
                <p id="photo_requred" class="mt-2 text-left text-danger d-none">Please attach at least 1 photo.</p>
            </div>
        </div>

        <div class="col-12">
            <div class="photos-items row" id="photos-items" data-items="{{$img_count}}">
                @isset($images)
                @foreach($images as $key => $img)
                <div class="col-4 pl-1 pr-1 {{$img->cover==1?'class-cover':''}}">
                    <div class="col-grid">
                        <span class="cover">
                            <input type="radio" name="radio_cover" class="radio_cover" onClick="coverClick(this)"
                                data-id="{{$img->id}}" {{$img->cover==1?'checked':''}} />
                            <span class="label">{{$img->cover==1?'Cover':''}}&nbsp;</span>
                        </span>
                        <i class="material-icons delete-icon_v2" onClick="deleteImageItems(this)">delete_forever</i>
                        <div class="photos_item_container">
                            <img src="{{asset('images/products/500x500')}}/{{$img->name}}" data-edit="update"
                                data-cover="{{$img->cover==1?1:0}}" data-name="{{$img->name}}"
                                class="photos-image-items" />
                        </div>
                    </div>
                </div>
                @endforeach
                @endisset
            </div>
        </div>

        <div class="col-12">
            <div class="select-photos text-center">
                <div class="form-group">
                    <input type="hidden" id="parent_category" value="{{$isNew==0?$post->parent_category:''}}"/>
                    <input type="hidden" id="listing_id" value="{{$isNew==0?$post->id:0}}"/>
                    <button type="button" class="form-control text-control text-left" data-id="{{$isNew==0?$post->category_id:0}}" id="btn-select">
                        <div class="data-select float-left">
                            @isset($post)
                                {{($lang=='kh'?$post->parent_cat_name->name_kh:$post->parent_cat_name->name)}}  > {{($lang=='kh'?$post->cat_name->name_kh:$post->cat_name->name)}}
                            @else 
                                <span class="placeholder_control">Select a category</span>
                            @endisset
                        </div>
                        <span class="arrow"><i class="material-icons">keyboard_arrow_right</i> </span>
                    </button>
                    <div class="dropdonw-item mb-4 d-none" id="dropdonw-item">
                        <div class="list-item d-none">
                            <input type="text" class="form-control" placeholder="Search of a category..."/>
                        </div>
                        @foreach($categories as $cat)
                            <div data-id="{{$cat->id}}" class="list-item list-parent">{{($lang=='kh'?$cat->name_kh:$cat->name)}}
                                @if(count($cat->children)>0)
                                    <span class="arrow"><i class="material-icons">keyboard_arrow_right</i></span>
                                @endif
                            </div>
                            @foreach($cat->children as $child)
                                <div class="list-item sub-item d-none item-{{$cat->id}}" data-id="{{$child->id}}" data-parent="{{($lang=='kh'?$cat->name_kh:$cat->name)}}">{{$lang=='kh'?$child->name_kh:$child->name}}</div>
                            @endforeach
                        @endforeach
                    </div>
                </div>

                <div class="text-left" id="listing_control">

                    <div class="form-group">
                        <h5>@lang('home.listing_title') <span class="text-danger">*</span></h5>
                        <input type="text" id="title" value="{{$post->title??''}}" class="form-control text-control required" placeholder="Listing Title" />
                    </div>

                    <div class="form-group">
                        <h5>@lang('home.location') <span class="text-danger">*</span></h5>
                        <select id="location_id" class="form-control text-control required">
                            <!-- <option value="">@lang('home.location_select')</option> -->
                            @foreach($zones as $z)
                                @isset($post->location_id)
                                    @if($post->location_id == $z->zone_id)
                                        <option value="{{$z->zone_id}}" selected>{{($lang=='kh'?$z->name_kh:$z->name)}}</option>
                                    @else
                                        <option value="{{$z->zone_id}}">{{($lang=='kh'?$z->name_kh:$z->name)}}</option>
                                    @endif
                                @else
                                    <option value="{{$z->zone_id}}">{{($lang=='kh'?$z->name_kh:$z->name)}}</option>
                                @endisset
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <h5 class="">@lang('home.condition')</h5>
                        <div class="custom-control custom-radio custom-control-inline">
                            @if($isNew==0 && $post->condition=='NEW')
                                <input type="radio" id="customRadioInline1" name="customRadioInline1" checked class="custom-control-input"/>
                            @else
                                <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input"/>
                            @endif
                            <label class="custom-control-label" for="customRadioInline1">@lang('home.new')</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline float-right-sm">
                            @if($isNew==0 && $post->condition=='USED')
                                <input type="radio" id="customRadioInline2" name="customRadioInline1" checked class="custom-control-input"/>
                            @else
                                <input type="radio" id="customRadioInline2" name="customRadioInline1" class="custom-control-input"/>
                            @endif
                            <label class="custom-control-label" for="customRadioInline2">@lang('home.used')</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>@lang('home.brand')</h5>
                        <input type="text" id="brand_name" value="{{$brand->name??''}}" class="form-control text-control" placeholder="Brand"/>
                    </div>

                    <div class="form-group">
                        <h5>@lang('home.price') <span class="text-danger">*</span></h5>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">&nbsp;&nbsp;$&nbsp;&nbsp;</span>
                            </div>
                            <input type="number" id="price" value="{{$post->price??''}}" class="form-control text-control required" placeholder="Price"/>
                            <div class="input-group-prepend">
                                <span class="input-group-text khr_riel_label">
                                    @isset($post->price)
                                        {{ number_format($post->price * 4000, 0)}}
                                    @else
                                         {{ number_format(0, 0)}}
                                    @endisset
                                    @lang('home.riel')
                                </span>
                            </div>

                        </di>
                    </div>
                    
                    <div class="form-group mt-3">
                        <h5>@lang('home.description') <span class="text-danger">*</span></h5>
                        <textarea class="form-control required" id="description" rows="8"  placeholder="Describe what you are selling and include any details a buyer might be interested in. People love items with stories! (Optional)">{{$post->description??''}}</textarea>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <h5>@lang('home.deal_method')</h5>
                        </div>
                        <div class="col-12">
                            <div class="custom-control custom-checkbox mb-3 mt-1">
                                <input type="checkbox" class="custom-control-input" id="meet_up"  {{(isset($post) && $post->deal_method && $post->deal_method->meet_up)?'checked':''}}/>
                                <label class="custom-control-label col-12" for="meet_up">@lang('home.meet_up')</label>
                            </div>                  
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="mail_delivery" {{(isset($post) && $post->deal_method && $post->deal_method->mail_delivery)?'checked':''}}/>
                                <label class="custom-control-label col-12" for="mail_delivery">@lang('home.mail_delivery')</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>@lang('home.tags')</h5>
                        <!-- <input type="text" class="form-control text-control" id="tags" value="{{$post->tags??''}}" placeholder="ex. apple ios"/> -->
                        <input type="text" class="form-control text-control" id="tags" value="{{$post->tags??''}}" data-role="tagsinput"/>
                    </div>
                    
                    <div class="form-group mb-4">
                       <button type="button" {{(isset($post) && $post->status==-2)?'disabled':''}} class="btn btn-domrey col-sm-4" id="btn-list-now" data-lang="@lang('home.list_now')" data-lang-save="@lang('home.save')">
                            @if(isset($post))
                                 @lang('home.save')
                            @else
                                @lang('home.list_now')
                            @endif
                        </button>
                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

<script>
//Dropdonw list select
$('#btn-select').on({
    focusout: function(e) {
        //$('.dropdonw-item').addClass('d-none');
        console.log(e);
    },
    focusin: function(e) {

    },
    click: function(e) {
        if ($('.dropdonw-item').hasClass('d-none')) {
            $('.dropdonw-item').removeClass('d-none');
            $(this).find('span.arrow').html('<i class="material-icons">keyboard_arrow_down</i>');
        } else {
            $('.dropdonw-item').addClass('d-none');
            $(this).find('span.arrow').html('<i class="material-icons">keyboard_arrow_right</i>');
        }
    }
});

//List item select
$('#dropdonw-item div.list-parent').on({
    click: function() {
        var subItems = $('.item-' + $(this).attr('data-id'));
        $('#parent_category').val($(this).attr('data-id'));

        if (subItems.hasClass('d-none')) {
            $('.sub-item').addClass('d-none');
            subItems.removeClass('d-none');
            $(this).find('span.arrow').html('<i class="material-icons">keyboard_arrow_down</i>');
        } else {
            subItems.addClass('d-none');
            $(this).find('span.arrow').html('<i class="material-icons">keyboard_arrow_right</i>');
        }
    }
});

//Sub item select
$('#dropdonw-item div.sub-item').on({
    click: function() {
        $(".sub-item").addClass('d-none');
        $("#dropdonw-item").addClass('d-none');
        $('#btn-select').attr('data-id', $(this).attr('data-id'));
        $('#btn-select').find('div').html($(this).attr('data-parent') + ' > ' + $(this).html());

        displayFormControl();
    }
});
var displayFormControl = function() {
    $('#listing_control').removeClass('d-none');
};

//Brows photos
$('#button-photos').click(function(ent) {
    $('#form-photos').click();

});

$('#form-photos').change(function() {
    if (this.files) {
        $('#photo_requred').addClass('d-none');
        $(this.files).each(function(ind) {
            var classRotat = '';
            getOrientation(this, function(orientation) {
                //alert(orientation);
                if (orientation == 6) {
                    //classRotat = 'rotation-90';
                }
            });

            var countItems = parseInt($('#photos-items').attr('data-items'));
            if (countItems < 8) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#photos-items').append(`<div class="col-4 pl-1 pr-1">
                            <div class="col-grid">
                                <span class="cover">
                                    <input type="radio" name="radio_cover" class="radio_cover" onClick="coverClick(this)" data-id="0"/>
                                    <span class="label"></span>
                                </span>
                                <div class="photos_item_container">
                                    <img src="${e.target.result}"  data-edit="new" data-cover="0" data-name="${new Date().getTime()}.png" class="photos-image-items ${classRotat}"/>
                                    <i class="material-icons delete-icon_v2" onClick="deleteImageItems(this)">delete_forever</i>
                                </div>
                            </div>
                        </div>`);
                    //$($('#photos-items .col-grid span')[0]).text(`Cover`);
                    firstDefault();
                }
                reader.readAsDataURL(this);
                countItems++;
                $('#photos-items').attr('data-items', countItems);

            } // End if
        });
    }

});


// from http://stackoverflow.com/a/32490603
var getOrientation = function(file, callback) {
    var reader = new FileReader();
    reader.onload = function(event) {
        var view = new DataView(event.target.result);

        if (view.getUint16(0, false) != 0xFFD8) return callback(-2);

        var length = view.byteLength,
            offset = 2;

        while (offset < length) {
            var marker = view.getUint16(offset, false);
            offset += 2;

            if (marker == 0xFFE1) {
                if (view.getUint32(offset += 2, false) != 0x45786966) {
                    return callback(-1);
                }
                var little = view.getUint16(offset += 6, false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                var tags = view.getUint16(offset, little);
                offset += 2;

                for (var i = 0; i < tags; i++)
                    if (view.getUint16(offset + (i * 12), little) == 0x0112)
                        return callback(view.getUint16(offset + (i * 12) + 8, little));
            } else if ((marker & 0xFF00) != 0xFF00) break;
            else offset += view.getUint16(offset, false);
        }
        return callback(-1);
    };

    reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
};


//Button list
$('#btn-list-now').click(function(ent) {
    //$('#listing_control').find('.required').addClass('is-invalid');
    $('#listing_control .required').each(function(index) {
        if ($(this).val() == '') {
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
        }
    });


    var imageItems = $('.photos-image-items');
    var imageNone = $('.photos-items').attr('data-items');
    if (imageItems.length <= 0 || imageNone == 0) {
        $('#photo_requred').removeClass('d-none');
    } else {
        $('#photo_requred').addClass('d-none');
    }

    if ($('#btn-select').attr('data-id') == 0) {
        $('#btn-select').addClass('is-invalid');
    } else {
        $('#btn-select').removeClass('is-invalid');
    }

    //check title $ description

    if ($('#title').val() == '') {
        $('#title').next().addClass('is-invalid');
    } else {
        $('#title').next().removeClass('is-invalid');
    }

    if ($('#description').val() == '') {
        $('#description').next().addClass('is-invalid');
    } else {
        $('#description').next().removeClass('is-invalid');
    }


    if (imageItems.length <= 0 || $('#btn-select').attr('data-id') == 0 || $('#title').val() == '' || $('#description').val() == '' || imageNone == 0) {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return;
    }




    if ($('#listing_control .is-invalid').length == 0 && imageItems.length > 0) {
        var formData = new FormData();
        //Post images data
        for (var i = 0; i < imageItems.length; i++) {
            var img = $(imageItems[i]);
            var stringSrc = img.attr('src');
            var data_name = img.attr('data-name');
            var data_edit = img.attr('data-edit');
            var data_cover = img.attr('data-cover');

            if (img.attr('data-edit') == 'new') {

                formData.append("files[]", stringSrc);
                formData.append("data_edit[]", data_edit);
                formData.append("data_name[]", data_name);
                formData.append("data_cover[]", data_cover);

            } else if (img.attr('data-edit') == 'delete') {

                formData.append("files[]", '');
                formData.append("data_edit[]", data_edit);
                formData.append("data_name[]", data_name);
                formData.append("data_cover[]", data_cover);

            } else {

                formData.append("files[]", '');
                formData.append("data_edit[]", data_edit);
                formData.append("data_name[]", data_name);
                formData.append("data_cover[]", data_cover);
            }
        } //Emd foreach

        //foreach controls
        $('#listing_control .form-control').each(function(index) {
            formData.append($(this).attr('id'), $(this).val());
        });

        //Get listing id
        formData.append('id', $('#listing_id').val())
        //Category Id
        formData.append('parent_category', $('#parent_category').val());
        formData.append('category_id', $('#btn-select').attr('data-id'));

        //Condition
        var condition = '';
        if ($('#customRadioInline1').is(':checked')) {
            condition = 'NEW';
        }
        if ($('#customRadioInline2').is(':checked')) {
            condition = 'USED';
        }
        formData.append('condition', condition);

        //Deal method
        var deal_method = {
            meet_up: $('#meet_up').is(':checked'),
            mail_delivery: $('#mail_delivery').is(':checked')
        };
        formData.append('deal_method', JSON.stringify(deal_method));


        var btnText = $('#btn-list-now').attr('data-lang');
        if ($('#listing_id').val() > 0) {
            btnText = $('#btn-list-now').attr('data-lang-save');
        }

        $.ajax({
            //url: '/api/post/listNowV2',
            url: '/api/post/create',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                $('#btn-list-now').attr('disabled', true);


                $('#btn-list-now').html(
                    `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ${btnText}...`
                    );
            },
            complete: function() {
                $('#btn-list-now').html(btnText);
                $('#btn-list-now').removeAttr('disabled');
                //htmlAllert('Success!', 'Your data has been saved successfully!');
            },
            success: function(json) {
                window.location.href = "/post/detail/" + json.id;
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    } //Listing 

});


var deleteImageItems = function(element) {
    var countItems = parseInt($('#photos-items').attr('data-items'));
   

    var mainClass = $(element).closest('.col-4');
    mainClass.find('img').attr('data-edit', 'delete');
    $(mainClass).addClass('d-none');

    $('#photos-items').attr('data-items', countItems - 1);
    //Set defualt index 0 cover
    if (mainClass.find('img').attr('data-cover') == 1) {
        mainClass.find('img').attr('data-cover', 0);
        setDefaultCover();
    }

    /*if (countItems == 1) {
        htmlAllert('Alert!', 'Can not delete, at least 1 photo.');
        $('#photos-items').html('');
        return;
    }*/
};

var setDefaultCover = function() {
    $('#photos-items .col-4').each(function(index) {
        if (!$(this).hasClass('d-none')) {
            $(this).addClass('class-cover');
            $(this).find('img').attr('data-cover', 1);
            $(this).find('.radio_cover').attr('checked', true);
            $(this).find('span.label').text('Cover');
            return false;
        }
    });
};

var firstDefault = function() {
    if ($('#photos-items .class-cover').length == 0 && $('#listing_id').val() == 0) {
        $($('#photos-items .col-4')[0]).addClass('class-cover');
        $($('#photos-items .col-4')[0]).find('img').attr('data-cover', 1);
        $($('#photos-items .col-4')[0]).find('.radio_cover').attr('checked', true);
        $($('#photos-items .col-4')[0]).find('span.label').text('Cover');
    }
};

var coverClick = function(element) {
    $('#photos-items .radio_cover').closest('.col-4').removeClass('class-cover');
    $('#photos-items .photos_item_container img').attr('data-cover', 0);
    $($(element)).closest('.col-grid').find('.photos_item_container img').attr('data-cover', 1);
    $($(element)).closest('.col-4').addClass('class-cover');

    $('#photos-items span.label').text('');
    $($(element)).next('span.label').text('Cover');
};



$('#price').keyup(function () {
    var khr = ($('#price').val() * 4000).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    $('.khr_riel_label').html(khr + ' ៛');
});


</script>
@endsection