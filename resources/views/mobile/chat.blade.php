@extends('layouts.appMobile')
@section('content')
<div class="item">
    <div class="row">
        <div class="col-12" id="search-row">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>

            <div id="custom-search-input">
                <div class="input-group search-control">
                    <span class="input-group-btn">
                        <i class="material-icons search-icon">search</i>
                    </span>
                    <input type="text" id="text-home-search" class="search-control-input" placeholder="@lang('home.search') Domrey..." />
                    <input type="hidden" id="category_id" />
                </div>
            </div>
            <div class="text-center float-right">
                <a href="{{url('myLike')}}"><img src="{{asset('/images/like-outlined.svg')}}" class="icon-bar-img mr-1" /></a>
                <a href="{{url('profile/chatRoom')}}"><img src="{{asset('/images/chat-outlined.svg')}}" class="icon-bar-img ml-1" /> </a>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-12 pl-0 pr-0">
            <input type="hidden" id="make_offer" value="{{number_format($offer??0)}}"/>
            <input type="hidden" id="post_id" value="{{$post->id??0}}"/>
            <input type="hidden" id="chat_user_id" value="{{$chatUser->id??0}}"/>
            <input type="hidden" id="sender" value="{{$chated_id??0}}"/>
            <div class="card" style="border: none!important">
                <div class="card-header">
                    <div  class="row">
                        <div class="col-3">
                            <div class='thumbnail-domrey-50'>
                                <a href="{{url('/post/detail/'.$post->id)}}">
                                    <div class="grid_balckground_img_50"
                                        style="background-image: url('{{asset('images/loading.svg')}}');"
                                        data-src="{{asset('/images/products/150x150/'.($post_image??''))}}">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-9 pl-0 pr-0">
                            {{$post->title??''}} <br/>
                            <span class="badge badge-danger">
                                $  {{$post->price??''}} 
                            </span>
                        </div>
                    </div>

                   
                </div>
                <div class="card card-prirary cardutline direct-chat" style="border: none!important;margin-bottom: -20px;!important">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <!-- Conversations are loaded here -->
                        <div class="scroll direct-chat-messages" style="height: auto!important">
                            @foreach($messages as $m)
                                @if($m->sender == Auth::user()->id)
                                    <div class="direct-chat-msg" data-id="{{$m->id}}">
                                        <div class="direct-chat-infos clearfix">
                                            <div class="direct-chat-name float-right">{{$m->sender_name}}</div>
                                            <div class="direct-chat-timestamp float-left">{{$m->sending_time}}</div>
                                        </div>
                                        <div class="">
                                            <img class="direct-chat-img float-right rounded-circle border border-warning" src="{{asset('/images/users/150x150/'.($m->sender_photo??'avata.png'))}}" style="width: 50px" />
                                            <div class="direct-chat-text float-right">
                                            
                                            @if($m->is_file == 1)
                                                <a href="{{asset('/images/messages/'.$m->file)}}" class="image-view">
                                                    <img src="/images/loading.svg" data-src="{{asset('/images/messages/'.$m->file)}}" style="width: 150px;" class="img-chat rounded"/>
                                                </a>
                                            @else
                                                    {!! $m->message !!}
                                            @endif

                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="direct-chat-msg" data-id="{{$m->id}}">
                                        <div class="direct-chat-infos clearfix">
                                            <div class="direct-chat-name-left float-left">{{$m->sender_name}} </div>
                                            <div class="direct-chat-timestamp-left float-right">{{$m->sending_time}}</div>
                                        </div>
                                        <div class="">
                                            <img class="direct-chat-img float-left rounded-circle border border-warning" src="{{asset('/images/users/150x150/'.($m->sender_photo??'avata.png'))}}" style="width: 50px"/>
                                            <div class="direct-chat-text float-left direct-chat-text-left">
                                            @if($m->is_file == 1)
                                                <a href="{{asset('/images/messages/'.$m->file)}}" class="image-view">
                                                    <img src="/images/loading.svg"  data-src="{{asset('/images/messages/'.$m->file)}}" style="width: 150px;" class="img-chat rounded"/>
                                                </a>
                                            @else
                                                    {!! $m->message !!}
                                            @endif
                                            </div>
                                        </div>
                                    </div>

                                @endif
                            @endforeach
                        </div>
                        <br/><br/>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer pl-0 pr-0 pt-0 pb-0 border-0 fixed-bottom">
                        <input type="file" id="file_brow" class="d-none" accept="image/*"/>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text btn" id="attach_file">
                                    <i class="material-icons">attach_file</i>
                                </div>
                            </div>

                            <textarea id="message" placeholder="@lang('home.type_your_message')"
                                class="form-control border-bottom-0 chat_control" style="height: 44px" autocomplete="off" autofocus></textarea>
                            <span class="input-group-append">
                                    <div class="input-group-text btn btn-domrey" id="btn-chat-send"><i class="material-icons">send</i></div>
                            </span>
                        </div>
                    </div>
                    <!-- /.card-footer-->
                </div>

            </div>
        </div>
    </div>

</div>

<script>

    $(function() {
        $('.grid_balckground_img_50').imageloader({
            background: true,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });
        $('.img-chat').imageloader({
            background: false,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });

    });

    $('.image-view').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		}
    });
    
    $('#attach_file').click(function(evt){
        $('#file_brow').click();
    });

    $('#file_brow').change(function () {
        if (this.files && this.files[0]) {
            var image = this.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                // var formData = new FormData();
                // formData.append('file', image);
                sendChatMessage(true);
                //$('#profile-image').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        } //End if

    });

    $('#btn-chat-send').click(function(event) {
        sendChatMessage();
    });

    $('#message').keyup(function (evt) {
        evt = evt || window.event;
        if (evt.keyCode == 13 && !evt.shiftKey) { //Key Enter
            sendChatMessage();
        }

    });

    var sendChatMessage = function(file = false){
        var sender = $('#sender').val();
        var post_id = $('#post_id').val();
        var chat_userid = $('#chat_user_id').val();
        var message = $('#message').val();
        message = message.replace(/(?:\r\n|\r|\n)/g, '<br />');

        var formData = new FormData();
        formData.append("sender", sender);
        formData.append("post_id", post_id);
        formData.append("chat_user_id", chat_userid);
        formData.append("message", message);

        if(file){
            formData.append("file", $('#file_brow')[0].files[0]);
        }
       
        if(message == '' && !file){
            htmlAllert('Alert!', 'Please enter message!');
            return false;
        }

        $.ajax({
            url: '/api/chat/chat/' + chat_userid,
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {

            },
            complete: function() {
                //$('#btn-save').html('Save');
                //htmlAllert('Success!', 'Your photo has been uploaded successfully!');
            },
            success: function(json) {
                $('#message').val('');
                $('#chat_user_id').val(json.chat_user_id);
                senderSMG(json);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };

    var senderSMG = function(json){
        var message = json.message;
        if(json.is_file == 1){
            message = '<img src="/images/messages/' + json.file + '" style="width: 150px;"/>';
        }

        var photo = json.sender_photo;
                    
        if(json.sender_photo == null) {
            photo = 'avata.png';
        }

        var senderHtml = '<div class="direct-chat-msg" data-id="'+json.id+'">'+
                            '<div class="direct-chat-infos clearfix">'+
                                '   <span class="direct-chat-name float-right">'+json.sender_name +'</span>'+
                                '  <span class="direct-chat-timestamp float-left">'+json.sending_time +'</span>'+
                            '</div>'+
                            '<div class="">'+
                                '<img class="direct-chat-img float-right rounded-circle border border-warning" src="/images/users/150x150/'+ photo +'" style="width: 50px" />'+
                                '<div class="direct-chat-text float-right">'+
                                    message +
                                '</div>'+
                            '</div>'+
                        '</div>';
        $('.direct-chat-messages').append(senderHtml);
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);

    };

    var readChatMessage = function(lastId){
        var formData = new FormData();
        var sender = $('#sender').val();
        var chat_user_id = $('#chat_user_id').val();
        var post_id = $('#post_id').val();

        var logUser = {{Auth::user()->id}};

        if(!chat_user_id){
            chat_user_id = 0;
        }

        formData.append("lastId", lastId);
        formData.append("chat_user_id", chat_user_id);
        formData.append("post_id", post_id);
        formData.append("sender", sender);

        $.ajax({
            url: '/api/chat/readChatMessage/',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {

            },
            complete: function() {
            },
            success: function(json) {
                
                var ids = [];

                for(i in json){
                    //If un read message
                    if(json[i].is_read == 0){
                        ids.push(json[i].id);
                    }

                    //Check default photo
                    var photo = json[i].sender_photo;
                    
                    if(json[i].sender_photo == null) {
                        photo = 'avata.png';
                    }

                    var message = json[i].message;

                    if(json[i].is_file == 1){
                        message = '<img src="/images/messages/' + json[i].file + '" style="width: 150px;"/>';
                    }

                    if(json[i].sender == parseInt(logUser)){
                        var senderHtml = '<div class="direct-chat-msg" data-id="'+json[i].id+'">'+
                                        '<div class="direct-chat-infos clearfix">'+
                                         '   <span class="direct-chat-name float-right">'+json[i].sender_name +'</span>'+
                                          '  <span class="direct-chat-timestamp float-left">'+json[i].sending_time +'</span>'+
                                        '</div>'+
                                        '<div class="">'+
                                            '<img class="direct-chat-img float-right rounded-circle border border-warning" src="/images/users/150x150/'+ photo +'" style="width: 50px" />'+
                                            '<div class="direct-chat-text float-right">'+
                                                message +
                                            '</div>'+
                                        '</div>'+
                                    '</div>';

                        $('.direct-chat-messages').append(senderHtml);
                    }else{
                        var revieverHtml = '<div class="direct-chat-msg" data-id="'+json[i].id+'">'+
                                    '<div class="direct-chat-infos clearfix">'+
                                    '<span class="direct-chat-name-left float-left">'+json[i].sender_name +'</span>'+
                                    '<span class="direct-chat-timestamp-left float-right">'+json[i].sending_time +'</span>'+
                                    '</div>'+
                                    '<div class="">'+
                                        '<img class="direct-chat-img float-left rounded-circle" src="/images/users/150x150/'+ photo +'" style="width: 50px">'+
                                        '<div class="direct-chat-text float-left direct-chat-text-left">'+
                                            message +
                                        '</div>'+
                                    '</div>'+
                                    '</div>';

                        $('.direct-chat-messages').append(revieverHtml);
                    }

                    $('#chat_user_id').val(json[i].chat_user_id);
                }//End
                if(json.length>0){
                    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                    //Update read message
                    updateReadMessage(ids);
                }

              
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    };

    //Update read message 
    var updateReadMessage = function(ids){
        var formData = new FormData();
        formData.append("ids", ids);

        $.ajax({
            url: '/api/chat/updateChatMessage',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {

            },
            complete: function() {
            },
            success: function(json) {


            }
        });
    };

    setInterval(function(){ 
        var smgItems = $('.direct-chat-messages').find('.direct-chat-msg');
        var lastItem = $(smgItems[smgItems.length - 1 ]).attr('data-id');

        if(smgItems.length == 0){
            lastItem = 0;
        }
        readChatMessage(lastItem);
    }, 5000);

   
    if($('#make_offer').val()!='0'){
        $('#message').val("Made the Offer  $ " + $('#make_offer').val());
        sendChatMessage();
    }

    $("html, body").animate({ scrollTop: $(document).height() }, 1000);


</script>


@endsection