 @foreach($posts as $post)
    <div class="col-6 col-sm-4 text-center product-item">
        <div class='thumbnail-domrey' style="height: 150px;">
            <a href="{{url('/post/detail/'.$post->id)}}">
                <div class="grid_balckground_img"
                    style="background-image: url('{{asset('images/loading.svg')}}');"
                    data-src="{{asset('/images/products/500x500/'.$post->image)}}">
                    
                </div>
            </a>
        </div>
        <div class="text-desc text-left">
            <div class="title">
                <a href="{{url('/post/detail/'.$post->id)}}">
                    {{Str::limit(Str::title($post->title), $limit = 16, $end = '...')}}
                </a>
            </div>
            <div class="price-font">
                ${{number_format($post->price)}}
            </div>
            <div class="description">
                <a href="{{url('/post/detail/'.$post->id)}}">
                    {{Str::limit(Str::title($post->description), $limit = 18, $end = '...')}}
                </a>
            </div>
            <div class="contition pt-2 pb-4">
                @if($post->condition=='USED')
                @lang('home.used')
                @else
                @lang('home.new')
                @endif
                
                <div class="btn-group float-right">
                    <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if($post->status==0)
                            <span class="disabled"><i class="material-icons">lock</i></span>
                        @elseif($post->status==-2)
                            <span class="disabled"><i class="material-icons">block</i></span>
                        @else
                            <span class="active"><i class="material-icons">public</i></span>
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        @if($post->status==0)
                            <button type="button" class="dropdown-item btn-publish" onclick="updateStatus({{$post->id}},  1, '{{Auth::user()->name}}');" data-status="{{$post->status}}"><i class="material-icons">public</i> Publish</button>
                        @elseif($post->status==-2)
                            <a class="dropdown-item btn-publish"  href="{{url('post/listingReport/'.$post->id)}}"><i class="material-icons">info</i> Info</a>
                        @else
                            <button type="button" class="dropdown-item btn-publish" onclick="updateStatus({{$post->id}},  0, '{{Auth::user()->name}}');" data-status="{{$post->status}}"><i class="material-icons">lock</i> Private</button>
                        @endif
                    </div>
                </div>
            </div>
            <div class="edit-listing text-center">
                <a href="{{url('/post/createNew/'.$post->id??0)}}" class="float-left">
                        <i class="material-icons">edit</i>
                </a>
                <span class="text-center">
                    <img src="{{asset('/images/like-outlined.svg')}}" class="text-center" width="20px"/> 
                    <span class="pl-0">{{$post->rate>0?$post->rate:''}}</span>
                </span>
                <a onclick="deleteListing({{$post->id}}, -1, '{{Auth::user()->name}}');" class="float-right">
                        <i class="material-icons">delete</i>
                </a>
            </div>
        </div>
    </div>
@endforeach