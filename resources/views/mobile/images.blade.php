@extends('layouts.appMobile')
@section('content')
    <style>
        body{
            background-color: #57585a!important;
        }
    </style>
    <div class="row">
        <div class="col-12 pl-0 pr-0 item-photos">
            <div class="back">
                <a href="javascript:goBack()"><i class="material-icons">keyboard_backspace</i></a>
            </div>
            @foreach($images as $image)
                <div class="item-image">
                    <img src="{{asset('images/loading.svg')}}" data-src="{{ asset('images/products/500x500/'.$image->name)}}" class="image-items"/>
                </div>
            @endforeach
            <div class="neet-more-image">
                <i class="material-icons">chat_bubble</i> <br/>
                Need more photos? chat with the lister!
            </div>
        </div>
    </div>
    
    <script>
    $(function() {
        $('.image-items').imageloader({
                //selector: '.defferred',
                callback: function (elm) {
                    $(elm).slideDown();
                }
            });
         });
</script>

@endsection