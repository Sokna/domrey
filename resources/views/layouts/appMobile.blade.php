<!DOCTYPE html>
<html lang="en">

<head data-view="{{$mobile}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">

    <!-- Property -->
    <meta property="og:url" content="@yield('url','http://supervong.com')" />
    <meta property="og:type" content="Supervong.com" />
    <meta property="og:title"
        content="@yield('title', 'Snap to Sell, Chat to Buy for FREE on the Supervong marketplace!')" />
    <meta property="og:description" content="@yield('description','Supervong.com')" />
    <meta property="og:image" content="@yield('image', asset('images/icon.png'))?time={{time()}}" />

    <!-- Favicons -->
    <link rel="icon" href="{{ asset('images/icon.png') }}" sizes="32x32" type="image/jpg">
    <title>@yield('title', 'Snap to Sell, Chat to Buy for FREE on the Supervong marketplace!')</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/fonts/material.css') }}" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Hanuman" rel="stylesheet">

    <!-- <link href="./css/fonts/font-awesome.min.css" rel="stylesheet"> -->
    <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/mobile.css')}}?t={{time()}}" rel="stylesheet" />


    <script src="{{ asset('js/bootstrap//jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

    <!--Owl Carousel-->
    <link rel="stylesheet" href="{{ asset('OwlCarousel2/dist/assets/owl.carousel.min.css')}}" />
    <script src="{{ asset('OwlCarousel2/dist/owl.carousel.min.js')}}"></script>

    <!-- croppie -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>


    <!-- Image loading lib -->
    <script type="text/javascript" src="{{asset('js/jquery.imageloader.js')}}"></script>

    <!-- Gallery -->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css' />
    <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js'></script>


     <!-- tagsinput -->
    <link rel="stylesheet" href="{{asset('tagsinput/tagsinput.css')}}"/>
    <script src="{{asset('tagsinput/tagsinput.js')}}"></script>

</head>

<body>
    <div class="container-fluid" id="container-fluid">
        @yield('content')
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" data-user="{{(Auth::user()->id??0)}}"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-title="@lang('home.report_listing')">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('home.close')</button>
                    <button type="button" class="btn btn-domrey" disabled>@lang('home.send')</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalAlert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">@lang('home.alert')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('home.close')</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Table sell -->
    @if(!request()->is('post/detail*') &&
    !request()->is('post/images*') &&
    !request()->is('profile/settings') &&
    !request()->is('profile/account') &&
    !request()->is('profile/profile') &&
    !request()->is('password/reset') &&
    !request()->is('profile/chat*') &&
    !request()->is('register'))
    <div class="footer-fix" id="footer-fix">
        <div class="row">
            <div class="col-4 pt-0 pl-0 pr-0 text-center item {{(request()->is('home*') || request()->is('post/category*') || request()->is('post/detail*') || request()->is('/'))?'active':''}}"
                data-id="explore">
                <a href="{{url('home')}}" class="btn btn-light border-0 rounded-0 w-100">
                    <i class="material-icons domrey-color">explore</i><br />
                    <span>@lang('home.explore_bottom')</span>
                </a>
            </div>

            <div class="col-4 pt-0 pl-0 pr-0 text-center item {{(request()->is('post/create*'))?'active':''}}" data-id="add">
                <a href="{{url('post/createNew')}}" class="btn btn-light border-0 rounded-0 w-100">
                    <i class="material-icons text-danger">add_box</i><br />
                    <span>@lang('home.sell')</span>
                </a>
            </div>
            <div class="col-4 pt-0 pl-0 pr-0 text-center item {{(request()->is('me/*') || request()->is('login*') || request()->is('register*') || request()->is('profile/*'))?'active':''}}"
                data-id="me">
                <a href="{{url('/me')}}" class="btn btn-light border-0 rounded-0 w-100">
                    <i class="material-icons domrey-color">person</i><br />
                    <span class="">@lang('home.me')</span>
                </a>
            </div>
        </div>
    </div>
    @endif

    <script src="{{ asset('js/mobile.js')}}?t={{time()}}"></script>
    <!-- Detect layout -->
    <script src="{{ asset('js/screen.js')}}"></script>
</body>

</html>