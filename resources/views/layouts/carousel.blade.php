<!DOCTYPE html>
<html>

<head>
    <title>Slick Playground</title>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Hanuman" rel="stylesheet">
    <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/slick/slick.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/slick/slick-theme.css') }}" />
    <style type="text/css">
    body{
        font-family: -apple-system,BlinkMacSystemFont,segoe ui,Roboto,helvetica neue,Arial,sans-serif,apple color emoji,segoe ui emoji,segoe ui symbol,noto color emoji,hanuman!important;   
    }
    h1, h2, h3, h4, h5, h6{
        font-family: -apple-system,BlinkMacSystemFont,segoe ui,Roboto,helvetica neue,Arial,sans-serif,apple color emoji,segoe ui emoji,segoe ui symbol,noto color emoji,hanuman!important;   
    }

    a {
        text-decoration: none!important;
        color: #6c757d!important;
    }

   
    .slider {
        width: 95%;
        margin: 0px auto;
    }

    .slick-slide {
        margin: 0px 0px;
    }

    .slick-slide img {
        width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
        color: black;
    }

    .span{
        padding: 5px;
    }
    .tages-btn a{
        font-size: 10px;
    }
    </style>
</head>

<body>

    <section class="regular slider">
        @foreach($categories as $category)
            <div class="text-center row">
                <div class="col-12 col-sm-12">
                    <a href="{{url('/post/category/'.$category->id)}}" target="_PARENT">
                        <img src="{{ asset('images/explore/'.($category->icon??'following.png'))}}" class="img-fluid"/>
                        <div class="text-center d-none d-md-block" style="padding: 5px;font-size: 13px;">{{($lang=='kh'?$category->name_kh: $category->name)}}</div>
                    </a>
                </div>
            </div>
        @endforeach
    </section>

    <!-- <div class="d-block d-md-none tages-btn">
        @foreach($categories as $category)
            <a href="{{url('/post/category/'.$category->id)}}" target="_PARENT" class="btn btn-outline-secondary btn-sm rounded-pill">{{$category->name}}</a>
        @endforeach
    </div> -->


    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="{{ asset('/slick/slick.js') }}" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
    $(document).on('ready', function() {
        $(".regular").slick({
            dots: false,
            infinite: false,
            slidesToShow: 12,
            slidesToScroll: 12
        });
    });
    </script>

</body>

</html>