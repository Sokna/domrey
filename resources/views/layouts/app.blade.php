<!DOCTYPE html>
<html lang="en">

<head data-view="{{$mobile}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Property -->
    <meta property="og:url" content="@yield('url','http://Supervong.com')" />
    <meta property="og:type" content="Supervong.com" />
    <meta property="og:title" content="@yield('title', 'Snap to Sell, Chat to Buy for FREE on the Supervong marketplace!')" />
    <meta property="og:description" content="@yield('description','Supervong.com')" />
    <meta property="og:image" content="@yield('image', asset('images/icon.png'))?time={{time()}}" />

    <!-- Favicons -->
    <link rel="icon" href="{{ asset('images/icon.png') }}" sizes="32x32" type="image/jpg">
    <title>@yield('title', 'Snap to Sell, Chat to Buy for FREE on the Supervong marketplace!') </title>

    <link href="https://fonts.googleapis.com/css?family=Hanuman" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/fonts/material.css') }}" rel="stylesheet" />
    <!-- <link href="./css/fonts/font-awesome.min.css" rel="stylesheet"> -->
    <link href="{{ asset('css/bootstrap/bootstrap.min.css') }}" rel="stylesheet" />

    <script src="{{ asset('js/bootstrap//jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
	
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/blog.css') }}?t={{time()}}" rel="stylesheet" />

    <link href="{{ asset('css/style.css')}}?t={{time()}}" rel="stylesheet" />

    <link href="{{ asset('css/autocomplet.css')}}" rel="stylesheet" />

    <!-- croppie -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/> -->

    <!-- emoji -->
    <script type="text/javascript" src="https://rawgit.com/mervick/emojionearea/master/dist/emojionearea.js"></script>
    <link rel="stylesheet" type="text/css" href="https://rawgit.com/mervick/emojionearea/master/dist/emojionearea.min.css"/>


    <!--Owl Carousel-->
    <link rel="stylesheet" href="{{ asset('OwlCarousel2/dist/assets/owl.carousel.min.css')}}" />
    <script src="{{ asset('OwlCarousel2/dist/owl.carousel.min.js')}}"></script>
    
    <!-- Detect layout -->
    <script src="{{ asset('js/screen.js')}}"></script>

    <!-- Gallery -->
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css'/>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js'></script>
    <style>
        body{
            font-family: -apple-system,BlinkMacSystemFont,segoe ui,Roboto,helvetica neue,Arial,sans-serif,apple color emoji,segoe ui emoji,segoe ui symbol,noto color emoji,hanuman!important;   
        }
        h1, h2, h3, h4, h5, h6{
            font-family: -apple-system,BlinkMacSystemFont,segoe ui,Roboto,helvetica neue,Arial,sans-serif,apple color emoji,segoe ui emoji,segoe ui symbol,noto color emoji,hanuman!important;   
        }
    </style>

    <!-- Image loading lib -->
    <script type="text/javascript" src="{{asset('js/jquery.imageloader.js')}}"></script>

    <!-- tagsinput -->
    <link rel="stylesheet" href="{{asset('tagsinput/tagsinput.css')}}"/>
    <script src="{{asset('tagsinput/tagsinput.js')}}"></script>
</head>

<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0">
    </script>

    <div class="container" style="max-width: 1290px;">
        <div class="row">
            <div class="col-6 text-left d-block d-md-none">
                <div class="pt-2" style="font-size: 13px;">
                    <a class="text-muted" href="{{url('/')}}">
                        <img alt="" style="width: 150px;" src="{{ asset('images/logo.png') }}" class="img-fluid" title="Supervong.com">
                    </a>
                    <span class="english-flage flage-lang" data-lang="kh" data-csrf_token="{{ csrf_token() }}"></span>
                </div>
            </div>
            <div class="col-1 text-right d-block d-md-none header-lang">
                <span class="{{($lang=='kh')?'english-flage':'khmer-flage'}} flage-lang" data-lang="{{($lang=='kh')?'en':'kh'}} " data-csrf_token="{{ csrf_token() }}"></span>
            </div>
            <div class="col-5 text-right d-block d-md-none">
                @guest
                <div class="pt-3 pr-1" style="font-size: 13px;font-weight: bold;">
                    <a href="{{url('/login')}}">@lang('home.login')</a> | <a href="{{url('/register')}}">@lang('home.register_here')</a>
                </div>
                @endguest
                @auth
                <div class="dropdown" id="profile" style="top: 6px;left: -10px;">
                    
                    <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{ asset('images/users/150x150') }}/{{Auth::user()->photo??'avata.png'}}?t={{time()}}" alt=""
                            class="rounded-circle border border-warning" style="width: 40px; height: 40px;"/>
                            @if($un_read>0)
                            <span class="notification">{{$un_read}}</span>
                            @endif
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <ul>
                            <li class="{{request()->is('post/create*')?'item-active':''}}">
                                <a href="{{url('post/createNew')}}">
                                    <i class="material-icons">add_box</i><span>@lang('home.sell')</span>
                                </a>
                            </li>
                            <li class="{{request()->is('post/list*')?'item-active':''}}">
                                <a href="{{url('post/list')}}">
                                    <i class="material-icons">folder_open</i><span>@lang('home.my_item')</span>
                                </a>
                            </li>
                            <li class="{{request()->is('sellers*')?'item-active':''}}">
                                <a href="{{url('sellers/'.Auth::user()->uuserid)}}">
                                    <i class="material-icons">shop</i><span>@lang('home.shop_page')</span>
                                </a>
                            </li>
                            <li class="{{request()->is('profile/chat*')?'item-active':''}}">
                                <a href="{{ url('/profile/chatRoom') }}">
                                    <i class="material-icons">chat</i><span>@lang('home.message')</span>
                                    @if($un_read>0)
                                        <span class="notification-1">{{$un_read}}</span>
                                    @endif
                                </a>
                            </li>
                            <li class="{{request()->is('profile/profile')?'item-active':''}}">
                                <a href="{{url('/profile/profile')}}"><i class="material-icons">info</i><span>@lang('home.my_address')</span></a>
                            </li>
                            <li class="{{request()->is('profile/account')?'item-active':''}}">
                                <a href="{{url('/profile/account')}}">
                                    <i class="material-icons">settings</i><span>@lang('home.setting')</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/logout')}}">
                                    <i class="material-icons">exit_to_app</i><span>@lang('home.logout')</span>
                                </a>
                            </li>

                        </ul>

                    </div>
                </div>
                @endauth
            </div>
        </div>
        <header class="blog-header py-3">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-12 col-sm-12 col-md-2 col-xl-2 pt-1 d-none d-lg-block">
                    <a class="text-muted" href="{{url('/')}}">
                        <img alt="" style="width: 163px;" src="{{ asset('images/logo.png') }}" class="img-fluid" title="Supervong.com">
                    </a>
                   
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-xl-6 text-center">
                    <div class="input-group search-box">
                        <input type="hidden" name="search_param" value="all" id="search_param">

                        <input type="text" class="form-control text-box-search" value="{{ request()->text}}" id="text-search" placeholder="@lang('home.search')" />

                        <span class="input-group-btn" style="position: relative;">
                            <button class="btn btn-default" type="button" id="btn-search" data-url="{{url('/')}}">
                                <i class="material-icons">search</i>
                            </button>
                        </span>
                        <div id="text-searchautocomplete-list" class="autocomplete-items">
                        </div>

                    </div>
                  
                </div>
                <div class="col-12 col-sm-12 col-md-1 col-xl-1 text-center">
                      <span class="{{($lang=='kh')?'english-flage':'khmer-flage'}} flage-lang" data-lang="{{($lang=='kh')?'en':'kh'}} " data-csrf_token="{{ csrf_token() }}"></span>
                </div>
                @auth
                <div class="col-12 col-sm-12 col-md-1 col-xl-1 text-right d-none d-md-block" style="font-size: 12px;position: relative;left: 42px;top: 0px;">
                    <span class="d-none d-lg-block">@lang('home.hi'), {{ Auth::user()->name }}</span>
                </div>
                @endauth
                <div class="col-12 col-sm-12 col-md-2 col-xl-1 text-right">
                    @guest
                        <div class="text-center" style="font-size: 13px;width: 180px;font-weight: bold;">
                            <a href="{{url('/login')}}">@lang('home.login')</a>&nbsp;|&nbsp;<a href="{{url('/register')}}">@lang('home.register_here')</a>
                        </div>
                    @endguest

                    @auth
                    <div class="dropdown" id="profile">
                        <a href="#" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="{{ asset('images/users/150x150') }}/{{Auth::user()->photo??'avata.png'}}?t={{time()}}" alt=""
                                class="rounded-circle border border-warning" style="width: 40px; height: 40px;"/>
                              @if($un_read>0)
                                <span class="notification">{{$un_read}}</span>
                              @endif
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <ul>
                                <li class="{{request()->is('post/create*')?'item-active':''}}">
                                    <a class="" href="{{url('post/createNew')}}">
                                        <i class="material-icons">add_box</i><span>@lang('home.sell')</span>
                                    </a>
                                </li>
                                <li class="{{request()->is('post/list*')?'item-active':''}}">
                                    <a class="" href="{{url('post/list')}}">
                                        <i class="material-icons">folder</i><span>@lang('home.my_item')</span>
                                    </a>
                                </li>
                                <li class="{{request()->is('sellers*')?'item-active':''}}">
                                    <a class="" href="{{url('sellers/'.Auth::user()->uuserid)}}">
                                        <i class="material-icons">shop</i><span>@lang('home.shop_page')</span>
                                    </a>
                                </li>
                                <li class="{{request()->is('profile/chat*')?'item-active':''}}">
                                    <a href="{{ url('/profile/chatRoom') }}">
                                        <i class="material-icons">chat</i><span>@lang('home.message')</span>
                                        @if($un_read>0)
                                            <span class="notification-1">{{$un_read}}</span>
                                        @endif
                                    </a>
                                </li>
                                <li class="{{request()->is('profile/profile')?'item-active':''}}">
                                    <a href="{{url('/profile/profile')}}"><i class="material-icons">info</i><span>@lang('home.my_address')</span></a>
                                </li>
                                <li class="{{request()->is('profile/account')?'item-active':''}}">
                                    <a class="" href="{{url('/profile/account')}}">
                                        <i class="material-icons">settings</i><span>@lang('home.setting')</span>
                                    </a>
                                </li>
                                <li>
                                    <a class="" href="{{url('/logout')}}">
                                        <i class="material-icons">exit_to_app</i><span>@lang('home.logout')</span>
                                    </a>
                                </li>

                            </ul>

                        </div>
                    </div>
                    @endauth
                </div>
                <div class="col-12 col-sm-12 col-md-1 col-xl-1 text-center">
                    <a href="{{url('/post/createNew')}}" class="btn btn-warning btn-domrey">@lang('home.sell')</a>
                </div>
            </div>
        </header>
        <nav class="megamenu d-block d-md-none" style="padding-left: 90px;overflow-x: auto;">
            <ul class="megamenu-nav d-flex justify-content-center" role="menu">
                @foreach($mega_menu as $m)
                <li class="nav-item is-parent">
                    <a class="nav-link sm-menu-a" href="#" id="megamenu-dropdown-1" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">{{$m['icon']}}</i>
                    </a>
                    <div class="megamenu-content" aria-labelledby="megamenu-dropdown-1">
                        <div class="container scroll" style="overflow-y: auto;height: 400px;max-width: 1262px;">
                            <div class="row">
                                @foreach($m['children'] as $ch)
                                <div class="col-{{$m['col']}}">
                                    <div class="">
                                        <strong>
                                            <a href="{{url('/post/category/'.$ch['id'])}}">
                                                @if($lang=='en')
                                                    {{$ch['name']}}
                                                @else
                                                     {{$ch['name_kh']}}
                                                @endif
                                            </a>
                                        </strong>
                                    </div>
                                    <hr>
                                    <ul class="subnav" style="padding-bottom: 10px;">
                                        @foreach($ch['children'] as $item)
                                        <li class="subnav-item">

                                            <a href="{{url('/post/category/'.$item->id)}}"
                                                class="subnav-link">
                                                @if($lang=='en')
                                                    {{$item->name}}
                                                @else
                                                    {{$item->name_kh}}
                                                @endif
                                                
                                                </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="megamenu-background" id="megamenu-background"></div>
        </nav>

        <nav class="megamenu d-none d-md-block">
            <ul class="megamenu-nav d-flex justify-content-center" role="menu" >
                @foreach($mega_menu as $m)
                <li class="nav-item is-parent">
                    <a class="nav-link" href="#" id="megamenu-dropdown-1" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">{{$m['icon']}}</i><span class="icon {{$lang=='kh'?'span_font_kh':''}}">{{$lang=='en'?$m['name']:$m['name_kh']}}</span>
                    </a>
                    <div class="megamenu-content" aria-labelledby="megamenu-dropdown-1">
                        <div class="container scroll" style="overflow-y: auto;height: 400px;max-width: 1262px;">
                            <div class="row">
                                @foreach($m['children'] as $ch)
                                <div class="col-{{$m['col']}}">
                                    <div class="">
                                        <strong>
                                            <a href="{{url('/post/category/'.$ch['id'])}}">
                                                @if($lang=='en')
                                                    {{$ch['name']}}
                                                @else
                                                     {{$ch['name_kh']}}
                                                @endif
                                            </a>
                                        </strong>
                                    </div>
                                    <hr>
                                    <ul class="subnav" style="padding-bottom: 10px;">
                                        @foreach($ch['children'] as $item)
                                        <li class="subnav-item">
                                            <a href="{{url('/post/category/'.$item->id)}}"
                                                class="subnav-link">
                                                
                                                @if($lang=='en')
                                                    {{$item->name}}
                                                @else
                                                    {{$item->name_kh}}
                                                @endif
                                                
                                                
                                                </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="megamenu-background" id="megamenu-background"></div>
        </nav>

        @yield('content')

        <div class="space-domrey">

        </div>
    </div>
     <!-- Table sell -->
    <div class="footer-fix">
        <div class="row">
            <div class="col-4 text-center item btn  {{(request()->is('home*') || request()->is('post/category*') || request()->is('post/detail*') || request()->is('/'))?'active':''}}">
                <a href="{{url('/home')}}">
                    <i class="material-icons domrey-color">search</i><br/>
                    <span class="{{(request()->is('home*') || request()->is('post/category*') || request()->is('post/detail*') || request()->is('/'))?'active':''}}">@lang('home.explore_bottom')</span>
                </a>
            </div>
            <div class="col-4 text-center item btn {{request()->is('post/create*')?'active':''}}">
                <a href="{{url('post/createNew')}}">
                    <i class="material-icons domrey-color">add_box</i><br/>
                    <span class="{{request()->is('post/create*')?'active':''}}">@lang('home.sell')</span>
                </a>
            </div>
            <div class="col-4 text-center item btn {{(request()->is('sellers/*') || request()->is('login*') || request()->is('register*'))?'active':''}}">
                @auth
                    <a href="{{url('/sellers/'.Auth::user()->name)}}">
                        <i class="material-icons domrey-color">person</i><br/>
                        <span class="{{request()->is('sellers/*')?'active':''}}">@lang('home.me')</span>
                    </a>
                @endauth

                @guest
                    <a href="{{url('/login')}}">
                        <i class="material-icons domrey-color">person</i><br/>
                        <span class="{{(request()->is('login*') || request()->is('register*'))?'active':''}}">@lang('home.me')</span>
                    </a>
                @endguest
            </div>
        </div>
    </div>

    <footer class="blog-footer d-none d-md-block">
        <div style="max-width: 1290px;margin: 0 auto;padding-right: 20px;padding-left: 20px;">
            <div class="row">
                <div class="col-md-2 text-left">
                    <strong>Follow us</strong>
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="{{url('pages/contact')}}">Contact Us</a></li>
                        <li><a href="#">Advertise with Us</a></li>
                    </ul>
                </div>

                @foreach($mega_menu as $m)
                <div class="col-md-2 text-left">
                    <strong>{{($lang=='kh'?$m['name_kh']:$m['name'])}}</strong>
                    <ul>
                        @foreach($m['children'] as $ch)
                        <li><a href="{{url('post/category/'.$ch['id'])}}">{{($lang=='kh'?$ch['name_kh']:$ch['name'])}}</a></li>
                        @endforeach
                    </ul>
                </div>
                @endforeach

            </div>

            <div class="text-left"
                style="font-size: 13px;margin-top: 40px;border-top: 1px solid #e9ecef;padding-top: 20px;">
                <img alt="Supervong.com" style="width: 100px;" src="{{asset('/images/logo.png')}}" title="Supervong.com"> ©
                2020 Supervong.com
            </div>
        </div>
    </footer>

    <!-- Tooast -->
    <div role="alert" aria-live="assertive" aria-atomic="true" id="toast" class="toast" data-autohide="true"
        style="display: none;">
        <!-- style="position: absolute; top: 8px;right: 10px;" -->
        <div class="toast-header">
            <strong class="mr-auto text-success">Success!</strong>
            <!-- <small>11 mins ago</small> -->
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body text-success">
            Your status has been changed successfully.
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" data-user="{{(Auth::user()->id??0)}}"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-title="@lang('home.report_listing')">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('home.close')</button>
                    <button type="button" class="btn btn-domrey" disabled>@lang('home.send')</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModalAlert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">@lang('home.alert')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('home.close')</button>
                </div>
            </div>
        </div>
    </div>


    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.hoverintent/1.9.0/jquery.hoverIntent.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js'></script>


    <script src="{{ asset('js/script.js') }}?t={{time()}}"></script>
    <script src="{{ asset('js/index.js') }}?t={{time()}}"></script>
    <!-- API -->
    <script src="{{ asset('js/api/post.js')}}?t={{time()}}"></script>
    <script src="{{ asset('js/api/profile.js')}}?t={{time()}}"></script>
    <script src="{{ asset('js/api/user.js')}}?t={{time()}}"></script>
    
</body>

</html>