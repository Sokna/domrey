<ul class="list-group mb-2 d-none d-md-block">
    <li class="list-group-item {{request()->is('post/create*')?'item-active':''}}"><a href="{{ url('post/createNew') }}"><i class="material-icons">add_box</i>@lang('home.sell')</a></li>
    <li class="list-group-item {{request()->is('post/list*')?'item-active':''}}"><a href="{{ url('/post/list') }}"><i class="material-icons">folder</i>@lang('home.my_item')</a></li>
    <li class="list-group-item "><a href="{{ url('sellers/'.(Auth::user()->uuserid??''))}}"><i class="material-icons">shop</i>@lang('home.shop_page')</a></li>
    <li class="list-group-item {{request()->is('profile/chat*')?'item-active':''}}">
        <a href="{{ url('/profile/chatRoom') }}">
            <i class="material-icons">chat</i>@lang('home.message')
            @if($un_read>0)
                <span class="notification-1">{{$un_read}}</span>
            @endif
        </a>
    </li>
    <li class="list-group-item {{request()->is('profile/profile')?'item-active':''}}"><a href="{{ url('/profile/profile') }}"><i class="material-icons">info</i>@lang('home.my_address')</a></li>
    <li class="list-group-item {{request()->is('profile/account')?'item-active':''}}"><a href="{{ url('/profile/account') }}"><i class="material-icons">settings</i>@lang('home.setting')</a></li>
    <li class="list-group-item"><a href="{{url('/logout')}}"><i class="material-icons">exit_to_app</i>@lang('home.logout')</a></li>
</ul>

