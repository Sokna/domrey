<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">{{$dashboard}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$page_title}}</li>
    </ol>
</nav>