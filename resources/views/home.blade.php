@extends('layouts.app')

@section('content')
<div class="banner">
    <div id="demo" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ul class="carousel-indicators d-none">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
            <div class="carousel-item text-center active">
                <img src="{{asset('images/loading.svg')}}" data-src="{{asset('/images/banner.jpg')}}" class="img-fluid item-slide" />
            </div>
            <div class="carousel-item text-center">
                <img src="{{asset('images/loading.svg')}}" data-src="{{asset('/images/banner.jpg')}}" class="img-fluid item-slide" />
            </div>
            <div class="carousel-item text-center">
                <img src="{{asset('images/loading.svg')}}" data-src="{{asset('/images/banner.jpg')}}" class="img-fluid item-slide" />
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
    </div>
</div>

<div class="product-content">
    <div class="col-12 box-sadow">
        <h3>@lang('home.explore')</h3>
        <!--<iframe src="{{url('/carousel')}}" style="border:none; width: 100%; height: 140px;" scrolling="no"></iframe>-->
        <div class="owl-carousel owl-theme">
            @foreach($categories as $category)
                @if(!$category->parent_id)
                    <div class="item-owl-domrey-category">
                        <a href="{{url('/post/category/'.$category->id)}}">
                            <img src="{{ asset('images/explore/'.($category->icon??'following.png'))}}" class="img-fluid"/>
                            <div class="text-center" style="padding: 5px;font-size: 13px;">{{($lang=='kh'?$category->name_kh: $category->name)}}</div>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</div>
<div class="product-content">
    <div class="col-12 box-sadow">
        <h3>@lang('home.top_product')</h3>
        <div class="row" id="list_items">
            @foreach($posts as $p)
              <div class="col-6 col-md-4 col-lg-3 col-xl-2 text-center products">
                  <div class="imag-product">
                      <div class="avatar">
                          <div class="row">
                              <div class="col-3 col-md-2 text-left">
                                <a href="{{url('/sellers/'.$p->uuser_id)}}">
                                    <img src="{{asset('/images/users/150x150/'.$p->uuser_photo)}}?t={{time()}}" alt="" class="circle responsive-img"/>
                                </a>
                               
                              </div>
                              <div class="col-9 col-md-10 user-avatar text-left">
                                <a href="{{url('/sellers/'.$p->uuser_id)}}">
                                  <span class="name">{{$p->uuser_id}}</span><br />
                                  <span class="time">

                                    @if($p->Y>0)
                                        {{$p->Y}} @lang('home.hours_ago')
                                    @elseif($p->MO >0)
                                        {{$p->Y}} @lang('home.months_ago')
                                    @elseif($p->D >0)
                                        @if($p->H>12)
                                            {{$p->D + 1}} @lang('home.days_ago')
                                        @else
                                            {{$p->D}} @lang('home.days_ago')
                                        @endif
                                    @elseif($p->H >0)
                                        {{$p->H}} @lang('home.hours_ago')
                                    @elseif($p->M >0 )
                                        {{$p->M}} @lang('home.minutes_ago')
                                    @elseif($p->S >0 )
                                        {{$p->S}} @lang('home.seconds_ago')
                                    @endif

                                  </span>
                                </a>
                              </div>
                          </div>
                      </div>
                      <div class='thumbnail-domrey'>
                        <a href="{{url('/post/detail/'.$p->id)}}">
                           <div class="grid_balckground_img" 
                           style="background-image: url('{{asset('images/loading.svg')}}');" 
                           data-src="{{asset('/images/products/500x500/'.$p->image)}}">
                           </div>
                        </a>
                      </div>
                      <div class="text-desc text-left">
                          <div class="title">
                            <a href="{{url('/post/detail/'.$p->id)}}">
                             {{Str::limit($p->title, $limit = 30, $end = '...')}}
                            </a>
                          </div>
                          <div class="price-font">
                              ${{number_format($p->price)}}
                          </div>
                          <div class="description">
                            <a href="{{url('/post/detail/'.$p->id)}}">
                              {{Str::limit($p->description, $limit = 30, $end = '...')}}
                            </a>
                              
                          </div>
                          @if($p->condition=='USED')
                            <div class="">
                                @lang('home.used')
                            </div>
                           @else
                            <div class="">
                                @lang('home.new')
                            </div>
                            @endif
                          <div class="like">
                            <a class="btn-like-listing" data-id="{{$p->id}}" onclick="btnHeartLike(this)">
                                @if($p->like_user==(Auth::user()->id??-1))
                                    <img src="{{asset('/images/like-filled.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$p->rate>0?$p->rate:''}}</span>
                                @else
                                    <img src="{{asset('/images/like-outlined.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$p->rate>0?$p->rate:''}}</span>
                                @endif
                                
                                
                            </a>
                            <a id="dropdownMenuRe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <svg  height="16" width="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" class="_3EdKwhRm4f float-right">
                                    <path fill-rule="nonzero" d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z" fill="#57585a"></path>
                                </svg>
                            </a>
                            <div class="dropdown-menu report-listing" aria-labelledby="dropdownMenuRe">
                                <button class="dropdown-item btn-report-listing" data-id="{{$p->id}}" onclick="btnReportListing({{$p->id}})" type="button">@lang('home.report_listing')</button>
                            </div>
                          </div>
                      </div>

                  </div>
              </div>
            @endforeach
            
        </div>
        @if($total_item > $limit_item)
            <div class="row">
                <div class="col-12 text-center" style="margin-top:20px;">
                    <button type="button" id="view_more" class="btn btn-outline-secondary mb-2" data-lid="{{$p->id}}">@lang('home.view_more')</button>
                </div>
            </div>
        @endif
    </div>
</div>
<div class="product-content">
    <div class="col-12 box-sadow">
        <h3>@lang('home.top_seller')</h3>
        <div class="owl-carousel owl-theme owl-seller">
            @foreach($users as $u)
                <div class="item-owl-domrey">
                    <a href="{{url('/sellers/'.$u->uuserid)}}">
                        <img src="{{asset('images/loading.svg')}}"  data-src="{{asset('images/users/150x150/'. ($u->photo??'avata.png'))}}?t={{time()}}" class="rounded seller-profile"/>
                        <div class="text-center">
                            {{$u->uuserid}}<br/>
                            <div style="font-size: 13px;">
                                @lang('home.online') 
                                @if($u->Y>0)
                                    {{$user->Y}} @lang('home.years_ago')
                                @elseif($u->MO >0)
                                    {{$u->MO}} @lang('home.months_ago')
                                @elseif($u->D >0)
                                    {{$u->D}} @lang('home.days_ago')
                                @elseif($u->H >0)
                                    {{$u->H}} @lang('home.hours_ago')
                                @elseif($u->M >0 )
                                    {{$u->M}} @lang('home.minutes_ago')
                                @elseif($u->S >0 )
                                    {{$u->S}} @lang('home.seconds_ago')
                                @endif
                                <br/>
                                {{$u->products}} Listings 
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
</div>

<script>

    $(function() {
        $('.grid_balckground_img').imageloader({
            background: true,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });

        $('.item-slide,.seller-profile').imageloader({
            background: false,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });
    });

    $(document).ready(function(){
        $('.owl-carousel').owlCarousel(
            {
                nav: true,
                navText: ["<i class='material-icons'>navigate_before</i>","<i class='material-icons'>navigate_next</i>"],
                margin: 2,
                autoWidth: true,
            }
        );
    });

    $('#view_more').click(function(){
        var formData = new FormData();
        var lid= $(this).attr('data-lid');
        $.ajax({
            url: '/post/getLastId/' + lid,
            type: 'GET',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                 $('#view_more').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            },
            complete: function () {
                $('#view_more').html("@lang('home.view_more')");
            },
            success: function (json) {
                if(json.lid > 0){
                    $('#view_more').attr('data-lid', json.lid);
                    loadContent(lid);
                }

                if(json.count < json.limit){
                    $('#view_more').addClass('d-none');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });


        var loadContent = function(lid){
            $.ajax({
                url: '/post/viewMore/' + lid,
                type: 'GET',
                data: {
                },
                dataType: 'html',
                beforeSend: function () {
                
                },
                complete: function () {

                },
                success: function (datahtml) {
                    $('#list_items').append(datahtml);

                     $('#list_items .grid_balckground_img').imageloader({
                        background: true,
                        callback: function (elm) {
                            $(elm).slideDown('slow');
                        }
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
    };

    
    });
</script>
@endsection