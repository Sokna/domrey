@extends('layouts.app')
@section('title', $user->first_name.' '.$user->last_name)
@section('content')
<div class="category-page">
    <div class="seller-background" style="background-image: url({{ asset('images/'.($user->cover_photo?'users/'.$user->cover_photo:'web_cover.png'))}}?t={{time()}});">
        @auth
            @if($user->id == Auth::user()->id)
                <i class="material-icons save d-none" title="Save" onclick="javascript:saveCover();">save</i>
                <i class="material-icons photo_camera" title="Change Conver" onclick="javascript:changeCover();">photo_camera</i><br/><br/>
                <i class="material-icons clear d-none" title="Cancel" onclick="javascript:clearCover();">clear</i>
                <input type="file" id="file_cover" accept="image/png, image/jpeg" class="d-none"/>

                <div class="text-center">Dimension 1260 x 120 pixels</div>
            @endif
        @endauth
    </div>
</div>
<div class="row">
    <div class="col-md-3 profile">
        <img src="{{asset('images/loading.svg')}}" data-src="{{asset('/images/users/150x150/'.($user->photo??'avata.png'))}}?t={{time()}}" class="image-profile img-fluid small-screen-img-profile d-block d-md-none"  style="height: 60px; width: 60px;"/>
        <img src="{{asset('images/loading.svg')}}" data-src="{{asset('/images/users/150x150/'.($user->photo??'avata.png'))}}?t={{time()}}" class="image-profile img-fluid d-none d-md-block"  style="height: 130px; width: 130px;"/>
        <div class="text-left col-md-12 col-profile">
            <h4>{{$user->first_name}} {{$user->last_name}}</h4>
            <span>&#64;{{$user->uuserid}}</span>
            @isset($user)
                <table>
                    <tr>
                        <td style="width: 60px;">@lang('home.joined'):</td>
                        <td>
                            {{$user->dayname}}
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 60px;">@lang('home.online'):</td>
                        <td>
                            @if($user->Y>0)
                                {{$user->Y}} @lang('home.years_ago')
                            @elseif($user->MO >0)
                                {{$user->MO}} @lang('home.months_ago')
                            @elseif($user->D >0)
                                {{$user->D}} @lang('home.days_ago')
                            @elseif($user->H >0)
                                {{$user->H}} @lang('home.hours_ago')
                            @elseif($user->M >0 )
                                {{$user->M}} @lang('home.minutes_ago')
                            @elseif($user->S >0 )
                                {{$user->S}} @lang('home.seconds_ago')
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="align-top" colspan="2">
                            <p>
                                @if(strlen($user->email_verified_at) <= 0 && strlen($user->uphone)<= 0 && !$user->facebook_id)
                                    @lang('home.not_verified')
                                @else
                                    @lang('home.verified')
                                @endif

                                @if( strlen($user->email_verified_at) > 0)
                                    <img  src="{{asset('images/verification-email.svg')}}" width="20px"/>
                                @endif

                                @if(strlen ($user->uphone) > 0)
                                    <img  src="{{asset('images/verification-mobile.svg')}}" width="20px"/>
                                @endif

                                @if($user->facebook_id)
                                    <img src="{{asset('images/verification-facebook.svg')}}" width="20px">
                                @endif
                            </p>
                        </td>
                    </tr>
                   <!-- <tr>
                        <td class="align-middle"><i class="material-icons domrey-color">person</i></td>
                        <td class="align-middle">&#64;{{$user->uuserid}}</td>
                    </tr>-->
                    @if($user->phone)
                        <tr>
                            <td class="align-middle" data-val="{{$user->phone}}">
                               <i class="material-icons domrey-color">phone</i>@if($user->phones->phone_number1){{__('1')}}@endif
                            </td>
                            <td lass="align-middle">
                                <a href="tel:{{$user->phone}}" class="domrey-link">
                                    {{$user->phone}}
                                </a>
                            </td>
                        </tr>
                    @endif
                    @if($user->phones)
                        @if($user->phones->phone_number1)
                        <tr>
                            <td class="align-middle" data-val="{{$user->phones->phone_number1}}">
                                <i class="material-icons domrey-color">phone</i>2
                            
                            </td>
                            <td lass="align-middle">
                                <a href="tel:{{$user->phones->phone_number1}}" class="domrey-link">
                                    {{$user->phones->phone_number1}}
                                </a>

                            </td>
                        </tr>
                        @endif
                        @if($user->phones->phone_number2)
                        <tr>
                            <td class="align-middle" data-val="{{$user->phones->phone_number2}}">
                               <i class="material-icons domrey-color">phone</i>3
                                
                            </td>
                            <td class="align-middle">
                                <a href="tel:{{$user->phones->phone_number2}}" class="domrey-link">
                                    {{$user->phones->phone_number2}}
                                </a>    
                            </td>
                        </tr>
                        @endif
                    @endif
                    @if($user->email)
                        <tr>
                            <td class="align-middle pt-2"><i class="material-icons domrey-color">mail</i></td>
                            <td class="align-middle">
                                <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                            </td>
                        </tr>
                    @endif
                    @if($user->address)
                        <tr>
                            <td class="align-top"><i class="material-icons domrey-color">map</i></td>
                            <td class="align-text-top">{{$user->address}}</td>
                        </tr>
                    @endif
                </table>
            @endisset
            
        </div>
        @if($user->payment_method)
            <div class="col-md-12 mb-3 mt-3 domrey-seller-page">
                <h4>@lang('home.payment_method')</h4>
                @if($user->payment_method->cash)
                    <img src="{{asset('images/payments/cash.png')}}"  width="50px"/>
                @endif
                @if($user->payment_method->aba)
                    <img src="{{asset('images/payments/aba.png')}}"  width="50px"/>
                @endif 
                @if($user->payment_method->wing)
                    <img src="{{asset('images/payments/wing.png')}}" width="50px"/>
                @endif
                @if($user->payment_method->true_money)
                    <img src="{{asset('images/payments/true.png')}}" width="90px"/>
                @endif
            </div>
        @endif
        <div class="col-md-12 mb-3 domrey-seller-page">
            <table>
                @if($user->payment_account && $user->payment_account->aba_acc)
                    <tr>
                        <td style="width: 120px;"><strong>ABA Account</strong></td>
                        <td>
                            {{$user->payment_account->aba_acc}}
                        </td>
                    </tr>
                @endif
                @if($user->payment_account &&  $user->payment_account->wing_acc)
                    <tr>
                        <td style="width: 120px;"><strong>WING</strong></td>
                        <td>
                            {{$user->payment_account->wing_acc}}
                        </td>
                    </tr>
                @endif
                @if($user->payment_account && $user->payment_account->true_money_acc)
                    <tr>
                        <td style="width: 120px;"><strong>True Money</strong></td>
                        <td>
                            {{$user->payment_account->true_money_acc}}        
                        </td>
                    </tr>
                @endif
            </table>
        </div>
        <div class="col-md-12 about col-about domrey-seller-page">
            {{$user->about??''}}
        </div>
    </div>
    <div class="col-md-9 domrey-seller-page">
        <div class="product-content">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                        aria-controls="nav-home" aria-selected="true">@lang('home.listing') ({{$total_item}})</a>
                    <a class="nav-item nav-link d-none" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                        aria-controls="nav-profile" aria-selected="false">@lang('home.reviews')</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab" style="padding: 10px;">
                    <div class="row box-sadow" id="list_items">
                        @foreach($posts as $post)
                            <div class="col-6 col-md-4 col-lg-3 col-xl-3 text-center products" data-id="{{$post->id}}">
                                <div class="imag-product">
                                    <div class='thumbnail-domrey'>
                                        <a href="{{url('/post/detail/'.$post->id)}}">
                                            <div class="grid_balckground_img"
                                            style="background-image: url('{{asset('images/loading.svg')}}');"
                                            data-src="{{asset('/images/products/500x500/'.$post->image)}}">
                                      
                                            </div>
                                        </a>
                                    </div>
                                    <div class="text-desc text-left">
                                        <div class="title">
                                            <a href="{{url('/post/detail/'.$post->id)}}">
                                                {{Str::limit($post->title, $limit = 30, $end = '...')}}
                                            </a>
                                        </div>
                                        <div class="price-font">
                                            ${{number_format($post->price)}}
                                        </div>
                                        <div class="description">
                                            <a href="{{url('/post/detail/'.$post->id)}}">
                                                {{Str::limit($post->description, $limit = 30, $end = '...')}}
                                            </a>
                                        </div>
                                        @if($post->condition=='USED')
                                            <div class="">
                                                @lang('home.used')        
                                            </div>
                                        @else
                                            <div class="">
                                                @lang('home.new')
                                            </div>
                                        @endif
                                        <div class="like">
                                            <a class="btn-like-listing" data-id="{{$post->id}}" onclick="btnHeartLike(this)">
                                                @if($post->like_user==(Auth::user()->id??-1))
                                                    <img src="{{asset('/images/like-filled.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$post->rate>0?$post->rate:''}}</span>
                                                @else
                                                    <img src="{{asset('/images/like-outlined.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$post->rate>0?$post->rate:''}}</span>
                                                @endif
                                            </a>
                                            <a id="dropdownMenuRe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <svg  height="16" width="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" class="_3EdKwhRm4f float-right">
                                                    <path fill-rule="nonzero" d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z" fill="#57585a"></path>
                                                </svg>
                                            </a>
                                            <div class="dropdown-menu report-listing" aria-labelledby="dropdownMenuRe">
                                                <!-- <button class="dropdown-item btn-report-listing" data-id="{{$post->id}}" type="button">@lang('home.report_listing')</button> -->
                                                @if($post->created_by==(Auth::user()->id??0))
                                                    <a href="{{url('/post/createNew/'.$post->id??0)}}"><button  class="dropdown-item"><i class="material-icons">edit</i>@lang('home.edit_listing')</button></a>
                                                    <button  class="dropdown-item panorama_wide_angle" onclick="updateStatus({{$post->id}}, 0, '{{Auth::user()->name}}');"><i class="material-icons">panorama_wide_angle</i>@lang('home.disabled')</button>
                                                    <button  class="dropdown-item delete_listing"      onclick="deleteListing({{$post->id}}, -1, '{{Auth::user()->name}}');"><i class="material-icons">delete</i>@lang('home.delete')</button>
                                                @else
                                                    <button class="dropdown-item btn-report-listing" type="button" onclick="btnReportListing({{$post->id}})"> @lang('home.report_listing')</button>
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        @endforeach

                        @if(count($posts) <=0 )
                            <div style="padding: 13px;">
                                @lang('home.no_listing')
                            </div>
                        @endif
                    </div>


                    @if($total_item > $limit_item)
                        <div class="row">
                            <div class="col-12 text-center" style="margin-top:20px;">
                                <button type="button" id="view_more" class="btn btn-outline-secondary mb-2" data-uid="{{$post->created_by}}" data-lid="{{$post->id}}">@lang('home.view_more')</button>
                            </div>
                        </div>
                    @endif
                    
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $(function() {

        $('.image-profile').imageloader({
            background: false,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });

        $('.grid_balckground_img').imageloader({
            background: true,
            callback: function (elm) {
                $(elm).slideDown('slow');
            }
        });
    });

    var phonePrefix = [
        'cellcard,011,012,017,061,076,077,078,079,085,089,092,095,099',
        'smart,010,015,016,069,070,081,086,087,093,098,096',
        'metfone,031,060,066,067,068,071,088,090,097',
        'qb,013,080,083,084',
        'cootel,038',
        'seatel,018'
    ];

    var phoneDiv = $(".phones_prefix");

    for(var i=0; i < phoneDiv.length; i++){
        var sim = $(phoneDiv[i]).attr('data-val');
        var sim3 = `${sim}`.substring(0, 3);
        
        //console.log(sim3);
        for(var j=0; j< phonePrefix.length; j++){
            if(phonePrefix[j].search(sim3) > 0){
                var prefix = phonePrefix[j].split(',');
                //console.log(prefix[0]);
                $(phoneDiv[i]).find('img').attr('src', `/images/sim/${prefix[0]}.png`);
                $(phoneDiv[i]).find('img').attr('title', prefix[0]);
                break;
            }
        }
        
    }

    $('#view_more').click(function(){
        var formData = new FormData();
        var uid= $(this).attr('data-uid');
        var lid= $(this).attr('data-lid');
        $.ajax({
            url: '/seller/getLastSellerId/'+ uid +'/' + lid,
            type: 'GET',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                 $('#view_more').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
            },
            complete: function () {
                $('#view_more').html("@lang('home.view_more')");
            },
            success: function (json) {
                if(json.lid > 0){
                    $('#view_more').attr('data-lid', json.lid);
                    loadContent(uid, lid);
                }

                if(json.count < json.limit){
                    $('#view_more').addClass('d-none');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });

    });

    //
    var loadContent = function(uid, lid){
            $.ajax({
                url: '/seller/viewMore/'+ uid +'/' + lid,
                type: 'GET',
                data: {
                },
                dataType: 'html',
                beforeSend: function () {
                
                },
                complete: function () {

                },
                success: function (datahtml) {
                    $('#list_items').append(datahtml);
                    $('#list_items .grid_balckground_img').imageloader({
                        background: true,
                        callback: function (elm) {
                            $(elm).slideDown('slow');
                        }
                    });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
    };

    var changeCover = function(){
        $('#file_cover').click();
    };

    var formData = new FormData();

    $('#file_cover').on('change', function () {
        if (this.files && this.files[0]) {
            if(this.files[0].type=='image/jpeg'|| this.files[0].type=='image/jpg' || this.files[0].type=='image/png'){

                var reader = new FileReader();
                reader.onload = function (e) {
                    //console.log(e.target.result);
                    //var img = new Image();
                    //img.onload = function(){
                        //htmlAllert('Alert',this.width+' '+ this.height );
                        //console.log(this.files[0]);
                    //};
                    //img.src = e.target.result;

                    formData.set('cover', e.target.result);
                    
                    $('.seller-background').css('background-image', 'url(' + e.target.result + ')');
                    $('.seller-background i.photo_camera').addClass('d-none');
                    $('.seller-background i.save').removeClass('d-none');
                    $('.seller-background i.clear').removeClass('d-none');
                };
                reader.readAsDataURL(this.files[0]);

            }else{
                htmlAllert('Error', 'Invalid image type.');
            }


        }else{
            htmlAllert('Error', 'Invalid image type.');
        }
    });

    var saveCover = function(){
        
        $.ajax({
            url: '/api/profile/changeCover',
            type: 'post',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('.seller-background i.save').html('loop');
            },
            complete: function () {

            },
            success: function (data) {
                if(data.id){
                    htmlAllert('Successful', 'Your cover has been saved successfully');
                    
                    $('.seller-background i.photo_camera').removeClass('d-none');
                    $('.seller-background i.save').html('save');
                    $('.seller-background i.save').addClass('d-none');
                    $('.seller-background i.clear').addClass('d-none');

                }

            }
            
        });

    };

    var clearCover = function(){
        $('.seller-background').removeAttr('style');
        $('.seller-background i.photo_camera').removeClass('d-none');
        $('.seller-background i.save').addClass('d-none');
        $('.seller-background i.clear').addClass('d-none');
    };
    

</script>
@endsection