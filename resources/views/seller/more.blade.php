@foreach($posts as $post)
    <div class="col-6 col-md-4 col-lg-3 col-xl-3 text-center products" data-id="{{$post->id}}">
        <div class="imag-product">
            <div class='thumbnail-domrey'>
                <a href="{{url('/post/detail/'.$post->id)}}">
                    <div class="grid_balckground_img"
                    style="background-image: url('{{asset('images/loading.svg')}}');"
                    data-src="{{asset('/images/products/500x500/'.$post->image)}}">
                    </div>
                </a>
            </div>
            <div class="text-desc text-left">
                <div class="title">
                    <a href="{{url('/post/detail/'.$post->id)}}">
                        {{Str::limit($post->title, $limit = 30, $end = '...')}}
                    </a>
                </div>
                <div class="price-font">
                    ${{number_format($post->price)}}
                </div>
                <div class="description">
                    <a href="{{url('/post/detail/'.$post->id)}}">
                        {{Str::limit($post->description, $limit = 30, $end = '...')}}
                    </a>
                </div>
                @if($post->condition=='USED')
                    <div class="">
                        @lang('home.used')
                    </div>
                @else
                    <div class="">
                        @lang('home.new')
                    </div>
                @endif
                <div class="like">
                    <a class="btn-like-listing" data-id="{{$post->id}}" onclick="btnHeartLike(this)">
                        @if($post->like_user==(Auth::user()->id??-1))
                            <img src="{{asset('/images/like-filled.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$post->rate>0?$post->rate:''}}</span>
                        @else
                            <img src="{{asset('/images/like-outlined.svg')}}" class="float-left"/> <span style="padding: 3px;">{{$post->rate>0?$post->rate:''}}</span>
                        @endif
                    </a>
                    <a id="dropdownMenuRe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <svg  height="16" width="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" class="_3EdKwhRm4f float-right">
                            <path fill-rule="nonzero" d="M6.95 13.45a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm0-6a1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1 1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1zm2.1-3.9a1.44 1.44 0 0 1-2.1 0 1.44 1.44 0 0 1 0-2.1 1.44 1.44 0 0 1 2.1 0 1.44 1.44 0 0 1 0 2.1z" fill="#57585a"></path>
                        </svg>
                    </a>
                    <div class="dropdown-menu report-listing" aria-labelledby="dropdownMenuRe">
                        @if($post->created_by==(Auth::user()->id??0))
                            <a href="{{url('/post/createNew/'.$post->id??0)}}"><button  class="dropdown-item"><i class="material-icons">edit</i>@lang('home.edit_listing')</button></a>
                            <button  class="dropdown-item panorama_wide_angle" onclick="updateStatus({{$post->id}}, 0, '{{Auth::user()->name}}');"><i class="material-icons">panorama_wide_angle</i>@lang('home.disabled')</button>
                            <button  class="dropdown-item delete_listing"      onclick="deleteListing({{$post->id}}, -1, '{{Auth::user()->name}}');"><i class="material-icons">delete</i>@lang('home.delete')</button>
                        @else
                            <button class="dropdown-item btn-report-listing"  type="button" onclick="btnReportListing({{$post->id}})">@lang('home.report_listing')</button>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
@endforeach
<script>
    var itemW = $('.thumbnail-domrey').width();
    var itemWd = $('.thumbnail-domrey-detail').width();
    if(itemW > 0){
        $('.thumbnail-domrey').css('height', itemW);
    }

    if (itemWd > 0) {
        $('.thumbnail-domrey-detail').css('height', itemWd);
    }
</script>