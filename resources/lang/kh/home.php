<?php

return [
    'home'=>'ទំព័រដើម',
    'dashboard'=>'ទំព័រគ្រប់គ្រងការលក់',
    'sell' =>  'លក់',
    'login' =>  'ចូល',
    'register' => 'ចុះឈ្មោះ',
    'sign_up' => 'ចុះឈ្មោះ',
    'search'=> 'ស្វែងរក ',
    'explore_bottom'=>'ទិញ',
    'me'=>'ខ្ញុំ',
    'explore'=> 'ស្វែងរកតាមផ្នែក',
    'top_seller'=> 'អ្នកលក់របស់យើង',
    'top_product'=> 'ផលិតផលចុងក្រោយ',
    'view_more' => 'ច្រើនទៀត',

    //Seller page
    'joined' => 'ថ្ងៃចូល',
    'online' => 'អនឡាញ',
    'verified' => 'បានផ្ទៀងផ្ទាត់',
    'not_verified' =>'មិនទាន់ផ្ទៀងផ្ទាត់',
    'listing' => 'ផលិតផល',
    'no_listing' => 'មិនមានផលិតផលទេ',    
    'reviews' => 'ពិនិត្យ',
    'seconds_ago' => 'វិនាទីមុន',
    'minutes_ago' => 'នាទីមុន',
    'hours_ago' => 'ម៉ោងមុន',
    'days_ago' => 'ថ្ងៃមុន',
    'months_ago' => 'ខែមុន',
    'years_ago' => 'ឆ្នាំមុន',
    'minutes' => 'នាទី',
    'hours' => 'ម៉ោង',
    'days' => 'ថ្ងៃ',
    'months' => 'ខែ',
    'years' => 'ឆ្នាំ',
    'chat' => 'ផ្ញើរសារ',
    'make_offer' =>'តថ្លៃ',
    'offer' => 'តថ្លៃ',
    'share' => 'ចែករំលែក',
    'like' =>'ចូលចិត្ត',
    
    //Login
    'email'=>'អ៊ីម៉ែល',
    'password'=>'ពាក្សសំងាត់',
    'remember'=>'ចង់ចាំពាក្សសំងាត់',
    'forgot_password'=>'ភ្លេចពាក្សសំងាត់',
    'no_account'=>'មិនទាន់មានគណនីមែនទេ?',
    'register_here'=>'ចុះឈ្មោះទីនេះ',
    'current_password'=>'លេខសំងាត់កំពុងប្រើ',
    'new_password'=>'លេខសំងា់ថ្មី',
    'confirm_password'=>'បញ្ជាក់ពាក្សសំងាត់',
    'mobile_phone'=>'លេខទូរស័ព្ទ',
    'name'=>'ឈ្មោះ',
    'verify'=>'ផ្ទៀង​ផ្ទាត់',
    'have_account'=>'មានគណនីរួចហើយ?',
    'login_here'=>'ចូលទីនេះ',
    'register_new_label'=>'ចុះឈ្មោះដើម្បីដាក់លក់ដោយឥតគិតថ្លៃ',
    'hi'=>'Hi',
    'my_item'=>'ផលិតផលរបស់ខ្ញុំ',
    'shop_page'=>'ហាងរបស់ខ្ញុំ',
    'message'=>'ប្រអប់សារ',
    'profile'=>'គណនី',
    'my_address'=>'អាស័យដ្នាន',
    'setting'=>'ការកំណត់',
    'logout'=>'ចាកចេញ',

     //New listing page
    'new_sell'=>'ដាក់លក់ទីនេះ',
    'add_photos'=>'រូបភាព',
    'category'=>'ជ្រើសរើសប្រភេទ',
    'sub_category'=>'ជ្រើសរើសប្រភេទផ្សេង',
    'listing_title'=>'ចំណងជើង',
    'location'=>'ទីកន្លែង',
    'location_select'=>'ជ្រើសទីកន្លែង',
    'condition'=>'លក្ខខណ្ឌ',
    'brand'=>'ម៉ាក',
    'price'=>'តម្លៃ',
    'description'=>'ការពិពណ៌នា',
    'status'=>'បង្ហាញ',
    'tags'=>'ប្រភេទអ្វីខ្លះ',
    'submit'=>'ដាក់លក់',
    'riel' =>'៛',
    'crop_image'=>'កាត់រូបភាព',
    'brows_image'=>'បញ្ជូលរូបភាព',
    'original'=>'រូបដើម',
    'crop_now'=>'កាត់រូបភាព',
    'read_more'=>'ច្រើនទៀត',

    //message page
    'no_message' => 'មិនទាន់មានសារ!',
    'type_your_message' => 'វាយសារបស់អ្នកទីនេះ...',
    'selling' => 'ការលក់',
    'buying' => 'ការទិញ',
    'send'=>'ផ្ញើរ',
    'send_email'=>'ផ្ញើរពាក្យសំងាត់ទៅអ៊ីម៉ែល',
    'close'=>'បោះបង់',
    'alert'=>'ព័ត៌មាន',

    //Profile
    'profile_page' => 'អំពីហាង',
    'general_info' => 'ព័ត៌មានទូទៅ',
    'payment_method'=>'ការទូទាត់ប្រាក់',
    'shipping' => 'ដឹកជញ្ជូន',
    'first_name'=>'នាម',
    'last_name'=>'គោត្តនាម',
    'gender'=>'ភេទ',
    'phone_number'=>'លេខទូរស័ព្ទ',
    'phone_number1'=>'លេខទូរស័ព្ទ១',
    'phone_number2'=>'លេខទូរស័ព្ទ២',
    'bank_account' =>'លេខគណនី ABA',
    'email1'=>'អ៊ីម៉ែល',
    'user_email'=>'ឈ្មោះ ឬ អ៊ីម៉ែល',
    'address'=>'អាស័យដ្ខាន',
    'about'=>'អំពីហាង',
    'contact'=>'ទំនាក់ទំនង',
    'save'=>'រក្សាទុក',
    'select_payment'=>'ជ្រើសរើសការទូទាត់ប្រាក់',
    'fee'=>'ថ្លៃសេវ៉ា $',
    'change'=>'ប្តូរូបភាព',
    'account_setting'=>'ការកំណត់គណនី',
    'edit_account'=>'កែប្រែគណនី',
    'reset_password'=>'ដូរពាក្សសំងាត់',
    'change_password'=>'ដូរពាក្យសំងាត់',
    'language'=>'ភាសា',

    //Filter
    'filter'=>'ជ្រើសរើស',
    'new'=>'ថ្មី',
    'used'=>'ប្រើរួច',
    'from_min'=>'$ តម្លៃចាប់ពី',
    'to_max'=>'$ ទៅដល់',
    'go'=>'បញ្ជូន',
    'buy_des'=>'ទិញ លក់ សេវាកម្ម',
    'domrey_des'=>'ថ្មីឬប្រើរួចនៅក្នុងប្រទេសកម្ពុជាលើគេហទំព័រ Domrey.com',
    'search_for'=>'អ្នកកំពុងស្វែងរកអំពី ',
    'report_listing'=>'រាយការណ៍ទីនេះ',
    //Total Items
    'total_item'=>'សរុបផលិតផល',
    //detail page 
    'shipping_lbl'=>'ការដឹងជញ្ជូន',
    'package_lbl'=>'រយះពេលរៀបចំដឹកជញ្ជូន',
    'payment_lbl'=>'ការទូទាត់ប្រាក់',
    'protection'=>'Domrey ទូទាត់ប្រាក់តាមមធ្យោបាយខាងក្រោម៖',
    'search_not_found'=>'អ្វីដែលអ្នកកំពុងតែស្វែងរកមិនមាននៅក្នុងគេហទំព័ររបស់យើងទេ!',
    'search_not_found_1'=>'សូមធ្វើការស្វែងរកអ្វីផ្សេងទៀត ហើយ ប្រាកដថាការបញ្ជូលរបស់អ្នកពិតជាត្រឹមត្រូវ។ សូមអរគុណ!',
    'deal_method'=>'លក្ខខណ្ឌយល់ព្រម',
    'meet_up'=>'ណាត់ជួប',
    'mailing'=>'ផ្ញើរអ៊ីម៉ែល',
    'mail_delivery'=>'ផ្ញើរសានិងដឹកជញ្ជូន',
    'email_verify'=>"សូមបញ្ចូលអ៊ីម៉ែលរួចចុចប៊ូតុង (ផ្ទៀងផ្ទាត់) ហើយចូលមើលក្នុងប្រអប់សារបស់អ្នក",
    'required'=>'តំរូវអោយបញ្ចូល',
    'not_required'=>'មិនចាំចាប់',
    'wait_code'=>"សូមចាំបន្តិច រួចចូលមើលសាក្នុងទូរស័ព្វរបស់អ្នក",
    'list_now'=>'ដាក់លក់',
    'select_photos'=>'ជ្រើសរើសរូបភាព',
    'up_to_photos'=>'(បានរហូតដល់​៨រូបភាព)',
    'what_to_list'=>'តើអ្នកចង់ដាក់លក់អ្វីដែរ?',
    'what_to_descrip'=>'សូមបញ្ចូលរូបភាពផលិតផលនិងរើសផ្នែក',
    'warning_block'=>'ប្រព័ន្ធរបស់យើងនឹងធ្វើការបិទទំនិញរបស់អ្នកមួយនេះប្រសិនមានការ Reported ចំនួនលើសពី៥ដង! សូមធ្វើការទំនាក់ទំនងទៅកាន់អ្នកគ្រប់គ្រងគេហទំព័រ។',
    'admin_block'=>'ទំនិញរបស់អ្នកត្រូវបានបិទដោយអ្នកគ្រប់គ្រងគេហទំព័រ!',
    'system_block'=>'ទំនិញរបស់អ្នកនឹងត្រូវបិទប្រសិនមានការ Report ចំនួនលើសពី៥ដង សូមទំនាក់ទំនងទៅកាន់អ្នកគ្រប់គ្រងគេហទំព័រ​!',
    'back'=>'ត្រឡប់',
    'list'=>'បញ្ជី',
    'edit_listing'=>'កែប្រែ',
    'disabled'=>'យកចេញ',
    'delete'=>'លុប',

    //Settings
    'edit_profile'=>'កែប្រូហ្វាល់',
    'getting_this'=>'ស្ថិតនៅ',
    'share_this_listing'=>'ចែករំលែក',
    'meet_seller'=>'ជួបអ្នកលក់',

    'contact_page'=>'ទំព័រ',
    'contact_message'=>'សូមបំពេញព៍តមានខាងក្រោម',
    'contact_info'=>'ទំនាក់ទំនង',
    'contact_your_name'=>'ឈ្មោះរបស់អ្នក',
    'contact_contact_us'=>'ទំនាក់ទំនង',
    'contact_success'=>'សារបស់អ្នកត្រូវបានបញ្ជូនរួចរាល់ហើយ!'

    
];